var reporter = require("cucumber-html-reporter");

var options = {
  theme: "bootstrap",
  jsonFile: "report/cucumber_report.json",
  output: "report/cucumber_report.html",
  reportSuiteAsScenarios: true,
  launchReport: true,
  metadata: {
    OS: "Linux",
    Browser: "Chrome Headless",
    "EBS Version": "1.0",
  },
};

reporter.generate(options);
