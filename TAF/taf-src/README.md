# TEST AUTOMATION FRAMEWORK
Test Automation Framework provides test automation using 

# Prerequisites:
	- Java 8
	- NodeJS
	- IDE - You can use any IDE that supports NodeJS/JavaScript development. For this document, Visual Studio Code was used

# Visual Studio Code Recommended Extensions:
	- Cucumber (Gherkin) Full Support
	- Cuke Step Definition Generator
	- Gherkin Table Formatter

# Features
Running tests on Chrome, Firefox and Edge (chromium).
Support for Selenium Grid.
Tests written in Gherkin syntax.

# To run docker version
1. From automated-test directory, copy TAF directory to local
2. Open taf-src folder
3. Open env file and declare the following variables
	```sh
		CMD_USER=<email account that will be used>
		CMD_PASS=<account password>
		CMD_MODE=<MICROSOFT | GOOGLE>
        CMD_ENV=<url like https://ebs-staging-cb.irri.org/>
	```
4. Open cucumber.conf.js and do the following:
	- At line 2, add this line: 
	```sh
	require('dotenv').config();
	```
	- And comment out closeSession();	
	```sh
	//closeSession();
	```
	- Save changes to file
5. Open nightwatch.conf.js and do the following:
	- Change `host: "127.0.0.1",` to `host : "chrome",`
	- Comment the lines:
	```sh
		`
		//selenium_port  : 4444,
		//selenium_host  : "hub",
		`
	```
	- Save changes to file
6. To pass variable from docker-compose.yml using .env file, open docker-compose.yml and add the following `environment` variables for taf:
	```sh
		`
		taf:
		build:
			context:
		environment:
			- CMD_USER=${CMD_USER}
			- CMD_PASS=${CMD_PASS}
			- CMD_MODE=${CMD_MODE}
			- CMD_ENV=${CMD_ENV}  
		`
	```
	- Save changes to file
7. Open nightwatch.conf.js and update `launch_url` to point to CMD_ENV variable:
	- Locate `test_settings: {...}` and set the following:
		- `launch_url: process.env.CMD_ENV`
8. To use the env variables in the steps definition, call `login` with the following parameters:
	```sh
		`b4rHelpers.login(process.env.CMD_USER, process.env.CMD_PASS, process.env.CMD_MODE)`
	```
9. To run all feature files, go to `taf-src/` directory in the terminal, then run the command 
	```sh
		`sudo docker-compose --env-file env up --build --exit-code-from taf` 
	```
	NOTE: This will run all feature files inside TAF/taf-src/src/features folder, thus may take a long time
10. After container exited, see `~/TAF/reports/cucumber_report.html` for test run summary report
11. To force stop the execution, hit `Ctrl+Z`
12. And finally run command `sudo docker-compose down` to stop container and remove containers, networks, volumes, and images created by up.


# To run in your local machine
1. Clone the repository to target local directory
	$ `git clone https://dpagtananan@bitbucket.org/ebsproject/automated-tests.git`
2. Open editor and go to `TAF/taf-src` folder
3. Open cucumber.conf.js and uncomment `closeSession();` then save changes to file.
4. Open nightwatch.conf.js and do the following:
	- Locate the first `selenium: {...}` right after the `page_objects_path`
		- Change `host: "chrome",` to `host : "127.0.0.1",` 
	- Locate `test_settings: {...}`
        - Set `launch_url:` to your target test environment for local run:
            `launch_url: "https://cb-qa.ebsproject.org/",`
		- Comment out the following lines
		```sh
			`
			//selenium_port  : 4444,
			//selenium_host  : "hub",
			`
		```
	- Locate `globals: {...}`
		- Update the `userName` and `userPassword` with your respective login details
		```sh
			userName: "<your gmail account>",
			userPassword: "<your account password>",
		```
		- Then add the line: `loginMode: "GOOGLE"`
	- You may also need to update the `launch_url` to your target test environment to make sure you will be running against a correct environment:
		`launch_url: "https://cb-qa.ebsproject.org/",`
	- Then save changes to file.
5. Open any of the steps definition file inside `TAF/taf-src/src/step_definitions`
	- Example: `seedSearchSteps.js`
	- Search for `b4rHelpers.login` text and update as follow:
		- From this:
		```sh
			`b4rHelpers.login(process.env.CMD_USER, process.env.CMD_PASS, process.env.CMD_MODE)`
		```
		- To this:
		```sh
			`await b4rHelpers.login(client.globals.userName, client.globals.userPassword, client.globals.loginMode);`
		```
	- Then save changes to file.
6. Check your current Google Chrome browser version installed (in your browser, go to `Help > About Google Chrome`)
7. Open package.json
	- Under `devDependencies`, locate `chromedriver` and check if it has the same version as the current Google Chrome browser version 
		- i.e. `"chromedriver": "^92.0.0` for Google Chrome Version 92.0.4515.159 (Official Build) (64-bit))
		- Update the `"chromedriver"` declared version to your Google Chrome Version
	- Then save changes to file.
8. Open a terminal and go to `TAF/taf-src` folder, then run the following
		```sh
			$ `npm install`
		```
	- Followed by
	```sh
		`npm audit fix`
	```
9. To try running an initial test, open any .feature file inside `TAF/taf-src/src/features` 
	- Example: `SeedSearch.feature`
	- Write `@smoke` above any `Scenario` statements
	```sh
		`
		@smoke
        	Scenario: Search for package information for seeds harvested in a specific LOCATION
		`
	```
	- Then save changes to file.
10. Now you are ready to run your test (e.g. any test scenarios with the `@smoke` in this example) with different browsers by using any of the following commands:
	```sh
		$ npm run test -- --env 'chrome' --tags '@smoke'
		$ npm run test -- --env 'firefox' --tags '@smoke'
		$ npm run test -- --env 'edge' --tags '@smoke'
	```
11. To run your test with different browser in HEADLESS mode:
	```sh
		$ npm run test -- --env 'headless.chrome' --tags '@smoke'
		$ npm run test -- --env 'headless.firefox' --tags '@smoke'
		$ npm run test -- --env 'headless.edge' --tags '@smoke'
	```
12. To run smoke tests in parallel, locate the line `test:parallel` inside `package.json`
	- Change the parameter after `--tags` to your target test suite/s, e.g. `@smoke` in our example `SeedSearch.feature`
	- Parallel runs by default in HEADLESS mode
	- Run the command
	```sh
		$ npm run test:parallel
	```
13. If after any npm run that an html report does not automatically opens in  your browser, you can run the command below:
	```sh
		$ npm run report
	```