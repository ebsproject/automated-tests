const fs = require("fs");
require('dotenv').config();
const { setDefaultTimeout, After, AfterAll, BeforeAll, Before } = require("cucumber");
const {
  createSession,
  closeSession,
  startWebDriver,
  stopWebDriver,
  getNewScreenshots
} = require("nightwatch-api");
const reporter = require("cucumber-html-reporter");
const { client } = require("nightwatch-api");


// NightwatchTestSettingGeneric

let b4rVersion;

setDefaultTimeout(300000);

Before(async()=>{
  await createSession();
  await client.maximizeWindow();
})
/*
BeforeAll(async () => {
  await startWebDriver({
    env: process.argv[process.argv.indexOf("--env") + 1]
  });
});*/
BeforeAll(async () => {
  let browser;
  console.log("For docker run :", process.env.CMD_ENV);
  //if --parallel is passed, only 2 argv present, use headless.chrome
  if (process.argv.length == 2) browser = "headless.chrome";
  //else use the --en value
  else browser = process.argv[process.argv.indexOf("--env") + 1];
  await startWebDriver({
    env: browser,
  });
});

AfterAll(async () => {
  let version, browserName, platform;

  await client.sessions((result) => {
    browserName = result.value[0]["capabilities"]["browserName"];
    switch (browserName) {
      case "chrome":
        version = result.value[0]["capabilities"]["version"];
        platform = result.value[0]["capabilities"]["platform"];
        break;
      case "firefox":
        version = result.value[0]["capabilities"]["browserVersion"];
        platform = result.value[0]["capabilities"]["platformName"];
        break;
      case "internet explorer":
        platform = result.value[0]["capabilities"]["platformName"];
        version = result.value[0]["capabilities"]["browserVersion"];
        break;
    }
  });

  await stopWebDriver();

  setTimeout(() => {
    reporter.generate({
      theme: "bootstrap",
      jsonFile: "report/cucumber_report.json",
      output: "report/cucumber_report.html",
      reportSuiteAsScenarios: true,
      launchReport: true,
      metadata: {
        OS: platform,
        Browser: browserName,
        Version: version,
        "B4R Version": b4rVersion,
      },
    });
  }, 0);
});

After(function () {
  getNewScreenshots().forEach(file => this.attach(fs.readFileSync(file), 'image/png'));
  //closeSession(); //comment this if to run in docker
});
