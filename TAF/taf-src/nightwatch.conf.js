const chromedriver = require("chromedriver");

module.exports = {
    globals_path: "globals.js",
    page_objects_path: "./src/page_objects", //page_objects folder where selectors are saved
    selenium: {
        start_process: true,
        host: "chrome",
        port: 4444,
        server_path: "./node_modules/selenium-server/lib/runner/selenium-server-standalone-3.141.59.jar", //require("selenium-server").path
        cli_args: {
            "webdriver.gecko.driver": require("geckodriver").path,
            "webdriver.chrome.driver": require("chromedriver").path,
            "webdriver.edge.driver":
                "./driver/msedgedriver.exe",
        },
    },

    test_settings: {
        default: {
            launch_url: process.env.CMD_ENV,
            screenshots: {
                //selenium_port  : 4444,
                //selenium_host  : "hub",
                enabled: true,
                path: "./screenshots",
            },
            globals: {
                //userName: "b4r.tester.ps@gmail.com",
                //userPassword: "t3$t3r@cc",
                userName: "bims.irri@gmail.com",
                userPassword: "B4R1RR1B4R",
                loginMode: "GOOGLE"
            },
        },

        docker: {
            selenium: {
                start_process: false,
                host: "chrome",
                port: 4444,
                server_path: "./node_modules/selenium-server/lib/runner/selenium-server-standalone-3.141.59.jar", //require("selenium-server").path,
                cli_args: {
                    "webdriver.gecko.driver": require("geckodriver").path,
                    "webdriver.chrome.driver": require("chromedriver").path,
                    "webdriver.edge.driver":
                        "./driver/msedgedriver.exe",
                },
            },
        },

        chrome: {
            desiredCapabilities: {
                browserName: "chrome",
                chromeOptions: {
                    args: ["--no-sandbox"],
                    w3c: false,
                    'prefs': {
                        'download': {
                            'prompt_for_download': false,
                            'default_directory': process.cwd() + '/downloads'
                        }
                    }
                },
            },
        },
        firefox: {
            desiredCapabilities: {
                browserName: "firefox",
            },
        },
        edge: {
            desiredCapabilities: {
                browserName: "MicrosoftEdge",
                edgeOptions: {
                    args: ["--no-sandbox"],
                    w3c: false,
                },
            },
        },
        "headless.chrome": {
            extends: "chrome",
            desiredCapabilities: {
                chromeOptions: {
                    args: ["--headless", "--disable-gpu", "--window-size=1280,1024"],
                    w3c: false,
                },
            },
        },
        "headless.firefox:": {
            extends: "firefox",
            moz: {
                firefoxOptions: {
                    args: ["-headless", "-width=1280", "-height=1024"],
                },
            },
        },
        "headless.edge": {
            extends: "edge",
            desiredCapabilities: {
                edgeOptions: {
                    args: ["--no-sandbox", "--headless", "--disable-gpu", "--window-size=1280,1024"],
                    w3c: false,
                },
            },
        },
        "docker.headless": {
            extends: "docker",
            desiredCapabilities: {
                browserName: "chrome",
                chromeOptions: {
                    args: ["--headless", "--disable-gpu", "--window-size=1280,1024"],
                    w3c: false,
                },
            },
        }
    },
};
