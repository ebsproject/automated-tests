@rice_multi_crop @rice_column_filtering_SS
Feature: CORB-2023 Column Filtering

    Background: Login User
        Given the user is in "Seed search" Search
        And the user sets Data filters parameters for Seed Search
            | Program                           |
            | Irrigated South-East Asia (IRSEA) |
        And user sets Seed search parameters
            | Season   | Experiment Type                                                  |
            | Dry, Wet | Breeding Trial, Generation Nursery, Intentional Crossing Nursery |

    @riceColumnFilterSS1 @riceSeedsFiltering1
    Scenario: SS-BROWSER-001 User filter the column PARENTAGE from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | PARENTAGE |
            | %IR%      |
        Then results will show all matching package records

    @riceColumnFilterSS2
    Scenario: SS-BROWSER-002 User filter the column GERMPLASM from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | GERMPLASM |
            | IR%       |
        Then results will show all matching package records

    @riceColumnFilterSS3
    Scenario: SS-BROWSER-003 User filter the column LABEL from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | LABEL |
            | %IR%  |
        Then results will show all matching package records

    @riceColumnFilterSS4
    Scenario: SS-BROWSER-004 User filter the column SEED NAME from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | SEED NAME |
            | 300%      |
        Then results will show all matching package records

    @riceColumnFilterSS5
    Scenario: SS-BROWSER-005 User filter the column SEED from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | SEED |
            | 300% |
        Then results will show all matching package records

    @riceColumnFilterSS6 @riceSeedsFiltering2
    Scenario: SS-BROWSER-006 User filter the column PACKAGE QTY. from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | PACKAGE QTY. |
            | 1%           |
        Then results will show all matching package records

    @riceColumnFilterSS7
    Scenario: SS-BROWSER-007 User filter the column UNIT from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | UNIT |
            | g%   |
        Then results will show all matching package records

    @riceColumnFilterSS8
    Scenario: SS-BROWSER-008 User filter the column OTHER_NAME from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | OTHER_NAME |
            | %IR%       |
        Then results will show all matching package records

    @riceColumnFilterSS9
    Scenario: SS-BROWSER-009 User filter the column YEAR from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | YEAR |
            | 20%  |
        Then results will show all matching package records

    @riceColumnFilterSS10 @riceSeedsFiltering3
    Scenario: SS-BROWSER-009 User filter the column EXPERIMENT TYPE from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | EXPERIMENT TYPE |
            | Breed%          |
        Then results will show all matching package records

    @riceColumnFilterSS11
    Scenario: SS-BROWSER-010 User filter the column EXPERIMENT from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | EXPERIMENT |
            | %IRSEA%    |
        Then results will show all matching package records

    @riceColumnFilterSS12
    Scenario: SS-BROWSER-011 User filter the column OCCURRENCE from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | OCCURRENCE |
            | %IRSEA%    |
        Then results will show all matching package records

    @riceColumnFilterSS13
    Scenario: SS-BROWSER-012 User filter the column SEASON from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | SEASON |
            | DS     |
        Then results will show all matching package records