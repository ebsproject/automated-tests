@rice_multi_crop @rice_input_list_GS
Feature: SQA-720 Additional Query Parameters

    Background: Login User
        Given the user is in "Germplasm" Search entrypoint
        And the user sets Data filters parameters for Germplasm Search
            | Program                           |
            | Irrigated South-East Asia (IRSEA) |

    @riceInputListGS1
    Scenario: GS-USE INPUT LIST FOR SEARCH Designation
        And the user selects "Designation" input list in additional parameters
        And user loads input list data from "R_GermplasmInputListDesignation.txt" file to "germplasmInputList"
        And user sets "germplasmInputList" contents to the input field
        Then the germplasm in the input list is loaded

    @riceInputListGS2
    Scenario: GS-USE INPUT LIST FOR SEARCH Germplasm Code
        And the user selects "Germpalsm Code" input list in additional parameters
        And user loads input list data from "R_GermplasmInputListGermCode.txt" file to "germplasmInputList"
        And user sets "germplasmInputList" contents to the input field
        Then the germplasm in the input list is loaded