@EM_filtering
Feature: SQA-635 Column Filtering

    Background: Login user and go to experiment manager
        Given user goes to Experiment Manager
        And the user sets Data filters parameters for experiment manager
            | Program                           |
            | Irrigated South-East Asia (IRSEA) |
    #needs section: browser fro location value
    @em_filtering1 @em_smoke1
    Scenario: EM-BROWSER-001 User filter the column EXPERIMENT CODE from the search results
        Given user can see EM browser grid ready
        When user set column filters of the search results in EM browser
            | EXPERIMENT CODE |
            | EXP0002097%     |
        Then results show all matching experiment occurrences
    @em_smoke2
    Scenario: EM-BROWSER-002 User filter the column EXPERIMENT from the search results
        Given user can see EM browser grid ready
        When user set column filters of the search results in EM browser
            | EXPERIMENT |
            | IR1%       |
        Then results show all matching experiment occurrences
    @em_smoke3
    Scenario: EM-BROWSER-003 User filter the column OCCURRENCE from the search results
        Given user can see EM browser grid ready
        When user set column filters of the search results in EM browser
            | OCCURRENCE |
            | %DS-%      |
        Then results show all matching experiment occurrences
    @em_smoke4
    Scenario: EM-BROWSER-004 User filter the column SITE from the search results
        Given user can see EM browser grid ready
        When user set column filters of the search results in EM browser
            | SITE |
            | 301% |
        Then results show all matching experiment occurrences
    @em_smoke5
    Scenario: EM-BROWSER-005 User filter the column LOCATION from the search results
        Given user can see EM browser grid ready
        When user set column filters of the search results in EM browser
            | LOCATION      |
            | SEED00000008% |
        Then results show all matching experiment occurrences

    @em_filtering2 @em_smoke6
    Scenario: EM-BROWSER-006 User filter the column EXPERIMENT TYPE from the search results
        Given user can see EM browser grid ready
        When user set column filters of the search results in EM browser
            | EXPERIMENT TYPE |
            | 0               |
        Then results show all matching experiment occurrences
    @em_smoke7
    Scenario: EM-BROWSER-007 User filter the column STAGE from the search results
        Given user can see EM browser grid ready
        When user set column filters of the search results in EM browser
            | STAGE |
            | g%    |
        Then results show all matching experiment occurrences
    @em_smoke8
    Scenario: EM-BROWSER-009 User filter the column YEAR from the search results
        Given user can see EM browser grid ready
        When user set column filters of the search results in EM browser
            | YEAR |
            | 2021 |
        Then results show all matching experiment occurrences

    @em_filtering3 @em_smoke9
    Scenario: EM-BROWSER-009 User filter the column SEASON from the search results
        Given user can see EM browser grid ready
        When user set column filters of the search results in EM browser
            | SEASON |
            | Breed% |
        Then results show all matching experiment occurrences
    @em_smoke10
    Scenario: EM-BROWSER-010 User filter the column DESIGN from the search results
        Given user can see EM browser grid ready
        When user set column filters of the search results in EM browser
            | DESIGN |
            | %OYT%  |
        Then results show all matching experiment occurrences
    @em_smoke11
    Scenario: EM-BROWSER-011 User filter the column ENTRY COUNT from the search results
        Given user can see EM browser grid ready
        When user set column filters of the search results in EM browser
            | ENTRY COUNT |
            | %F1-%       |
        Then results show all matching experiment occurrences