@seeds_addalltolist
Feature: B4R-7370 Add All Items to Working List
    Background: Login User
        Given the user is in "Seed search" Search
        And the user sets Data filters parameters for Seed Search
            | Program                           |
            | Irrigated South-East Asia (IRSEA) |
    @seeds_run5
    Scenario: SS-WORKINGLIST-001 User adds all query results, with more than 500 items but less than 1k, to Working List
        Given user sets Seed search parameters
            | Facility | Sub-facility |
            | KJL      | KJL          |
        And the query results display more than "500" items
        When user selects "Add ALL records to Seedlist" button
        Then all query result items are added to the Working List
    @seeds_run6
    Scenario: SS-WORKINGLIST-002 User adds all query results, with more than 500 items but less than 2k, to Working List
        Given user sets Seed search parameters
            | Facility  |
            | KJL, NCBL |
        And the query results display more than "1000" items
        When user selects "Add ALL records to Seedlist" button
        Then all query result items are added to the Working List