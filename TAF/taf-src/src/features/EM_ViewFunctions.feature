Feature: Experiment Manager Test

    Background: Login user and go to experiment manager
        Given user goes to Experiment Manager
        And the user sets Data filters parameters for experiment manager
            | Program                           |
            | Irrigated South-East Asia (IRSEA) |
    #needs section: browser fro location value @EM2 @SumRun
    Scenario: EM-FEATURE-002 View plots of the Location
        When user clicks the Location value
        Then user sees the basic info of the Location
        And user clicks the Plot tab
        Then user sees all plots within the Location
    #browser
    @EM3 @SumRun
    Scenario: EM-FEATURE-003 View status of Trait data collection for the Location
        When user clicks the Location value
        Then user should be able to view the status of trait data
    @EM8 @SumRun
    Scenario: EM-FEATURE-009 View an occurrence
        When the user selects an occurrence
        And the user clicks view
        Then the information of the selected occurrence will be shown