@germplasm_search @smoke_build
Feature: Germplasm basic search

    Background: Login User
        Given the user is in "Germplasm" Search entrypoint
        And the user sets Data filters parameters for Germplasm Search
            | Program           |
            | Program X (PROGX) |
    @germplasm_search_001
    Scenario: GS-BASIC-SEARCH-001 Search by Germplasm Names
        Given user perform generic germplasm name search with input "CMS%"
        When user navigates to "Last" page of Germplasm Search browser
        And user goes back to the "First" page of Germplasm Search browser
        Then user can see Germplasm browser grid ready

    @germplasm_search_002
    Scenario: GS-BASIC-SEARCH-002 Search by Germplasm Type
        Given user perform generic germplasm type search
            | Germplasm Type                                                                          |
            | accession, F1, fixed_inbred_line, hybrid, inbred_line, progeny, segregating, F1F, F1TOP |
        When user navigates to "Last" page of Germplasm Search browser
        And user goes back to the "First" page of Germplasm Search browser
        Then user can see Germplasm browser grid ready

