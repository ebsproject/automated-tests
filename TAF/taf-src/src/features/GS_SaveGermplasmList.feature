@saveGermplasmList
Feature: Save Germplasm List

    Background: Login User
        Given the user is in "Germplasm" Search entrypoint
        And the user sets Data filters parameters for Germplasm Search
            | Program                           |
            | Irrigated South-East Asia (IRSEA) |

    @germplasm_list_001
    Scenario: GS-SAVE-GERMPLASM-LIST-001 Save Germplasm List
        Given user perform generic germplasm name search with input "CMSS14Y001%%"
		And user selects a set of checkboxes in the current page of Germplasm Search browser
        When the user save the germplasm as new list
            | Name       | Description                                   | Remarks           |
            | AUTOMATED* | This is an automated saved list for Germplasm | QC Automated Test |
        When user navigates to "List manager" from Experiment Manager
        Then user can search the saved germplasm list and can see "Name" match in Lists browser