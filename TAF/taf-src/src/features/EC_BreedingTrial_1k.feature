@ECBT1k @ECBT @parallel_run_ec
Feature: Experiment Creation Breeding Trial - 1k entries
    The user should be able to assign an experimental design when creating an experiment
    Different options of experimental design must be available
    The user configures specific parameters for a selected design.

    Background: Create a breeding trial experiment
        Given the user is in Experiment Creation
        And the user sets Data filter parameters
            | Program                           |            
            | Irrigated South-East Asia (IRSEA) |        
        And user can see EC browser grid ready        
        And user sets Basic tab values
            | CreateExperiment | ExperimentType | Stage | EvaluationSeason | ExperimentName | SaveBasic |
            |                  | Breeding Trial | AYT   | WS               |                |           |        
        And user has previously created an entry list from "IRSEA-OYT-2014-WS-001"
        And user edits current experiment
    
    @try1k
    Scenario Outline: EC-TRIAL-LATTICE-001 Randomize the experiment as Alpha-Lattice | CIMMYT with layout
        And user sets "Alpha-Lattice | CIMMYT" as Design
        And user sets "<occ>" as Number of Occurrences
        And user sets "<rep>" as Number of Replicates
        And user sets "<plotBlk>" as Number of Plots Per Block
        And user sets "<shape>" as Define Shape Field
        And user sets "<rows>" as Rows
        And user sets "<turn>" as Plots Until Turn The Serpentine
        And user selects Generate Design
        Then user sees the request successfully sent
        And user waits for design generation to finish
        And user sees the "<result>"
        Then user enters Site
        And user sets "IRRI, Los Baños, Laguna, Philippines" as Site for a Single Occurence
        Then user enters Review
        #Then user re-enters Site
        #Then user Finalizes Experiment

        Examples:
            | occ | rep | plotBlk | shape | rows | turn | order      | result |
            | 1   | 2   | 17      | Yes   | 2    | 4    | Horizontal | layout |

    Scenario Outline: EC-TRIAL-LATTICE-002 Randomize the experiment as Alpha-Lattice | Wheat with layout
        And user sets "IWIN Design" as Design
        And user sets "<occ>" as Number of Occurrences
        And user sets "<rep>" as Number of Replicates
        And user sets "<plotBlk>" as Number of Plots Per Block
        And user sets "<shape>" as Define Shape Field
        And user sets "<rows>" as Rows
        And user sets "<turn>" as Plots Until Turn The Serpentine
        And user sets "<rand1>" as Randomize 1st Step
        And user selects Generate Design
        Then user sees the request successfully sent
        And user waits for design generation to finish
        And user sees the "<result>"
        Then user enters Site
        And user sets "IRRI, Los Baños, Laguna, Philippines" as Site for a Single Occurence
        Then user enters Review
        #Then user re-enters Site
        #Then user Finalizes Experiment

        Examples:
            | occ | rep | plotBlk | shape | rows | turn | rand1 | result |
            | 1   | 2   | 17      | Yes   | 2    | 3    | Yes   | layout |
            | 1   | 3   | 17      | Yes   | 17   | 4    | Yes   | layout |
