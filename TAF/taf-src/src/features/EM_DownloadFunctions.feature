Feature: Experiment Manager Test

    Background: Login user and go to experiment manager
        Given user goes to Experiment Manager
        And the user sets Data filters parameters for experiment manager
            | Program                           |
            | Irrigated South-East Asia (IRSEA) |
    #needs section: browser fro location value
    #locationview
    @EM4 @SumRun
    Scenario: EM-FEATURE-004 Download plots of the location
        When user clicks the Location value
        Then user sees the basic info of the Location
        And user clicks the Plot tab
        Then user sees all plots within the Location
        And user clicks the export plots button
        Then user should be able to download plots information within the Location
    #browser
    @EM5  @SumRun
    Scenario: EM-FEATURE-005 Add shortcut to download trait data collection files for Planted
        When the user selects a planted occurrence
        And the user clicks download trait data collection
        Then the user is redirected to Download Data Collection files tool
    @EM5  @SumRun @specific
    Scenario: EM-FEATURE-006 Add shortcut to download trait data collection files for Not Planted
        When the user selects a not planted occurrence
        And the user clicks download trait data collection
        Then the user is prompted that the Occurrence and or Location is not yet ready for Data collection
    @EM13 @SumRun
    Scenario: EM-FEATURE-013 Browse occurrences grouped by Experiments download
        When the user selects an occurrence
        And the user clicks download "csv"
        Then the "csv" of file is downloaded
    @EM13 @SumRun
    Scenario: EM-FEATURE-014 Browse occurrences grouped by Experiments download
        When the user selects an occurrence
        And the user clicks download "json"
        Then the "json" of file is downloaded
    @EM21 @Demo3 @SumRun
    Scenario: EM-FEATURE-028 Export design arrays of an occurrence
        When the user views an experiment with a design
        And the user downloads the design
        Then the design is downloaded