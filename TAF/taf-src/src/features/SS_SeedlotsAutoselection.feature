@toggle_run
Feature: B4R-7557 Query Result Browser Configuration - Single-Package Seed Automatic Selection
    Background: Login User
        Given the user is in "Seed search" Search
        And the user sets Data filters parameters for Seed Search
            | Program                        |
            | BW Wheat Breeding Program (BW) |
    @toggle_run1
    Scenario: SS-QUERYRESULT-001 User sets automatic selection of single-package result groups and single-package result groups are selected
        Given user can see grid ready
        And user has selected "GERMPLASM NAME - Designation and Germplasm Names information" as "Input List" values
        And user loads input list items from "Wheat Germplasm Names" file to "variable"
        And user sets "variable" contents to Seed Search input list field
        And query results with single-package results groups
        When user turns on the automatic selection of single-package results
        Then all single-package results in the current browser page are checked or selected
    @toggle_run2
    Scenario: SS-QUERYRESULT-002 User sets automatic selection of single-package result groups, navigates to another page and single-package result groups are selected in the current page
        Given user can see grid ready
        And user has selected "GERMPLASM NAME - Designation and Germplasm Names information" as "Input List" values
        And user loads input list items from "Wheat Germplasm Names" file to "variable"
        And user sets "variable" contents to Seed Search input list field
        And query results with single-package results groups
        When user turns on the automatic selection of single-package results
        And user navigates to "2" page
        Then all single-package results in the current browser page are checked or selected