@seeds_filtering
Feature: CORB-2023 Column Filtering

    Background: Login User
        Given the user is in "Seed search" Search
        And the user sets Data filters parameters for Seed Search
            | Program                           |
            | Irrigated South-East Asia (IRSEA) |
        #And user sets Seed search parameters
        #        | Season  | Experiment Year |
        #        | Dry       | 2021    |
        #And user sets Seed search parameters
        #        | Season  |
        #        | Dry     |
        And user sets Seed search parameters
            | Season   |
            | Dry, Wet |
    @seeds_filtering2 @smoke1
    Scenario: SS-BROWSER-001 User filter the column PARENTAGE from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | PARENTAGE  |
            | %IRRI 154% |
        Then results will show all matching package records
    @smoke2
    Scenario: SS-BROWSER-002 User filter the column GERMPLASM from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | GERMPLASM |
            | IR1%      |
        Then results will show all matching package records
    @smoke3
    Scenario: SS-BROWSER-003 User filter the column LABEL from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | LABEL |
            | %DS-% |
        Then results will show all matching package records
    @smoke4
    Scenario: SS-BROWSER-004 User filter the column SEED NAME from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | SEED NAME |
            | 301%      |
        Then results will show all matching package records
    @smoke5
    Scenario: SS-BROWSER-005 User filter the column SEED from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | SEED          |
            | SEED00000008% |
        Then results will show all matching package records

    @seeds_filtering2 @smoke6
    Scenario: SS-BROWSER-006 User filter the column PACKAGE QTY. from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | PACKAGE QTY. |
            | 0            |
        Then results will show all matching package records
    @smoke7
    Scenario: SS-BROWSER-007 User filter the column UNIT from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | UNIT |
            | g%   |
        Then results will show all matching package records
    @smoke8
    Scenario: SS-BROWSER-008 User filter the column OTHER_NAME from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | OTHER_NAME |
            | %RGA-%     |
        Then results will show all matching package records
    @smoke9
    Scenario: SS-BROWSER-009 User filter the column YEAR from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | YEAR |
            | 2021 |
        Then results will show all matching package records

    @seeds_filtering3 @smoke10
    Scenario: SS-BROWSER-009 User filter the column EXPERIMENT TYPE from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | EXPERIMENT TYPE |
            | Breed%          |
        Then results will show all matching package records
    @smoke11
    Scenario: SS-BROWSER-010 User filter the column EXPERIMENT from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | EXPERIMENT |
            | %OYT%      |
        Then results will show all matching package records
    @smoke12
    Scenario: SS-BROWSER-011 User filter the column OCCURRENCE from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | OCCURRENCE |
            | %F1-%      |
        Then results will show all matching package records
    @smoke13
    Scenario: SS-BROWSER-012 User filter the column SEASON from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | SEASON |
            | DS     |
        Then results will show all matching package records