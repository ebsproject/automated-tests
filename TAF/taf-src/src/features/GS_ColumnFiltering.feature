@Germplasm_filtering
Feature: SQA-570 Column Filtering

    Background: Login User
        Given the user is in "Germplasm" Search entrypoint
        And the user sets Data filters parameters for Germplasm Search
            | Program                           |
            | Irrigated South-East Asia (IRSEA) |
    #needs section: browser fro location value
    @germ_filtering1 @germ_smoke1
    Scenario:   
        Given user perform generic germplasm name search with input "CMS%"
        When user set column filters of the search results in Germplasm browser
            | GERMPLASM   |
            | EXP0002097% |
        Then results show all matching germplasm
    @germ_smoke2
    Scenario: GS-BROWSER-002 User filter the column OTHER NAMES from the search results
        Given user perform generic germplasm name search with input "CMS%"
        When user set column filters of the search results in Germplasm browser
            | OTHER NAMES |
            | IR1%        |
        Then results show all matching germplasm
    @germ_smoke3
    Scenario: GS-BROWSER-003 User filter the column PARENTAGE from the search results
        Given user perform generic germplasm name search with input "CMS%"
        When user set column filters of the search results in Germplasm browser
            | PARENTAGE |
            | %DS-%     |
        Then results show all matching germplasm
    @germ_smoke4
    Scenario: GS-BROWSER-004 User filter the column GENERATION from the search results
        Given user perform generic germplasm name search with input "CMS%"
        When user set column filters of the search results in Germplasm browser
            | GENERATION |
            | 301%       |
        Then results show all matching germplasm
    @germ_smoke5
    Scenario: GS-BROWSER-005 User filter the column GERMPLASM NAME TYPE from the search results
        Given user perform generic germplasm name search with input "CMS%"
        When user set column filters of the search results in Germplasm browser
            | GERMPLASM NAME TYPE |
            | SEED00000008%       |
        Then results show all matching germplasm

    @germ_filtering2 @germ_smoke6
    Scenario: GS-BROWSER-006 User filter the column GERMPLASM TYPE from the search results
        Given user perform generic germplasm name search with input "CMS%"
        When user set column filters of the search results in Germplasm browser
            | GERMPLASM TYPE |
            | 0              |
        Then results show all matching germplasm
    @germ_smoke7
    Scenario: GS-BROWSER-007 User filter the column GERMPLASM STATE from the search results
        Given user perform generic germplasm name search with input "CMS%"
        When user set column filters of the search results in Germplasm browser
            | GERMPLASM STATE |
            | g%              |
        Then results show all matching germplasm
