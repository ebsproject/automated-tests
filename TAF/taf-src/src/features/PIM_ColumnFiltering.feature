@PIM_filtering
Feature: SQA-569 Column Filtering of Planting Instructions Manager browser

    Background: The packing jobs are loaded
        Given the user is in Planting Instructions Manager
    # And the user sets Data filters parameters for Planting Instructions Manager
    #     | Program                           |
    #     | Irrigated South-East Asia (IRSEA) |

    @PIM_filter1
    Scenario: PIM-BROWSER-001 User filter the column JOB CODE from the browser results
        Given user can see Planting Instructions Manager grid ready
        When user set column filters of the search results of the Planting Instructions Manager
            | JOB CODE |
            | PJOB000% |
        Then matching results will show in the Planting Instructions Manager browser

    @PIM_filter2
    Scenario: PIM-BROWSER-02 User filter the column EXPERIMENTS from the browser results
        Given user can see Planting Instructions Manager grid ready
        When user set column filters of the search results of the Planting Instructions Manager
            | EXPERIMENTS |
            | IRSEA%      |
        Then matching results will show in the Planting Instructions Manager browser

    @PIM_filter3
    Scenario: PIM-BROWSER-003 User filter the column OCCURRENCE COUNT from the browser results
        Given user can see Planting Instructions Manager grid ready
        When user set column filters of the search results of the Planting Instructions Manager
            | OCCURRENCE COUNT |
            | 1%               |
        Then matching results will show in the Planting Instructions Manager browser

    @PIM_filter4
    Scenario: PIM-BROWSER-004 User filter the column ENVELOPE COUNT from the browser results
        Given user can see Planting Instructions Manager grid ready
        When user set column filters of the search results of the Planting Instructions Manager
            | ENVELOPE COUNT |
            | 1%             |
        Then matching results will show in the Planting Instructions Manager browser

    @PIM_filter5
    Scenario: PIM-BROWSER-005 User filter the column FACILITY from the browser results
        Given user can see Planting Instructions Manager grid ready
        When user set column filters of the search results of the Planting Instructions Manager
            | FACILITY |
            | NCBL     |
        Then matching results will show in the Planting Instructions Manager browser