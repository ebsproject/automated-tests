
Feature: CORB-2347 Sorting of selected traits browser both for an experiment's location and occurrence

    Background: Go to Download Data Collection for an Occurrence Traits
        Given user goes to Experiment Manager
        And the user sets Data filters parameters for experiment manager
            | Program                           |
            | Irrigated South-East Asia (IRSEA) |
    #@ddctraitssmoke
    #Scenario: DDC-BROWSER-000 Go to Download Data Collection
        When the user selects a planted occurrence
        And the user clicks download trait data collection
        Then the user is redirected to Download Data Collection files tool

    @ddctraitssmoke1
    Scenario: DDC-BROWSER-001 Sort by LABEL
        Given traits are added in the Selected Traits browser
        When user sorts the Selected Traits browser
            | Column Order    |
            | LABEL Ascending |
        Then traits items are sorted accordingly
            | Sort Order      |
            | LABEL Ascending |
