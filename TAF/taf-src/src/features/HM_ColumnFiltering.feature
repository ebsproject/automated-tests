Feature: Germplasm state column in plot browser
    The user should be able to see the germplasm state column in the plot browser
    The germplasm state column should display the germplasm state of the plot
    The should be able to sort and filter the germplasm state column

    #@hmsmoke
    @germ_state_col
    Scenario: HM-BROWSER-001 Germplasm state added in the plot browser
        Given the user is in Harvest Manager
        And the plots for "Wheat Bulk" seedlot creation are loaded
        When the user checks the query results
        Then the user sees the germplasm state column