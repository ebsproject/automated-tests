Feature: Create a packing job
    The user should be able to create a packing job
    The created packing job should be able to see in the list of the packing jobs
    The user should be able to configure specific parameters for packing jobs

    Background: Login user and go to experiment manager
        Given the user is in Experiment Manager
        And the user sets Data filters parameters for Planting Instructions Manager
            | Program                           |
            | Irrigated South-East Asia (IRSEA) |

    @pimtaf
    Scenario: PIM-PACKING-001 Create a packing job
        When the user selects a created occurrence
        And the user clicks the create packing job button
        Then the user is redirected to Planting Instructions Manager

    @pimtaf
    Scenario: PIM-PACKING-002 Update the values of the selected Occurrence
        Given the user has created a packing job
        And the user is redirected to Planting Instructions Manager
        When the user set the quantities of seeds per envelope, unit and envelopes per plot
        Then the user sees the packing job summary and instructions
        Then a success message for packing job is shown
