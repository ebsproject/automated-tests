@ECBT10 @ECBT @parallel_run_ec
Feature: Experiment Creation Breeding Trial - 10 entries
    The user should be able to assign an experimental design when creating an experiment
    Different options of experimental design must be available
    The user configures specific parameters for a selected design.

    Background: Create a breeding trial experiment
        Given the user is in Experiment Creation
        And the user sets Data filter parameters
            | Program                           |
            | Irrigated South-East Asia (IRSEA) |
        And user can see EC browser grid ready
        And user sets Basic tab values
            | CreateExperiment | ExperimentType | Stage | EvaluationSeason | ExperimentName | SaveBasic |
            |                  | Breeding Trial | AYT   | WS               |                |           |
        And user has previously created an entry list from "IRSEA-PN-2014-DS-003"
        And user enters Design

    Scenario Outline: EC-TRIAL-RCBD-001 Randomize the experiment as RCBD with layout
        When user sets "RCBD" as Design
        And user sets "<occ>" as Number of Occurrences
        And user sets "<rep>" as Number of Replicates
        And user sets "<shape>" as Define Shape Field
        And user sets "<rowRep>" as Number of Rows Per Rep
        And user sets "<row>" as Rows
        And user selects Generate Design
        Then user sees the request successfully sent
        And user waits for design generation to finish
        And user sees the "<result>"
        Then user enters Site
        And user sets "IRRI, Los Baños, Laguna, Philippines" as Site for a Single Occurence
        Then user enters Review
        Then user Finalizes Experiment

        Examples:
            | occ | rep | shape | rowRep | row | order      | result |
            | 1   | 4   | Yes   | 1      | 4   | Serpentine | layout |
            | 1   | 8   | Yes   | 1      | 2   | Plot Order | layout |
@try10
    Scenario Outline: EC-TRIAL-RCBD-002 Randomize the experiment as RCBD without layout
        When user sets "RCBD" as Design
        And user sets "<occ>" as Number of Occurrences
        And user sets "<rep>" as Number of Replicates
        And user sets "<shape>" as Define Shape Field
        And user selects Generate Design
        Then user sees the request successfully sent
        And user sees this result
        Then user enters Site
        And user sets "IRRI, Los Baños, Laguna, Philippines" as Site for a Single Occurence
        Then user enters Review
        Then user Finalizes Experiment

        Examples:
            | occ | rep | shape |
            | 1   | 4   | No    |

    Scenario Outline: EC-TRIAL-RCBD-003 Randomize the experiment as RCBD | CIMMYT with define shape
        When user sets "RCBD | CIMMYT" as Design
        And user sets "<occ>" as Number of Occurrences
        And user sets "<rep>" as Number of Replicates
        And user sets "<shape>" as Define Shape Field
        And user sets "<rows>" as Rows
        And user sets "<turn>" as Plots Until Turn The Serpentine
        And user selects Generate Design
        Then user sees the request successfully sent
        And user waits for design generation to finish
        And user sees the "<result>"
        Then user enters Site
        And user sets "IRRI, Los Baños, Laguna, Philippines" as Site for a Single Occurence
        Then user enters Review
        Then user Finalizes Experiment

        Examples:
            | occ | rep | shape | rows | turn | order      | result |
            | 1   | 4   | Yes   | 4    | 6    | Horizontal | layout |


    Scenario Outline: EC-TRIAL-RCBD-004 Randomize the experiment as RCBD | CIMMYT without layout
        When user sets "RCBD | CIMMYT" as Design
        And user sets "<occ>" as Number of Occurrences
        And user sets "<rep>" as Number of Replicates
        And user sets "<shape>" as Define Shape Field
        And user selects Generate Design
        Then user sees the request successfully sent
        And user sees this result
        Then user enters Site
        And user tries to Save Site
        And user sets "IRRI, Los Baños, Laguna, Philippines" as Site for a Single Occurence
        Then user enters Review
        Then user Finalizes Experiment

        Examples:
            | occ | rep | shape |
            | 1   | 4   | No    |
