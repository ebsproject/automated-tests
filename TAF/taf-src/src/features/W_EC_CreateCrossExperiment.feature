@WECICN @parallel_run_ec
Feature: Experiment Creation-Wheat-Intentional Crossing Nursery-EC-Wheat-ICN
    The user should be able to create an ICN experiment
    Different options for crossing must be available    

    Background: Create an ICN experiment
        Given the user is in Experiment Creation
        And the user sets Data filter parameters
            | Program                           |
            | BW Wheat Breeding Program (BW) |
        And user can see EC browser grid ready
        And user sets Basic tab values
           | CreateExperiment | ExperimentType               | Stage | EvaluationSeason | ExperimentName | SaveBasic |
           |                  | Intentional Crossing Nursery | HB    | WS               |                |           |
        And user has previously created an entry list from "BW-HB-2017-B-002"
        And user enters Crosses

    Scenario: Wheat-EC-ICN-001 Self Cross
        When user performs Self Cross
        And user enters Manage Crosses
        And user enters Planting Arrangement-Entry Order
        And user enters Protocol
        Then user enters Site
        And user sets "IRRI, Los Baños, Laguna, Philippines" as Site for a Single Occurence
        Then user enters Review
        Then user Finalizes Experiment

    #Scenario: Wheat-EC-ICN-002 Female-Male Cross