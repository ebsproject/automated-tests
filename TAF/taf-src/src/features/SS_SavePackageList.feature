
Feature: SQA-496 Add to Seedlist and Save as a package list for viewing in List Manager

    Background: Login user and proceed to Seed search
        Given the user is in "Seed search" Search


    # @debug
    Scenario: SS-SAVELIST-001 Set Seed search parameters
        When user sets Seed search parameters
            | Program                                                                   | Experiment Type                                  | Facility | Sub-facility             |
            # | Barley Test Program, KE Maize Breeding Program | Dry, Wet, Season A, Season B | Generation Nursery, International Crossing Nursery | 2015DS     | PGF      | PGF117-5-36, PGF117-5-1 |
            | Irrigated South-East Asia, Barley Test Program, KE Maize Breeding Program | Generation Nursery, Intentional Crossing Nursery | PGF      | PGF117-5-36, PGF117-5-11 |

    # @debug
    Scenario: SS-SAVELIST-002 Set Filter Search Parameters
        When the user sets Data filters parameters for Seed Search
            | Program                           |
            # Year        | Season | Stage | Site                  |
            | Irrigated South-East Asia (IRSEA) |
        # 2014,  2017 | A,B    | F1,HB | Afghanistan, El Batan |
        When user sets Seed search parameters
            | Experiment Type                                  | Season  | Occurrence               | Facility | Sub-facility             |
            | Generation Nursery, Intentional Crossing Nursery | Dry,Wet | IRSEA-F6-2016-WS-001-001 | PGF      | PGF117-5-36, PGF117-5-11 |
    # | Irrigated South-East Asia (IRSEA) |


    @debug @saveworkinglist
    Scenario: SS-SAVELIST-003 Add to the working list and save as a new package list
        When the user sets Data filters parameters for Seed Search
            | Program                           |
            | Irrigated South-East Asia (IRSEA) |
        When user clears the working list
        When user sets Seed search parameters
            | Experiment Type                                                  | Occurrence                                                                 |
            | Breeding Trial, Generation Nursery, Intentional Crossing Nursery | IRSEA-F1-2015-DS-003-001,IRSEA-F1-2015-DS-001-001,IRSEA-F1-2015-WS-001-001 |
        When user can see grid ready
        When user set "Not Group Results" for Search Result Settings
        When user can see grid ready
        When user checks all programs
        When user selects 'Add to Seedlist' button
        #When user can see grid ready
        When user selects 'Save list' button
        When user selects 'Proceed to Save' button
        When user creates list
            | Name       | Description                             | Remarks           |
            | AUTOMATED* | This is an automated saved package list | QC Automated Test |
        When user selects 'Close' button of modal
        When user navigates to "List manager"
        #Then user sets "CV1test" by column "Name" in Lists
        Then user can search the saved list and can see "Name" match in Lists browser


