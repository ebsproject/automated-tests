Feature: Planting Instructions Manager
    The user should be able to see the list of the packing jobs
    The packing job should display the packing job summary and additional instructions
    The user should be able to configure specific parameters for packing jobs

    Background: The packing jobs are loaded
        Given the user is in Planting Instructions Manager
        And the user sets Data filters parameters for Planting Instructions Manager
            | Program                           |
            | Irrigated South-East Asia (IRSEA) |

    @pimView @pimtaf
    Scenario: PIM-FEATURE-001 View a packing job
        When the user clicks on the view packing job
        Then the user sees the packing job summary and additional instructions

    @pimUpdateSingle @pimtaf
    Scenario: PIM-FEATURE-002 Single update a packing job
        When the user clicks on the update packing job
        And the user set the quantities of seeds per envelope, unit and envelopes per plot
        Then a success message for update is shown

    @pimUpdateBulk @pimtaf
    Scenario: PIM-FEATURE-003 Perform Bulk Update for All Plots using "Seeds / Envelope"
        When the user clicks on the update packing job
        And the user update all plots using "Seeds / Envelope" method
        Then a success message for update is shown

    @pimUpdateBulk @pimtaf
    Scenario: PIM-FEATURE-004 Perform Bulk Update for All Plots using "Unit"
        When the user clicks on the update packing job
        And the user update all plots using "Unit" method
        Then a success message for update is shown

    @pimUpdateBulk @pimtaf
    Scenario: PIM-FEATURE-005 Perform Bulk Update for All Plots using "Envelopes / Plot"
        When the user clicks on the update packing job
        And the user update all plots using "Envelopes / Plot" method
        Then a success message for update is shown

    @pimFilter @pimtaf
    Scenario: PIM-FEATURE-006 Filter "STATUS" column by "draft"
        When the user filters "STATUS" browser column by "draft"
        Then only "DRAFT" items are displayed on the results

    @pimFilter @pimtaf
    Scenario: PIM-FEATURE-007 Filter "STATUS" column by "packed"
        When the user filters "STATUS" browser column by "packed"
        Then only "PACKED" items are displayed on the results

    @pimFilter @pimtaf
    Scenario: PIM-FEATURE-008 Filter "STATUS" column by "packing"
        When the user filters "STATUS" browser column by "packing"
        Then only "PACKING" items are displayed on the results

    @pimFilter @pimtaf
    Scenario: PIM-FEATURE-009 Filter "STATUS" column by "packing on hold"
        When the user filters "STATUS" browser column by "packing on hold"
        Then only "PACKING ON HOLD" items are displayed on the results

    @pimFilter @pimtaf
    Scenario: PIM-FEATURE-010 Filter "STATUS" column by "packing cancelled"
        When the user filters "STATUS" browser column by "packing cancelled"
        Then only "PACKING CANCELLED" items are displayed on the results

    @pimFilter @pimtaf
    Scenario: PIM-FEATURE-011 Filter "STATUS" column by "ready for packing"
        When the user filters "STATUS" browser column by "ready for packing"
        Then only "READY FOR PACKING" items are displayed on the results

    @pimFilter @pimtaf
    Scenario: PIM-FEATURE-012 Filter "JOB TYPE" column by "packing job"
        When the user filters "JOB TYPE" browser column by "packing job"
        Then only "packing job" items are displayed on the results