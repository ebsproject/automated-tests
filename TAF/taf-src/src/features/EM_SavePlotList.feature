Feature: From Experiment Manager's Plot tab, save records as plot list and view in List Manager

    Background: Login user and go to experiment manager
        Given user goes to Experiment Manager
        And the user sets Data filters parameters for experiment manager
            | Program                           |
            | Irrigated South-East Asia (IRSEA) |


    @em_saveplotlist
    Scenario: EM-SAVE-PLOTLIST-001
        Given the user view the first occurrence
        And navigates to "Plot" tab of the occurrence
        And occurrence plot grid is ready
        When the user save the plots as new list
            | Name       | Description                                    | Type | Remarks           |
            | AUTOMATED* | This is an automated saved list for occurrence | Plot | QC Automated Test |
        When user navigates to "List manager" from Experiment Manager
        Then user can search the saved plot list and can see "Name" match in Lists browser
