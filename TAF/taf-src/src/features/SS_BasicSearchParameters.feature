@smoke_build    @seeds_basicsearch
Feature: Seed Search's basic search parameters
    Background: Login User
        Given the user is in "Seed search" Search
        And the user sets Data filters parameters for Seed Search
            | Program                           |
            | Irrigated South-East Asia (IRSEA) |

    @seeds_basicsearch_001
    Scenario: SS-BASIC-SEARCH-001 Set Program parameter
        Given user can see grid ready
        Then user sets Seed search parameters
            | Program                                                                                           |
            | BW Wheat Breeding Program (BW), KE Maize Breeding Program (KE), Irrigated South-East Asia (IRSEA) |

    @seeds_basicsearch_002
    Scenario: SS-BASIC-SEARCH-002 Set Season parameter
        Given user can see grid ready
        Then user sets Seed search parameters
            | Season   |
            | Dry, Wet |

    @seeds_basicsearch_003
    Scenario: SS-BASIC-SEARCH-003 Set Experiment Year parameter
        Given user can see grid ready
        Then user sets Seed search parameters
            | Experiment Year |
            | 2016, 2021      |

    @seeds_basicsearch_004
    Scenario: SS-BASIC-SEARCH-004 Set Experiment Type parameter
        Given user can see grid ready
        And user sets Seed search parameters
            | Experiment Type                                                  |
            | Breeding Trial, Generation Nursery, Intentional Crossing Nursery |

    @seeds_basicsearch_005
    Scenario: SS-BASIC-SEARCH-005 Set Occurrence parameter
        Given user can see grid ready
        And user sets Seed search parameters
            | Occurrence                                                                      |
            | IRSEA-AYT-2014-WS-001-001, IRSEA-AYT-2015-DS-001-001, IRSEA-AYT-2016-DS-001-001 |

    @seeds_basicsearch_006
    Scenario: SS-BASIC-SEARCH-006 Set season parameter
        Given user can see grid ready
        Then user sets Seed search parameters
            | Facility | Sub-facility |
            | KJL, PGF | KJL, PGF     |