@list_filtering
Feature: CORB-2222 Column Filtering

    Background: Go to List Manager and set filters
        Given user navigates to List Manager on the left menu
        And the user sets Data filters parameters for List Manager
            | Program                           |
            | Irrigated South-East Asia (IRSEA) |

    @listsmoke1
    Scenario: LM-BROWSER-001 User filter the column TYPE from the search results
            Given user can see lists browser grid ready
            When user set column filters of lists browser
                  | TYPE  |
                  | Trait |
    @listsmoke2
    Scenario: LM-BROWSER-002 User filter the column ABBREV from the search results
        Given user can see lists browser grid ready
        When user set column filters of lists browser
            | ABBREV      |
            | %TRAITLIST% |
    @listsmoke3
    Scenario: LM-BROWSER-003 User filter the column NAME from the search results
        Given user can see lists browser grid ready
        When user set column filters of lists browser
            | NAME            |
            | %GermplasmList% |
    @listsmoke4
    Scenario: LM-BROWSER-004 User filter the column DISPLAY NAME from the search results
        Given user can see lists browser grid ready
        When user set column filters of lists browser
            | DISPLAY NAME |
            | %TraitList%  |
    @listsmoke5
    Scenario: LM-BROWSER-005 User filter the column DESCRIPTION from the search results
        Given user can see lists browser grid ready
        When user set column filters of lists browser
            | DESCRIPTION |
            | QA_%        |
    @listsmoke6
    Scenario: LM-BROWSER-006 User filter the column OWNED BY from the search results
        Given user can see lists browser grid ready
        When user set column filters of lists browser
            | OWNED BY |
            | %Dolores |
    @listsmoke7
    Scenario: LM-BROWSER-007 User filter the column ACCESS TYPE from the search results
        Given user can see lists browser grid ready
        When user set column filters of lists browser
            | ACCESS TYPE |
            | Shared with me |
    @listsmoke8
    Scenario: LM-BROWSER-008 User filter the column SETTINGS from the search results
        Given user can see lists browser grid ready
        When user set column filters of lists browser
            | SETTINGS |
            | Shared |