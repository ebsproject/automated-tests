@seeds_addfilters
Feature: B4R-7578 Add LOCATION variable as search parameter

    Background: Login User
        Given the user is in "Seed search" Search
        And the user sets Data filters parameters for Seed Search
            | Program                           |
            | Irrigated South-East Asia (IRSEA) |
        And user sets Seed search parameters
            | Season   | Experiment Type                                                  |
            | Dry, Wet | Breeding Trial, Generation Nursery, Intentional Crossing Nursery |

    @seedsaddfilter1
    Scenario: SS-FILTER-001 Search for package information for seeds harvested in a specific LOCATION
        Given user can see grid ready
        And user has selected "IRRIHQ-2014-WS-004" as "Location" values
        When user clicks on Find
        Then results will show all matching package records
    @seedsaddfilter2
    Scenario: SS-FILTER-002 Search with additional parameter Location and an input Germplasm List
        Given user can see grid ready
        When user has selected "IRRIHQ-2014-WS-004" as "Location" values
        And user has selected "GERMPLASM NAME - Designation and Germplasm Names information" as "Input List" values
        And user loads input list items from "Rice Germplasm Names" file to "variable"
        And user sets "variable" contents to Seed Search input list field
        When user clicks on Find
        Then results will show all matching package records