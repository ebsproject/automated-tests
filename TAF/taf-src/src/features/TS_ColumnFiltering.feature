@traits_filtering
Feature: SQA-708 Traits Column Filtering

    Background: Login user and go to Traits Search
        Given the user is in Traits Search
        And the user sets Data filters parameters for Traits Search
            | Program                           |
            | Irrigated South-East Asia (IRSEA) |
        And user perform generic traits name search with input "%_"

    @traits_filter1
    Scenario: TS-BROWSER-001 User filter the column ABBREV from the browser results
        When user set column filters of the search results of the Traits Search
            | ABBREV |
            | GRAIN% |
        Then matching results will show in the Traits Search browser

    @traits_filter2
    Scenario: TS-BROWSER-002 User filter the column LABEL from the browser results
        When user set column filters of the search results of the Traits Search
            | LABEL |
            | HT%   |
        Then matching results will show in the Traits Search browser

    @traits_filter3
    Scenario: TS-BROWSER-003 User filter the column NAME from the browser results
        When user set column filters of the search results of the Traits Search
            | NAME   |
            | Plant% |
        Then matching results will show in the Traits Search browser

    @traits_filter4
    Scenario: TS-BROWSER-004 User filter the column DISPLAY NAME from the browser results
        When user set column filters of the search results of the Traits Search
            | DISPLAY NAME |
            | Plant%       |
        Then matching results will show in the Traits Search browser

    @traits_filter5
    Scenario: TS-BROWSER-005 User filter the column DATA TYPE from the browser results
        When user set column filters of the search results of the Traits Search
            | DATA TYPE |
            | char%     |
        Then matching results will show in the Traits Search browser

    @traits_filter6
    Scenario: TS-BROWSER-006 User filter the column TYPE from the browser results
        When user set column filters of the search results of the Traits Search
            | TYPE    |
            | observ% |
        Then matching results will show in the Traits Search browser

    @traits_filter7
    Scenario: TS-BROWSER-007 User filter the column USAGE from the browser results
        When user set column filters of the search results of the Traits Search
            | USAGE |
            | study |
        Then matching results will show in the Traits Search browser

    @traits_filter8
    Scenario: TS-BROWSER-008 User filter the column DATA LEVEL from the browser results
        When user set column filters of the search results of the Traits Search
            | DATA LEVEL |
            | entry      |
        Then matching results will show in the Traits Search browser

    @traits_filter9
    Scenario: TS-BROWSER-009 User filter the column STATUS from the browser results
        When user set column filters of the search results of the Traits Search
            | STATUS |
            | active |
        Then matching results will show in the Traits Search browser