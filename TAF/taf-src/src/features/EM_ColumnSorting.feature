@EM_sorting
Feature: SQA-718 Column sorting

    Background: Login user and go to experiment manager
        Given user goes to Experiment Manager
        And the user sets Data filters parameters for experiment manager
            | Program                        |
            | BW Wheat Breeding Program (BW) |

    @em_sorting_smoke
    Scenario: EM-PLOT-BROWSER-001 User sorts the column PLOT NO from the Plot Tab in ASC order
        Given user can see plot browser grid ready
        When user sorts the Plot Tab browser column
            | Column Order      |
            | Plot No Ascending |
        Then the browser loads completely after sorting
            | Sort Order        |
            | Plot No Ascending |

    @em_sorting_smoke
    Scenario: EM-PLOT-BROWSER-002 User sorts the column PLOT NO from the Plot Tab in DESC order
        Given user can see plot browser grid ready
        When user sorts the Plot Tab browser column
            | Column Order       |
            | Plot No Descending |
        Then the browser loads completely after sorting
            | Sort Order         |
            | Plot No Descending |

    @em_sorting_smoke
    Scenario: EM-PLOT-BROWSER-003 User sorts the column ENTRY NO from the Plot Tab in ASC order
        Given user can see plot browser grid ready
        When user sorts the Plot Tab browser column
            | Column Order       |
            | Entry No Ascending |
        Then the browser loads completely after sorting
            | Sort Order         |
            | Entry No Ascending |

    @em_sorting_smoke
    Scenario: EM-PLOT-BROWSER-004 User sorts the column ENTRY NO from the Plot Tab in DESC order
        Given user can see plot browser grid ready
        When user sorts the Plot Tab browser column
            | Column Order        |
            | Entry No Descending |
        Then the browser loads completely after sorting
            | Sort Order          |
            | Entry No Descending |

    @em_sorting_smoke
    Scenario: EM-PLOT-BROWSER-005 User sorts the column GERMPLASM NAME from the Plot Tab in ASC order
        Given user can see plot browser grid ready
        When user sorts the Plot Tab browser column
            | Column Order             |
            | Germplasm Name Ascending |
        Then the browser loads completely after sorting
            | Sort Order               |
            | Germplasm Name Ascending |

    @em_sorting_smoke
    Scenario: EM-PLOT-BROWSER-006 User sorts the column GERMPLASM NAME from the Plot Tab in DESC order
        Given user can see plot browser grid ready
        When user sorts the Plot Tab browser column
            | Column Order              |
            | Germplasm Name Descending |
        Then the browser loads completely after sorting
            | Sort Order                |
            | Germplasm Name Descending |

    @em_sorting_smoke
    Scenario: EM-PLOT-BROWSER-007 User sorts the column GERMPLASM STATE from the Plot Tab in ASC order
        Given user can see plot browser grid ready
        When user sorts the Plot Tab browser column
            | Column Order              |
            | Germplasm State Ascending |
        Then the browser loads completely after sorting
            | Sort Order                |
            | Germplasm State Ascending |

    @em_sorting_smoke
    Scenario: EM-PLOT-BROWSER-008 User sorts the column GERMPLASM STATE from the Plot Tab in DESC order
        Given user can see plot browser grid ready
        When user sorts the Plot Tab browser column
            | Column Order               |
            | Germplasm State Descending |
        Then the browser loads completely after sorting
            | Sort Order                 |
            | Germplasm State Descending |