@dashboardsmoke_run
Feature: Change the dashboard data filters from each tool's landing page and be able to still access the tool

    Background: Login user and go to specific tool's landing page
        Given user logs in to EBS system

    @dashboardsmoke1
    Scenario: DASH-NAVIGATION-001 When in Experiment Creation page, applying dashboard data filters are reflected in the Current Filter menu
        Given the user sets Data filters parameters in Dashboard
            | Program                           |
            | Irrigated South-East Asia (IRSEA) |
        When user navigates to "Experiment creation" on the left menu
        Then user is redirected to "Experiment creation" entrypoint page
        And current filter menu in Dashboard is updated
            | Program                           |
            | Irrigated South-East Asia (IRSEA) |
    @dashboardsmoke
    Scenario: DASH-NAVIGATION-002 When in Experiment Manager page, applying dashboard data filters are reflected in the Current Filter menu
        Given the user sets Data filters parameters in Dashboard
            | Program                        |
            | KE Maize Breeding Program (KE) |
        When user navigates to "Experiment manager" on the left menu
        Then user is redirected to "Experiment manager" entrypoint page
        And current filter menu in Dashboard is updated
            | Program                        |
            | KE Maize Breeding Program (KE) |
    @dashboardsmoke
    Scenario: DASH-NAVIGATION-003 When in Planting Instructions Manager page, applying dashboard data filters are reflected in the Current Filter menu
        Given the user sets Data filters parameters in Dashboard
            | Program                        |
            | BW Wheat Breeding Program (BW) |
        When user navigates to "Planting instructions manager" on the left menu
        Then user is redirected to "Planting instructions manager" entrypoint page
        And current filter menu in Dashboard is updated
            | Program                        |
            | BW Wheat Breeding Program (BW) |
    @dashboardsmoke
    Scenario: DASH-NAVIGATION-004 When in Data Collection page, applying dashboard data filters are reflected in the Current Filter menu
        Given the user sets Data filters parameters in Dashboard
            | Program                   |
            | Barley Test Program (BTP) |
        When user navigates to "Data collection" on the left menu
        Then user is redirected to "Data collection" entrypoint page
        And current filter menu in Dashboard is updated
            | Program                   |
            | Barley Test Program (BTP) |
    @dashboardsmoke5
    Scenario: DASH-NAVIGATION-005 When in Harvest Manager page, applying dashboard data filters are reflected in the Current Filter menu
        Given the user sets Data filters parameters in Dashboard
            | Program                   |
            | Barley Test Program (BTP) |
        When user navigates to "Harvest manager" on the left menu
        Then user is redirected to "Harvest manager" entrypoint page
        And current filter menu in Dashboard is updated
            | Program                   |
            | Barley Test Program (BTP) |
    @dashboardsmoke6
    Scenario: DASH-NAVIGATION-006 When in Search Germplasm page, applying dashboard data filters are reflected in the Current Filter menu
        Given the user sets Data filters parameters in Dashboard
            | Program                        |
            | BW Wheat Breeding Program (BW) |
        When user navigates to "Germplasm" on the left menu
        Then user is redirected to "Germplasm" entrypoint page
        And current filter menu in Dashboard is updated
            | Program                        |
            | BW Wheat Breeding Program (BW) |
    @dashboardsmoke7
    Scenario: DASH-NAVIGATION-007 When in Seed Search page, applying dashboard data filters are reflected in the Current Filter menu
        Given the user sets Data filters parameters in Dashboard
            | Program                        |
            | KE Maize Breeding Program (KE) |
        When user navigates to "Seed search" on the left menu
        Then user is redirected to "Seed search" entrypoint page
        And current filter menu in Dashboard is updated
            | Program                        |
            | KE Maize Breeding Program (KE) |
    @dashboardsmoke8
    Scenario: DASH-NAVIGATION-008 When in Search Traits page, applying dashboard data filters are reflected in the Current Filter menu
        Given the user sets Data filters parameters in Dashboard
            | Program                        |
            | KE Maize Breeding Program (KE) |
        When user navigates to "Traits" on the left menu
        Then user is redirected to "Traits" entrypoint page
        And current filter menu in Dashboard is updated
            | Program                        |
            | KE Maize Breeding Program (KE) |
    @dashboardsmoke9
    Scenario: DASH-NAVIGATION-009 When in List Manager page, applying dashboard data filters are reflected in the Current Filter menu
        Given the user sets Data filters parameters in Dashboard
            | Program                        |
            | KE Maize Breeding Program (KE) |
        When user navigates to "List manager" on the left menu
        Then user is redirected to "List manager" entrypoint page
        And current filter menu in Dashboard is updated
            | Program                        |
            | KE Maize Breeding Program (KE) |

    @dashboardsmoke10
    Scenario: DASH-NAVIGATION-010 Navigate from one tool to another via the left menu
        Given the user sets Data filters parameters in Dashboard
            | Program                           |
            | Irrigated South-East Asia (IRSEA) |
        When user navigates to "Experiment creation" on the left menu
        Then user is redirected to "Experiment creation" entrypoint page
        When user navigates to "Experiment manager" on the left menu
        Then user is redirected to "Experiment manager" entrypoint page
        When user navigates to "Planting instructions manager" on the left menu
        Then user is redirected to "Planting instructions manager" entrypoint page
        When user navigates to "Data collection" on the left menu
        Then user is redirected to "Data collection" entrypoint page
        When user navigates to "Harvest manager" on the left menu
        Then user is redirected to "Harvest manager" entrypoint page
        When user navigates to "Germplasm" on the left menu
        Then user is redirected to "Germplasm" entrypoint page
        When user navigates to "Seed search" on the left menu
        Then user is redirected to "Seed search" entrypoint page
        When user navigates to "Traits" on the left menu
        Then user is redirected to "Traits" entrypoint page
        When user navigates to "List manager" on the left menu
        Then user is redirected to "List manager" entrypoint page
        And current filter menu in Dashboard is updated
            | Program                           |
            | Irrigated South-East Asia (IRSEA) |