@seeds_addfilters
Feature: B4R-7532 Query Parameter Input List
    Background: Login User
        Given the user is in "Seed search" Search
        And the user sets Data filters parameters for Seed Search
            | Program                           |
            | Irrigated South-East Asia (IRSEA) |
        And user can see grid ready
        And user sets Seed search parameters
            | Program                                                                         |
            | BW Wheat Breeding Program, KE Maize Breeding Program, Irrigated South-East Asia |
    @seeds_inputlist1
    Scenario: SS-INPUTLIST-001 Search for package information for seeds using Designation and Germplasm Names as Input List values
        Given user has selected "GERMPLASM NAME - Designation and Germplasm Names information" as "Input List" values
        And user loads input list items from "Wheat Germplasm Names" file to "variable"
        And user sets "variable" contents to Seed Search input list field
        When user clicks on Find
        Then results will show all matching package records
    @seeds_inputlist2
    Scenario: SS-INPUTLIST-002 Search for package information for seeds using Seed Names as Input List values
        Given user has selected "SEED NAME - Seed Name information" as "Input List" values
        And user loads input list items from "Rice Seed Names" file to "variable"
        And user sets "variable" contents to Seed Search input list field
        When user clicks on Find
        Then results will show all matching package records
    @seeds_inputlist3
    Scenario: SS-INPUTLIST-003 Search for package information for seeds using Package Label as Input List values
        Given user has selected "PACKAGE LABEL - Package Label information" as "Input List" values
        And user loads input list items from "Rice Package Labels" file to "variable"
        And user sets "variable" contents to Seed Search input list field
        When user clicks on Find
        Then results will show all matching package records