Feature: Experiment Manager Test

    Background: Login user and go to experiment manager
        Given user goes to Experiment Manager
        And the user sets Data filters parameters for experiment manager
            | Program                           |
            | Irrigated South-East Asia (IRSEA) |
    #needs section: browser fro location value
    @EM1 @SumRun
    Scenario: EM-FEATURE-001 User clicks a location value
        When user clicks the Location value
        Then user sees the basic info of the Location
    @EM6 @SumRun
    Scenario: EM-FEATURE-007 Add shortcut to upload Trait data collection files in EM browser
        When  the user clicks the "Upload" button
        And the user clicks "Trait data collection file"
        Then the user is redirected to the Data collection upload page
    @EM7 @SumRun
    Scenario: EM-FEATURE-008 Update an occurrence
        When the user selects a not planted occurrence
        And the user updates info of the occurrence
        Then the info of the occurrence is updated
    @EM9 @SumRun
    #possible use of table
    Scenario: EM-FEATURE-010 Query experiments and occurrences
        When the user selects these data filters
            | Program                           | Year | Season | Stage | Site                                 |
            | Irrigated South-East Asia (IRSEA) | 2013 | A      | AGR   | IRRI, Los Baños, Laguna, Philippines |
        And the user clicks apply
        Then the result is reloaded
    @EM11 @SumRun
    Scenario: EM-FEATURE-011 Browse occurrences grouped by Experiments type in filter
        When the user clicks the Experiment filter
        And the user inputs in Experiment filter
        Then the results shown is reloaded with filter
    @EM12 @SumRun
    Scenario: EM-FEATURE-012 Browse occurrences grouped by Experiments change page size
        When the user clicks personalized grid settings
        And the user changes the page size
        And the user clicks apply on page size
        Then the results shown is reloaded with page size
    @EM14 @SumRun
    Scenario: EM-FEATURE-015 Navigate to Trait Data Collection (Occurrence) for a single occurrence
        When the user selects an occurrence
        And the user clicks occurrence on trait data collection
        Then the user is shown the Data Collection page
    @EM15 @SumRun
    Scenario: EM-FEATURE-016 Navigate to Trait Data Collection (Location) for a single occurrence
        When the user selects an occurrence
        And the user clicks location on trait data collection
        Then the user is shown the Data Collection page
    @EM16 @Demo  @SumRun
    Scenario: EM-FEATURE-017 Navigate to Trait Data Collection (Occurrence) for multiple occurrences
        When the user selects multiple occurrence
        And the user clicks location on trait data collection
        Then a warning about selecting occurrences is shown
    @EM16 @Demo @SumRun
    Scenario: EM-FEATURE-018 Navigate to Trait Data Collection (Occurrence) for zero occurrences
        When the user selects no occurrence
        And the user clicks location on trait data collection
        Then a warning about selecting occurrences is shown
    # table needed in 17
    @EM17 @Demo @SumRun
    Scenario: EM-FEATURE-019 Reset/Set Filter Experiments by Program
        When the user clicks filter experiments
        And the user selects a "program"
        And the user clicks Apply Experiment Filtler
        Then a prompt on successful application of filters is shown
    @EM17 @Demo @SumRun
    Scenario: EM-FEATURE-020 Reset/Set Filter Experiments by Year
        When the user clicks filter experiments
        And the user selects a "year"
        And the user clicks Apply Experiment Filtler
        Then a prompt on successful application of filters is shown
    @EM17 @Demo @SumRun
    Scenario: EM-FEATURE-021 Reset/Set Filter Experiments by Location
        When the user clicks filter experiments
        And the user selects a "location"
        And the user clicks Apply Experiment Filtler
        Then a prompt on successful application of filters is shown
    @EM18 @Demo2 @SumRun
    Scenario: EM-FEATURE-022 Filter Occurrences in EM browser by specific
        When the query result is displayed
        And the user inputs "location" on the location filter input
        Then the occurrences are filtered by "location" location
    @EM18 @Demo2 @SumRun
    Scenario: EM-FEATURE-023 Filter Occurrences in EM browser by Location wildcard
        When the query result is displayed
        And the user inputs "%" on the location filter input
        Then the occurrences are filtered by "%" location
    @EM19 @Demo2 @SumRun
    Scenario: EM-FEATURE-024 Filter Occurrences in EM browser by Status column
        When the user inputs "CREATED" on the status filter input
        Then the occurrences are filtered by "CREATED" status
    @EM19 @Demo2 @SumRun
    Scenario: EM-FEATURE-025 Filter Occurrences in EM browser by Status column
        When the user inputs "MAPPED" on the status filter input
        Then the occurrences are filtered by "MAPPED" status
    @EM19 @Demo2 @SumRun
    Scenario: EM-FEATURE-026 Filter Occurrences in EM browser by Status column
        When the user inputs "PLANTED" on the status filter input
        Then the occurrences are filtered by "PLANTED" status
    @EM20 @Demo3 @SumRun
    Scenario: EM-FEATURE-027 Generate New Location for a created Intentional Crossing Nursery
        When the user views an Intentional Crossing Nursery occurrence
        And the user sets the Intentional Crossing Nursery location
        Then the Intentional Crossing Nursery location is generated
    @EM22 @Demo3 @SumRun
    Scenario: EM-FEATURE-029 Display occurrences in ascending order per experiment
        When an experiment group has more than one occurrences
        Then the occurrences are displayed in descending order
    @EM23
    Scenario: EM-FEATURE-030 Generate New Location for 1k+ entries
        When the user views an Intentional Crossing Nursery occurrence
        And the user sets the Intentional Crossing Nursery location
        Then the Intentional Crossing Nursery location is generated
    @EM23 @parallel_run_EM1
    Scenario: EM-FEATURE-031 Generate New Location for 1k+ entries
        When the user searches for an occurrence with "1kentries"
        And the user generates the location using this information
            | Location Name     | Description       |
            | Loc-Test-parallel | Loc-Test-parallel |
        Then location of the experiment is generated

    @EM23 @parallel_run_EM
    Scenario: EM-FEATURE-032 Generate New Location for 1.5k+ entries
        When the user searches for an occurrence with "1_5kentries"
        And the user generates the location using this information
            | Location Name     | Description       |
            | Loc-Test-parallel | Loc-Test-parallel |
        Then location of the experiment is generated

    @EM23 @parallel_run_EM
    Scenario: EM-FEATURE-033 Generate New Location for 2k+ entries
        When the user searches for an occurrence with "2kentries"
        And the user generates the location using this information
            | Location Name     | Description       |
            | Loc-Test-parallel | Loc-Test-parallel |
        Then location of the experiment is generated
    @EM23
    Scenario: EM-FEATURE-034 Generate New Location for 2.5k+ entries
        When the user searches for an occurrence with "2_5kentries"
        And the user generates the location using this information
            | Location Name     | Description       |
            | Loc-Test-parallel | Loc-Test-parallel |
        Then location of the experiment is generated
