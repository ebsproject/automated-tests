Feature: Dashboard or Joblist basic UI test
    The user should be able to create a new analysis request for a given experiment

    #atomic tests
    Scenario: Go to Breeding Analytics
        Given the user is in the Analysis Request Manager
        When the user clicks the Add New Request button
        Then the user is redirected to the Add New Request page
        And experiment occurrences for analysis are displayed

    Scenario: Show Job list created
        Given the user is in Breeding Analytics
        When the user clicks Anaysis Request Manager
        Then the job list is shown 

    Scenario: Refresh Grid
        Given the user is in Breeding Analytics
        And the user sees the job list
        When the user clicks reset Grid
        Then the job list refreshed 
        
    Scenario: Show notifications
        Given the user is in Breeding Analytics
        And the user sees the job list
        When clicks the notification button
        Then the notification modal is shown

    Scenario: Go to specific page
        Given the user is in Breeding Analytics
        And the user sees the job list
        When the user inputs a number on go to page
        Then the user will see that specific page

    Scenario: Show specific number of pages
        Given the user is in Breeding Analytics
        And the user sees the job list
        When the user inputs a number in show
        Then the user will see that specific number of pages

    Scenario: View job
        Given the user is in Breeding Analytics
        And the user sees the job list
        When the user  clicks view request 
        Then the user will see information about that specific request

    Scenario: Search specific job by ID
        Given the user is in Breeding Analytics
        And the user sees the job list
        When the user filters job using RequestID
        Then the job list will be updated based on RequestID filter

    Scenario: Search specific job by status
        Given the user is in Breeding Analytics
        And the user sees the job list
        When the user filters job using status
        Then the job list will be updated based on status filter

    Scenario: Search specific job by institute
        Given the user is in Breeding Analytics
        And the user sees the job list
        When the user filters job using institute
        Then the job list will be updated based on institute filter

    Scenario: Search specific job by crop
        Given the user is in Breeding Analytics
        And the user sees the job list
        When the user filters job using crop
        Then the job list will be updated based on crop filter

    Scenario: Search specific job by analysis type
        Given the user is in Breeding Analytics
        And the user sees the job list
        When the user filters job using analysis type
        Then the job list will be updated based on analysis type filter

    Scenario: Scenario: Search specific job by experiments
        Given the user is in Breeding Analytics
        And the user sees the job list
        When the user filters job using experiments
        Then the job list will be updated based on experiments filter

    Scenario: Search specific job by location
        Given the user is in Breeding Analytics
        And the user sees the job list
        When the user filters job using location
        Then the job list will be updated based on location filter

    Scenario: Search specific job by trait
        Given the user is in Breeding Analytics
        And the user sees the job list
        When the user filters job using trait
        Then the job list will be updated based on trait filter

    Scenario: Search specific job by model
        Given the user is in Breeding Analytics
        And the user sees the job list
        When the user filters job using model
        Then the job list will be updated based on model filter

    Scenario: Search specific job by spatial
        Given the user is in Breeding Analytics
        And the user sees the job list
        When the user filters job using spatial
        Then the job list will be updated based on spatial filter

    Scenario: Search specific job by objective
        Given the user is in Breeding Analytics
        And the user sees the job list
        When the user filters job using objective
        Then the job list will be updated based on objective filter
