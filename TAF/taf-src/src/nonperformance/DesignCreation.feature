Feature: Design Creation
      The user should be able to assing an experimental design when creating an experiment
      Different options of experimental design must be available
      The user configures specific parameters for a selected design.

        Background: User are in "Stage 3 Design" in Experiment Creation process
            Given user logs in to B4R
             When user goes to Experiments
              And user creates Experiments
              And user sets "Breeding Trial" in Experiment Type
              And user sets "UAT20.07_Design_#" in Experiment Name
              And user sets "UAT Design" in Experiment Objective
              And user sets "AYT" in Stage
              And user sets "CS" in Evaluation Season
              And user sets "A" in Planting Season
              And user sets "Allan, Jennifer" in Experiment Steward
              And user sets "IRSEA_PIPELINE" in Pipeline
              And user sets "IRSEA_PROJECT" in Project
              And user selects Save
              And user add from Saved List "ENTRYLIST_003"
              And user selects "check" for rows "1,2"
              And user selects Next

      #   @debug
        Scenario Outline: 1- Randomize the experiment as Alpha-Lattice with define shape
             When user sets "Alpha-Lattice" as Design
              And user sets "<occ>" as Number of Occurrences
              And user sets "<rep>" as Number of Replicates
              And user sets "<blkRep>" as Number of Blocks Per Rep
              And user sets "<shape>" as Define Shape Field
              And user sets "<rowBlk>" as Number of Rows Per Block
              And user sets "<rowRep>" as Number of Rows Per Rep
              And user sets "<rows>" as Rows
              And user selects Generate Design
             Then user sees the request successfully sent
              And users sees the "<result>"

        Examples:
                  | occ | rep | blkRep | shape | rowBlk | rowRep | rows | result |
                  | 1   | 2   | 3      | Yes   | 1      | 3      | 3    | layout |
                  | 2   | 6   | 3      | Yes   | 3      | 3      | 6    | fail   |
                  #bug | 40 | 2 | 3 | Yes | 1 | 5 | 5 | layout |
                  | 10 | 4 | 3 | Yes | 3 | 3 | 6 | fail   |
                  | 15 | 2 | 3 | Yes | 1 | 3 | 3 | layout |

        Scenario Outline: 2- Randomize the experiment as Alpha-Lattice | CIMMYT with define shape
             When user sets "Alpha-Lattice | CIMMYT" as Design
              And user sets "<occ>" as Number of Occurrences
              And user sets "<rep>" as Number of Replicates
              And user sets "<plotBlk>" as Number of Plots Per Block
              And user sets "<shape>" as Define Shape Field
              And user sets "<rows>" as Rows
              And user sets "<turn>" as Plots Until Turn The Serpentine
              And user selects Generate Design
             Then user sees the request successfully sent
              And users sees the "<result>"

        Examples:
                  | occ | rep | plotBlk | shape | rows | turn | result |
                  | 1   | 2   | 3       | Yes   | 5    | 5    | layout |
                  | 1   | 2   | 5       | Yes   | 3    | 5    | layout |
                  | 2   | 2   | 3       | Yes   | 5    | 5    | layout |
                  | 25  | 2   | 3       | Yes   | 5    | 5    | layout |
                  | 100 | 2   | 3       | Yes   | 5    | 5    | layout |

        Scenario Outline: 3- Randomize the experiment as Alpha-Lattice | Wheat with define shape
             When user sets "Alpha-Lattice | WHEAT" as Design
              And user sets "<occ>" as Number of Occurrences
              And user sets "<rep>" as Number of Replicates
              And user sets "<plotBlk>" as Number of Plots Per Block
              And user sets "<shape>" as Define Shape Field
              And user sets "<rows>" as Rows
              And user sets "<turn>" as Plots Until Turn The Serpentine
              And user sets "<rand1>" as Randomize 1st Step
              And user selects Generate Design
             Then user sees the request successfully sent
              And users sees the "<result>"

        Examples:
                  | occ | rep | plotBlk | shape | rows | turn | rand1 | result |
                  # | 2   | 2   | 5       | Yes   | 3    | 5    | Yes   | layout |
                  # | 2   | 3   | 3       | Yes   | 5    | 3    | Yes   | layout |
                  # | 30  | 2   | 3       | Yes   | 1    | 5    | Yes   | layout |
                  | 2 | 4 | 3 | Yes | 5 | 3 | Yes | layout |

        Scenario Outline: 4- Randomize the experiment as Augmented RCB with define shape
             When user sets "Augmented RCB" as Design
              And user sets "<occ>" as Number of Occurrences
              And user sets "<rep>" as Number of Replicates
              And user sets "<shape>" as Define Shape Field
              And user sets "<rows>" as Rows
              And user sets "<rowRep>" as Number of Rows Per Rep
              And user selects Generate Design
             Then user sees the request successfully sent
              And users sees the "<result>"

        Examples:
                  | occ | rep | shape | rowRep | rows | result |
                  | 1   | 2   | Yes   | 3      | 3    | fail   |
                  | 2   | 4   | Yes   | 5      | 5    | fail   |

        Scenario Outline: 5- Randomize the experiment as RCBD with define shape
             When user sets "RCBD" as Design
              And user sets "<occ>" as Number of Occurrences
              And user sets "<rep>" as Number of Replicates
              And user sets "<shape>" as Define Shape Field
              And user sets "<rows>" as Rows
              And user sets "<rowRep>" as Number of Rows Per Rep
              And user selects Generate Design
             Then user sees the request successfully sent
              And users sees the "<result>"

        Examples:
                  | occ | rep | shape | rows | rowRep | result |
                  | 1   | 2   | Yes   | 5    | 5      | layout |
                  | 1   | 8   | Yes   | 15   | 5      | fail   |
                  | 1   | 9   | Yes   | 15   | 5      | layout |
                  | 40  | 2   | Yes   | 5    | 5      | layout |

        @bug
        Scenario Outline: 6- Randomize the experiment as RCBD | CIMMYT with define shape
             When user sets "RCBD | CIMMYT" as Design
              And user sets "<occ>" as Number of Occurrences
              And user sets "<rep>" as Number of Replicates
              And user sets "<shape>" as Define Shape Field
              And user sets "<rows>" as Rows
              And user sets "<turn>" as Plots Until Turn The Serpentine
              And user selects Generate Design
             Then user sees the request successfully sent
              And users sees the "<result>"

        Examples:
                  | occ | rep | shape | rows | turn | result |
                  | 1   | 2   | Yes   | 5    | 3    | layout |
                  | 1   | 8   | Yes   | 15   | 3    | layout |
                  | 1   | 9   | Yes   | 15   | 3    | layout |
                  | 40  | 2   | Yes   | 5    | 3    | layout |

        Scenario Outline: 7- Randomize the experiment as Row-Column with define shape
             When user sets "Row-Column" as Design
              And user sets "<occ>" as Number of Occurrences
              And user sets "<rep>" as Number of Replicates
              And user sets "<rowBlks>" as Number of Row Blocks
              And user sets "<shape>" as Define Shape Field
              And user sets "<rows>" as Rows
              And user selects Generate Design
             Then user sees the request successfully sent
              And users sees the "<result>"

        Examples:
                  | occ | rep | rowBlks | shape | rows | result |
                  # | 1   | 2   | 3       | Yes   | 3    | layout |
                  | 1 | 3 | 3 | Yes | 6 | layout |
                  # | 1  | 3 | 3 | Yes | 5 | fail   |
                  # | 40 | 2 | 3 | Yes | 6 | layout |
                  # | 3  | 4 | 5 | Yes | 5 | layout |