Feature: Reset Parameters, Plots, and Crosses
    The user should be able to reset the query parameters, plots, and crosses
    Any updates made should be reverted after resetting the browser
    The browser should be emptied after clearing the search parameters


    Background: The plots are loaded
        Given the user is in Harvest Manager    

    #@regression
    Scenario: Reset all query parameters
     And the plots for "Maize Bulk" seedlot creation are loaded
     When the user checks the query results
     And the user clicks clear search parameters
     Then the query parameters are emptied
     And the plot data browser is emptied

    #@regression
    Scenario: Reset all query parameters
     And the plots for "Maize Bulk" seedlot creation are loaded
     When the user filters the plot number
     And the user clicks Reset Grid
     Then the updates for "Maize Bulk" are reverted
