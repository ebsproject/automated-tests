Feature: Create Seedlots for Wheat
    The user should be create seedlots for Wheat harvest
    Different harvest methods should be used
    The user configures specific parameters for harvest data.


      Background: The plots are loaded
	      Given the user is in Harvest Manager            

      @hmsmoke
      Scenario: Create seedlots for Bulk
	      And the plots for "Wheat Bulk" seedlot creation are loaded
        When the user updates the selected plots using "Bulk" method 
        And the user validates the harvest data
        And the user creates seedlot
	      Then a notification appears
	      And the seedlot is created successfully

      @hmsmoke
      Scenario: Create seedlots for Single Plant Selection
	      And the plots for "Wheat Single Plant Selection" seedlot creation are loaded
        When the user updates the selected plots using "Single Plant Selection" method 
        And the user validates the harvest data
        And the user creates seedlot
	      Then a longer notification appears
	      And the seedlot is created successfully

     @hmsmoke
      Scenario: Create seedlots for Individual Spike
	      And the plots for "Wheat Individual Spike" seedlot creation are loaded
        When the user updates the selected plots using "Individual Spike" method 
        And the user validates the harvest data
        And the user creates seedlot
	      Then a longer notification appears
	      And the seedlot is created successfully

      @hmsmoke
      Scenario: Create seedlots for Selected Bulk
	      And the plots for "Wheat Selected Bulk" seedlot creation are loaded
        When the user updates the selected plots using "Modified Bulk" method 
        And the user validates the harvest data
        And the user creates seedlot
	      Then a longer notification appears
	      And the seedlot is created successfully

