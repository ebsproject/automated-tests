Feature: Bulk Update Selected Plots for Wheat
    The user should be able to bulk update selected plots for Wheat harvest
    Different harvest methods should be used
    The user configures specific parameters for harvest data.


    Background: The plots are loaded
        Given the user is in Harvest Manager            
    
    #@hmtaf
    Scenario: Perform Bulk Update for Selected Plots using "Bulk"
        And the plots for "Wheat Bulk" seedlot creation are loaded
        When the user updates the selected plots using "Bulk" method
        Then a success message is shown
        And the "Bulk" data for wheat selected plots is displayed in the plot data browser
    
    #@hmtaf
    Scenario: Perform Bulk Update for Selected Plots using "Single Plant Selection"
        And the plots for "Wheat Single Plant Selection" seedlot creation are loaded
        When the user updates the selected plots using "Single Plant Selection" method
        Then a success message is shown
        And the "Single Plant Selection" data for wheat selected plots is displayed in the plot data browser

    #@hmtaf
    Scenario: Perform Bulk Update for Selected Plots using "Selected Bulk"
        And the plots for "Wheat Selected Bulk" seedlot creation are loaded
        When the user updates the selected plots using "Modified Bulk" method
        Then a success message is shown
        And the "Modified Bulk" data for wheat selected plots is displayed in the plot data browser

    #@hmtaf
    Scenario: Perform Bulk Update for Selected Plots using "Individual Spike"
        And the plots for "Wheat Individual Spike" seedlot creation are loaded
        And the user updates the selected plots using "Individual Spike" method
        Then a success message is shown
        And the "Individual Spike" data for wheat selected plots is displayed in the plot data browser