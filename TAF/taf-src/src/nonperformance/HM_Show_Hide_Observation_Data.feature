Feature: Show/Hide Observation Data
    The user should be able to filter the plots
    Different filter variables can be used
    The user specifies filter parameters for harvest data.

    Background: The plots are loaded
        Given the user is in Harvest Manager            

    @hmsmoke
    Scenario: Harvest Date with No Observation Data
        And the plots for "Wheat Single Plant Selection" seedlot creation are loaded
        When the user updates the selected plots using "Single Plant Selection" method 
        And the user clicks the ShowHide Observation Data button
        And the user selects "Harvest Date" with no observation data
        Then the query results for "Harvest Date with No Data" are filtered

    @hmsmoke
    Scenario: Harvest Method with No Observation Data
        And the plots for "Wheat Single Plant Selection" seedlot creation are loaded
        When the user updates the selected plots using "Single Plant Selection" method 
        And the user clicks the ShowHide Observation Data button
        And the user selects "Harvest Method" with no observation data
        Then the query results for "Harvest Method with No Data" are filtered

    @hmsmoke
    Scenario: Numeric Variable with No Observation Data
        And the plots for "Wheat Single Plant Selection" seedlot creation are loaded
        When the user updates the selected plots using "Single Plant Selection" method 
        And the user clicks the ShowHide Observation Data button
        And the user selects "Numeric Variable" with no observation data
        Then the query results for "Numeric Variable with No Data" are filtered

    @hmsmoke
    Scenario: Numeric Variable with No Observation Data
        And the plots for "Wheat Bulk" seedlot creation are loaded
        When the user updates the selected plots using "Bulk" method 
        And the user clicks the ShowHide Observation Data button
        And the user selects "All Variables" with no observation data
        Then the query results for "All Variables with No Data" are filtered

    @hmsmoke
    Scenario: Harvest Date with Observation Data
        And the plots for "Maize Individual Ear" seedlot creation are loaded
        When the user updates the selected plots using "Individual Ear" method 
        And the user clicks the ShowHide Observation Data button
        And the user selects "Harvest Date" with observation data
        Then the query results for "Harvest Date with Data" are filtered

    @hmsmoke
    Scenario: Harvest Method with Observation Data
        And the plots for "Maize Individual Ear" seedlot creation are loaded
        When the user updates the selected plots using "Individual Ear" method 
        And the user clicks the ShowHide Observation Data button
        And the user selects "Harvest Method" with observation data
        Then the query results for "Harvest Method with Data" are filtered

    @hmsmoke
    Scenario: Harvest Method with Observation Data
        And the plots for "Maize Individual Ear" seedlot creation are loaded
        When the user updates the selected plots using "Individual Ear" method 
        And the user clicks the ShowHide Observation Data button
        And the user selects "Numeric Variable" with observation data
        Then the query results for "Numeric Variable with Data" are filtered

    @hmsmoke
    Scenario: Numeric Variable with Observation Data
        And the plots for "Wheat Individual Spike" seedlot creation are loaded
        When the user updates the selected plots using "Individual Spike" method 
        And the user clicks the ShowHide Observation Data button
        And the user selects "All Variables" with observation data
        Then the query results for "All Variables with Data" are filtered

    