Feature: Display committed data
    The user should be able to update harvest data traits
    The user should be able to create seeds, germplasm, and packages
    The user should be able to see the committed data after successfully creating records

    #@hmsmoke
    Scenario: Display committed data on the plot data browser
     Given the user is in Harvest Manager
     And the plots for "Maize Individual Ear" seedlot creation are loaded
     And "Bulk" seeds, packages, and germplasm are created
     When the user goes back to the harvest data tab
     Then the committed data is displayed in highlighted green background

    #sprint21.07
    Scenario: Display committed data on the cross data browser
    Given the user is in Harvest Manager
     And the crosses for "Maize Bulk" seedlot creation are loaded
     And "Bulk" seeds, packages, and germplasm are created
     When the user goes back to the harvest data tab
     Then the committed data is displayed in highlighted green background