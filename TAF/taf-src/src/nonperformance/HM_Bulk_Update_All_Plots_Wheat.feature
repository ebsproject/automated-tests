Feature: Bulk Update All Plots for Wheat
    The user should be able to bulk update all plots for Wheat harvest
    Different harvest methods should be used
    The user configures specific parameters for harvest data.


    Background: The plots are loaded
        Given the user is in Harvest Manager

    #@hmtest
    Scenario: Perform Bulk Update for All Plots using "Bulk"
        And the search parameters are specified
            | Program                        | Experiment       | Location Code  |
            | BW Wheat Breeding Program (BW) | BW-F1-2014-A-003 | EBN-2017-B-002 |
        And the experiment location details are displayed
            | Experiment Type    | Site     | Steward      |
            | Generation Nursery | El Batan | Ramos, Asiya |
        When the user loads the "plots" for seedlot creation
        And the user bulks update harvest data of all plots
        And the user specified the following harvest traits for all plots
            | Harvest Date | Harvest Method | Numeric Variables |
            | 2021-06-11   | Bulk           | NA                |
        Then a success message is shown
        And the harvest data for all plots is displayed in the plot data browser
            | Harvest Method |
            | Bulk           |

    #@hmtest
    Scenario: Perform Bulk Update for All Plots using "Single Plant Selection"
        And the search parameters are specified
            | Program                        | Experiment       | Location Code  |
            | BW Wheat Breeding Program (BW) | BW-F1-2014-A-004 | EBN-2017-B-003 |
        And the experiment location details are displayed
            | Experiment Type    | Site     | Steward       |
            | Generation Nursery | El Batan | Goff, Khadija |
        When the user loads the "plots" for seedlot creation
        And the user bulks update harvest data of all plots
        And the user specified the following harvest traits for all plots
            | Harvest Date | Harvest Method         | Numeric Variables |
            | 2021-06-11   | Single Plant Selection | 10                |
        Then a success message is shown
        And the harvest data for all plots is displayed in the plot data browser
            | Harvest Method         |
            | Single Plant Selection |

    #@hmtest
    Scenario: Perform Bulk Update for All Plots using "Modified Bulk"
        And the search parameters are specified
            | Program                        | Experiment       | Location Code  |
            | BW Wheat Breeding Program (BW) | BW-F1-2014-A-002 | EBN-2017-B-001 |
        And the experiment location details are displayed
            | Experiment Type    | Site     | Steward       |
            | Generation Nursery | El Batan | Carlson, Ansh |
        When the user loads the "plots" for seedlot creation
        And the user bulks update harvest data of all plots
        And the user specified the following harvest traits for all plots
            | Harvest Date | Harvest Method | Numeric Variables |
            | 2021-06-11   | Selected bulk  | 10                |
        Then a success message is shown
        And the harvest data for all plots is displayed in the plot data browser
            | Harvest Method |
            | Selected bulk  |

    #@hmtaf
    Scenario: Perform Bulk Update for All Plots using "Individual Spike"
        And the search parameters are specified
            | Program                        | Experiment       | Location Code  |
            | BW Wheat Breeding Program (BW) | BW-F1-2014-A-001 | EBN-2014-A-001 |
        And the experiment location details are displayed
            | Experiment Type    | Site     | Steward       |
            | Generation Nursery | El Batan | Costa, Nicola |
        When the user loads the "plots" for seedlot creation
        And the user bulks update harvest data of all plots
        And the user specified the following harvest traits for all plots
            | Harvest Date | Harvest Method   | Numeric Variables |
            | 2021-06-11   | Individual Spike | 10                |
        Then a success message is shown
        And the harvest data for all plots is displayed in the plot data browser
            | Harvest Method   |
            | Individual Spike |