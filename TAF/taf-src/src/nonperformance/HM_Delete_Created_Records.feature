Feature: Delete created records
    The user should be able to delete the created seeds, package, and germplasm
    The harvest summary should be updated
    The user should be able to delete selected/all records

    Background: The plots are loaded
     Given the user is in Harvest Manager  

    #@hmrun
    Scenario: Bulk Delete Selected Packages
     And the plots for "Wheat Bulk" seedlot creation are loaded
     And seeds, packages, and germplasm are created
     When the user deletes selected records
     Then the selected packages are deleted