Feature: Create Seedlots for Maize
  The user should be create seedlots for Maize harvest
  Different harvest methods should be used
  The user configures specific parameters for harvest data.


  Background: The plots are loaded
    Given the user is in Harvest Manager


  #@retest
  Scenario: Create seedlots for Bulk
    And the search parameters are specified
      | Program                        | Experiment           | Location Code   |
      | KE Maize Breeding Program (KE) | KE-Parallel-Test-004 | AFG-2021-DS-003 |
    And the experiment location details are displayed
      | Experiment Type    | Site        | Steward                  |
      | Generation Nursery | Afghanistan | Go (IRRI), Lois Angelica |
    When the user loads the "plots" for seedlot creation
    And the user bulks update harvest data of all plots
    And the user specified the following harvest traits
      | Harvest Date | Harvest Method | Numeric Variables |
      | 2021-06-11   | Bulk           | NA                |
    And the user validates the harvest data
    And the user creates seedlot
    Then a notification appears
    And the seedlot is created successfully

  @retest
  Scenario: Create seedlots for Individual Ear
    And the search parameters are specified
      | Program                        | Experiment         | Location Code  |
      | KE Maize Breeding Program (KE) | KE-UND-2021-DS-003 | ID-2021-DS-002 |
    And the experiment location details are displayed
      | Experiment Type | Site      | Steward                  |
      | Observation     | Indonesia | Go (IRRI), Lois Angelica |
    When the user loads the "plots" for seedlot creation
    And the user bulks update harvest data of all plots
    And the user specified the following harvest traits for all plots
      | Harvest Date | Harvest Method | Numeric Variables |
      | 2021-06-11   | Individual ear | 20                |
  #And the user validates the harvest data
  #And the user creates seedlot
  #Then a notification appears
  #And the seedlot is created successfully


  #start of parallel run
  Scenario: Create seedlots for Bulk #1
    And the search parameters are specified
      | Program                        | Experiment           | Location Code  |
      | KE Maize Breeding Program (KE) | KE-Parallel-Test-005 | BD-2021-WS-001 |
    And the experiment location details are displayed
      | Experiment Type              | Site       | Steward                  |
      | Intentional Crossing Nursery | Bangladesh | Go (IRRI), Lois Angelica |
    When the user loads the "plots" for seedlot creation
    And the user bulks update harvest data of all plots
    And the user specified the following harvest traits
      | Harvest Date | Harvest Method | Numeric Variables |
      | 2021-10-11   | Bulk           | NA                |
    And the user validates the harvest data
    And the user creates seedlot
    Then a notification appears
    And the seedlot is created successfully


  Scenario: Create seedlots for Bulk #2
    And the search parameters are specified
      | Program                        | Experiment           | Location Code  |
      | KE Maize Breeding Program (KE) | KE-Parallel-Test-006 | BT-2021-DS-001 |
    And the experiment location details are displayed
      | Experiment Type      | Site   | Steward                  |
      | Cross Parent Nursery | Bhutan | Go (IRRI), Lois Angelica |
    When the user loads the "plots" for seedlot creation
    And the user bulks update harvest data of all plots
    And the user specified the following harvest traits
      | Harvest Date | Harvest Method | Numeric Variables |
      | 2021-06-11   | Bulk           | NA                |
    And the user validates the harvest data
    And the user creates seedlot
    Then a notification appears
    And the seedlot is created successfully


# KE-Parallel-Test-004	(212 plots, not fixed)
# KE-Parallel-Test-005	(212 plots, not fixed)
# 	KE-Parallel-Test-006 (314 plots, not fixed)
# 	KE-Parallel-Test-007 (314 plots, not fixed)
# KE-Parallel-Test-008 (314 plots, not fixed)


