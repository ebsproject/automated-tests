Feature: Germplasm Tests

        Background: Login User
            Given user logs in to B4R

        Scenario: Query names set to IR% returns 51,861
             When user goes to Experiments
#   And user selects Find
#  Then user sees "51,861" items
#   And user sees "DESIGNATION" in column names
#   And user sees "OTHER NAMES" in column names
#   And user sees "PARENTAGE" in column names
#   And user sees "GENERATION" in column names
#   And user sees "GERMPLASM NAME TYPE" in column names
#   And user sees "GERMPLASM STATE" in column names
#   And user sees "GERMPLASM NORMALIZED NAME" in column names
#   And user logs out

# Scenario: Query names set IR% and Germplasm State set hybrid returns 1 item
#      When user sets "IR%" in Names
#       And user sets "hybrid" in Germplasm State
#       And user selects Find
#      Then user sees "1" items
#       And user sees "1-1" pagination
#       And user logs out

# Scenario: Validate IR values
#      When user sets "IR%" in Names
#       And user sets "hybrid" in Germplasm State
#       And user selects Find
#       And user selects "1" row
#      Then user sees modal box
#       And users sees Basic tab in modal box
#       And users sees Names section in modal box
#       And users sees Seeds section in modal box
#       And users sees Pedigree section in modal box
#       And users sees Eventlogs section in modal box
#       And user logs out


