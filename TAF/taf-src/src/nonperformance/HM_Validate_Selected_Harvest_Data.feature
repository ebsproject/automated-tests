Feature: Validate Selected Harvest Data
    The user should be able to see the germplasm state column in the plot browser
    The germplasm state column should display the germplasm state of the plot
    The should be able to sort and filter the germplasm state column

    #@regression
    Scenario: Validate Selected Harvest Data
     Given the user is in Harvest Manager
     And the plots for "Wheat Bulk" seedlot creation are loaded    
     When the user updates the selected plots using "Bulk" method
     And the user validates the harvest data
     Then the status is changed to For Seedlot Creation
