Feature: Add notification bell button
    The user should be able to delete the created seeds, package, and germplasm
    The harvest summary should be updated
    The user should be able to delete selected/all records

    #@testhm
    Scenario: A notifcation bell is displayed after seedlot creation
        Given the user is in Harvest Manager
        And the search parameters are specified
            | Program                        | Experiment       | Location Code  |
            | BW Wheat Breeding Program (BW) | BW-F1-2014-A-004 | EBN-2017-B-003 |
        And the experiment location details are displayed
            | Experiment Type    | Site     | Steward       |
            | Generation Nursery | El Batan | Goff, Khadija |
        When the user loads the "plots" for seedlot creation
        And the user bulks update harvest data
        And the user specified the following harvest traits
            | Harvest Date | Harvest Method | Numeric Variables |
            | 2021-06-11   | Bulk           | NA                |
        And the user creates seeds, packages, and germplasm
        Then the progress is displayed in the notification bell
        And the create tab is emptied

    #start of parallel run
    #@parallel_run_hm
    Scenario: BW-001-211 items
        Given the user is in Harvest Manager
        And the search parameters are specified
            | Program                        | Experiment       | Location Code  |
            | BW Wheat Breeding Program (BW) | BW-F1-2014-A-005 | EBN-2017-B-004 |
        And the experiment location details are displayed
            | Experiment Type    | Site     | Steward         |
            | Generation Nursery | El Batan | Allan, Jennifer |
        When the user loads the "plots" for seedlot creation
        And the user bulks update harvest data
        And the user specified the following harvest traits
            | Harvest Date | Harvest Method | Numeric Variables |
            | 2021-06-11   | Bulk           | NA                |
        And the user creates seeds, packages, and germplasm
        Then the progress is displayed in the notification bell
        And the create tab is emptied

    #@parallel_run_hm
    Scenario: BW-002-198 items
        Given the user is in Harvest Manager
        And the search parameters are specified
            | Program                        | Experiment       | Location Code  |
            | BW Wheat Breeding Program (BW) | BW-HB-2017-B-005 | CON-2017-B-005 |
        And the experiment location details are displayed
            | Experiment Type              | Site           | Steward         |
            | Intentional Crossing Nursery | Ciudad Obregon | Allan, Jennifer |
        When the user loads the "plots" for seedlot creation
        And the user bulks update harvest data
        And the user specified the following harvest traits
            | Harvest Date | Harvest Method | Numeric Variables |
            | 2021-06-11   | Bulk           | NA                |
        And the user creates seeds, packages, and germplasm
        Then the progress is displayed in the notification bell
        And the create tab is emptied

    #@parallel_run_hm
    Scenario: KE-003-212 items
        Given the user is in Harvest Manager
        And the search parameters are specified
            | Program                        | Experiment           | Location Code        |
            | KE Maize Breeding Program (KE) | KE-Parallel-Test-002 | PH_BO_SG-2021-CS-001 |
        And the experiment location details are displayed
            | Experiment Type    | Site            | Steward                  |
            | Generation Nursery | Sagbayan, Bohol | Go (IRRI), Lois Angelica |
        When the user loads the "plots" for seedlot creation
        And the user bulks update harvest data
        And the user specified the following harvest traits
            | Harvest Date | Harvest Method | Numeric Variables |
            | 2021-06-11   | Bulk           | NA                |
        And the user creates seeds, packages, and germplasm
        Then the progress is displayed in the notification bell
        And the create tab is emptied

    #@parallel_run_hm
    Scenario: KE-004-212 items
        Given the user is in Harvest Manager
        And the search parameters are specified
            | Program                        | Experiment           | Location Code      |
            | KE Maize Breeding Program (KE) | KE-Parallel-Test-003 | IRRIHQ-2021-DS-126 |
        And the experiment location details are displayed
            | Experiment Type              | Site                                 | Steward                  |
            | Intentional Crossing Nursery | IRRI, Los Baños, Laguna, Philippines | Go (IRRI), Lois Angelica |
        When the user loads the "plots" for seedlot creation
        And the user bulks update harvest data
        And the user specified the following harvest traits
            | Harvest Date | Harvest Method | Numeric Variables |
            | 2021-06-11   | Bulk           | NA                |
        And the user creates seeds, packages, and germplasm
        Then the progress is displayed in the notification bell
        And the create tab is emptied

    #@parallel_run_hm
    Scenario: IRSEA-005-780 items
        Given the user is in Harvest Manager
        And the search parameters are specified
            | Program                           | Experiment              | Location Code  |
            | Irrigated South-East Asia (IRSEA) | IRSEA-Parallel-Test-001 | KH-2021-WS-001 |
        And the experiment location details are displayed
            | Experiment Type    | Site     | Steward                  |
            | Generation Nursery | Cambodia | Go (IRRI), Lois Angelica |
        When the user loads the "plots" for seedlot creation
        And the user bulks update harvest data
        And the user specified the following harvest traits
            | Harvest Date | Harvest Method | Numeric Variables |
            | 2021-06-11   | Bulk           | NA                |
        And the user creates seeds, packages, and germplasm
        Then the progress is displayed in the notification bell
        And the create tab is emptied
