Feature: Bulk Update Selected Plots for Maize
    The user should be able to bulk update selected plots for Maize harvest
    Different harvest methods should be used
    The user configures specific parameters for harvest data.


    Background: The plots are loaded
        Given the user is in Harvest Manager

    #@hmtest
    Scenario: Perform Bulk Update for Selected Plots using "Bulk"
        #And the plots for "Maize Bulk" seedlot creation are loaded
        #When the user updates the selected plots using "Bulk" method
        And the search parameters are specified
            | Program                        | Experiment       | Location Code  |
            | KE Maize Breeding Program (KE) | KE-F6-2019-A-001 | KIO-2019-B-001 |
        And the experiment location details are displayed
            | Experiment Type    | Site   | Steward      |
            | Generation Nursery | Kiboko | Hudson, Phil |
        When the user loads the "plots" for seedlot creation
        And the user bulks update harvest data
        And the user specified the following harvest traits for all plots
            | Harvest Date | Harvest Method | Numeric Variables |
            | 2021-06-11   | Bulk           | NA                |
        Then a success message is shown
        And the harvest data for all plots is displayed in the plot data browser
            | Harvest Method |
            | Bulk           |

    #@hmtest
    Scenario: Perform Bulk Update for Selected Plots using "Individual Ear"
        And the search parameters are specified
            | Program                        | Experiment       | Location Code  |
            | KE Maize Breeding Program (KE) | KE-F6-2019-A-001 | KIO-2019-B-001 |
        And the experiment location details are displayed
            | Experiment Type    | Site   | Steward      |
            | Generation Nursery | Kiboko | Hudson, Phil |
        When the user loads the "plots" for seedlot creation
        And the user bulks update harvest data
        And the user specified the following harvest traits for all plots
            | Harvest Date | Harvest Method | Numeric Variables |
            | 2021-06-11   | Individual Ear | 10                |
        Then a success message is shown
        And the harvest data for all plots is displayed in the plot data browser
            | Harvest Method |
            | Individual Ear |