Feature: Design Creation for success cases
    The user should be able to assing an experimental design when creating an experiment
    Different options of experimental design must be available
    The user configures specific parameters for a selected design.


        Background: Login and go to Experiment
            Given user logs in to B4R
             When user goes to Experiments
              And user creates Experiments
              And user sets "Breeding Trial" in Experiment Type
              And user sets "UAT20.07_Design_#" in Experiment Name
              And user sets "UAT Design" in Experiment Objective
              And user sets "AYT" in Stage
              And user sets "CS" in Evaluation Season
              And user sets "A" in Planting Season
              And user sets "Allan, Jennifer" in Experiment Steward
              And user sets "IRSEA_PIPELINE" in Pipeline
              And user sets "IRSEA_PROJECT" in Project
              And user selects Save
              And user add from Saved List "ENTRYLISTA"
              And user selects "check" for rows "1,2"
              And user selects Next
              
        Scenario: 1- Randomize the experiment as Alpha-Lattice CIMMYT with define shape- Success
             When user sets "Alpha-Lattice | CIMMYT" as Design
              And user sets "1" as Number of Occurrences
              And user sets "2" as Number of Replicates
              And user sets "2" as Number of Plots Per Block
              And user sets "Yes" as Define Shape Field
              And user sets "4" as Rows
              And user sets "2" as Plots Until Turn The Serpentine
              And user selects Generate Design
             Then user sees the request successfully sent

        Scenario: 2- Randomize the experiment as Alpha-Lattice Wheat with define shape - Success
             When user sets "Alpha-Lattice | WHEAT" as Design
              And user sets "1" as Number of Occurrences
              And user sets "2" as Number of Replicates
              And user sets "2" as Number of Plots Per Block
              And user sets "Yes" as Define Shape Field
              And user sets "4" as Rows
              And user sets "2" as Plots Until Turn The Serpentine
              And user sets "No" as Randomize 1st Step
              And user selects Generate Design
             Then user sees the request successfully sent

        Scenario: 3- Randomize the experiment as Alpha-Lattice | CIMMYT with define shape - Success
             When user sets "Alpha-Lattice | WHEAT" as Design
              And user sets "1" as Number of Occurrences
              And user sets "2" as Number of Replicates
              And user sets "5" as Number of Plots Per Block
              And user sets "Yes" as Define Shape Field
              And user sets "5" as Rows
              And user sets "2" as Plots Until Turn The Serpentine
              And user selects Generate Design
             Then user sees the request successfully sent

        Scenario: 4- Randomize the experiment as RCBD | CIMMYT with define shape - Success
             When user sets "Alpha-Lattice | WHEAT" as Design
              And user sets "1" as Number of Occurrences
              And user sets "2" as Number of Replicates
              And user sets "2" as Number of Plots Per Block
              And user sets "Yes" as Define Shape Field
              And user sets "5" as Rows
              And user sets "2" as Plots Until Turn The Serpentine
              And user selects Generate Design
             Then user sees the request successfully sent

        Scenario: 5- Randomize the experiment as RCBD IRRI with define shape - Success
             When user sets "RCBD IRRI" as Design
              And user sets "1" as Number of Occurrences
              And user sets "2" as Number of Replicates
              And user sets "Yes" as Define Shape Field
              And user sets "5" as Rows
              And user sets "2" as Plots Until Turn The Serpentine
              And user selects Generate Design
             Then user sees the request successfully sent

        Scenario: 6 - Randomize the experiment as RCBD without shape- Success
             When user sets "RCBD" as Design
              And user sets "1" as Number of Occurrences
              And user sets "2" as Number of Replicates
              And user sets "No" as Define Shape Field
              And user selects Generate Design
             Then user sees the request successfully sent

        Scenario: 7 - Randomize the experiment as Row-Column Design with define shape - Success
             When user sets "Row-Column" as Design
              And user sets "1" as Number of Occurrences
              And user sets "2" as Number of Replicates
              And user sets "5" as Number of Row Blocks
              And user sets "Yes" as Define Shape Field
              And user sets "5" as Rows
              And user selects Generate Design
             Then user sees the request successfully sent

        
        Scenario: 8 - Randomize the experiment as Augmented RCBD with define shape - Success
             When user sets "Augmented RCB" as Design
              And user sets "1" as Number of Occurrences
              And user sets "2" as Number of Replicates
              And user sets "Yes" as Define Shape Field
              And user sets "5" as Rows
              And user sets "5" as Number of Rows Per Rep
              And user selects Generate Design
             Then user sees the request successfully sent




