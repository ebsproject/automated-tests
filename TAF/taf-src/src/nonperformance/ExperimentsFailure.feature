Feature: Experiments

        Background: Login and go to Experiment
            Given user logs in to B4R
              And user goes to Experiments
              And user edits experiment "Test_Trial_001"
              And user enters Design

        Scenario: 9- Randomize the experiment as Alpha-Lattice | CIMMYT – Error message: Prime number
             When user sets "Alpha-Lattice | CIMMYT" as Design
             Then users sees error message: "Entry list with prime number of entries. Balanced Alpha-Lattice is not an appropriated design in this case. Kindly update your entry list or design."
              And user sees no fields for parameters are be displayed
              And users sees the "Generate Design" is disabled

        Scenario: 10- Randomize the experiment as Alpha-Lattice | WHEAT – Error message: Prime number
             When user sets "Alpha-Lattice | WHEAT" as Design
             Then users sees error message: "Entry list with prime number of entries. Balanced Alpha-Lattice is not an appropriated design in this case. Kindly update your entry list or design."
              And user sees no fields for parameters are be displayed
              And users sees the "Generate Design" is disabled

        Scenario: 11 - Randomize the experiment as Alpha-Lattice – Error message: Prime number
             When user sets "Alpha-Lattice" as Design
             Then users sees error message: "Entry list with prime number of entries. Balanced Alpha-Lattice is not an appropriated design in this case. Kindly update your entry list or design."
              And user sees no fields for parameters are be displayed
              And users sees the "Generate Design" is disabled
             
        Scenario: 12 - Randomize the experiment as Row-Column – Error message: Prime number
             When user sets "Row-Column" as Design
             Then users sees error message: "Entry list with prime number of entries. Balanced Alpha-Lattice is not an appropriated design in this case. Kindly update your entry list or design."
              And user sees no fields for parameters are be displayed
              And users sees the "Generate Design" is disabled
             
           







