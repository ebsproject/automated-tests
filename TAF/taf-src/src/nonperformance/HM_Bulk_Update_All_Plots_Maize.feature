Feature: Bulk Update All Plots for Maize
    The user should be able to bulk update all plots for Maize harvest
    Different harvest methods should be used
    The user configures specific parameters for harvest data.


    Background: The plots are loaded
        Given the user is in Harvest Manager

    #@hmretest
    Scenario: Perform Bulk Update for All Plots using "Bulk"
        And the search parameters are specified
            | Program                        | Experiment        | Location Code  |
            | KE Maize Breeding Program (KE) | KE-HB-2021-DS-001 | NP-2021-DS-001 |
        And the experiment location details are displayed
            | Experiment Type              | Site  | Steward                  |
            | Intentional Crossing Nursery | Nepal | Go (IRRI), Lois Angelica |
        When the user loads the "plots" for seedlot creation
        And the user bulks update harvest data of all plots
        And the user specified the following harvest traits for all plots
            | Harvest Date | Harvest Method | Numeric Variables |
            | 2021-06-11   | Bulk           | NA                |
        Then a success message is shown
        And the harvest data for all plots is displayed in the plot data browser
            | Harvest Method |
            | Bulk           |

    #@hmretest
    Scenario: Perform Bulk Update for All Plots using "Individual Ear"
        And the search parameters are specified
            | Program                        | Experiment        | Location Code  |
            | KE Maize Breeding Program (KE) | KE-F3-2021-DS-001 | BD-2021-DS-001 |
        And the experiment location details are displayed
            | Experiment Type    | Site       | Steward                  |
            | Generation Nursery | Bangladesh | Go (IRRI), Lois Angelica |
        When the user loads the "plots" for seedlot creation
        And the user bulks update harvest data of all plots
        And the user specified the following harvest traits for all plots
            | Harvest Date | Harvest Method | Numeric Variables |
            | 2021-06-11   | Individual ear | 10                |
        Then a success message is shown
        And the harvest data for all plots is displayed in the plot data browser
            | Harvest Method |
            | Individual ear |
