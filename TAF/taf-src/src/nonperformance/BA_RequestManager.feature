    Background: User lands in ARM first page
        Given user access "dev.ebsproject.org"
        And inputs its credentials
        And user selects "Breeding Analytics" product
        And user selects "Analysis Request Manager" at left panel
        And user selects "ADD NEW ANALYSIS REQUEST" button

    Scenario Outline: Successfully creating a few Analysis requests for AF2021_001 experiment
        When user searches experiment <expt> in Search field of Experiment column
        And user clicks the radio button of the experiment <expt>
        And user selects "NEXT" button
        And user sets <traits> as "Response Variable" field
        And user selects "NEXT" button
        And user sets <TAP> as "Trait Analysis Pattern"
        And user sets <ELAP> as "Experiment Location Analysis pattern"
        And user sets <AnalysisConfig> as "Analysis Configuration"
        And user sets <MainModel> as "Main Model"
        And user sets <SpatialAdjusting> as "SpatialAdjusting"
        And user selects "SUBMIT ANALYSIS REQUEST" button
        Then user lands on "job list page" with the new request as "Pending" status

        Examples:
            | expt       | traits    | TAP        | ELAP                                | AnalysisConfig  | MainModel                            | SpatialAdjusting      |
            | AF2021_001 | AYLD_CONT | Univariate | Single Experiment - Single Location | RCBD Univariate | RCBD SESL Univariate entry as random | (AR1row x AR1col)     |
            | AF2021_001 | AYLD_CONT | Univariate | Single Experiment - Single Location | RCBD Univariate | RCBD SESL Univariate entry as random | (IDrow x AR1col)      |
            | AF2021_001 | AYLD_CONT | Univariate | Single Experiment - Single Location | RCBD Univariate | RCBD SESL Univariate entry as random | (AR1row x IDcol)      |
            | AF2021_001 | AYLD_CONT | Univariate | Single Experiment - Single Location | RCBD Univariate | RCBD SESL Univariate entry as random | No spatial adjustment |