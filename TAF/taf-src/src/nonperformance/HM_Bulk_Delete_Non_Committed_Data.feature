Feature: Display committed data
  The user should be able to update harvest data traits
  The user should be able to bulk delete non-committed harvest data
  The browser should be emptied after deleting the data

  Background:
    Given the user is in Harvest Manager

  #@retest
  Scenario: Bulk Delete Non-committed Harvest Data for Selected Plots
    And the search parameters are specified
      | Program                        | Experiment           | Location Code   |
      | BW Wheat Breeding Program (BW) | BW-Parallel-Test-001 | AFG-2021-CS-001 |
    And the experiment location details are displayed
      | Experiment Type    | Site        | Steward                  |
      | Generation Nursery | Afghanistan | Go (IRRI), Lois Angelica |
    When the user loads the "plots" for seedlot creation
    And the user bulks update harvest data
    And the user specified the following harvest traits
      | Harvest Date | Harvest Method | Numeric Variables |
      | 2021-06-11   | Bulk           | NA                |
    And the user bulk deletes the non-committed harvest data for selected plots
    Then the "non-committed" harvest data are deleted

  #@retest
  Scenario: Bulk Delete Non-committed Harvest Data for All Plots
    And the search parameters are specified
      | Program                        | Experiment           | Location Code   |
      | BW Wheat Breeding Program (BW) | BW-Parallel-Test-003 | AFG-2021-DS-001 |
    And the experiment location details are displayed
      | Experiment Type              | Site        | Steward                  |
      | Intentional Crossing Nursery | Afghanistan | Go (IRRI), Lois Angelica |
    When the user loads the "plots" for seedlot creation
    And the user bulks update harvest data
    And the user specified the following harvest traits
      | Harvest Date | Harvest Method | Numeric Variables |
      | 2021-06-11   | Bulk           | NA                |
    And the user bulk deletes the non-committed harvest data for all plots
    Then the "non-committed" harvest data are deleted

  #start of parallel run - SELECTED PLOTS
  #@parallel_run_hm
  Scenario: BW-001-351 plots
    And the search parameters are specified
      | Program                        | Experiment           | Location Code   |
      | BW Wheat Breeding Program (BW) | BW-Parallel-Test-003 | AFG-2021-DS-001 |
    And the experiment location details are displayed
      | Experiment Type              | Site        | Steward                  |
      | Intentional Crossing Nursery | Afghanistan | Go (IRRI), Lois Angelica |
    When the user loads the "plots" for seedlot creation
    And the user bulks update harvest data
    And the user specified the following harvest traits
      | Harvest Date | Harvest Method | Numeric Variables |
      | 2021-06-11   | Bulk           | NA                |
    And the user bulk deletes the non-committed harvest data for selected plots
    Then the "non-committed" harvest data are deleted

  #@parallel_run_hm
  Scenario: BW-002-501 plots
    And the search parameters are specified
      | Program                        | Experiment           | Location Code     |
      | BW Wheat Breeding Program (BW) | BW-Parallel-Test-004 | MM_NY-2021-CS-001 |
    And the experiment location details are displayed
      | Experiment Type    | Site                      | Steward                  |
      | Generation Nursery | Yezin, Naypyitaw, Myanmar | Go (IRRI), Lois Angelica |
    When the user loads the "plots" for seedlot creation
    And the user bulks update harvest data
    And the user specified the following harvest traits
      | Harvest Date | Harvest Method | Numeric Variables |
      | 2021-06-11   | Bulk           | NA                |
    And the user bulk deletes the non-committed harvest data for selected plots
    Then the "non-committed" harvest data are deleted

  #@parallel_run_hm
  Scenario: BW-003-501 plots
    And the search parameters are specified
      | Program                        | Experiment           | Location Code   |
      | BW Wheat Breeding Program (BW) | BW-Parallel-Test-005 | AFG-2021-DS-002 |
    And the experiment location details are displayed
      | Experiment Type      | Site        | Steward                  |
      | Cross Parent Nursery | Afghanistan | Go (IRRI), Lois Angelica |
    When the user loads the "plots" for seedlot creation
    And the user bulks update harvest data
    And the user specified the following harvest traits
      | Harvest Date | Harvest Method | Numeric Variables |
      | 2021-06-11   | Bulk           | NA                |
    And the user bulk deletes the non-committed harvest data for selected plots
    Then the "non-committed" harvest data are deleted

  #@parallel_run_hm
  Scenario: BW-004-351 plots
    And the search parameters are specified
      | Program                        | Experiment           | Location Code   |
      | BW Wheat Breeding Program (BW) | BW-Parallel-Test-001 | AFG-2021-CS-001 |
    And the experiment location details are displayed
      | Experiment Type    | Site        | Steward                  |
      | Generation Nursery | Afghanistan | Go (IRRI), Lois Angelica |
    When the user loads the "plots" for seedlot creation
    And the user bulks update harvest data
    And the user specified the following harvest traits
      | Harvest Date | Harvest Method | Numeric Variables |
      | 2021-06-11   | Bulk           | NA                |
    And the user bulk deletes the non-committed harvest data for selected plots
    Then the "non-committed" harvest data are deleted

  #start of parallel run - ALL PLOTS
  #@parallel_run_hm
  Scenario: BW-001-351 plots
    And the search parameters are specified
      | Program                        | Experiment           | Location Code   |
      | BW Wheat Breeding Program (BW) | BW-Parallel-Test-003 | AFG-2021-DS-001 |
    And the experiment location details are displayed
      | Experiment Type              | Site        | Steward                  |
      | Intentional Crossing Nursery | Afghanistan | Go (IRRI), Lois Angelica |
    When the user loads the "plots" for seedlot creation
    And the user bulks update harvest data
    And the user specified the following harvest traits
      | Harvest Date | Harvest Method | Numeric Variables |
      | 2021-06-11   | Bulk           | NA                |
    And the user bulk deletes the non-committed harvest data for all plots
    Then the "non-committed" harvest data are deleted

  #@parallel_run_hm
  Scenario: BW-002-501 plots
    And the search parameters are specified
      | Program                        | Experiment           | Location Code     |
      | BW Wheat Breeding Program (BW) | BW-Parallel-Test-004 | MM_NY-2021-CS-001 |
    And the experiment location details are displayed
      | Experiment Type    | Site                      | Steward                  |
      | Generation Nursery | Yezin, Naypyitaw, Myanmar | Go (IRRI), Lois Angelica |
    When the user loads the "plots" for seedlot creation
    And the user bulks update harvest data
    And the user specified the following harvest traits
      | Harvest Date | Harvest Method | Numeric Variables |
      | 2021-06-11   | Bulk           | NA                |
    And the user bulk deletes the non-committed harvest data for all plots
    Then the "non-committed" harvest data are deleted

  #@parallel_run_hm
  Scenario: BW-003-501 plots
    And the search parameters are specified
      | Program                        | Experiment           | Location Code   |
      | BW Wheat Breeding Program (BW) | BW-Parallel-Test-005 | AFG-2021-DS-002 |
    And the experiment location details are displayed
      | Experiment Type      | Site        | Steward                  |
      | Cross Parent Nursery | Afghanistan | Go (IRRI), Lois Angelica |
    When the user loads the "plots" for seedlot creation
    And the user bulks update harvest data
    And the user specified the following harvest traits
      | Harvest Date | Harvest Method | Numeric Variables |
      | 2021-06-11   | Bulk           | NA                |
    And the user bulk deletes the non-committed harvest data for all plots
    Then the "non-committed" harvest data are deleted


  #@parallel_run_hm
  Scenario: BW-004-351 plots
    And the search parameters are specified
      | Program                        | Experiment           | Location Code   |
      | BW Wheat Breeding Program (BW) | BW-Parallel-Test-001 | AFG-2021-CS-001 |
    And the experiment location details are displayed
      | Experiment Type    | Site        | Steward                  |
      | Generation Nursery | Afghanistan | Go (IRRI), Lois Angelica |
    When the user loads the "plots" for seedlot creation
    And the user bulks update harvest data
    And the user specified the following harvest traits
      | Harvest Date | Harvest Method | Numeric Variables |
      | 2021-06-11   | Bulk           | NA                |
    And the user bulk deletes the non-committed harvest data for all plots
    Then the "non-committed" harvest data are deleted



