Feature: Core system - User management - Add institution contact
    As an Administrator, I should be able to register an institution contact and add general information of an institution.

    Background: Register an Oganization
        Given the user is in User management
        And the user goes to create an institution contact feature
        Then the user saves the institution contact
            | name | legalName                           | website      | phone        | contact  | principalContact  | address                        |
            | IRRI | Internation Rice Research Institute | www.irri.org | +63285805600 | contact1 | principalContact1 | Los Banos, Laguna, Philippines |