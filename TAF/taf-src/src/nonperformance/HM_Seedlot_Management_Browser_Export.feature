Feature: Add export in the seedlot manager browser
    The user should be able to see the export button in the seedlot management browser
    The user should be able to export records in the seedlot management browser
    The selected data format should be exported/downloaded

    Background:
      Given the user is in Harvest Manager

    @hmsmoke
    Scenario: User is able to export HTML file from seedlot manager browser
      And the plots for "Wheat Single Plant Selection" seedlot creation are loaded
      And "Single Plant Selection" seeds, packages, and germplasm are created
      When the user exports data in "HTML" format
      Then the "HTML" file is exported

    @hmsmoke
    Scenario: User is able to export CSV file from seedlot manager browser
      And the plots for "Wheat Selected Bulk" seedlot creation are loaded
      And "Selected Bulk" seeds, packages, and germplasm are created
      When the user exports data in "CSV" format
      Then the "CSV" file is exported

    @hmsmoke
    Scenario: User is able to export Text file from seedlot manager browser
      And the plots for "Wheat Selected Bulk" seedlot creation are loaded
      And "Selected Bulk" seeds, packages, and germplasm are created
      When the user exports data in "Text" format
      Then the "Text" file is exported

    @hmsmoke
    Scenario: User is able to export Excel 95 file from seedlot manager browser
      And the plots for "Wheat Bulk" seedlot creation are loaded
      And "Bulk" seeds, packages, and germplasm are created
      When the user exports data in "Excel 95" format
      Then the "Excel 95" file is exported

    @hmsmoke
    Scenario: User is able to export Excel 2007 file from seedlot manager browser
      And the plots for "Wheat Individual Spike" seedlot creation are loaded
      And "Individual Spike" seeds, packages, and germplasm are created
      When the user exports data in "Excel 2007" format
      Then the "Excel 2007" file is exported

   