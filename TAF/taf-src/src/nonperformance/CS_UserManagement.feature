Feature: Core system - User management - Add person contact
    As an Administrator, I should be able to register a contact and add general information of an person.

        Background: Register a contact
            Given the user is in User management
              And the user goes to create contact feature
              Then the user Saves the Contact information
                | familyName | givenName | addName | email | phone | contact | country | locality | region | postalCode | street | flag |
                | Doe | John | Jr. | john.doe@irri.org | 09099099999 | Type 1 | Philippines | Laguna | CALABARZON | 4030 | Los Banos | flag1 |
                | Doe | Jane |  | jane.doe@irri.org | 09099098888 | Type 2 | Philippines | Laguna | CALABARZON | 4030 | Los Banos | flag2 |
