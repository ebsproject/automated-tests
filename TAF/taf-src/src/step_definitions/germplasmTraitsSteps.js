const { client } = require("nightwatch-api");
const { Given, Then, When } = require("cucumber");
const b4rPage = client.page.b4rPage();
const germplasmTraitsPage = client.page.germplasmTraitsPage();
const isPresent = client.page.isPresent();
const b4rHelpers = require("./b4rHelpers.js");
const fs = require("fs");
const { fileExists } = require("nightwatch/lib/utils");
const randGen = require("crypto");
const qc_germplasmList = "_SAVEDGERMPLASMLIST_" + Math.floor(Math.random() * 10000);

When(/^the user is in "([^"]*)" Search entrypoint$/, async (args1) => {
    const linkText = {
        selector: `//a[text()='${args1}']`,
        locateStrategy: "xpath",
    };
    //login non-docker
    //await b4rHelpers.login(client.globals.userName, client.globals.userPassword, client.globals.loginMode);
    //login docker
    await b4rHelpers.login(process.env.CMD_USER, process.env.CMD_PASS, process.env.CMD_MODE);
    await germplasmTraitsPage.waitForElementNotPresent("div.overlay", 5000, false);

    await checkLeftNav(); //accessSideNavigation();

    await germplasmTraitsPage.waitForElementVisible(linkText);
    await germplasmTraitsPage.click(linkText);

    //wait for the entrypoint of the tool
    if (args1 == "Seed search") {
        await germplasmTraitsPage.waitForElementPresent("@pageLabelSeeds");
    } else if (args1 == "Germplasm") {
        await germplasmTraitsPage.waitForElementPresent("@pageLabelGermplasm");
    } else if (args1 == "Traits") {
        await germplasmTraitsPage.waitForElementPresent("@pageLabelTraits");
    } else {
        console.log("Search argument ", +args1 + " not available");
    }
});


//Opens the data filters and set filters
//Single row only
When(/^the user sets Data filters parameters for Germplasm Search$/, async (table) => {
    await germplasmTraitsPage.click("@dataFiltersMenu");
    //give time to load the program value
    await germplasmTraitsPage.pause(1000);

    for (column of table.raw()[0]) {
        switch (column) {
            case "Program":
                await germplasmTraitsPage.searchSelectSetValue(
                    germplasmTraitsPage.section.dataFilters.elements["programSearchSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Year":
                await germplasmTraitsPage.listBoxSetValue(
                    germplasmTraitsPage.section.dataFilters.elements["yearSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Season":
                await germplasmTraitsPage.listBoxSetValue(
                    germplasmTraitsPage.section.dataFilters.elements["seasonSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Stage":
                await germplasmTraitsPage.listBoxSetValue(
                    germplasmTraitsPage.section.dataFilters.elements["stageSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Site":
                await germplasmTraitsPage.listBoxSetValue(
                    germplasmTraitsPage.section.dataFilters.elements["siteSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Experiments":
                switch (table.hashes()[0][column]) {
                    case "All":
                        await germplasmTraitsPage.sliderSetValue(
                            germplasmTraitsPage.section.dataFilters.elements["experimentsSlider"],
                            "Left"
                        );
                        break;
                    case "My":
                        await germplasmTraitsPage.sliderSetValue(
                            germplasmTraitsPage.section.dataFilters.elements["experimentsSlider"],
                            "Right"
                        );
                        break;
                    default:
                        throw new Error(`Unexpected Experiment option.`);
                }
                break;
            default:
                throw new Error(`Unexpected ${column} name.`);
        }
    };
    await germplasmTraitsPage.section.dataFilters.click("@applyButton");
    await germplasmTraitsPage.section.dataFilters.assert.not.visible("@applyButton");
});

//Set the germplasm search parameters
//Single line table only
When(/^user perform generic germplasm type search$/, async (table) => {
    //reset/clear any set search parameters first
    //await resetSearchQueryParameters("Germplasm");

    for (column of table.raw()[0]) {
        switch (column) {
            case "Germplasm Type":
                await germplasmTraitsPage.listBoxSetValue(
                    germplasmTraitsPage.section.queryParam.elements["germplasmTypeSearch"],
                    table.hashes()[0][column]
                );
                break;
            case "Additional Search Parameters":
                //todo
                break;
            default:
                throw new Error(`Unexpected ${column} name.`);
        }
    }
    await germplasmTraitsPage.click("@queryFindButton");
});

When(/^user can see Germplasm browser grid ready$/, async () => {
    //wait for the browser to load fully
    await germplasmTraitsPage.waitGridReady(germplasmTraitsPage.section.searchGrid);
    await germplasmTraitsPage.pause(2000);
});

When(
    /^user perform generic germplasm name search with input "([^"]*)"$/,
    async (namesearch) => {
        //let wildcardName = 'IR%';

        await germplasmTraitsPage.assert.visible("@queryName");
        await germplasmTraitsPage.setValue("@queryName", namesearch);
        germplasmTraitsPage.pause(1000);

        await germplasmTraitsPage.click("@queryFindButton");
        await germplasmTraitsPage.waitForElementVisible("@queryResultsSummary");
    }
);

Then(/^user selects a set of checkboxes in the current page of Germplasm Search browser$/, async () => {
    //assert that results are fully loaded with page 1 already active
    await germplasmTraitsPage.assert.elementPresent("@queryResultsSummary");
    await germplasmTraitsPage.waitForElementPresent("@browserPageOne");

    await germplasmTraitsPage.click("@selectAllResults");
    await checkElementPresent("#germplasm-select-all-id", "checked");
    //assert that there is a count on selected items
    await germplasmTraitsPage.assert.elementPresent("@selectedItemCount");
    console.log("@selectedItemCount");
});

Then(/^user navigates to "([^"]*)" page of Germplasm Search browser$/, async (pagenum) => {
    await goToBrowserPage(pagenum);
    await germplasmTraitsPage.waitGridReady(germplasmTraitsPage.section.searchGrid);
});

Then(/^user goes back to the "([^"]*)" page of Germplasm Search browser$/, async (pagenum) => {
    await goToBrowserPage(pagenum);
    await germplasmTraitsPage.waitGridReady(germplasmTraitsPage.section.searchGrid);
});

Then(/^checkbox selection should be retained in Germplasm Search browser$/, async () => {
    await germplasmTraitsPage.waitForElementPresent("@browserPageOne");
    await checkElementPresent(
        "#germplasm-select-all-id",
        "checked",
        "checked"
    );
    await germplasmTraitsPage.assert.elementPresent("@selectedItemCount");
    germplasmTraitsPage.pause(2000);
    console.log("Selection has been retained for page: ", "@browserPageOne");
});
When(/^user set column filters of the search results in Germplasm browser$/, async (table) => {
    for (column of table.raw()[0]) {
        switch (column) {
            case "STATUS":
                break;
            case "GERMPLASM":
                await germplasmTraitsPage.section.germBrowserColFilters.setValue(
                    "@germColFilter",
                    table.hashes()[0][column]
                );
                break;
            case "OTHER NAMES":
                await germplasmTraitsPage.section.germBrowserColFilters.setValue(
                    "@otherNamesColFilter",
                    table.hashes()[0][column]
                );
                break;
            case "PARENTAGE":
                await germplasmTraitsPage.section.germBrowserColFilters.setValue(
                    "@parentageColFilter",
                    table.hashes()[0][column]
                );
                break;
            case "GENERATION":
                await germplasmTraitsPage.section.germBrowserColFilters.setValue(
                    "@generationColFilter",
                    table.hashes()[0][column]
                );
                break;
            case "GERMPLASM NAME TYPE":
                await germplasmTraitsPage.section.germBrowserColFilters.setValue(
                    "@germNameTypeColFilter",
                    table.hashes()[0][column]
                );
                break;
            case "GERMPLASM TYPE":
                await germplasmTraitsPage.section.germBrowserColFilters.setValue(
                    "@germTypeColFilter",
                    table.hashes()[0][column]
                );
                break;
            case "GERMPLASM STATE":
                await germplasmTraitsPage.section.germBrowserColFilters.setValue(
                    "@germStateColFilter",
                    table.hashes()[0][column]
                );
                break;
            default:
                throw new Error(`Unexpected ${column} name.`);
        }
    }
    //wait for the browser to load fully
    await germplasmTraitsPage.waitGridReady(germplasmTraitsPage.section.searchGrid);
    await germplasmTraitsPage.pause(2000);
});
Then(/^results show all matching germplasm$/, async () => {
    await germplasmTraitsPage.pause(2000);
    await germplasmTraitsPage.waitGridReady(germplasmTraitsPage.section.searchGrid);
});

When(/^the user selects "([^"]*)" input list in additional parameters$/, async (args1) => {
    await germplasmTraitsPage.waitForElementVisible("@additionalQueryParam");
    await germplasmTraitsPage.assert.visible("@additionalQueryParam");
    await germplasmTraitsPage.click("@additionalQueryParam");
    await germplasmTraitsPage.assert.visible("@inputListButton");
    await germplasmTraitsPage.click("@inputListButton");
    await germplasmTraitsPage.assert.visible(germplasmTraitsPage.section.inputListSelection);
    await germplasmTraitsPage.click(germplasmTraitsPage.section.inputListSelection);

    if (args1 == "Designation") {
        await germplasmTraitsPage.assert.visible(germplasmTraitsPage.section.inputListSelection.elements['designationSelect']);
        await germplasmTraitsPage.click(germplasmTraitsPage.section.inputListSelection.elements['designationSelect']);
    }
    if (args1 == "Germplasm Code") {
        await germplasmTraitsPage.assert.visible(germplasmTraitsPage.section.inputListSelection.elements['germplasmCodeSelect']);
        await germplasmTraitsPage.click(germplasmTraitsPage.section.inputListSelection.elements['germplasmCodeSelect']);
    }
});

// When(/^the user inputs list from "([^"]*)" file$/, async (args1) => {
//     var fs = require("fs");
//     var file = process.cwd() + "/src/files/" + args1;
//     if (fs.existsSync(file)) {
//         file = fs.readFileSync(file);
//     }
//     else {
//         throw new Error("File not found.");
//     }
//     this.parameters[args2] = file;
// });

When(/^user loads input list data from "([^"]*)" file to "([^"]*)"$/, async function (args1, args2) {
    var fs = require("fs");
    var file = process.cwd() + "/src/files/" + args1;

    if (fs.existsSync(file)) {
        file = fs.readFileSync(file);
    }
    else {
        throw new Error("File not found.");
    }
    this.parameters[args2] = file;
}
);

When(/^user sets "([^"]*)" contents to the input field$/, async function (args1) {
    console.log("Variable here" + this.parameters[args1]);
    var inputValue = this.parameters[args1]
    await germplasmTraitsPage.assert.visible("@inputTextField");
    await germplasmTraitsPage.setValue("@inputTextField", inputValue);


    await germplasmTraitsPage.assert.visible("@confirmInputList");
    await germplasmTraitsPage.click("@confirmInputList");
    //setvlue for input list 
    //click copnfirm
});


When(/^the user clicks find germplasm$/, async () => {
    await germplasmTraitsPage.assert.visible("@findGerm");
    await germplasmTraitsPage.click("@findGerm");
});

Then(/^the germplasm in the input list is loaded$/, async () => {
    await germplasmTraitsPage.pause(2000);
    await germplasmTraitsPage.waitGridReady(germplasmTraitsPage.section.searchGrid);
});


async function checkElementPresent(control, attribute) {
    console.log("Checking attribute = " + attribute + " of control = " + control);
    await germplasmTraitsPage.getAttribute(
        "css selector",
        control,
        attribute,
        function (checkAll) {
            console.log("checked status", checkAll);
            //assert that check all is in 'checked' state, otherwise it will have a null value
            germplasmTraitsPage.assert.equal(checkAll.value, "true");
        }
    );
}

async function goToBrowserPage(pagenum) {
    const pagelink = {
        selector: `//a[text()[contains(.,'${pagenum}')]]`,
        locateStrategy: "xpath",
    };
    await germplasmTraitsPage.waitForElementPresent(pagelink);
    await germplasmTraitsPage.assert.visible(pagelink);
    await germplasmTraitsPage.click(pagelink);

    //after pagelink is clicked, browser is reloaded, assert again that the pagelink is visible
    await germplasmTraitsPage.waitForElementVisible(pagelink);
}

//check if left nav is showing
async function checkLeftNav() {
    await b4rPage.waitForElementNotPresent("div.overlay", 5000, false);

    client.isVisible("#go-to-show-left-nav", (res) => {
        console.log(res.value);
        if (res.value === false) {
            console.log("left nav showing");
        } else {
            console.log("left nav NOT showing");
            b4rPage.waitForElementVisible("@showLeftNav");
            b4rPage.pause(2000);
            b4rPage.click("@showLeftNav");
        }
    });
}

When(/^the user save the germplasm as new list$/, async (table) => {
    germplasmTraitsPage.assert.visible(germplasmTraitsPage.section.searchGrid.elements["addToGermplasmListButton"]);
    germplasmTraitsPage.click(germplasmTraitsPage.section.searchGrid.elements["addToGermplasmListButton"]);

    await germplasmTraitsPage.section.createGermplasmListModal.waitForElementVisible("@nameField");
    for (column of table.raw()[0]) {
        switch (column) {
            case "Name":
                await germplasmTraitsPage.section.createGermplasmListModal.setValue(
                    "@nameField",
                    table.hashes()[0][column].replace("*", qc_germplasmList)
                );
                break;
            case "Description":
                await germplasmTraitsPage.section.createGermplasmListModal.setValue(
                    "@descriptionField",
                    table.hashes()[0][column]
                );
                break;
            case "Remarks":
                await germplasmTraitsPage.section.createGermplasmListModal.setValue(
                    "@remarksField",
                    table.hashes()[0][column]
                );
                break;
            default:
                throw new Error(`Unexpected ${args1} column name.`);
        };
    }
    await germplasmTraitsPage.section.createGermplasmListModal.waitForElementVisible("@createButton");
    await germplasmTraitsPage.section.createGermplasmListModal.click("@createButton");
    await germplasmTraitsPage.waitForElementNotPresent("@saveGermplasmListNotif");
});

When(/^user navigates to "([^"]*)" from Germplasm Search$/, async (args1) => {
    //check if left navigation is open
    await germplasmTraitsPage.waitForElementVisible(
        "#slide-out[style='transform: translateX(0px);']",
        1000,
        1000,
        false,
        (result) => {
            if (!result.value) {
                //click show left nav
                germplasmTraitsPage.click("@showLeftNav");
            }
        }
    );
    switch (args1) {
        case "Experiment creation":
            break;
        case "Experiment manager":
            break;
        case "List manager":
            germplasmTraitsPage.section.leftNavigation.click("@listManagerLink");
            break;
        default:
            throw new Error(`Unexpected ${args1} navigation menu name.`);
    }
});

//Filter Lists browser by recently created saved list
async function filterListsBrowserbyColumn(colname) {
    await germplasmTraitsPage.section.listsGrid.waitForElementVisible(
        "@personalizeGridSettingsButton"
    );
    switch (colname) {
        case "Name":
            await germplasmTraitsPage.section.listsGrid.setValue("@nameField", "%" + qc_germplasmList);
            await germplasmTraitsPage.section.listsGrid.sendKeys("@nameField", client.Keys.TAB);
            break;
        default:
            throw new Error(`Unexpected ${args2} column name.`);
    }
}

Then(/^user can search the saved germplasm list and can see "([^"]*)" match in Lists browser$/, async (args1) => {
    filterListsBrowserbyColumn(args1);
    await germplasmTraitsPage.section.listsGrid.waitForElementVisible("@summaryTable");
    await germplasmTraitsPage.section.listsGrid.assert.not.elementPresent("@listBrowserEmpty"); //List browser, after filtering the desired column, should have returned results
    await germplasmTraitsPage.section.listsGrid.assert.containsText(
        "@summaryTable",
        "AUTOMATED" + qc_germplasmList
    );
});