const { client } = require("nightwatch-api");
const b4rPage = client.page.b4rPage();
let loginOption = "@googleLogin"; //default option when running tests in local

module.exports = {
    login: async function (user, password, mode) {
        await client.url(client.launch_url);
        await b4rPage.click("@csLoginButton");
        console.log("Login via: ", mode);

        //Start adding here for WS02 "Your connection is not private" 
        /*
          let notsecureconnection;
          await b4rPage.waitForElementVisible("#details-button", ({ value }) => {
            notsecureconnection = value;
            console.log('notsecureconnection: ' + notsecureconnection);
            return;
          });
      
              
          if (notsecureconnection) {
            await b4rPage.click("#details-button");
            b4rPage.pause(1000);
            await b4rPage.click("#proceed-link");
            b4rPage.pause(1000);
          }
        */
        //End here for WS02 "Your connection is not private" 

        /* NOTE: uncomment after successful login
            let visible;
            /*
            await b4rPage.waitForElementVisible("#icon-3", ({ value }) => {
              visible = value;
              return;
            });
            
        
            if (visible) {
              await b4rPage.click("#icon-3");
            } else {
              await b4rPage.waitForElementVisible("@googleButton", 5000);
              await b4rPage.click("@googleButton");
            }
        */
        //update loginOption if other than "Google"
        if (mode == "MICROSOFT") {
            loginOption = "@microsoftLogin";
        }
        console.log("Click sign in with", loginOption);
        await b4rPage.waitForElementVisible(loginOption, ({ value }) => {
            visible = value;
            return;
        });
        if (visible) {
            await b4rPage.assert.visible(loginOption);
            await b4rPage.click(loginOption);
        }
        //headless mode
        let headless = false;
        await b4rPage.waitForElementVisible(
            "@loginEmail",
            false,
            ({ value }) => {
                headless = value;
                //console.log("Running in headless = ", headless);
                return;
            }
        );

        if (mode == "MICROSOFT") {
            await b4rPage.setValue("@loginEmail", user);
            await b4rPage.click("@loginSubmitButton");
            await b4rPage.waitForElementVisible("@loginPassword");
            await b4rPage.assert.visible("@loginPassword");
            await b4rPage.setValue("@loginPassword", password);
            await b4rPage.click("@loginSubmitButton");
            b4rPage.pause(2000);

            client.isVisible("#idBtn_Back", (res) => {
                console.log(res.value);
                if (res.value === false) {
                    console.log("Redirecting to Dashboard page");
                } else {
                    console.log("Opt out for Stay Signed-in");
                    b4rPage.click("@staySignedinButton");
                }
            });
        } else if (mode == "GOOGLE") {
            await b4rPage.setValue("@loginEmail", user);

            //check if #identifierNext is visible to proceed with non-headless GOOGLE login
            await b4rPage.isVisible('#identifierNext', function (result) {
                console.log("Non-headless = ", result.value);

                if (result.value === true) {
                    console.log("Running in browser, use Google next buttons");
                    b4rPage.click("@googleUsernameNextButton");
                    b4rPage.waitForElementVisible("@loginPassword");
                    b4rPage.assert.visible("@loginPassword");
                    b4rPage.setValue("@loginPassword", password);
                    b4rPage.click("@googlePasswordNextButton");
                }
            });

            //otherwise check if xpath=//*[@type='submit'] is visible to proceed with headless GOOGLE login
            await b4rPage.isVisible('@loginSubmitButton', function (result) {
                console.log("Headless = ", result.value);

                if (result.value === true) {
                    console.log("Use @type='submit'");
                    b4rPage.click("@loginSubmitButton");
                    b4rPage.waitForElementVisible("@loginPassword");
                    b4rPage.assert.visible("@loginPassword");
                    b4rPage.setValue("@loginPassword", password);
                    b4rPage.click("@loginSubmitButton");
                }
            });
        }

        //Start adding here for WS02 USER CONSENTS and OPENID USER CLAIMS
        /*
        b4rPage.pause(2000);

        let userconsents;
        await b4rPage.waitForElementVisible("#consent_select_all", ({ value }) => {
          userconsents = value;
          console.log('userconsents: ' + userconsents);
          return;
        });
    
        if (userconsents) {
          console.log("Checking for USER CONSENTS page for first time run");
          b4rPage.waitForElementVisible("#consent_select_all");
          b4rPage.click("#consent_select_all");
          b4rPage.pause(1000);
          b4rPage.waitForElementVisible("#approve");
          b4rPage.click("#approve");
          b4rPage.pause(1000);
        } else {
          console.log('Checking for USER CONSENTS: '+ userconsents); 
          return; 
        }

        let openiduserclaims;
        await b4rPage.waitForElementVisible("#approveAlwaysCb", ({ value }) => {
          openiduserclaims = value;
          console.log('openiduserclaims: ' + openiduserclaims);
          return;
        });
    
        if (openiduserclaims) {
          console.log("Checking for OPENID USER CLAIMS first time run");
          b4rPage.waitForElementVisible("#approveAlwaysCb");
          b4rPage.click("#approveAlwaysCb");
          b4rPage.pause(1000);
          b4rPage.waitForElementVisible("#approve");
          b4rPage.click("#approve");
          b4rPage.pause(1000);
        } else {
          console.log('Checking for OPENID USER CLAIMS: '+ openiduserclaims); 
          return; 
        }
        *///End here for WS02 USER CONSENTS and OPENID USER CLAIMS
    },

    capitalize: function (word) {
        return word
            .toLowerCase()
            .split(" ")
            .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
            .join(" ");
    },
};
