const { client } = require("nightwatch-api");
const { Given, Then, When } = require("cucumber");
const b4rPage = client.page.b4rPage();
const dashboardPage = client.page.dashboardPage();
const isPresent = client.page.isPresent();
const b4rHelpers = require("./b4rHelpers.js");
const fs = require("fs");
const randGen = require("crypto");

Given(/^user logs in to EBS system$/, async () => {
    //login non-docker
    //await b4rHelpers.login(client.globals.userName, client.globals.userPassword, client.globals.loginMode);
    //login docker
    await b4rHelpers.login(process.env.CMD_USER, process.env.CMD_PASS, process.env.CMD_MODE);
    await dashboardPage.waitForElementNotPresent("div.overlay", 5000, false);
});

//Opens the data filters and set filters
//Single row only
When(/^the user sets Data filters parameters in Dashboard$/, async (table) => {
    await dashboardPage.click("@dataFiltersMenu");
    //give time to load the program value
    await dashboardPage.pause(1000);

    for (column of table.raw()[0]) {
        switch (column) {
            case "Program":
                await dashboardPage.searchSelectSetValue(
                    dashboardPage.section.dataFilters.elements["programSearchSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Year":
                await dashboardPage.listBoxSetValue(
                    dashboardPage.section.dataFilters.elements["yearSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Season":
                await dashboardPage.listBoxSetValue(
                    dashboardPage.section.dataFilters.elements["seasonSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Stage":
                await dashboardPage.listBoxSetValue(
                    dashboardPage.section.dataFilters.elements["stageSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Site":
                await dashboardPage.listBoxSetValue(
                    dashboardPage.section.dataFilters.elements["siteSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Experiments":
                switch (table.hashes()[0][column]) {
                    case "All":
                        await dashboardPage.sliderSetValue(
                            dashboardPage.section.dataFilters.elements["experimentsSlider"],
                            "Left"
                        );
                        break;
                    case "My":
                        await dashboardPage.sliderSetValue(
                            dashboardPage.section.dataFilters.elements["experimentsSlider"],
                            "Right"
                        );
                        break;
                    default:
                        throw new Error(`Unexpected Experiment option.`);
                }
                break;
            default:
                throw new Error(`Unexpected ${column} name.`);
        }
    };
    await dashboardPage.section.dataFilters.click("@applyButton");
    await dashboardPage.section.dataFilters.assert.not.visible("@applyButton");
});


Then(/^current filter menu in Dashboard is updated$/, async (table) => {

    for (column of table.raw()[0]) {
        switch (column) {
            case "Program":
                dashboardPage.getProgramAbbrev(
                    table.hashes()[0][column],
                    function (programAbbrev) {
                        console.log("callback returned abbrev: ", programAbbrev); // this is where you get the return value
                        //dashboardPage.click("@currentFiltersMenu");
                        /*dashboardPage.expect
                          .element("@filtersMenuItems")
                          .text.to.contain(programAbbrev);*/
                        //dashboardPage.assert.text.to(".menu-item-filters", programAbbrev);
                        //dashboardPage.expect.element(".menu-item-filters").text.to.contain(programAbbrev);
                    }
                );
                break;

            default:
                throw new Error(`Unexpected ${column} name.`);
        }
    };
});

When(
    /^user goes to "([^"]*)" Search under "([^"]*)" program$/,
    async (args1, args2) => {
        const linkText = {
            selector: `//a[text()='${args1}']`,
            locateStrategy: "xpath",
        };
        //wait for login to complete
        await b4rPage.waitForElementNotPresent("div.overlay", 5000, false);

        await checkLeftNav();

        await b4rPage.waitForElementVisible(linkText);
        await b4rPage.click(linkText);

        //wait for the entrypoint of the tool
        if (args1 == "Seed search") {
            await b4rPage.waitForElementPresent("@pageLabelSeeds");
        } else if (args1 == "Germplasm") {
            await b4rPage.waitForElementPresent("@pageLabelGermplasm");
        } else if (args1 == "Traits") {
            await b4rPage.waitForElementPresent("@pageLabelTraits");
        } else {
            console.log("Search argument ", +args1 + " not available");
        }

        await setDataFilterProgram(args2);
        await resetSearchQueryParameters(args1);
    }
);


Given(/^user navigates to "([^"]*)" on the left menu$/, async (args1) => {
    //check if left navigation is open
    await dashboardPage.waitForElementVisible(
        "#slide-out[style='transform: translateX(0px);']",
        1000,
        1000,
        false,
        (result) => {
            if (!result.value) {
                //click show left nav
                dashboardPage.click("@showLeftNav");
            }
        }
    );
    switch (args1) {
        case "Experiment creation":
            await dashboardPage.section.leftNavigation.waitForElementPresent("@experimentCreationLink");
            await dashboardPage.section.leftNavigation.click("@experimentCreationLink");
            break;
        case "Experiment manager":
            await dashboardPage.section.leftNavigation.waitForElementPresent("@experimentManagerLink");
            await dashboardPage.section.leftNavigation.click("@experimentManagerLink");
            break;
        case "Planting instructions manager":
            await dashboardPage.section.leftNavigation.waitForElementPresent("@plantingInstructionsManagerLink");
            await dashboardPage.section.leftNavigation.click("@plantingInstructionsManagerLink");
            break;
        case "Data collection":
            await dashboardPage.section.leftNavigation.waitForElementPresent("@dataCollectionLink");
            await dashboardPage.section.leftNavigation.click("@dataCollectionLink");
            break;
        case "Harvest manager":
            await dashboardPage.section.leftNavigation.waitForElementPresent("@harvestManagerLink");
            await dashboardPage.section.leftNavigation.click("@harvestManagerLink");
            break;
        case "Germplasm":
            await dashboardPage.section.leftNavigation.waitForElementPresent("@germplasmLink");
            await dashboardPage.section.leftNavigation.click("@germplasmLink");
            break;
        case "Seed search":
            await dashboardPage.section.leftNavigation.waitForElementPresent("@seedsearchLink");
            await dashboardPage.section.leftNavigation.click("@seedsearchLink");
            break;
        case "Traits":
            await dashboardPage.section.leftNavigation.waitForElementPresent("@traitsLink");
            await dashboardPage.section.leftNavigation.click("@traitsLink");
            break;
        case "List manager":
            await dashboardPage.section.leftNavigation.waitForElementPresent("@listManagerLink");
            await dashboardPage.section.leftNavigation.click("@listManagerLink");
            break;
        default:
            throw new Error(`Unexpected ${args1} navigation menu name.`);
    }
});


Then(/^user is redirected to "([^"]*)" entrypoint page$/, async (args1) => {
    //assert entrypoint page
    switch (args1) {
        case "Experiment creation":
            await dashboardPage.waitForElementVisible("@pageLabelExperiments");
            await dashboardPage.assert.visible("@pageLabelExperiments");
            break;
        case "Experiment manager":
            await dashboardPage.waitForElementVisible("@pageLabelExpManager");
            await dashboardPage.assert.visible("@pageLabelExpManager");
            break;
        case "Planting instructions manager":
            await dashboardPage.waitForElementVisible("@pageLabelPlantingInsMgr");
            await dashboardPage.assert.visible("@pageLabelPlantingInsMgr");
            break;
        case "Data collection":
            await dashboardPage.waitForElementVisible("@pageLabelDataCollection");
            await dashboardPage.assert.visible("@pageLabelDataCollection");
            break;
        case "Harvest manager":
            await dashboardPage.waitForElementVisible("@pageLabelHarvestMgr");
            await dashboardPage.assert.visible("@pageLabelHarvestMgr");
            break;
        case "Germplasm":
            await dashboardPage.waitForElementVisible("@pageLabelGermplasm");
            await dashboardPage.assert.visible("@pageLabelGermplasm");
            break;
        case "Seed search":
            await dashboardPage.waitForElementVisible("@pageLabelSeeds");
            await dashboardPage.assert.visible("@pageLabelSeeds");
            break;
        case "Traits":
            await dashboardPage.waitForElementVisible("@pageLabelTraits");
            await dashboardPage.assert.visible("@pageLabelTraits");
            break;
        case "List manager":
            await dashboardPage.waitForElementVisible("@pageLabelLists");
            await dashboardPage.assert.visible("@pageLabelLists");
            break;
        default:
            throw new Error(`Unexpected ${args1} navigation menu name.`);
    }
});

//check if left nav is showing
async function checkLeftNav() {
    await dashboardPage.waitForElementNotPresent("div.overlay", 5000, false);

    client.isVisible("#go-to-show-left-nav", (res) => {
        console.log(res.value);
        if (res.value === false) {
            console.log("left nav showing");
        } else {
            console.log("left nav NOT showing");
            dashboardPage.waitForElementVisible("@showLeftNav");
            dashboardPage.pause(2000);
            dashboardPage.click("@showLeftNav");
        }
    });
}
