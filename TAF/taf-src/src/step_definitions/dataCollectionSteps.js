const { client } = require("nightwatch-api");
const { Given, Then, When } = require("cucumber");
const dcPage = client.page.dataCollectionPage();
const isPresent = client.page.isPresent();
const b4rHelpers = require("./b4rHelpers.js");
const fs = require("fs");
const { fileExists } = require("nightwatch/lib/utils");
const randGen = require("crypto");
const { default: cli } = require("cucumber/lib/cli");


Given(/^traits are added in the Selected Traits browser$/, async () => {
    let idx = 0;

    //steps here
    console.log("Traits are added in the Selected Traits browser");
    await dcPage.waitForElementVisible("#select2-export_workbook-select-variable-container");
    //await dcPage.click("#select2-export_workbook-select-variable-container");

    for (var i = 1; i <= 5; i++) {
        await dcPage.selectAndAddTraits("#select2-export_workbook-select-variable-container", i);
        if (i == 1) {
            //replace all existing traits for the 1st addition of traits
            await dcPage.assert.visible("#export_workbook-replace-var-btn");
            await dcPage.click("#export_workbook-replace-var-btn");
            //confirm replacement
            await dcPage.assert.visible("#replace-with-trait-btn");
            await dcPage.click("#replace-with-trait-btn");
        } else { //continue adding of succeeding traits
            await dcPage.assert.visible("#export_workbook-add-var-btn");
            await dcPage.click("#export_workbook-add-var-btn");
        }
        await dcPage.pause(2000);
        console.log("Trait #" + i + "added!");
    }
});

When(/^user sorts the Selected Traits browser$/, async (table) => {

    for (column of table.raw()[0]) {
        console.log("Fetching: ", table.hashes()[0][column]);
        switch (column) {
            case "Column Order":
                let orderValue = table.hashes()[0][column];
                console.log("User sort " + orderValue + " column of Selected Traits browser");

                if (orderValue == "LABEL Ascending") {
                    await dcPage.section.dcTraitsColumns.assert.visible("@labelColumn");
                    await dcPage.section.dcTraitsColumns.click("@labelColumn");
                } else if (orderValue == "LABEL Descending") {
                    await dcPage.section.dcTraitsColumns.click("@labelColumn");
                    await dcPage.section.dcTraitsColumns.waitForElementPresent("@labelColAsc", 3000);
                    await dcPage.section.dcTraitsColumns.click("@labelColumn");
                }
                break;
            default:
                throw new Error(`Unexpected ${column} name.`);
        }
    }
    //wait for the browser to load fully
    await dcPage.waitGridReady(dcPage.section.dcTraitsColumns);
    await dcPage.pause(2000);
});

Then(/^traits items are sorted accordingly$/, async (table) => {
    for (column of table.raw()[0]) {
        switch (column) {
            case "Sort Order":
                let order = table.hashes()[0][column];
                if (order == "LABEL Ascending") {
                    await dcPage.section.dcTraitsColumns.waitForElementPresent("@labelColAsc", 3000);
                } else if (order == "LABEL Descending") {
                    await dcPage.section.dcTraitsColumns.waitForElementPresent("@labelColDesc", 3000);
                } else if (order == "DISPLAY NAME Ascending") {
                    await dcPage.section.dcTraitsColumns.waitForElementPresent("@dispnameColAsc", 3000);
                } else if (order == "DISPLAY NAME Descending") {
                    await dcPage.section.dcTraitsColumns.waitForElementPresent("@dispnameColDesc", 3000);
                } else if (order == "SCALE Ascending") {
                    await dcPage.section.dcTraitsColumns.waitForElementPresent("@scaleColAsc", 3000);
                } else if (order == "SCALE Descending") {
                    await dcPage.section.dcTraitsColumns.waitForElementPresent("@scaleColDesc", 3000);
                }
                break;
            default:
                throw new Error(`Unexpected ${column} name.`);
        }
        //wait for the browser to load fully
        await dcPage.waitGridReady(dcPage.section.dcTraitsColumns);
        await dcPage.pause(2000);

        //assert that after sorting, page entrypoint is still displayed
        //commenting this as bug report CORB-2347 is still recurring
        //await dcPage.section.dcTraitsColumns.assert.visible("@pageLabelDDC");
    }
});
