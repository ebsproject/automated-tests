const { client } = require("nightwatch-api");
const { Given, Then, When } = require("cucumber");
const pimPage = client.page.plantingInstructionsManagerPage();
const b4rPage = client.page.b4rPage();
const isPresent = client.page.isPresent();
const b4rHelpers = require("./b4rHelpers.js");
const fs = require("fs");
const randGen = require("crypto");


//check if left nav is showing
async function checkLeftNav() {
    await b4rPage.waitForElementNotPresent("div.overlay", 5000, false);

    client.isVisible("#go-to-show-left-nav", (res) => {
        console.log(res.value);
        if (res.value === false) {
            console.log("left nav showing");
        } else {
            console.log("left nav NOT showing");
            b4rPage.waitForElementVisible("@showLeftNav");
            b4rPage.pause(2000);
            b4rPage.click("@showLeftNav");
        }
    });
}

// PIM_Create_Packing_Job.feature
Given(/^the user is in Experiment Manager$/, async () => {
    //login non-docker
    //await b4rHelpers.login(client.globals.userName, client.globals.userPassword, client.globals.loginMode);
    //login docker
    await b4rHelpers.login(process.env.CMD_USER, process.env.CMD_PASS, process.env.CMD_MODE);
    await b4rPage.waitForElementNotPresent("div.overlay", 5000, false);

    console.log("Will update this step next sprint…");
});

When(/^the user selects a created occurrence$/, async () => {
    console.log("Will update this step next sprint…");
});

When(/^the user clicks the create packing job button$/, async () => {
    console.log("Will update this step next sprint…");
});

//
Given(/^the user has created a packing job$/, async () => {
    console.log("Will update this step next sprint…");
});

When(/^the user is redirected to Planting Instructions Manager$/, async () => {
    console.log("Will update this step next sprint…");
});

When(/^the user set the quantities of seeds per envelope, unit and envelopes per plot$/, async () => {
    console.log("Will update this step next sprint…");
});

Then(/^the user sees the packing job summary and instructions$/, async () => {
    console.log("Will update this step next sprint…");
});

Then(/^a success message for packing job is shown$/, async () => {
    console.log("Will update this step next sprint…");
});

//
Given(/^the user is in Planting Instructions Manager$/, async () => {
    //login
    await b4rHelpers.login(client.globals.userName, client.globals.userPassword, client.globals.loginMode);
    // await b4rHelpers.login(process.env.CMD_USER, process.env.CMD_PASS, process.env.CMD_MODE)
    await b4rPage.waitForElementNotPresent("div.overlay", 5000, false);

    await checkLeftNav(); //accessSideNavigation();

    await pimPage.waitForElementVisible("@viewMenu");
    await pimPage.click("@viewMenu");
});


//Opens the data filters and set filters
When(/^the user sets Data filters parameters for Planting Instructions Manager$/, async (table) => {
    await b4rPage.click("@dataFiltersMenu");
    //give time to load the program value
    await b4rPage.pause(1000);

    for (column of table.raw()[0]) {
        switch (column) {
            case "Program":
                await b4rPage.searchSelectSetValue(
                    b4rPage.section.dataFilters.elements["programSearchSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Year":
                await b4rPage.listBoxSetValue(
                    b4rPage.section.dataFilters.elements["yearSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Season":
                await b4rPage.listBoxSetValue(
                    b4rPage.section.dataFilters.elements["seasonSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Stage":
                await b4rPage.listBoxSetValue(
                    b4rPage.section.dataFilters.elements["stageSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Site":
                await b4rPage.listBoxSetValue(
                    b4rPage.section.dataFilters.elements["siteSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Experiments":
                switch (table.hashes()[0][column]) {
                    case "All":
                        await b4rPage.sliderSetValue(
                            b4rPage.section.dataFilters.elements["experimentsSlider"],
                            "Left"
                        );
                        break;
                    case "My":
                        await b4rPage.sliderSetValue(
                            b4rPage.section.dataFilters.elements["experimentsSlider"],
                            "Right"
                        );
                        break;
                    default:
                        throw new Error(`Unexpected Experiment option.`);
                }
                break;
            default:
                throw new Error(`Unexpected ${column} name.`);
        }
    };
    await b4rPage.section.dataFilters.click("@applyButton");
    await b4rPage.section.dataFilters.assert.not.visible("@applyButton");
});

When(/^the user clicks on the view packing job$/, async () => {
    await b4rPage.pause(4000);
    // jobCode = pimPage.getText('@jobCode');
    await pimPage.click("@viewPlantingJob");
    await pimPage.assert.elementPresent("@viewCollapsible");
    await pimPage.click("@viewCollapsible");
    await pimPage.assert.elementPresent("@viewPackingJobStatus");
});

Then(
    /^the user sees the packing job summary and additional instructions$/,
    async () => {
        await pimPage.assert.elementPresent("@viewPlantingJobModal");
        await pimPage.assert.containsText("@viewDueDdate", "Packing Job Due Date:");
        await pimPage.assert.containsText("@viewFacility", "Packing Job Facility:");
        await pimPage.assert.containsText(
            "@viewInstructions",
            "Packing Job Instructions:"
        );
    }
);

When(/^the user clicks on the update packing job$/, async () => {
    console.log("Will update this step next sprint…");
});

When(/^the user update all plots using "([^"]*)" method$/, async (args1) => {
    var logMessage = "Will update this step next sprint…";
    if (args1 == "Seeds / Envelope") {
        console.log(logMessage);
    } else if (args1 == "Unit") {
        console.log(logMessage);
    } else if (args1 == "Envelopes / Plot") {
        console.log(logMessage);
    }
});

Then(/^a success message for update is shown$/, async () => {
    console.log("Will update this step next sprint…");
});

When(
    /^the user filters "([^"]*)" browser column by "([^"]*)"$/,
    async (args1, args2) => {
        await pimPage.waitForElementPresent("@statusFilter");
        if (args1 == "STATUS") {
            if (args2 == "draft") {
                await pimPage.click("@statusFilter");
                await pimPage.setValue("@statusFilter", "draft");
                await pimPage.setValue("@statusFilter", client.Keys.ENTER);
            } else if (args2 == "packed") {
                await pimPage.click("@statusFilter");
                await pimPage.setValue("@statusFilter", "packed");
                await pimPage.setValue("@statusFilter", client.Keys.ENTER);
            } else if (args2 == "packing") {
                await pimPage.click("@statusFilter");
                await pimPage.setValue("@statusFilter", "packing");
                await pimPage.setValue("@statusFilter", client.Keys.ENTER);
            } else if (args2 == "packing on hold") {
                await pimPage.click("@statusFilter");
                await pimPage.setValue("@statusFilter", "packing on hold");
                await pimPage.setValue("@statusFilter", client.Keys.ENTER);
            } else if (args2 == "packing cancelled") {
                await pimPage.click("@statusFilter");
                await pimPage.setValue("@statusFilter", "packing cancelled");
                await pimPage.setValue("@statusFilter", client.Keys.ENTER);
            } else if (args2 == "ready for packing") {
                await pimPage.click("@statusFilter");
                await pimPage.setValue("@statusFilter", "ready for packing");
                await pimPage.setValue("@statusFilter", client.Keys.ENTER);
            }
        } else if (args1 == "JOB TYPE") {
            if (args2 == "packing job") {
                await pimPage.click("@jobTypeFilter");
                await pimPage.setValue("@jobTypeFilter", "packing job");
                await pimPage.setValue("@jobTypeFilter", client.Keys.ENTER);
            }
        }
    }
);

Then(/^only "([^"]*)" items are displayed on the results$/, async (args1) => {
    await pimPage.waitForElementPresent("@statusFilter");
    if (args1 == "draft") {
        await pimPage.expect.element("@statusLabel").text.to.contain("DRAFT");
        // To check if the other status is not present
        await pimPage.waitForElementNotPresent("@statusPacked");
        await pimPage.waitForElementNotPresent("@statusPacking");
        await pimPage.waitForElementNotPresent("@statusPackingOnHold");
        await pimPage.waitForElementNotPresent("@statusPackingCancelled");
        await pimPage.waitForElementNotPresent("@statusReadyForPacking");
    } else if (args1 == "packed") {
        await pimPage.expect.element("@statusLabel").text.to.contain("PACKED");
        // To check if the other status is not present
        await pimPage.waitForElementNotPresent("@statusDraft");
        await pimPage.waitForElementNotPresent("@statusPacking");
        await pimPage.waitForElementNotPresent("@statusPackingOnHold");
        await pimPage.waitForElementNotPresent("@statusPackingCancelled");
        await pimPage.waitForElementNotPresent("@statusReadyForPacking");
    } else if (args1 == "packing") {
        await pimPage.expect.element("@statusLabel").text.to.contain("PACKING");
        // To check if the other status is not present
        await pimPage.waitForElementNotPresent("@statusDraft");
        await pimPage.waitForElementNotPresent("@statusPacked");
        await pimPage.waitForElementNotPresent("@statusPackingOnHold");
        await pimPage.waitForElementNotPresent("@statusPackingCancelled");
        await pimPage.waitForElementNotPresent("@statusReadyForPacking");
    } else if (args1 == "packing on hold") {
        await pimPage.expect
            .element("@statusLabel")
            .text.to.contain("PACKING ON HOLD");
        // To check if the other status is not present
        await pimPage.waitForElementNotPresent("@statusDraft");
        await pimPage.waitForElementNotPresent("@statusPacked");
        await pimPage.waitForElementNotPresent("@statusPacking");
        await pimPage.waitForElementNotPresent("@statusPackingCancelled");
        await pimPage.waitForElementNotPresent("@statusReadyForPacking");
    } else if (args1 == "packing cancelled") {
        await pimPage.expect
            .element("@statusLabel")
            .text.to.contain("PACKING CANCELLED");
        // To check if the other status is not present
        await pimPage.waitForElementNotPresent("@statusDraft");
        await pimPage.waitForElementNotPresent("@statusPacked");
        await pimPage.waitForElementNotPresent("@statusPacking");
        await pimPage.waitForElementNotPresent("@statusPackingOnHold");
        await pimPage.waitForElementNotPresent("@statusReadyForPacking");
    } else if (args1 == "ready for packing") {
        await pimPage.expect
            .element("@statusLabel")
            .text.to.contain("READY FOR PACKING");
        // To check if the other status is not present
        await pimPage.waitForElementNotPresent("@statusDraft");
        await pimPage.waitForElementNotPresent("@statusPacked");
        await pimPage.waitForElementNotPresent("@statusPacking");
        await pimPage.waitForElementNotPresent("@statusPackingOnHold");
        await pimPage.waitForElementNotPresent("@statusPackingCancelled");
    } else if (args1 == "packing job") {
        await pimPage.expect
            .element("@jobTypeLabel")
            .text.to.contain("packing job");
    }
    await pimPage.waitForElementNotPresent("@noResultsFound");
});

When(/^user can see Planting Instructions Manager grid ready$/, async () => {
    await b4rPage.pause(2000);
});

//Set the column filters of the Packing
//Single line table only
When(/^user set column filters of the search results of the Planting Instructions Manager$/, async (table) => {
    //give time to load the program value
    await b4rPage.pause(1000);

    for (column of table.raw()[0]) {
        switch (column) {
            case "JOB CODE":
                await pimPage.section.PIMbrowserColFilters.setValue(
                    "@jobCodeColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "EXPERIMENTS":
                await pimPage.section.PIMbrowserColFilters.setValue(
                    "@experimentsColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "OCCURRENCE COUNT":
                await pimPage.section.PIMbrowserColFilters.setValue(
                    "@occurrenceCountColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "ENVELOPE COUNT":
                await pimPage.section.PIMbrowserColFilters.setValue(
                    "@envelopeCountColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "FACILITY":
                await pimPage.section.PIMbrowserColFilters.setValue(
                    "@facilityColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            default:
                throw new Error(`Unexpected ${column} name.`);
        }
    }
    //wait for the browser to load fully
    await pimPage.waitGridReady(b4rPage.section.searchGrid);
    await pimPage.pause(2000);
});

Then(/^matching results will show in the Planting Instructions Manager browser$/, async () => {
    await b4rPage.waitForElementNotPresent("@noresultsfound");
    await b4rPage.pause(2000);
    await b4rPage.waitGridReady(b4rPage.section.searchGrid);

    //add assert to ensure application did not exit with error after loading grid
    await b4rPage.pause(2000);
    // await b4rPage.assert.visible("@pageLabelSeeds");
});