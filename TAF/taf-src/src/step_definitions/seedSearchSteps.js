const { client } = require("nightwatch-api");
const { Given, Then, When } = require("cucumber");
const b4rPage = client.page.b4rPage();
const seedSearchPage = client.page.seedSearchPage();
const isPresent = client.page.isPresent();
const b4rHelpers = require("./b4rHelpers.js");
const fs = require("fs");
const randGen = require("crypto");
const { fileExists } = require("nightwatch/lib/utils");
//const seedSearchPage = require("../page_objects/seedSearchPage.js");
const qc_packageList = "_SAVEDPACKAGELIST_" + Math.floor(Math.random() * 10000);

//Start for TAF - Find Seeds
When(/^the user is in "([^"]*)" Search$/, async (args1) => {
    const linkText = {
        selector: `//a[text()='${args1}']`,
        locateStrategy: "xpath",
    };
    //login non-docker
    await b4rHelpers.login(client.globals.userName, client.globals.userPassword, client.globals.loginMode);
    //login docker
    // await b4rHelpers.login(process.env.CMD_USER, process.env.CMD_PASS, process.env.CMD_MODE);
    await b4rPage.waitForElementNotPresent("div.overlay", 5000, false);

    await checkLeftNav(); //accessSideNavigation();

    await b4rPage.waitForElementVisible(linkText);
    await b4rPage.click(linkText);

    //wait for the entrypoint of the tool
    if (args1 == "Seed search") {
        await b4rPage.waitForElementPresent("@pageLabelSeeds");
    } else if (args1 == "Germplasm") {
        await b4rPage.waitForElementPresent("@pageLabelGermplasm");
    } else if (args1 == "Traits") {
        await b4rPage.waitForElementPresent("@pageLabelTraits");
    } else {
        console.log("Search argument ", +args1 + " not available");
    }
});

When(/^user has reset the query parameters for "([^"]*)"$/, async (args1) => {
    await resetSearchQueryParameters(args1);
});

When(
    /^user goes to "([^"]*)" Search under "([^"]*)" program$/,
    async (args1, args2) => {
        const linkText = {
            selector: `//a[text()='${args1}']`,
            locateStrategy: "xpath",
        };
        //wait for login to complete
        await b4rPage.waitForElementNotPresent("div.overlay", 5000, false);

        await checkLeftNav();

        await b4rPage.waitForElementVisible(linkText);
        await b4rPage.click(linkText);

        //wait for the entrypoint of the tool
        if (args1 == "Seed search") {
            await b4rPage.waitForElementPresent("@pageLabelSeeds");
        } else if (args1 == "Germplasm") {
            await b4rPage.waitForElementPresent("@pageLabelGermplasm");
        } else if (args1 == "Traits") {
            await b4rPage.waitForElementPresent("@pageLabelTraits");
        } else {
            console.log("Search argument ", +args1 + " not available");
        }

        await setDataFilterProgram(args2);
        await resetSearchQueryParameters(args1);

    }
);

When(/^"([^"]*)" minimum parameter is set$/, async (args1) => {
    //await b4rPage.waitForElementNotPresent("div.overlay", 5000, false);

    const facilityval = "PGF";
    const subfacilityval1 = "PGF117-5-36";
    const subfacilityval2 = "PGF117-5-1";
    const occurrenceval = "2015DS";

    const labelText = {
        selector: `//*[@id="variable-filters"]//label[text()='${args1}']`,
        locateStrategy: "xpath",
    };
    await b4rPage.waitForElementVisible(labelText);

    //Search Parameter: Season
    if (args1 == "Season") {
        await b4rPage.click("@seasonfield");
        await b4rPage.pause(3000);
        await b4rPage.waitForElementVisible("@selectSeasonFilter");

        await b4rPage.getTagName(
            "css selector",
            "#select2-season-filter-results > li",
            function (result) {
                console.log("result", result);
                console.log("Season LI Size: " + result.value.length); //(if there are 3 li, here should be a count of 3)
                for (var i = 1; i <= result.value.length; i++) {
                    const selectorlist = {
                        selector: `//*[@id='select2-season-filter-results']/li[${i}]`,
                        locateStrategy: "xpath",
                    };
                    console.log(
                        "Iterating through season list option: " + selectorlist.selector
                    );
                    b4rPage.click(selectorlist);
                    b4rPage.pause(2000);
                    b4rPage.click("@seasonfield");
                    b4rPage.pause(3000);
                    b4rPage.waitForElementVisible("@selectSeasonFilter");
                }
            }
        );
        //click outside the dropdown
        b4rPage.click(labelText);
    }
    //Search Parameter: Experiment Type
    else if (args1 == "Experiment Type") {
        await b4rPage.click("@expttypefield");
        await b4rPage.pause(3000);
        await b4rPage.waitForElementVisible("@selectExptTypeFilter");

        await b4rPage.getTagName(
            "css selector",
            "#select2-experiment_type-filter-results > li",
            function (result) {
                console.log("result", result);
                console.log("Expt LI Size: " + result.value.length); //(if there are 3 li, here should be a count of 3)
                for (var i = 1; i <= result.value.length; i++) {
                    const selectorlist = {
                        selector: `//*[@id='select2-experiment_type-filter-results']/li[${i}]`,
                        locateStrategy: "xpath",
                    };
                    console.log(
                        "Iterating through expt list option: " + selectorlist.selector
                    );
                    b4rPage.click(selectorlist);
                    b4rPage.pause(2000);
                    b4rPage.click("@expttypefield");
                    b4rPage.pause(3000);
                    b4rPage.waitForElementVisible("@selectExptTypeFilter");
                }
            }
        );
        //click outside the dropdown
        b4rPage.click(labelText);
    }
    //Search Parameter: Occurrence
    else if (args1 == "Occurrence") {
        await setOccurrenceSearchParam(occurrenceval);
    }
    //Search Parameter: Facility
    else if (args1 == "Facility") {
        await setFacilitySearchParam(facilityval);
    }
    //Search Parameter: Sub-facility
    else if (args1 == "Sub-facility") {
        await setSubFacilitySearchParam(subfacilityval1);
        await setSubFacilitySearchParam(subfacilityval2);
    } else {
        console.log(
            "Search argument ",
            +args1 + " not available in the navigation menu"
        );
    }
});

When(
    /^user has selected "([^"]*)" as "([^"]*)" values$/,
    async (paramvalue, filtername) => { //removed "crop" parameter
        await isCollapsedAdditionalParam();

        if (filtername == "Input List") {
            await setInputListValues(paramvalue); //removed "crop" parameter
        } else {
            await setAdditionalFilterParameter(paramvalue, filtername);
        }
    }
);

When(/^user clicks on Find$/, async () => {
    await b4rPage.click("@findSeedsButton");
    //await b4rPage.waitForElementNotPresent("@noresultsfound");
});

Given(/^query results with single-package results groups$/, async () => {
    await b4rPage.click("@findSeedsButton");
    await b4rPage.waitForElementNotPresent("@noresultsfound");
    await b4rPage.expect.element("@resultsummary").visible.after(6000);

    //asserting page 1 link is loaded; applicable for more than 1 page results only
    await b4rPage.waitForElementPresent("@browserPageOne");

    //await b4rPage.assert.elementPresent("@singlepackage");
});

When(
    /^user turns on the automatic selection of single-package results$/,
    async () => {
        await client.execute("window.scrollTo(0,0);");
        await b4rPage.waitForElementVisible("@resultsConfigbutton");
        await b4rPage.assert.visible("@resultsConfigbutton");

        await b4rPage.click("@resultsConfigbutton");
        await b4rPage.waitForElementVisible("@configtoggle");

        await groupResultsMustbeToggledOn();
        //asserting page 1 link is loaded after a toggling on; applicable for more than 1 page results only
        await b4rPage.waitForElementPresent("@browserPageOne");

        await automaticSelectionMustbeToggledOn();
        //asserting page 1 link is loaded after a toggling on; applicable for more than 1 page results only
        await b4rPage.waitForElementPresent("@browserPageOne");
    }
);

Then(
    /^all single-package results in the current browser page are checked or selected$/,
    async () => {
        const require = "#search-grid-table-con-container input[type='checkbox']";
        client.element("css selector", require, function (response) {
            client.assert
                .ok(response.value.ELEMENT, "Checkbox response is OK")
                .elementIdSelected(response.value.ELEMENT, function (result) {
                    client.verify.ok(result.value, "Checkbox is selected");
                });
        });

        await seedSearchPage.section.searchGrid.assert.not.visible("@checkAllCheckbox");
        await seedSearchPage.section.searchGrid.assert.containsText(
            "@selectedItemCount",
            "selected items"
        );
    }
);

Then(/^user navigates to "([^"]*)" page$/, async (pagenum) => {
    await goToBrowserPage(pagenum);
    await seedSearchPage.waitGridReady(seedSearchPage.section.searchGrid);
});

Then(
    /^the query results display more than "([^"]*)" items$/,
    async (itemcount) => {
        //clear the working list first before adding any items
        await clearWorkingList();

        //await b4rPage.click("@findSeedsButton");
        await b4rPage.expect.element("@resultsummary").visible.after(10000);

        //getting the total items resulting from the search is not possible since element used has value: null
        /*
        await b4rPage.getValue("#search-grid-table-con > div > div.kv-panel-before > div:nth-child(2) > div > b:nth-child(2)", function(result){
          console.log("result", result);
          inputVal = result.value;
        });
        console.log("Query Results Total Count: " + parseInt(inputVal) + "versus minimum count: " + itemcount );
        */
    }
);

Then(/^user selects "([^"]*)" button$/, async (button) => {
    if (button == "Add ALL records to Seedlist") {
        console.log("About to add all records from the results to working list...");
        b4rPage.pause(3000);
        await addAllRecordstoWorkingList();
    } else {
        console.log(
            "Action ",
            +button +
            " not available in the actions menu of the Query Results browser"
        );
    }
});

Then(/^all query result items are added to the Working List$/, async () => {
    //assert Working List is not empty after add all button is clicked
    b4rPage.pause(2000);
    await b4rPage.waitForElementNotPresent("@workinglistnoresults");
    await b4rPage.expect.element("@wlistresultsummary").visible.after(6000);

    //assert that the search results grid has been emptied -
    //Clear working list if will not be saved as a package list
    //await clearWorkingList();
});

Then(/^results will show all matching package records$/, async () => {
    await b4rPage.waitForElementNotPresent("@noresultsfound");
    await b4rPage.pause(2000);
    await b4rPage.waitGridReady(b4rPage.section.searchGrid);

    //add assert to ensure application did not exit with error after loading grid
    await b4rPage.pause(2000);
    await b4rPage.assert.visible("@pageLabelSeeds");
});

//Set the seed search parameters
//Single line table only
When(/^user sets Seed search parameters$/, async (table) => {
    //reset/clear any set search parameters first
    await resetSearchQueryParameters("Seed search");

    for (column of table.raw()[0]) {
        switch (column) {
            case "Program":
                await b4rPage.listBoxSetValue(
                    b4rPage.section.seedsSearch.elements["programSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Season":
                await b4rPage.listBoxSetValue(
                    b4rPage.section.seedsSearch.elements["seasonSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Experiment Year":
                await b4rPage.listBoxSetValue(
                    b4rPage.section.seedsSearch.elements["experimentYearSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Experiment Type":
                await b4rPage.listBoxSetValue(
                    b4rPage.section.seedsSearch.elements["experimentTypeSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Occurrence":
                await b4rPage.listBoxSetValue(
                    b4rPage.section.seedsSearch.elements["occurrenceSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Facility":
                await b4rPage.listBoxSetValue(
                    b4rPage.section.seedsSearch.elements["facilitySelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Sub-facility":
                await b4rPage.listBoxSetValue(
                    b4rPage.section.seedsSearch.elements["subFacilitySelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Additional Search Parameters":
                //todo
                break;
            default:
                throw new Error(`Unexpected ${column} name.`);
        }
    }
    await b4rPage.section.seedsSearch.click("@findButton");
});

When(/^user can see grid ready$/, async () => {
    await b4rPage.pause(2000);
    await b4rPage.waitGridReady(b4rPage.section.searchGrid);
});

//Set the column filters of the search results
//Single line table only
When(/^user set column filters of the search results$/, async (table) => {

    for (column of table.raw()[0]) {
        switch (column) {
            case "PARENTAGE":
                await seedSearchPage.section.browserColFilters.setValue(
                    "@parentageColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "GERMPLASM":
                break;
            case "LABEL":
                await seedSearchPage.section.browserColFilters.setValue(
                    "@labelColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "SEED NAME":
                await seedSearchPage.section.browserColFilters.setValue(
                    "@seednameColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "SEED":
                await seedSearchPage.section.browserColFilters.setValue(
                    "@gidColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "PACKAGE QTY.":
                await seedSearchPage.section.browserColFilters.setValue(
                    "@qtyColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "UNIT":
                await seedSearchPage.section.browserColFilters.setValue(
                    "@unitColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "OTHER_NAME":
                await seedSearchPage.section.browserColFilters.setValue(
                    "@othernameColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "YEAR":
                await seedSearchPage.section.browserColFilters.setValue(
                    "@yearColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "EXPERIMENT TYPE":
                await seedSearchPage.section.browserColFilters.setValue(
                    "@exptypeColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "EXPERIMENT":
                await seedSearchPage.section.browserColFilters.setValue(
                    "@expnameColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "OCCURRENCE":
                await seedSearchPage.section.browserColFilters.setValue(
                    "@occnameColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "SEASON":
                await seedSearchPage.section.browserColFilters.setValue(
                    "@seasonColumnSearch",
                    table.hashes()[0][column]
                );
                break;

            case "Other Column Filters":
                //todo
                break;
            default:
                throw new Error(`Unexpected ${column} name.`);
        }
    }
    //wait for the browser to load fully
    await seedSearchPage.waitGridReady(b4rPage.section.searchGrid);
    await seedSearchPage.pause(2000);
});


When(/^user clears the working list$/, async () => {
    await checkDisplayOfWorkingList();
    await b4rPage.section.workingListGrid.click("@clearListButton");
    await b4rPage.section.clearWorkingListModal.waitForElementVisible(
        "@clearButton"
    );
    await b4rPage.section.clearWorkingListModal.click("@clearButton");
});

When(/^user set "([^"]*)" for Search Result Settings$/, async (args1) => {
    await b4rPage.section.searchGrid.click("@settingsButton");
    switch (args1) {
        case "Group Results":
            await b4rPage.sliderSetValue(
                b4rPage.section.searchGrid.elements["groupResultsSlider"],
                "Right"
            );
            break;
        case "Not Group Results":
            await b4rPage.sliderSetValue(
                b4rPage.section.searchGrid.elements["groupResultsSlider"],
                "Left"
            );
            break;
        default:
            throw new Error(`Unexpected ${args1} name.`);
    }
});

When(/^user checks all programs$/, async () => {
    await b4rPage.pause(5000);
    await b4rPage.setCheckbox(
        b4rPage.section.searchGrid.elements["checkAllCheckbox"],
        true
    );
});

When(/^user selects 'Add to Seedlist' button$/, async () => {
    await b4rPage.section.searchGrid.click("@addToSeedListButton");
    //await b4rPage.waitForElementPresent(b4rPage.section.workingListGrid);
    //await b4rPage.waitGridReady(b4rPage.section.workingListGrid);
    await b4rPage.waitForElementNotPresent("@workinglistnoresults");
    await b4rPage.expect.element("@wlistresultsummary").visible.after(6000);
});

When(/^user selects 'Save list' button$/, async () => {
    await b4rPage.section.workingListGrid.assert.visible("@saveButton");
    await b4rPage.section.workingListGrid.click("@saveButton");
});

When(/^user selects 'Proceed to Save' button$/, async () => {
    await b4rPage.section.confirmSaveModal.waitForElementVisible("@saveButton");
    await b4rPage.section.confirmSaveModal.click("@saveButton");
});

//Single entry only
When(/^user creates list$/, async (table) => {
    await b4rPage.section.createListModal.waitForElementVisible("@nameField");
    for (column of table.raw()[0]) {
        switch (column) {
            case "Name":
                await b4rPage.section.createListModal.setValue(
                    "@nameField",
                    table.hashes()[0][column].replace("*", qc_packageList)//Date.now())
                );
                break;
            case "Abbrev":
                await b4rPage.section.createListModal.setValue(
                    "@abbrevField",
                    table.hashes()[0][column]
                );
                break;
            case "Display Name":
                await b4rPage.section.createListModal.setValue(
                    "@displayNameField",
                    table.hashes()[0][column]
                );
                break;
            case "Description":
                await b4rPage.section.createListModal.setValue(
                    "@descriptionField",
                    table.hashes()[0][column]
                );
                break;
            case "Remarks":
                await b4rPage.section.createListModal.setValue(
                    "@remarksField",
                    table.hashes()[0][column]
                );
                break;
            default:
                throw new Error(`Unexpected ${args1} column name.`);
        };
        await b4rPage.section.createListModal.click("@createButton");
    }
});

When(/^user selects 'Close' button of modal$/, async () => {
    await b4rPage.section.saveConfirmModal.waitForElementVisible("@closeButton");
    await b4rPage.section.saveConfirmModal.click("@closeButton");
});

When(/^user navigates to "([^"]*)"$/, async (args1) => {
    //check if left navigation is open
    await b4rPage.waitForElementVisible(
        "#slide-out[style='transform: translateX(0px);']",
        1000,
        1000,
        false,
        (result) => {
            if (!result.value) {
                //click show left nav
                b4rPage.click("@showLeftNav");
            }
        }
    );
    switch (args1) {
        case "Experiment creation":
            break;
        case "Experiment manager":
            break;
        case "List manager":
            b4rPage.section.leftNavigation.click("@listManagerLink");
            break;
        default:
            throw new Error(`Unexpected ${args1} navigation menu name.`);
    }
});

Then(
    /^user sets "([^"]*)" by column "([^"]*)" in Lists$/,
    async (args1, args2) => {
        await b4rPage.section.listsGrid.waitForElementVisible(
            "@personalizeGridSettingsButton"
        );
        switch (args2) {
            case "Name":
                await b4rPage.section.listsGrid.setValue("@nameField", args1);
                await b4rPage.section.listsGrid.sendKeys("@nameField", client.Keys.TAB);
                break;
            default:
                throw new Error(`Unexpected ${args2} column name.`);
        }
    }
);

Then(/^user can search the saved list and can see "([^"]*)" match in Lists browser$/, async (args1) => {
    filterListsBrowserbyColumn(args1);
    await b4rPage.section.listsGrid.waitForElementVisible("@summaryTable");
    await b4rPage.section.listsGrid.assert.not.elementPresent("@listBrowserEmpty"); //List browser, after filtering the desired column, should have returned results
    await b4rPage.section.listsGrid.assert.containsText(
        "@summaryTable",
        "AUTOMATED" + qc_packageList//args1 + " item."
    );
});

/********************************************************************/
//-------------------- GENERIC FUNCTIONS START ---------------------//
//Opens the data filters and set filters
//Single row only
When(/^the user sets Data filters parameters for Seed Search$/, async (table) => {
    await b4rPage.click("@dataFiltersMenu");
    //give time to load the program value
    await b4rPage.pause(1000);

    for (column of table.raw()[0]) {
        switch (column) {
            case "Program":
                await b4rPage.searchSelectSetValue(
                    b4rPage.section.dataFilters.elements["programSearchSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Year":
                await b4rPage.listBoxSetValue(
                    b4rPage.section.dataFilters.elements["yearSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Season":
                await b4rPage.listBoxSetValue(
                    b4rPage.section.dataFilters.elements["seasonSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Stage":
                await b4rPage.listBoxSetValue(
                    b4rPage.section.dataFilters.elements["stageSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Site":
                await b4rPage.listBoxSetValue(
                    b4rPage.section.dataFilters.elements["siteSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Experiments":
                switch (table.hashes()[0][column]) {
                    case "All":
                        await b4rPage.sliderSetValue(
                            b4rPage.section.dataFilters.elements["experimentsSlider"],
                            "Left"
                        );
                        break;
                    case "My":
                        await b4rPage.sliderSetValue(
                            b4rPage.section.dataFilters.elements["experimentsSlider"],
                            "Right"
                        );
                        break;
                    default:
                        throw new Error(`Unexpected Experiment option.`);
                }
                break;
            default:
                throw new Error(`Unexpected ${column} name.`);
        }
    };
    await b4rPage.section.dataFilters.click("@applyButton");
    await b4rPage.section.dataFilters.assert.not.visible("@applyButton");
});

Then(
    /^current filter menu is updated with "([^"]*)" set to "([^"]*)"$/,
    async (args1, args2) => {
        let programAbbrev;
        if (args2 == "Irrigated South-East Asia (IRSEA)") {
            programAbbrev = "IRSEA";
        } else if (args2 == "BW Wheat Breeding Program (BW)") {
            programAbbrev = "BW";
        } else if (args2 == "KE Maize Breeding Program (KE)") {
            programAbbrev = "KE";
            //succeeding programs are used for cimmyt wheat database tests
        } else if (args2 == "CIMMYT Wheat (CIMW)") {
            programAbbrev = "CIMW";
        } else if (args2 == "Wide Crosses (WC)") {
            programAbbrev = "WC";
        } else if (args2 == "Wheat Germplasm Bank (WGB)") {
            programAbbrev = "WGB";
        } else if (args2 == "Seed Wheat (SD)") {
            programAbbrev = "SD";
        } else if (args2 == "Winter Wheat (WW)") {
            programAbbrev = "WW";
        } else
            console.log(
                "Program filter not ye defined int the test script. Please add."
            );

        /*await b4rPage.setFilterProgramAbbrev(function(result) {
        programAbbrev.value = result.value;
      });*/

        b4rPage.pause(2000);
        await b4rPage.maximizeWindow();
        await b4rPage.assert.visible("@currentFiltersMenu");
        await b4rPage.expect
            .element(".menu-item-filters")
            .text.to.contain(programAbbrev);
    }
);

Then(/^reset dashboard "([^"]*)" filter$/, async (args1) => {
    b4rPage.pause(5000);

    console.log("Calling resetDashboardDataFilterProgram");
    await resetDashboardDataFilterProgram(args1);
});

//check if working list grid is showing
async function checkDisplayOfWorkingList() {

    client.isVisible("#dynagrid-find-seed-list", (res) => {
        console.log(res.value);
        if (res.value === false) {
            console.log("Working List Grid is NOT YET in view");
            b4rPage.section.searchGrid.assert.visible("@workingListViewerButton");
            b4rPage.section.searchGrid.click("@workingListViewerButton");
            b4rPage.pause(2000);
            b4rPage.assert.visible("#dynagrid-find-seed-list");
            b4rPage.section.workingListGrid.assert.visible("@clearListButton");
        } else {
            console.log("Working List Grid is displayed");
        }
    });
}

//Filter Lists browser by recently created saved list
async function filterListsBrowserbyColumn(colname) {
    await b4rPage.section.listsGrid.waitForElementVisible(
        "@personalizeGridSettingsButton"
    );
    switch (colname) {
        case "Name":
            await b4rPage.section.listsGrid.setValue("@nameField", "%" + qc_packageList);
            await b4rPage.section.listsGrid.sendKeys("@nameField", client.Keys.TAB);
            break;
        default:
            throw new Error(`Unexpected ${args2} column name.`);
    }
}

//check if left nav is showing
async function checkLeftNav() {
    await b4rPage.waitForElementNotPresent("div.overlay", 5000, false);

    client.isVisible("#go-to-show-left-nav", (res) => {
        console.log(res.value);
        if (res.value === false) {
            console.log("left nav showing");
        } else {
            console.log("left nav NOT showing");
            b4rPage.waitForElementVisible("@showLeftNav");
            b4rPage.pause(2000);
            b4rPage.click("@showLeftNav");
        }
    });
}

//Setting of additional Parameters section for Seed Search
async function setAdditionalFilterParameter(paramvalue, filtername) {
    //assumption is that Additional Parameters section was collapsed, ensured by the previous call to isCollapsedAdditionalParam()
    await b4rPage.assert.visible("@addtlParamDiv");

    await b4rPage.waitForElementVisible("@filtersButton");
    await b4rPage.click("@filtersButton");
    await b4rPage.waitForElementVisible("@filtersSelectParam");

    const setfilter = {
        selector: `//*[@id="dropdown-add-params"]//a[text()='${filtername}']`,
        locateStrategy: "xpath",
    };
    const filterlabel = {
        selector: `//*[@id="selected-variable-filters"]//*[text()='${filtername}']`,
        locateStrategy: "xpath",
    };

    await b4rPage.click(setfilter);
    await b4rPage.waitForElementVisible(filterlabel);

    //Additional Filter Parameter: Location
    if (filtername == "Location") {
        const setfiltervalue = {
            selector: `//*[@id='select2-location-filter-selected-results']//li[text()='${paramvalue}']`,
            locateStrategy: "xpath",
        };
        const locationvalue = {
            selector: `//*[@id="selected-variable-filters"]//li[text()='${paramvalue}']`, //`//*[@id='location-filter-selected']//*[text()='${paramvalue}']`,
            locateStrategy: "xpath",
        };

        await b4rPage.click("@locationfield"); //selection container of values to choose from
        //await b4rPage.waitForElementVisible("@selectLocation");
        await b4rPage.setValue("@locationfield", paramvalue);
        b4rPage.pause(3000);
        await b4rPage.waitForElementVisible(setfiltervalue);
        await b4rPage.click(setfiltervalue);
        b4rPage.pause(1000);
        await b4rPage.waitForElementVisible(locationvalue);
        await b4rPage.assert.visible(locationvalue);
    } else {
        console.log(
            "Additional filter parameter ",
            +filtername + " not available in the filters dropdown"
        );
    }
}

async function isCollapsedSearchParam() {
    // with explicit locate strategy
    await b4rPage.getAttribute(
        "css selector",
        "#query-params",
        "style",
        function (result) {
            console.log("result", result);

            if (result.value == "display: none;") {
                //if (client.assert.containsText(elementCss,'textValue');
                //section is NOT yet collapsed
                console.log("Collapsing..." + "@searchParamSection");
                b4rPage.click("@searchParamSection");
                b4rPage.waitForElementVisible("@addtlParamDiv");
            } //else section is already collapsed
        }
    );
}

async function isCollapsedAdditionalParam() {
    //ensuring the Search Parameter section is collapsed before going to Additional Parameters section
    await isCollapsedSearchParam();
    await b4rPage.assert.visible("@addtlParamDiv");
    // with explicit locate strategy
    await b4rPage.getAttribute(
        "css selector",
        "#additional-params-header",
        "class",
        function (result) {
            console.log("result", result);

            if (result.value == "collapsible-header") {
                //if (client.assert.containsText(elementCss,'textValue');
                //section is NOT yet collapsed
                console.log("Collapsing..." + "@addtlParamSection");
                b4rPage.click("@addtlParamSection");
                b4rPage.assert.visible("@filtersButton");
            } //else section is already collapsed
        }
    );
}

async function resetSearchQueryParameters(args1) {
    if (args1 == "Seed search") {
        await isCollapsedSearchParam();
        console.log(
            "Now in Seed search entrypoint. Reset search parameter ongoing..."
        );
        await b4rPage.assert.visible("@pageLabelSeeds");
        await b4rPage.assert.visible("@activeSeedsQueryParameters");
        await b4rPage.waitForElementVisible("@resetQueryParemeters");
        await b4rPage.click("@resetQueryParemeters");
        await b4rPage.pause(1000);
        await assertSearchParametersAreBlank();
    } else if (args1 == "Germplasm" || args1 == "Traits") {
        console.log(
            "Now in Germplasm/Traits search entrypoint. Reset searh parameter ongoing..."
        );
        await openQueryTab();
        await b4rPage.assert.visible("@resetQueryParam");
        await b4rPage.click("@resetQueryParam");
        await assertSearchParametersAreBlankForGermplasmTraits();
    } else {
        console.log("Search argument ", +args1 + " not available");
    }
}

async function assertSearchParametersAreBlank() {
    //Assert basic search parameters fields are empty after a clear all query parameter
    console.log("Seed search parameters should be blank after reset");
    b4rPage.assert.containsText("@seasonfield", "");
    b4rPage.assert.containsText("@exptyrfield", "");
    b4rPage.assert.containsText("@expttypefield", "");
    b4rPage.assert.containsText("@occfield", "");
    b4rPage.assert.containsText("@facfield", "");
    b4rPage.assert.containsText("@subfacfield", "");
    b4rPage.assert.containsText("@addtlParamDiv", "");
}

async function assertSearchParametersAreBlankForGermplasmTraits() {
    //Assert basic search parameters fields are empty after a clear all query parameter
    console.log("Germplasm/Traits parameters should be blank after reset");
    b4rPage.assert.containsText("@queryName", "");
    b4rPage.assert.containsText("@queryDataType", "");
    b4rPage.assert.containsText("@addtlParamContainer", "");
}


async function clearWorkingList() {
    console.log("About to clear the working list...");
    await checkDisplayOfWorkingList();
    await b4rPage.section.workingListGrid.click("@clearListButton");
    await b4rPage.section.clearWorkingListModal.waitForElementVisible(
        "@clearButton"
    );
    await b4rPage.section.clearWorkingListModal.click("@clearButton");

    //assert that Working List has been cleared, with empty results
    await b4rPage.waitForElementVisible("@workinglistnoresults");
    await b4rPage.assert.visible("@workinglistnoresults");
}

async function addAllRecordstoWorkingList() {
    await b4rPage.assert.elementPresent("#add-all-to-list-btn");
    await b4rPage.click("#add-all-to-list-btn");
    //b4rPage.pause(3000);
    //await b4rPage.assert.visible("@wlistinsertnotif");
    //await b4rPage.waitForElementNotPresent("@wlistinsertnotif");
    //b4rPage.pause(5000);
}


async function setInputListValues(paramvalue, crop) {
    const setfieldtype = {
        selector: `//*[@id="select2-field-type-selection-2-results"]//li[text()='${paramvalue}']`,
        locateStrategy: "xpath",
    };
    const inputarea = {
        selector: `//*[@id="input-area"]`,
        locateStrategy: "xpath",
    };

    await b4rPage.assert.visible("@addtlParamDiv");
    //await b4rPage.click("@addtlParamDiv");

    await b4rPage.waitForElementVisible("@inputListParamButton");
    await b4rPage.click("@inputListParamButton");
    await b4rPage.waitForElementVisible("@inputListParamSelection");
    await b4rPage.click("@inputListParamSelection");

    //Asserting the input list field type is in the dropdown selection
    await b4rPage.assert.visible(setfieldtype);

    await b4rPage.click(setfieldtype);
    await b4rPage.assert.containsText(inputarea, "");

    /* THIS PART HERE is replaced by file loding implementation of input list
    //Write the input list values depending on the selected input field type
    if (
        paramvalue == "GERMPLASM NAME - Designation and Germplasm Names information"
    ) {
        //await b4rPage.setValue(inputarea, nameinputlist);
        console.log("writeNameInputListValues\n");
        await writeNameInputListValues(inputarea, crop);
    } else if (paramvalue == "SEED NAME - Seed Name information") {
        //await b4rPage.setValue(inputarea, seedinputlist);
        console.log("writeSeedInputListValues\n");
        await writeSeedInputListValues(inputarea);
    } else if (paramvalue == "PACKAGE LABEL - Package Label information") {
        //await b4rPage.setValue(inputarea, labelinputlist);
        console.log("writeLabelInputListValues\n");
        await writeLabelInputListValues(inputarea);
    }
    */
}

async function groupResultsMustbeToggledOn() {
    await b4rPage.assert.elementPresent("#grouped-switch-id");
    await b4rPage.getAttribute(
        "css selector",
        "#grouped-switch-id",
        "checked",
        function (clearBtn) {
            console.log("clearBtn status", clearBtn);
            if (clearBtn.value == null) {
                //toggle off is switched OFF
                console.log("Toggle off is switched OFF");
                b4rPage.assert.visible("@sliderGroupSelection");
                b4rPage.click("@sliderGroupSelection");
                b4rPage.assert.attributeEquals("#grouped-switch-id", "value", "true");
            } else {
                //toggle off is switched ON
                console.log("Toggle off is switched ON");
            }
        }
    );
    await b4rPage.waitForElementNotPresent("@noresultsfound");
}

async function automaticSelectionMustbeToggledOn() {
    await b4rPage.assert.elementPresent("#autoSelectOption-switch-id");
    await b4rPage.getAttribute(
        "css selector",
        "#autoSelectOption-switch-id",
        "checked",
        function (clearBtn) {
            console.log("clearBtn status", clearBtn);
            if (clearBtn.value == null) {
                //toggle off is switched OFF
                console.log("Toggle off is switched OFF");
                b4rPage.assert.visible("@sliderAutomaticSelection");
                b4rPage.click("@sliderAutomaticSelection");
                //b4rPage.setAttribute("checked", "#autoSelectOption-switch-id");
                /*client.execute(
                `let element = document.querySelector("#autoSelectOption-switch-id");` +
                "return element.setAttribute='checked';"
            );*/

                b4rPage.assert.attributeEquals(
                    "#autoSelectOption-switch-id",
                    "checked",
                    "true"
                );
            } else {
                //toggle off is switched ON
                console.log("Toggle off is switched ON");
            }
        }
    );
    await b4rPage.waitForElementNotPresent("@noresultsfound");
}

async function goToBrowserPage(pagenum) {
    const pagelink = {
        selector: `//a[text()[contains(.,'${pagenum}')]]`,
        locateStrategy: "xpath",
    };
    await b4rPage.waitForElementPresent(pagelink);
    await b4rPage.assert.visible(pagelink);
    await b4rPage.click(pagelink);

    //after pagelink is clicked, browser is reloaded, assert again that the pagelink is visible
    await b4rPage.waitForElementVisible(pagelink);
}

When(
    /^user loads input list items from "([^"]*)" file to "([^"]*)"$/,
    async function (args1, args2) {
        var fs = require("fs");
        var file = process.cwd() + "/src/files/" + args1;

        if (fs.existsSync(file)) {
            file = fs.readFileSync(file);
        }
        else {
            throw new Error("File not found.");
        }
        this.parameters[args2] = file;
    }
);

When(/^user sets "([^"]*)" contents to Seed Search input list field$/, async function (args1) {
    const inputarea = {
        selector: `//*[@id="input-area"]`,
        locateStrategy: "xpath",
    };

    console.log("Variable contents here:/n" + this.parameters[args1]);

    await seedSearchPage.setValue(inputarea, "");
    await seedSearchPage.setValue(inputarea, this.parameters[args1]);
    await seedSearchPage.pause(2000);
});
//--------------------- GENERIC FUNCTIONS START ---------------------//
/********************************************************************/