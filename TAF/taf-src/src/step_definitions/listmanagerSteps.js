const { client } = require("nightwatch-api");
const { Given, Then, When } = require("cucumber");
const b4rPage = client.page.b4rPage();
const listmanagerPage = client.page.listmanagerPage();
const isPresent = client.page.isPresent();
const b4rHelpers = require("./b4rHelpers.js");
const fs = require("fs");

Given(/^user navigates to List Manager on the left menu$/, async () => {
    //login non-docker
    //await b4rHelpers.login(client.globals.userName, client.globals.userPassword, client.globals.loginMode);
    //login docker
    await b4rHelpers.login(process.env.CMD_USER, process.env.CMD_PASS, process.env.CMD_MODE);
    await listmanagerPage.waitForElementNotPresent("div.overlay", 5000, false);

    //check if left navigation is open
    await listmanagerPage.waitForElementVisible(
        "#slide-out[style='transform: translateX(0px);']",
        1000,
        1000,
        false,
        (result) => {
            if (!result.value) {
                //click show left nav
                listmanagerPage.click("@showLeftNav");
            }
        }
    );

    await listmanagerPage.section.leftNavigation.waitForElementPresent("@listManagerLink");
    await listmanagerPage.section.leftNavigation.click("@listManagerLink");
    await listmanagerPage.waitForElementVisible("@pageLabelLists");
    await listmanagerPage.assert.visible("@pageLabelLists");
});

//Opens the data filters and set filters
//Single row only
When(/^the user sets Data filters parameters for List Manager$/, async (table) => {
    await listmanagerPage.click("@dataFiltersMenu");
    //give time to load the program value
    await listmanagerPage.pause(1000);

    for (column of table.raw()[0]) {
        switch (column) {
            case "Program":
                await listmanagerPage.searchSelectSetValue(
                    listmanagerPage.section.dataFilters.elements["programSearchSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Year":
                await listmanagerPage.listBoxSetValue(
                    listmanagerPage.section.dataFilters.elements["yearSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Season":
                await listmanagerPage.listBoxSetValue(
                    listmanagerPage.section.dataFilters.elements["seasonSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Stage":
                await listmanagerPage.listBoxSetValue(
                    listmanagerPage.section.dataFilters.elements["stageSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Site":
                await listmanagerPage.listBoxSetValue(
                    listmanagerPage.section.dataFilters.elements["siteSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Experiments":
                switch (table.hashes()[0][column]) {
                    case "All":
                        await listmanagerPage.sliderSetValue(
                            listmanagerPage.section.dataFilters.elements["experimentsSlider"],
                            "Left"
                        );
                        break;
                    case "My":
                        await listmanagerPage.sliderSetValue(
                            listmanagerPage.section.dataFilters.elements["experimentsSlider"],
                            "Right"
                        );
                        break;
                    default:
                        throw new Error(`Unexpected Experiment option.`);
                }
                break;
            default:
                throw new Error(`Unexpected ${column} name.`);
        }
    };
    await listmanagerPage.section.dataFilters.click("@applyButton");
    await listmanagerPage.section.dataFilters.assert.not.visible("@applyButton");
});

When(/^user can see lists browser grid ready$/, async () => {
    await listmanagerPage.pause(2000);
    await listmanagerPage.waitGridReady(listmanagerPage.section.searchGrid);
});


//Set the column filters of the search results
//Single line table only
When(/^user set column filters of lists browser$/, async (table) => {

    for (column of table.raw()[0]) {
        switch (column) {
            case "TYPE":                
                await listmanagerPage.section.browserColFilters.click("@listtypeColumn"); 
                await select("@listtypeColumnSearch", table.hashes()[0][column]);
                break;
            case "ABBREV":
                await listmanagerPage.section.browserColFilters.setValue(
                    "@listabbrevColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "NAME":
                await listmanagerPage.section.browserColFilters.setValue(
                    "@listnameColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "DISPLAY NAME":
                await listmanagerPage.section.browserColFilters.setValue(
                    "@listdispnameColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "DESCRIPTION":
                await listmanagerPage.section.browserColFilters.setValue(
                    "@listdescColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "OWNED BY":
                await listmanagerPage.section.browserColFilters.setValue(
                    "@listcreatorColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "ACCESS TYPE":                
                await listmanagerPage.section.browserColFilters.click("@listpermissionColumn"); 
                await select("@listpermissionColumnSearch", table.hashes()[0][column]);
                break;
            case "SETTINGS":
                await listmanagerPage.section.browserColFilters.click("@listsharingColumn"); 
                await select("@listsharingColumnSearch", table.hashes()[0][column]);
                break;

            case "Other Column Filters":
                //todo
                break;
            default:
                throw new Error(`Unexpected ${column} name.`);
        }
    }
    //wait for the browser to load fully
    await listmanagerPage.waitGridReady(listmanagerPage.section.searchGrid);
    await listmanagerPage.pause(3000);
});

async function select(selector, option) {
    const element = {
        selector: `//li[text()='${option}']`,
        locateStrategy: "xpath",
    };
    //await listmanagerPage.click(selector);
    await listmanagerPage.section.browserColFilters.waitForElementVisible(element);
    await listmanagerPage.section.browserColFilters.click(element);    
}