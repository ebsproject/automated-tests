const { client } = require("nightwatch-api");
const { Given, Then, When } = require("cucumber");
const emPage = client.page.experimentManagerPage();
const b4rPageMain = client.page.b4rPage();
const isPresent = client.page.isPresent();
const b4rHelpers = require("./b4rHelpers.js");
const fs = require('fs');
const randGen = require("crypto");
const { default: cli } = require("cucumber/lib/cli");
const qc_plotList = "_SAVEDPLOTLIST_" + Math.floor(Math.random() * 10000);
var locationName;
var experimentDesignFileName;
var firstOccurrence;
var lastOccurrence;


//await emPage.perform(()=>{debugger;});

Given(/^user goes to Experiment Manager$/, async () => {
    //login non-docker
    //await b4rHelpers.login(client.globals.userName, client.globals.userPassword, client.globals.loginMode);
    //login docker
    await b4rHelpers.login(process.env.CMD_USER, process.env.CMD_PASS, process.env.CMD_MODE);
    await b4rPageMain.waitForElementNotPresent("div.overlay", 5000, false);
    await checkLeftNav();
    await emPage.waitForElementVisible("@experimentManagerMenu");
    await emPage.click("@experimentManagerMenu");
});
When(/^the user sets Data filters parameters for experiment manager$/, async (table) => {
    await emPage.waitForElementVisible("@statusFilterExpMgr");
    await emPage.click("@dataFiltersMenu");
    //give time to load the program value
    await emPage.pause(1000);

    for (column of table.raw()[0]) {
        switch (column) {
            case "Program":
                await emPage.searchSelectSetValue(
                    emPage.section.dataFilters.elements["programSearchSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Year":
                await emPage.listBoxSetValue(
                    emPage.section.dataFilters.elements["yearSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Season":
                await emPage.listBoxSetValue(
                    emPage.section.dataFilters.elements["seasonSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Stage":
                await emPage.listBoxSetValue(
                    emPage.section.dataFilters.elements["stageSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Site":
                await emPage.listBoxSetValue(
                    emPage.section.dataFilters.elements["siteSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Experiments":
                switch (table.hashes()[0][column]) {
                    case "All":
                        await emPage.sliderSetValue(
                            emPage.section.dataFilters.elements["experimentsSlider"],
                            "Left"
                        );
                        break;
                    case "My":
                        await emPage.sliderSetValue(
                            emPage.section.dataFilters.elements["experimentsSlider"],
                            "Right"
                        );
                        break;
                    default:
                        throw new Error(`Unexpected Experiment option.`);
                }
                break;
            default:
                throw new Error(`Unexpected ${column} name.`);
        }
    };
    await emPage.section.dataFilters.click("@applyButton");
    await emPage.section.dataFilters.assert.not.visible("@applyButton");
});
When(/^user clicks the Location value$/, async () => {
    await emPage.waitForElementPresent("@statusFilterExpMgr");
    await emPage.click("@statusFilterExpMgr");
    await emPage.setValue("@statusFilterExpMgrInput", 'Planted');
    emPage.pause(2000);
    await emPage.setValue("@statusFilterExpMgrInput", client.Keys.ENTER);
    await emPage.click("@statusFilterExpMgrInput");
    await emPage.setValue("@statusFilterExpMgrInput", client.Keys.ENTER);
    await emPage.setValue("@statusFilterExpMgrInput", client.Keys.ENTER);

    await emPage.waitForElementVisible("@firstExperimentLocation");
    await emPage.click("@firstExperimentLocation");
});

Then(/^user sees the basic info of the Location$/, async () => {
    await emPage.waitForElementPresent('@locationCode');
    locationCode = emPage.getText('@locationCode');
    await emPage.waitForElementVisible("@plotTab");
});

When(/^user clicks the Plot tab$/, async () => {
    await emPage.waitForElementVisible("@plotTab");
    await emPage.click("@plotTab");
});

When(/^navigates to "([^"]*)" tab of the occurrence$/, async (args1) => {
    const viewtab = {
        selector: `//*[@class='tabs']//*[contains(text(),'${args1}')]`,
        locateStrategy: "xpath",
    };
    await emPage.assert.visible(viewtab);
    await emPage.click(viewtab);
});

Then(/^occurrence plot grid is ready$/, async () => {
    await emPage.waitGridReady(emPage.section.occurrencePlotGrid);
    emPage.assert.visible(emPage.section.occurrencePlotGrid);
});

When(/^the user save the plots as new list$/, async (table) => {
    emPage.assert.visible(emPage.section.occurrencePlotGrid.elements["shoppingCart"]);
    emPage.click(emPage.section.occurrencePlotGrid.elements["shoppingCart"]);

    await emPage.section.createListModal.waitForElementVisible("@nameField");
    for (column of table.raw()[0]) {
        switch (column) {
            case "Name":
                await emPage.section.createListModal.setValue(
                    "@nameField",
                    table.hashes()[0][column].replace("*", qc_plotList)
                );
                break;
            case "Abbrev":
                await emPage.section.createListModal.setValue(
                    "@abbrevField",
                    table.hashes()[0][column]
                );
                break;
            case "Display Name":
                await emPage.section.createListModal.setValue(
                    "@displayNameField",
                    table.hashes()[0][column]
                );
                break;
            case "Description":
                await emPage.section.createListModal.setValue(
                    "@descriptionField",
                    table.hashes()[0][column]
                );
                break;
            case "Remarks":
                await emPage.section.createListModal.setValue(
                    "@remarksField",
                    table.hashes()[0][column]
                );
                break;
            case "Type":
                console.log("List Type: ", table.hashes()[0][column]);
                await emPage.section.createListModal.click("@listType");
                await emPage.pause(1000);
                await emPage.section.createListModal.assert.visible("@listTypeOptions");
                await emPage.select("@listTypeOptions", table.hashes()[0][column]);
                await emPage.pause(1000);
                break;
            default:
                throw new Error(`Unexpected ${args1} column name.`);
        };
    }
    await emPage.section.createListModal.waitForElementVisible("@createButton");
    await emPage.section.createListModal.click("@createButton");
    await emPage.waitForElementNotPresent("@saveListNotif");
});


Then(/^user sees all plots within the Location$/, async () => {
    await emPage.waitForElementVisible("@plotNo");
    emPage.assert.visible("@plotNo");
});

Then(/^user should be able to view the status of trait data$/, async () => {
    await emPage.waitForElementVisible("@traitsCollected");
    emPage.assert.visible("@traitsCollected");
});
When(/^user clicks the export plots button$/, async () => {
    await emPage.waitForElementVisible("@exportPlotsButton");
    await emPage.click("@exportPlotsButton");
});

Then(/^user should be able to download plots information within the Location$/, async () => {
    emPage.pause(5000);

    var holder = ("/home/rescoto/Downloads/" + locationCode + "-plotlist.csv");
    await fs.existsSync(holder);
});
When(/^the user selects a planted occurrence$/, async () => {
    const plantedExptStatus = {
        selector: "//*[@id='select2-occurrencesearch-occurrencestatus-results']/li[text()='planted']",
        locateStrategy: "xpath",
    };
    await emPage.waitForElementPresent("@statusFilterExpMgr");
    await emPage.click("@statusFilterExpMgr");
    await emPage.waitForElementVisible("@firstExptStatusCheck");
    await emPage.assert.visible(plantedExptStatus);
    await emPage.click(plantedExptStatus);
    //await emPage.setValue("@statusFilterExpMgrInput", 'Planted');
    //emPage.page(2000);
    //await emPage.setValue("@statusFilterExpMgrInput", client.Keys.ENTER);
    //emPage.pause(2000);

    //wait for the grid browser to completely load before doing any action
    await emPage.waitGridReady(emPage.section.searchGrid);
    //find an element with 
    await emPage.click("@plantedOccurrence");
});
When(/^the user selects a not planted occurrence$/, async () => {
    await emPage.waitForElementPresent("@statusFilterExpMgr");
    await emPage.click("@statusFilterExpMgr");
    // await emPage.filterSetValue("@statusFilterExpMgrInput", client.Keys.ENTER);
    await emPage.page(2000);
    await emPage.filterSetValue("@statusFilterExpMgrInput", ['Created', client.Keys.ENTER]);
    // emPage.page(2000);
    //await emPage.setValue("@statusFilterExpMgrInput", client.Keys.ENTER);
    // await emPage.waitForElementNotVisible("@firstExperiment");
    console.log("Activated");
    emPage.pause(5000);
    //find an element with 
    await emPage.click("@notPlantedOccurrence");
});
When(/^the user clicks download trait data collection$/, async () => {
    emPage.pause(2000);
    await emPage.waitForElementVisible("@occurrenceDownload");
    await emPage.click("@occurrenceDownload");
    emPage.pause(2000);
    await emPage.waitForElementVisible("@traitDataCollection");
    hoverLocation = emPage.getLocation("@traitDataCollection", (res) => {
        emPage.moveToElement("@traitDataCollection", res.value.x, res.value.y, (res) => {
            emPage.pause(2000)
        })
    });
    await emPage.waitForElementVisible("@traitDataCollectionOccurrence");
    await emPage.click("@traitDataCollectionOccurrence");
});
Then(/^the user is redirected to Download Data Collection files tool$/, async () => {
    await emPage.waitForElementVisible("@downloadDataCollection");
    await emPage.assert.visible("@downloadDataCollection");
});
Then(/^the user is prompted that the Occurrence and or Location is not yet ready for Data collection$/, async () => {
    await emPage.waitForElementNotPresent("@downloadDataCollection");
});

When(/^the user clicks the "Upload" button$/, async () => {
    await emPage.waitForElementVisible("@uploadButton");
    await emPage.click("@uploadButton");
});
When(/^the user clicks "Trait data collection file"$/, async () => {
    await emPage.waitForElementVisible("@uploadTraitDataButton");
    await emPage.click("@uploadTraitDataButton");
});
Then(/^the user is redirected to the Data collection upload page$/, async () => {
    await emPage.waitForElementVisible("@dataCollectionUpload");
    await emPage.assert.visible("@dataCollectionUpload");
});

When(/^the user selects an occurrence$/, async () => {
    await emPage.waitForElementVisible("@firstExperiment");
    await emPage.click("@firstExperiment");
});

When(/^the user view the first occurrence$/, async () => {
    await emPage.waitForElementVisible("@viewFirstOccurrence");
    await emPage.click("@viewFirstOccurrence");
    await emPage.waitForElementVisible("@viewOccurrenceTabs");
    await emPage.assert.visible("@viewOccurrenceTabs");
});
When(/^the user updates info of the occurrence$/, async () => {
    await emPage.waitForElementVisible("@updateOccurrenceButton");
    await emPage.click("@updateOccurrenceButton");
    await emPage.waitForElementVisible("@updateOccurrenceDescription");
    await emPage.click("@updateOccurrenceDescription");
    await emPage.waitForElementVisible("@updateOccurrenceDescription");
    await emPage.clearValue("@updateOccurrenceDescription");
    await emPage.waitForElementVisible("@updateOccurrenceDescription");
    await emPage.setValue('@updateOccurrenceDescription', "This is a a description generated by testing automation");
    await emPage.waitForElementVisible("@saveUpdateButton");
    await emPage.click("@saveUpdateButton");
});
Then(/^the info of the occurrence is updated$/, async () => {
    await emPage.waitForElementVisible("@updateNotif");
    await emPage.assert.visible("@updateNotif");
});

When(/^the user clicks view$/, async () => {
    await emPage.waitForElementVisible("@viewOccurrence");
    await emPage.click("@viewOccurrence");
});
Then(/^the information of the selected occurrence will be shown$/, async () => {
    await emPage.waitForElementVisible("@infoOccurrence");
    await emPage.assert.visible("@infoOccurrence");
});

When(/^the user selects these data filters$/, async (table) => {
    emPage.pause(2000);
    await emPage.waitForElementVisible("@dataFilters");
    await emPage.click("@dataFilters");

    for (column of table.raw()[0]) {
        switch (column) {
            case "Program":
                await emPage.filterSetValue(
                    emPage.section.browserDataFilters.elements["programFilter"],
                    table.hashes()[0][column]
                );
                break;
            case "Year":
                await emPage.filterSetValue(
                    emPage.section.browserDataFilters.elements["yearFilter"],
                    table.hashes()[0][column]
                );
                break;
            case "Season":
                await emPage.filterSetValue(
                    emPage.section.browserDataFilters.elements["seasonFilter"],
                    table.hashes()[0][column]
                );
                break;
            case "Stage":
                await emPage.filterSetValue(
                    emPage.section.browserDataFilters.elements["stageFilter"],
                    table.hashes()[0][column]
                );
                break;
            case "Site":
                await emPage.filterSetValue(
                    emPage.section.browserDataFilters.elements["siteFilter"],
                    table.hashes()[0][column]
                );
                break;
        }
    }
});
When(/^the user clicks apply$/, async () => {
    await emPage.waitForElementVisible("@applyFiltersButton");
    await emPage.click("@applyFiltersButton");
});
Then(/^the result is reloaded$/, async () => {
    await emPage.waitForElementVisible("@applyFiltersNotif");
    await emPage.assert.visible("@applyFiltersNotif");

    await emPage.waitForElementVisible("@dataFilters");
    await emPage.click("@dataFilters");

    await emPage.filterClearValue(
        emPage.section.browserDataFilters.elements["programFilter"]
    );
    await emPage.filterClearValue(
        emPage.section.browserDataFilters.elements["yearFilter"]
    );
    await emPage.filterClearValue(
        emPage.section.browserDataFilters.elements["seasonFilter"]
    );
    await emPage.filterClearValue(
        emPage.section.browserDataFilters.elements["stageFilter"]
    );
    await emPage.filterClearValue(
        emPage.section.browserDataFilters.elements["siteFilter"]
    );
    await emPage.filterClearValue(
        emPage.section.browserDataFilters.elements["programFilter"]
    );
    await emPage.waitForElementVisible("@applyFiltersButton");
    await emPage.click("@applyFiltersButton");
});
When(/^the user clicks "([^"]*)" header$/, async (args1) => {
    await saveHeaderValues();
});
Then(/^the results shown for "([^"]*)" is reloaded$/, async (args1) => {
    if (args1 == 'status') {
        await emPage.waitForElementNotPresent("@initialFirst");

    } else if (args1 == 'experiment code') {
        await emPage.waitForElementNotPresent("@initialFirst");

    }
    else if (args1 == 'experiment') {
        await emPage.waitForElementNotPresent("@initialFirst");

    }
    else if (args1 == 'occurrence') {
        await emPage.waitForElementNotPresent("@initialFirst");

    }
    else if (args1 == 'site') {
        await emPage.waitForElementNotPresent("@initialFirst");

    }
    else if (args1 == 'location') {
        await emPage.waitForElementNotPresent("@initialFirst");

    }
    else if (args1 == 'experiment type') {
        await emPage.waitForElementNotPresent("@initialFirst");

    }
    else if (args1 == 'stage') {
        await emPage.waitForElementNotPresent("@initialFirst");

    }
    else if (args1 == 'year') {
        await emPage.waitForElementNotPresent("@initialFirst");

    }
    else if (args1 == 'season') {
        await emPage.waitForElementNotPresent("@initialFirst");

    }
    else if (args1 == 'design') {
        await emPage.waitForElementNotPresent("@initialFirst");

    }
    else if (args1 == 'entry count') {
        await emPage.waitForElementNotPresent("@initialFirst");

    }
    else if (args1 == 'plot count') {
        await emPage.waitForElementNotPresent("@initialFirst");

    }
});
When(/^the user clicks personalized grid settings$/, async () => {
    await emPage.waitForElementVisible("@personalizeGrid");
    await emPage.click("@personalizeGrid");
});
When(/^the user changes the page size$/, async () => {
    await emPage.waitForElementVisible("@pageSizeInput");
    await emPage.click("@pageSizeInput");
    await emPage.clearValue("@pageSizeInput");
    await emPage.setValue("@pageSizeInput", '15');
});

When(/^the user clicks the Experiment filter$/, async () => {
    await emPage.waitForElementVisible("@browserExpFilterExp");
    await emPage.click("@browserExpFilterExp");
});
When(/^the user inputs in Experiment filter$/, async () => {
    await emPage.waitForElementVisible("@browserExpFilterExp");
    await emPage.setValue("@browserExpFilterExp", 'IRSEA-RGA-2013-WS-001');
    await emPage.setValue("@browserExpFilterExp", client.Keys.ENTER);
});
Then(/^the results shown is reloaded with filter$/, async () => {
    await emPage.waitForElementPresent("@browserExpFilterExpResult");
});

When(/^the user clicks apply on page size$/, async () => {
    await emPage.waitForElementVisible("@applyPageSize");
    await emPage.click("@applyPageSize");
});
Then(/^the results shown is reloaded with page size$/, async () => {
    await emPage.waitForElementVisible("@expMgrTable");
    await emPage.expect.elements("@expMgrTable").count.to.equal(30);
});

When(/^the user clicks download "([^"]*)"$/, async (args1) => {
    await emPage.click("@occurrenceDownload");

    if (args1 == 'csv') {
        await emPage.click("@occurrenceDownloadCSV");
    } else if (args1 == 'json') {
        await emPage.click("@occurrenceDownloadJSON");
    }
});
Then(/^the "([^"]*)" of file is downloaded$/, async (args1) => {
    await emPage.waitForElementVisible("@experimentName");
    experimentName = await emPage.getText("@experimentName");
    if (args1 == 'csv') {
        var holder = ("/home/rescoto/Downloads/" + experimentName + "-mapping-file.csv");
        await fs.existsSync(holder);
    } else if (args1 == 'json') {
        var holder = ("/home/rescoto/Downloads/" + experimentName + "-mapping-file.json");
        await fs.existsSync(holder);
    }
});

When(/^the user clicks occurrence on trait data collection$/, async () => {
    await emPage.click("@occurrenceDownload");
    await emPage.waitForElementVisible("@traitDataCollection");
    hoverLocation = emPage.getLocation("@traitDataCollection", (res) => {
        emPage.moveToElement("@traitDataCollection", res.value.x, res.value.y, (res) => {
            emPage.pause(2000)
        })
    });
    await emPage.waitForElementVisible("@traitDataCollectionOccurrence");
    await emPage.click("@traitDataCollectionOccurrence");
});

When(/^the user clicks location on trait data collection$/, async () => {
    await emPage.click("@occurrenceDownload");
    await emPage.waitForElementVisible("@traitDataCollection");
    hoverLocation = emPage.getLocation("@traitDataCollection", (res) => {
        emPage.moveToElement("@traitDataCollection", res.value.x, res.value.y, (res) => {
            emPage.pause(2000)
        })
    });
    await emPage.waitForElementVisible("@traitDataCollectionLocation");
    await emPage.click("@traitDataCollectionLocation");
});

Then(/^the user is shown the Data Collection page$/, async () => {
    await emPage.waitForElementPresent("@downloadDataCollectionHeader");
});


When(/^the user selects multiple occurrence$/, async () => {
    await emPage.waitForElementPresent("@selectAllOccurrences");
    await emPage.click("@selectAllOccurrences");

});
When(/^the user selects no occurrence$/, async () => {
    await emPage.waitForElementPresent("@firstExperiment");
    await emPage.expect.element("@firstExperiment").to.not.be.selected;

});

Then(/^a warning about selecting occurrences is shown$/, async () => {
    await emPage.waitForElementPresent("@toastOccurrenceWarning");
});

When(/^the user clicks filter experiments$/, async () => {
    await resetEMFilters();
});

When(/^the user selects a "([^"]*)"$/, async (args1) => {
    if (args1 == 'program') {
        await emPage.waitForElementPresent("@expFilterProgram");
        await emPage.click("@expFilterProgram");
        await emPage.setValue("@expFilterProgram", 'Irrigated South-East Asia (IRSEA)');
        await emPage.setValue("@expFilterProgram", client.Keys.ENTER);
    } else if (args1 == 'year') {
        await emPage.waitForElementPresent("@expFilterYear");
        await emPage.click("@expFilterYear");
        await emPage.setValue("@expFilterYear", '2021');
        await emPage.setValue("@expFilterYear", client.Keys.ENTER);
        await emPage.setValue("@expFilterYear", '2020');
        await emPage.setValue("@expFilterYear", client.Keys.ENTER);
    }
    else if (args1 == 'location') {
        await emPage.waitForElementPresent("@expFilterLocation");
        await emPage.click("@expFilterLocation");
        await emPage.setValue("@expFilterLocation", '10001');
        emPage.pause(3000);
        await emPage.setValue("@expFilterLocation", client.Keys.ENTER);

    }
});
Then(/^a prompt on successful application of filters is shown$/, async () => {
    await emPage.waitForElementPresent("@toastFilterApplicationSuccess");
    await emPage.waitForElementPresent("@filterExpButton");
    await emPage.click("@filterExpButton");
    await emPage.waitForElementPresent("@filterExpNav");
    //clear alll filters except program
    await resetEMFilters();
});
When(/^the user clicks Apply Experiment Filtler$/, async () => {
    await emPage.resizeWindow(960, 1080);
    await emPage.waitForElementPresent("@applyExpButton");
    await emPage.click("@applyExpButton");
    await emPage.maximizeWindow();

});
When(/^the query result is displayed$/, async () => {
    await emPage.waitForElementPresent("@firstExperimentLocation");
});
When(/^the user inputs "([^"]*)" on the location filter input$/, async (args1) => {
    await emPage.waitForElementPresent("@locationFilterExpMgr");
    await emPage.click("@locationFilterExpMgr");
    if (args1 == 'location') {
        await emPage.setValue("@locationFilterExpMgr", 'IRSEA-AYT-2021-DS-1_LOC1');
    } else {
        await emPage.setValue("@locationFilterExpMgr", 'IRSEA%');

    }
    await emPage.setValue("@locationFilterExpMgr", client.Keys.ENTER);
    await emPage.waitForElementNotPresent("occurrenceNotLocation");
});
Then(/^the occurrences are filtered by "([^"]*)" location$/, async (args1) => {
    if (args1 == 'location') {
        await emPage.waitForElementPresent("@firstExperimentLocation");
        await emPage.expect.element("@firstExperimentLocation").text.to.contain('IRSEA-AYT-2021-DS-1_LOC1');
    } else {
        await emPage.waitForElementPresent("@firstExperimentLocation");
        await emPage.expect.element("@firstExperimentLocation").text.to.contain('IRSEA');

    }
});
When(/^the user inputs "([^"]*)" on the status filter input$/, async (args1) => {
    if (args1 == 'CREATED') {
        await emPage.waitForElementPresent("@statusFilterExpMgr");
        await emPage.click("@statusFilterExpMgr");
        await emPage.setValue("@statusFilterExpMgrInput", 'Created');
        await emPage.setValue("@statusFilterExpMgrInput", client.Keys.ENTER);
    } else if (args1 == 'MAPPED') {
        await emPage.waitForElementPresent("@statusFilterExpMgr");
        await emPage.click("@statusFilterExpMgr");
        await emPage.setValue("@statusFilterExpMgrInput", 'Mapped');
        await emPage.setValue("@statusFilterExpMgrInput", client.Keys.ENTER);
    }
    else {
        await emPage.waitForElementPresent("@statusFilterExpMgr");
        await emPage.click("@statusFilterExpMgr");
        await emPage.setValue("@statusFilterExpMgrInput", 'Planted');
        await emPage.setValue("@statusFilterExpMgrInput", client.Keys.ENTER);
    }
});
Then(/^the occurrences are filtered by "([^"]*)" status$/, async (args1) => {
    emPage.pause(2000);
    if (args1 == 'CREATED') {
        await emPage.waitForElementPresent("@statusLabelExpMgr");
        await emPage.expect.element("@statusLabelExpMgr").text.to.contain('CREATED');
    } else if (args1 == 'MAPPED') {
        await emPage.waitForElementPresent("@statusLabelExpMgr");
        await emPage.expect.element("@statusLabelExpMgr").text.to.contain('MAPPED');
    }
    else {
        await emPage.waitForElementPresent("@statusLabelExpMgr");
        await emPage.expect.element("@statusLabelExpMgr").text.to.contain('PLANTED');
    }
});

When(/^the user views an Intentional Crossing Nursery occurrence$/, async () => {
    await emPage.waitForElementPresent("@expMgrExpTypeFilterInput");
    // search for intentional Crossing that are Created using filters
    emPage.click("@expMgrExpTypeFilterInput");
    await emPage.setValue("@expMgrExpTypeFilterInput", 'Intentional Crossing Nursery');
    await emPage.setValue("@expMgrExpTypeFilterInput", client.Keys.ENTER);

    //set to only created

    await emPage.click("@statusFilterExpMgr");
    await emPage.setValue("@statusFilterExpMgrInput", 'Created');
    await emPage.setValue("@statusFilterExpMgrInput", client.Keys.ENTER);

    //view first occurrence
    await emPage.click("@viewOccurrence");
    // once found, click view and verify if there is a generate location button

});
When(/^the user sets the Intentional Crossing Nursery location$/, async () => {
    //once verified, click the genrate button and complete the modal
    await emPage.waitForElementVisible("@generateLocButton");
    await emPage.click("@generateLocButton");
    await emPage.waitForElementVisible("@generateLocModal");
    await emPage.waitForElementVisible("@generateLocationName");
    await emPage.click("@generateLocationName");
    locationName = randGen.randomBytes(10).toString('hex') + '-location';
    await emPage.setValue("@generateLocationName", locationName);
    await emPage.waitForElementVisible("@generateLocationDesc");
    await emPage.click("@generateLocationDesc");
    await emPage.setValue("@generateLocationDesc", locationName);
    //press nexT
    await emPage.waitForElementVisible("@generateLocationModalNext");
    await emPage.click("@generateLocationModalNext");
    await emPage.waitForElementNotVisible("@generateLocationName");

    //press confirm
    await emPage.waitForElementVisible("@generateLocationProceedingText");
    await emPage.waitForElementVisible("@generateLocationModalConfirm");
    await emPage.click("@generateLocationModalConfirm");
    //wait for toast confirmation to appear
    await emPage.waitForElementVisible("@generateLocationModalSuccessToast");
    //verify if mapped
    await emPage.waitForElementVisible("@occurrenceViewStatusLabel");
    await emPage.expect.element("@occurrenceViewStatusLabel").text.to.contain('Mapped');

});
Then(/^the Intentional Crossing Nursery location is generated$/, async () => {
    //once verfied, click commit, then confirm
    await emPage.waitForElementVisible("@locationCommitButton");
    await emPage.click("@locationCommitButton");

    await emPage.waitForElementVisible("@locationCommitModal");

    await emPage.waitForElementVisible("@locationCommitConfirm");
    await emPage.click("@locationCommitConfirm");

    await emPage.waitForElementVisible("@occurrenceViewStatusLabel");
    await emPage.expect.element("@occurrenceViewStatusLabel").text.to.contain('Planted');

    //verify if planted
});

When(/^the user views an experiment with a design$/, async () => {
    await emPage.click("@experimentSearchInput");
    await emPage.clearValue("@experimentSearchInput");
    await emPage.setValue("@experimentSearchInput", 'EXP00000499');
    emPage.pause(5000);
    await emPage.setValue("@experimentSearchInput", client.Keys.ENTER);

    await emPage.click("@experimentSearchInput");
    await emPage.clearValue("@experimentSearchInput");
    await emPage.setValue("@experimentSearchInput", 'EXP00000499');
    emPage.pause(5000);
    await emPage.setValue("@experimentSearchInput", client.Keys.ENTER);


    //store experiment for file checking later
    //then click view button
    await emPage.waitForElementPresent("@sortByStatus");
    await emPage.click("@sortByStatus");
    await emPage.waitForElementVisible("@experimentDesignFileName");
    experimentDesignFileName = await emPage.getText("@experimentDesignFileName");
    await emPage.click("@expHasDesignViewButton");
    await emPage.waitForElementVisible("@designLayoutButton");
    await emPage.click("@designLayoutButton");

});
When(/^the user downloads the design$/, async () => {
    //click download button
    await emPage.waitForElementVisible("@downlaodDesignLayout");
    await emPage.click("@downlaodDesignLayout");
});
Then(/^the design is downloaded$/, async () => {
    //check if unclickable
    //or check if it's downloaded
    var holder = ("/home/rescoto/Downloads/" + experimentDesignFileName + ".xlsx");
    await fs.existsSync(holder);
});

When(/^an experiment group has more than one occurrences$/, async () => {
    await emPage.click("@experimentSearchInput");
    await emPage.clearValue("@experimentSearchInput");
    await emPage.setValue("@experimentSearchInput", 'EXP00000288');
    await emPage.setValue("@experimentSearchInput", client.Keys.ENTER);
});
Then(/^the occurrences are displayed in descending order$/, async () => {
    await emPage.waitForElementVisible("@ascendingFirst");
    await emPage.waitForElementVisible("@descendingFirst");

    await emPage.click("@headerOccurrence");

    await emPage.waitForElementVisible("@ascendingFirst");
    await emPage.waitForElementVisible("@descendingFirst");

    await emPage.click("@headerOccurrence");

    await emPage.assert.containsText("@ascendingFirst", 'IRSEA-AYT-2021-WS-004-001');
    //store location
    //click header
    await emPage.click("@headerOccurrence");

    await emPage.waitForElementVisible("@ascendingFirst");
    await emPage.waitForElementVisible("@descendingFirst");

    await emPage.assert.containsText("@descendingFirst", 'IRSEA-AYT-2021-WS-004-001');
});
When(/^the user searches for an occurrence with "([^"]*)"$/, async (args1) => {
    // await emPage.waitForElementVisible("@dataFilters");
    // await emPage.click("@dataFilters");
    // await emPage.filterSetValue(emPage.section.browserDataFilters.elements["programFilter"],'WTP');

    // await emPage.waitForElementVisible("@applyFiltersButton");
    // await emPage.click("@applyFiltersButton");
    if (args1 == '1kentries') {
        //
        console.log('here');
        //click the input for experiment
        await emPage.click("@experimentSearchInput");
        await emPage.setValue("@experimentSearchInput", 'GEN-LOCATION-TAF-PARALLEL-1K%');
        await emPage.setValue("@experimentSearchInput", client.Keys.ENTER);
        await emPage.waitForElementVisible("@onekentriesExperiment");
        await emPage.waitForElementVisible("@genLocPlusIcon");
        await emPage.click("@genLocPlusIcon");
        //enter specific name
        //click status
        //enter created
        //click location icon
        //wait for modal 

    }
    else if (args1 == '1_5kentries') {
        //click the input for experiment
        await emPage.click("@experimentSearchInput");
        await emPage.setValue("@experimentSearchInput", 'GEN-LOCATION-TAF-PARALLEL-1.5K%');
        await emPage.setValue("@experimentSearchInput", client.Keys.ENTER);
        await emPage.waitForElementVisible("@one5kentriesExperiment");
        await emPage.waitForElementVisible("@genLocPlusIcon");
        await emPage.click("@genLocPlusIcon");
        //enter specific name
        //click status
        //enter created
        //click location icon
        //wait for modal 

    }
    else if (args1 == '2kentries') {
        //click the input for experiment
        await emPage.click("@experimentSearchInput");
        await emPage.setValue("@experimentSearchInput", 'GEN-LOCATION-TAF-PARALLEL-2K%');
        await emPage.setValue("@experimentSearchInput", client.Keys.ENTER);
        await emPage.waitForElementVisible("@twokentriesExperiment");
        await emPage.waitForElementVisible("@genLocPlusIcon");
        await emPage.click("@genLocPlusIcon");
        //enter specific name
        //click status
        //enter created
        //click location icon
        //wait for modal 

    }
    else if (args1 == '2_5kentries') {
        //click the input for experiment
        await emPage.click("@experimentSearchInput");
        await emPage.setValue("@experimentSearchInput", 'GEN-LOCATION-TAF-PARALLEL-2.5K%');
        await emPage.setValue("@experimentSearchInput", client.Keys.ENTER);
        await emPage.waitForElementVisible("@two5kentriesExperiment");
        await emPage.waitForElementVisible("@genLocPlusIcon");
        await emPage.click("@genLocPlusIcon");
        //enter specific name
        //click status
        //enter created
        //click location icon
        //wait for modal 

    }
});
When(/^the user generates the location using this information$/, async (table) => {
    emPage.pause(2000);
    await emPage.waitForElementVisible("@generateLocModal");

    for (column of table.raw()[0]) {
        switch (column) {
            case "Location Name":
                await emPage.genLocationSetValue(
                    emPage.section.genLocModal.elements["locationNameInput"],
                    table.hashes()[0][column]
                );
                break;
            case "Description":
                await emPage.genLocationSetValue(
                    emPage.section.genLocModal.elements["descriptionInput"],
                    table.hashes()[0][column]
                );
                break;
        }
    }
    await emPage.waitForElementVisible("@generateLocationModalNext");
    await emPage.click("@generateLocationModalNext");
    //press confirm
    await emPage.waitForElementVisible("@generateLocationProceedingText");
    await emPage.waitForElementVisible("@generateLocationModalConfirm");
    await emPage.click("@generateLocationModalConfirm");
    //wait for toast confirmation to appear

});
Then(/^location of the experiment is generated$/, async () => {
    await emPage.waitForElementVisible("@generateLocationModalSuccessToast");
    //verify if mapped
    await emPage.waitForElementVisible("@occurrenceStatusLabel");
    await emPage.expect.element("@occurrenceStatusLabel").text.to.contain('MAPPED');
});
//change here
When(/^user can see EM browser grid ready$/, async () => {
    await emPage.pause(2000);
    await emPage.waitGridReady(emPage.section.searchGrid);
});
When(/^user set column filters of the search results in EM browser$/, async (table) => {
    for (column of table.raw()[0]) {
        switch (column) {
            case "STATUS":
                break;
            case "EXPERIMENT CODE":
                await emPage.section.emColFilters.setValue(
                    "@expCodeColFilter",
                    table.hashes()[0][column]
                );
                break;
            case "EXPERIMENT":
                await emPage.section.emColFilters.setValue(
                    "@expColFilter",
                    table.hashes()[0][column]
                );
                break;
            case "OCCURRENCE":
                await emPage.section.emColFilters.setValue(
                    "@occurrenceColFilter",
                    table.hashes()[0][column]
                );
                break;
            case "SITE":
                await emPage.section.emColFilters.setValue(
                    "@siteColFilter",
                    table.hashes()[0][column]
                );
                break;
            case "FIELD":
                await emPage.section.emColFilters.setValue(
                    "@fieldColFilter",
                    table.hashes()[0][column]
                );
                break;
            case "LOCATION":
                await emPage.section.emColFilters.setValue(
                    "@locationColFilter",
                    table.hashes()[0][column]
                );
                break;
            case "EXPERIMENT TYPE":
                await emPage.section.emColFilters.setValue(
                    "@expTypeColFilter",
                    table.hashes()[0][column]
                );
                break;
            case "STAGE":
                await emPage.section.emColFilters.setValue(
                    "@stageColFilter",
                    table.hashes()[0][column]
                );
                break;
            case "YEAR":
                await emPage.section.emColFilters.setValue(
                    "@yearColFilter",
                    table.hashes()[0][column]
                );
                break;
            case "SEASON":
                await emPage.section.emColFilters.setValue(
                    "@seasonColFilter",
                    table.hashes()[0][column]
                );
                break;
            case "DESIGN":
                await emPage.section.emColFilters.setValue(
                    "@designColFilter",
                    table.hashes()[0][column]
                );
                break;
            case "ENTRY COUNT":
                await emPage.section.emColFilters.setValue(
                    "@entryCountColFilter",
                    table.hashes()[0][column]
                );
                break;
            default:
                throw new Error(`Unexpected ${column} name.`);
        }
    }
    //wait for the browser to load fully
    await emPage.waitGridReady(emPage.section.searchGrid);
    await emPage.pause(2000);
});
Then(/^results show all matching experiment occurrences$/, async () => {
    await emPage.pause(2000);
    await emPage.waitGridReady(emPage.section.searchGrid);
});
When(/^user sorts the Plot Tab browser column$/, async (table) => {
    for (column of table.raw()[0]) {
        switch (column) {
            case "Column Order":
                let orderValue = table.hashes()[0][column];
                if (orderValue == "Entry No Ascending") {
                    await emPage.click("@entryColumn");
                } else if (orderValue == "Entry No Descending") {
                    await emPage.click("@entryColumn");
                    await emPage.waitForElementPresent("@entryTabAsc", 3000);
                    await emPage.click("@entryColumn");
                } else if (orderValue == "Germplasm Name Ascending") {
                    await emPage.click("@germplasmNameColumn");
                } else if (orderValue == "Germplasm Name Descending") {
                    await emPage.click("@germplasmNameColumn");
                    await emPage.waitForElementPresent("@germplasmNameTabAsc", 3000);
                    await emPage.click("@germplasmNameColumn");
                } else if (orderValue == "Germplasm State Ascending") {
                    await emPage.click("@germplasmStateColumn");
                } else if (orderValue == "Germplasm State Descending") {
                    await emPage.click("@germplasmStateColumn");
                    await emPage.waitForElementPresent("@germplasmStateTabAsc", 3000);
                    await emPage.click("@germplasmStateColumn");
                } else if (orderValue == "Plot No Ascending") {
                    await emPage.click("@plotColumn");
                } else if (orderValue == "Plot No Descending") {
                    await emPage.click("@plotColumn");
                    await emPage.waitForElementPresent("@plotTabAsc", 3000);
                    await emPage.click("@plotColumn");
                }
                break;
            default:
                throw new Error(`Unexpected ${column} name.`);
        }
    }
});
When(/^the browser loads completely after sorting$/, async (table) => {
    for (column of table.raw()[0]) {
        switch (column) {
            case "Sort Order":
                let order = table.hashes()[0][column];
                if (order == "Ascending") {
                    await emPage.waitForElementPresent("@plotTabAsc", 3000);
                    await emPage.waitForElementPresent("@expMngrTitle");
                } else if (order == "Descending") {
                    await emPage.waitForElementPresent("@plotTabDesc", 3000);
                    await emPage.waitForElementPresent("@expMngrTitle");
                } else if (order == "Entry No Ascending") {
                    await emPage.waitForElementPresent("@entryTabAsc", 3000);
                    await emPage.waitForElementPresent("@expMngrTitle");
                } else if (order == "Entry No Descending") {
                    await emPage.waitForElementPresent("@entryTabDesc", 3000);
                    await emPage.waitForElementPresent("@expMngrTitle");
                } else if (order == "Germplasm Name Ascending") {
                    await emPage.waitForElementPresent("@germplasmNameTabAsc", 3000);
                    await emPage.waitForElementPresent("@expMngrTitle");
                } else if (order == "Germplasm Name Descending") {
                    await emPage.waitForElementPresent("@germplasmNameTabDesc", 3000);
                    await emPage.waitForElementPresent("@expMngrTitle");
                } else if (order == "Germplasm State Ascending") {
                    await emPage.waitForElementPresent("@germplasmStateTabAsc", 3000);
                    await emPage.waitForElementPresent("@expMngrTitle");
                } else if (order == "Germplasm State Descending") {
                    await emPage.waitForElementPresent("@germplasmStateTabDesc", 3000);
                    await emPage.waitForElementPresent("@expMngrTitle");
                } else if (order == "Plot No Ascending") {
                    await emPage.waitForElementPresent("@plotTabAsc", 3000);
                    await emPage.waitForElementPresent("@expMngrTitle");
                } else if (order == "Plot No Descending") {
                    await emPage.waitForElementPresent("@plotTabDesc", 3000);
                    await emPage.waitForElementPresent("@expMngrTitle");
                }
                break;
            default:
                throw new Error(`Unexpected ${column} name.`);
        }
    }
});
When(/^user can see plot browser grid ready$/, async () => {
    await emPage.pause(2000);
    await emPage.waitGridReady(emPage.section.searchGrid);
    //filter status = PLANTED
    await emPage.waitForElementPresent("@filterStatusOption");
    await emPage.click("@filterStatusOption");
    await emPage.setValue("@statusInput", 'planted');
    await emPage.setValue("@statusInput", client.Keys.ENTER);
    await emPage.waitForElementPresent("@plantedStatus", 3000);
    //view occurrence details
    //await emPage.pause(10000);
    await emPage.waitForElementPresent("@viewOccurrencDetails");
    await emPage.click("@viewOccurrencDetails");
    await emPage.waitForElementPresent("@browserDetails", 5000);
    //go to plot tab
    await emPage.waitForElementPresent("@goToPlotTab", 3000);
    await emPage.click("@goToPlotTab");
    await emPage.waitGridReady(emPage.section.searchGrid);
    await emPage.waitForElementPresent("@plotColumn", 3000);
});

When(/^user navigates to "([^"]*)" from Experiment Manager$/, async (args1) => {
    //check if left navigation is open
    await b4rPageMain.waitForElementVisible(
        "#slide-out[style='transform: translateX(0px);']",
        1000,
        1000,
        false,
        (result) => {
            if (!result.value) {
                //click show left nav
                b4rPageMain.click("@showLeftNav");
            }
        }
    );
    switch (args1) {
        case "Experiment creation":
            break;
        case "Experiment manager":
            break;
        case "List manager":
            b4rPageMain.section.leftNavigation.click("@listManagerLink");
            break;
        default:
            throw new Error(`Unexpected ${args1} navigation menu name.`);
    }
});

Then(/^user can search the saved plot list and can see "([^"]*)" match in Lists browser$/, async (args1) => {
    filterListsBrowserbyColumn(args1);
    await b4rPageMain.section.listsGrid.waitForElementVisible("@summaryTable");
    await b4rPageMain.section.listsGrid.assert.not.elementPresent("@listBrowserEmpty"); //List browser, after filtering the desired column, should have returned results
    await b4rPageMain.section.listsGrid.assert.containsText(
        "@summaryTable",
        "AUTOMATED" + qc_plotList//args1 + " item."
    );
});


//------Experiment Manager General Functions-----\\
//Filter Lists browser by recently created saved list
async function filterListsBrowserbyColumn(colname) {
    await b4rPageMain.section.listsGrid.waitForElementVisible(
        "@personalizeGridSettingsButton"
    );
    switch (colname) {
        case "Name":
            await b4rPageMain.section.listsGrid.setValue("@nameField", "%" + qc_plotList);
            await b4rPageMain.section.listsGrid.sendKeys("@nameField", client.Keys.TAB);
            break;
        default:
            throw new Error(`Unexpected ${args2} column name.`);
    }
}

async function checkLeftNav() {
    await b4rPageMain.waitForElementNotPresent("div.overlay", 5000, false);

    client.isVisible("#go-to-show-left-nav", (res) => {
        console.log(res.value);
        if (res.value === false) {
            console.log("left nav showing");
        } else {
            console.log("left nav NOT showing");
            b4rPageMain.waitForElementVisible("@showLeftNav");
            b4rPageMain.pause(2000);
            b4rPageMain.click("@showLeftNav");
        }
    });
}

async function select(selector, option) {
    const element = {
        selector: `//li[text()='${option}']`,
        locateStrategy: "xpath",
    };
    await emPage.click(selector);
    await emPage.waitForElementVisible(element);
    await emPage.click(element);
    await emPage.pause(1000);
}

async function checkCheckBox(control, check) {
    const sel = {
        selector:
            emPage.elements[control.slice(1)].selector +
            "/ancestor::div[@class='row switch']//span",
        locateStrategy: "xpath",
    };

    await emPage.click(sel);
    await emPage.pause(2000);

    let val;

    await emPage.getAttribute(control, "value", ({ value }) => {
        if (value.error) {
            throw Error(value.error);
        }
        return (val = value);
    });

    await emPage.perform(() => {
        debugger;
    });

    if (val == "false" && check == true) await emPage.click(sel);
    if (val == "true" && check == false) await emPage.click(sel);
}

async function checkElementPresent(control, attribute) {
    await emPage.getAttribute(
        "css selector",
        control,
        attribute,
        function (btn) {
            console.log("checked status", btn);
            //assert that check all is in 'checked' state, otherwise it will have a null value
            emPage.assert.equal(btn.value, "true");
        }
    );
}
async function resetEMFilters() {
    await emPage.waitForElementPresent("@filterExpButton");
    await emPage.click("@filterExpButton");
    //clear alll filters except program
    await emPage.waitForElementPresent("@expFilterYear");
    await emPage.click("@expFilterYear");
    await emPage.setValue("@expFilterYear", client.Keys.BACK_SPACE);
    console.log("Value Cleared in Year")
    await emPage.click("@expFilterSeason");
    await emPage.setValue("@expFilterSeason", client.Keys.BACK_SPACE);
    console.log("Value Cleared in Season")
    await emPage.click("@expFilterStage");
    await emPage.setValue("@expFilterStage", client.Keys.BACK_SPACE);
    console.log("Value Cleared in stage")
    await emPage.click("@expFilterSite");
    await emPage.setValue("@expFilterSite", client.Keys.BACK_SPACE);
    console.log("Value Cleared in site")
    await emPage.click("@expFilterField");
    await emPage.setValue("@expFilterField", client.Keys.BACK_SPACE);
    console.log("Value Cleared in field")
    await emPage.click("@expFilterExpCode");
    await emPage.setValue("@expFilterExpCode", client.Keys.BACK_SPACE);
    console.log("Value Cleared in ExpCode")
    await emPage.click("@expFilterExp");
    await emPage.setValue("@expFilterExp", client.Keys.BACK_SPACE);
    console.log("Value Cleared in Exp")
    await emPage.click("@expFilterOccurrence");
    await emPage.setValue("@expFilterOccurrence", client.Keys.BACK_SPACE);
    console.log("Value Cleared in occurrence")
    await emPage.click("@expFilterLocation");
    await emPage.setValue("@expFilterLocation", client.Keys.BACK_SPACE);
    console.log("Value Cleared in location")

    await emPage.resizeWindow(960, 1080)
    await emPage.waitForElementPresent("@applyExpButton");
    await emPage.click("@applyExpButton");
    await emPage.maximizeWindow();

    await emPage.waitForElementPresent("@filterExpButton");
    await emPage.click("@filterExpButton");
}

/*async function select(selector, option) {
    const element = {
        selector: `//li[text()='${option}']`,
        locateStrategy: "xpath",
    };
    await emPage.section.createListModal.waitForElementVisible(element);
    await emPage.section.createListModal.click(element);
}*/