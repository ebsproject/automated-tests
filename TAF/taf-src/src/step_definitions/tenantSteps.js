const { client } = require("nightwatch-api");
const { Given, Then, When } = require("cucumber");
const tenantPage = client.page.tenantPage();
const isPresent = client.page.isPresent();
const CSHelpers = require("./CSHelpers.js");
const fs = require("fs");
const randGen = require("crypto");


// TenantLogin.feature
Given(/^the user is in Dashboard EBS page$/, async () => {
    //login non-docker
    // await CSHelpers.login(client.globals.userName, client.globals.userPassword, client.globals.loginMode);
    //login docker
    await CSHelpers.login(process.env.CMD_USER, process.env.CMD_PASS, process.env.CMD_MODE);
    await tenantPage.waitForElementNotPresent("div.overlay", 5000, false);
});

When(/^the user selects System Administrator instance$/, async () => {
    // To select System Administrator instance
    await tenantPage.pause(1000);
    await tenantPage.waitForElementVisible("@systemAdministratorInstance");
    // await tenantPage.waitForElementVisible("@systemAdministratorLogo");
    await tenantPage.click("@systemAdministratorInstance");
});

//
When(/^the user is in System Administrator page$/, async () => {
    await tenantPage.pause(1000);
    await tenantPage.waitForElementVisible("@systemAdministratorHeader");
    await tenantPage.waitForElementVisible("@systemAdministratorLogo");
});

When(/^the user clicks on "([^"]*)" SA menu$/, async (args1) => {
    // To select menu from System Administrator instance
    if (args1 == "Tenant Management") {
        await tenantPage.click("@tenantManagementMenu");
        await tenantPage.pause(1000);
        await tenantPage.click("@tenantManagementText");

    } else if (args1 == "User Management") {
        await tenantPage.click("@userManagementMenu");
        await tenantPage.pause(1000);
        await tenantPage.click("@userManagementText");

    } else if (args1 == "CRM") {
        await tenantPage.click("@CRMMenu");
        await tenantPage.pause(1000);
        await tenantPage.click("@CRMText");

    } else if (args1 == "Printout") {
        await tenantPage.click("@printoutMenu");
        await tenantPage.pause(1000);
        await tenantPage.click("@printoutText");

    } else if (args1 == "Shipment Manager") {
        await tenantPage.click("@shipmentManagerMenu");
        await tenantPage.pause(1000);
        // await tenantPage.click("@shipmentManagerText");

    }
});

Then(/^the user will be able to view the "([^"]*)" SA page$/, async (args1) => {
    // To check if the selected menu is successful
    if (args1 == "Tenant Management") {
        await tenantPage.pause(1000);
        await tenantPage.waitForElementVisible("@tenantManagementMenu");
        await tenantPage.assert.containsText("@tenantManagementText", "TENANTS");

    } else if (args1 == "User Management") {
        await tenantPage.pause(1000);
        await tenantPage.waitForElementVisible("@userManagementMenu");
        await tenantPage.assert.containsText("@userManagementText", "USERS");

    } else if (args1 == "CRM") {
        await tenantPage.pause(1000);
        await tenantPage.waitForElementVisible("@CRMMenu");
        await tenantPage.assert.containsText("@CRMText", "PEOPLE");

    } else if (args1 == "Printout") {
        await tenantPage.pause(1000);
        await tenantPage.waitForElementVisible("@printoutMenu");
        await tenantPage.assert.containsText("@printoutText", "PRINTOUT");

    } else if (args1 == "Shipment Manager") {
        await tenantPage.pause(1000);
        await tenantPage.waitForElementVisible("@shipmentManagerMenu");
        // await tenantPage.assert.containsText("@shipmentManagerText", "");

    }
});