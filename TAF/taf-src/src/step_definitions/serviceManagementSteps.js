const { client } = require("nightwatch-api");
const { Given, Then, When } = require("cucumber");
const serviceMgtPage = client.page.serviceMgtPage();
const isPresent = client.page.isPresent();
const CSHelpers = require("./CSHelpers.js");
const fs = require("fs");
const randGen = require("crypto");


// SM_MenuNavigation.feature
Given(/^the user is in EBS Dashboard page$/, async () => {
    //login non-docker
    // await CSHelpers.login(client.globals.userName, client.globals.userPassword, client.globals.loginMode);
    //login docker
    await CSHelpers.login(process.env.CMD_USER, process.env.CMD_PASS, process.env.CMD_MODE);
    await serviceMgtPage.waitForElementNotPresent("div.overlay", 5000, false);
});

When(/^the user selects Service Management instance$/, async () => {
    // To select Service Management instance
    await serviceMgtPage.pause(1000);
    await serviceMgtPage.waitForElementVisible("@serviceManagementInstance");
    // await serviceMgtPage.waitForElementVisible("@serviceManagementLogo");
    await serviceMgtPage.click("@serviceManagementInstance");
});

//
When(/^the user is in Service Management page$/, async () => {
    await serviceMgtPage.pause(1000);
    await serviceMgtPage.waitForElementVisible("@serviceManagementHeader");
    await serviceMgtPage.waitForElementVisible("@serviceManagementLogo");
});

When(/^the user clicks on "([^"]*)" SM menu$/, async (args1) => {
    // To select menu from Service Management instance
    if (args1 == "Request Manager") {
        await serviceMgtPage.click("@requestManagerMenu");
        await serviceMgtPage.pause(1000);
        await serviceMgtPage.waitForElementVisible("@requestManagerText");

    } else if (args1 == "Genotyping Service Manager") {
        await serviceMgtPage.click("@genotypingServiceManagerMenu");
        await serviceMgtPage.pause(1000);
        await serviceMgtPage.waitForElementVisible("@genotypingServiceManagerText");

    } else if (args1 == "Inspect") {
        await serviceMgtPage.click("@inspectMenu");
        await serviceMgtPage.pause(1000);
        await serviceMgtPage.click("@inspectText");

    } else if (args1 == "Design") {
        await serviceMgtPage.click("@designMenu");
        await serviceMgtPage.pause(1000);
        await serviceMgtPage.click("@designText");

    } else if (args1 == "Vendor") {
        await serviceMgtPage.click("@vendorMenu");
        await serviceMgtPage.pause(1000);
        // await serviceMgtPage.click("@vendorText");

    } else if (args1 == "Service Catalog") {
        await serviceMgtPage.click("@serviceCatalogMenu");
        await serviceMgtPage.pause(1000);
        // await serviceMgtPage.click("@serviceCatalogText");

    } else if (args1 == "Shipment Tool") {
        await serviceMgtPage.click("@shipmentToolMenu");
        await serviceMgtPage.pause(1000);
        // await serviceMgtPage.click("@shipmentToolText");

    }
});

Then(/^the user will be able to view the "([^"]*)" SM page$/, async (args1) => {
    // To check if the selected menu is successful
    if (args1 == "Request Manager") {
        await serviceMgtPage.pause(1000);
        await serviceMgtPage.waitForElementVisible("@requestManagerMenu");
        await serviceMgtPage.assert.containsText("@requestManagerText", "Request Manager Beta version 1.0.0");

    } else if (args1 == "Genotyping Service Manager") {
        await serviceMgtPage.pause(1000);
        await serviceMgtPage.waitForElementVisible("@genotypingServiceManagerMenu");
        await serviceMgtPage.assert.containsText("@genotypingServiceManagerText", "Genotyping Service Manager");

    } else if (args1 == "Inspect") {
        await serviceMgtPage.pause(1000);
        await serviceMgtPage.waitForElementVisible("@inspectMenu");
        await serviceMgtPage.assert.containsText("@inspectText", "Inspect Manager");

    } else if (args1 == "Design") {
        await serviceMgtPage.pause(1000);
        await serviceMgtPage.waitForElementVisible("@designMenu");
        await serviceMgtPage.assert.containsText("@designText", "Design Manager");

    } else if (args1 == "Vendor") {
        await serviceMgtPage.pause(1000);
        await serviceMgtPage.waitForElementVisible("@vendorMenu");
        // await serviceMgtPage.assert.containsText("@vendorText", "");

    } else if (args1 == "Service Catalog") {
        await serviceMgtPage.pause(1000);
        await serviceMgtPage.waitForElementVisible("@serviceCatalogMenu");
        // await serviceMgtPage.assert.containsText("@serviceCatalogText", "");

    } else if (args1 == "Shipment Tool") {
        await serviceMgtPage.pause(1000);
        await serviceMgtPage.waitForElementVisible("@shipmentToolMenu");
        // await serviceMgtPage.assert.containsText("@shipmentToolText", "");
    }
});