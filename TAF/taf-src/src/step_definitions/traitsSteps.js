const { client } = require("nightwatch-api");
const { Given, Then, When } = require("cucumber");
const traitsPage = client.page.traitsPage();
const isPresent = client.page.isPresent();
const b4rHelpers = require("./b4rHelpers.js");
const fs = require("fs");
const randGen = require("crypto");

Given(/^the user is in Traits Search$/, async () => {
    //login non-docker
    //await b4rHelpers.login(client.globals.userName, client.globals.userPassword, client.globals.loginMode);
    //login docker
    await b4rHelpers.login(process.env.CMD_USER, process.env.CMD_PASS, process.env.CMD_MODE);
    await traitsPage.waitForElementNotPresent("div.overlay", 5000, false);

    await checkLeftNav();

    await traitsPage.section.leftNavigation.waitForElementPresent("@traitsLink");
    await traitsPage.section.leftNavigation.click("@traitsLink");

    //add assertions to ensure page is completely loaded before the next action
    await traitsPage.waitForElementVisible("@pageLabelTraits");
    await traitsPage.assert.visible("@pageLabelTraits");
});

When(
    /^user perform generic traits name search with input "([^"]*)"$/,
    async (namesearch) => {
        //let wildcardName = '%_';

        await traitsPage.assert.visible("@queryName");
        await traitsPage.setValue("@queryName", namesearch);
        traitsPage.pause(1000);

        await traitsPage.click("@queryFindButton");
        await traitsPage.waitForElementVisible("@queryResultsSummary");
    }
);

//check if left nav is showing
async function checkLeftNav() {
    await traitsPage.waitForElementNotPresent("div.overlay", 5000, false);

    client.isVisible("#go-to-show-left-nav", (res) => {
        console.log(res.value);
        if (res.value === false) {
            console.log("left nav showing");
        } else {
            console.log("left nav NOT showing");
            traitsPage.waitForElementVisible("@showLeftNav");
            traitsPage.pause(2000);
            traitsPage.click("@showLeftNav");
        }
    });
}

//Opens the data filters and set filters
//Single row only
When(/^the user sets Data filters parameters for Traits Search$/, async (table) => {
    await traitsPage.click("@dataFiltersMenu");
    //give time to load the program value
    await traitsPage.pause(1000);

    for (column of table.raw()[0]) {
        switch (column) {
            case "Program":
                await traitsPage.searchSelectSetValue(
                    traitsPage.section.dataFilters.elements["programSearchSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Year":
                await traitsPage.listBoxSetValue(
                    traitsPage.section.dataFilters.elements["yearSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Season":
                await traitsPage.listBoxSetValue(
                    traitsPage.section.dataFilters.elements["seasonSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Stage":
                await traitsPage.listBoxSetValue(
                    traitsPage.section.dataFilters.elements["stageSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Site":
                await traitsPage.listBoxSetValue(
                    traitsPage.section.dataFilters.elements["siteSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Experiments":
                switch (table.hashes()[0][column]) {
                    case "All":
                        await traitsPage.sliderSetValue(
                            traitsPage.section.dataFilters.elements["experimentsSlider"],
                            "Left"
                        );
                        break;
                    case "My":
                        await traitsPage.sliderSetValue(
                            traitsPage.section.dataFilters.elements["experimentsSlider"],
                            "Right"
                        );
                        break;
                    default:
                        throw new Error(`Unexpected Experiment option.`);
                }
                break;
            default:
                throw new Error(`Unexpected ${column} name.`);
        }
    };
    await traitsPage.section.dataFilters.click("@applyButton");
    await traitsPage.section.dataFilters.assert.not.visible("@applyButton");
});

//Opens the data filters and set filters
When(/^user set column filters of the search results of the Traits Search$/, async (table) => {
    for (column of table.raw()[0]) {
        switch (column) {
            case "ABBREV":
                await traitsPage.section.traitsBrowserColFilters.setValue(
                    "@abbrevColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "LABEL":
                await traitsPage.section.traitsBrowserColFilters.setValue(
                    "@labelColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "NAME":
                await traitsPage.section.traitsBrowserColFilters.setValue(
                    "@nameColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "DISPLAY NAME":
                await traitsPage.section.traitsBrowserColFilters.setValue(
                    "@displayNameColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "DATA TYPE":
                await traitsPage.section.traitsBrowserColFilters.setValue(
                    "@dataTypeColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "TYPE":
                await traitsPage.section.traitsBrowserColFilters.setValue(
                    "@typeColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "USAGE":
                await traitsPage.section.traitsBrowserColFilters.setValue(
                    "@usageColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "DATA LEVEL":
                await traitsPage.section.traitsBrowserColFilters.setValue(
                    "@dataLevelColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            case "STATUS":
                await traitsPage.section.traitsBrowserColFilters.setValue(
                    "@statusColumnSearch",
                    table.hashes()[0][column]
                );
                break;
            default:
                throw new Error(`Unexpected ${column} name.`);
        }
    }
    await traitsPage.waitGridReady(traitsPage.section.searchGrid);
    await traitsPage.pause(2000);
});

Then(/^matching results will show in the Traits Search browser$/, async () => {
    await traitsPage.pause(2000);
    await traitsPage.waitGridReady(traitsPage.section.searchGrid);
    
    //add assertions to ensure page is completely loaded before the next action 
    await traitsPage.waitForElementVisible("@pageLabelTraits");
    await traitsPage.assert.visible("@pageLabelTraits");
});