const { client } = require("nightwatch-api");
const { Given, Then, When } = require("cucumber");
const armPage = client.page.analysisRequestManagerPage();
const isPresent = client.page.isPresent();
const baHelpers = require("./b4rHelpers.js");
const fs = require("fs");
const randGen = require("crypto");

Given(/^user logs in to BA systems$/, async () => {
    //login non-docker
    //await baHelpers.login(client.globals.userName, client.globals.userPassword, client.globals.loginMode);
    //login docker
    await baHelpers.login(process.env.CMD_USER, process.env.CMD_PASS, process.env.CMD_MODE);
    await armPage.waitForElementNotPresent("div.overlay", 5000, false);
    await armPage.waitForElementVisible('@breedingAnalytics');
    await armPage.click('@breedingAnalytics');
    await armPage.waitForElementPresent('@analysisManager');
    await armPage.click('@analysisManager');
});
When(/^the user click the new request button$/, async () => {
    await armPage.waitForElementPresent("@jobList")
    await armPage.click("@newRequestButton");
    //give time to load the program value
    await armPage.pause(1000);
    await armPage.waitForElementVisible('@experimentList');
}); 
When(/^selects a single occurrence in a specific experiment$/, async () => {
    await armPage.waitForElementPresent("@experimentFilter");
    await armPage.click("@experimentFilter");
    await armPage.setValue("@experimentFilter", "AF");
    await armPage.setValue("@experimentFilter", client.Keys.ENTER);
    await armPage.waitForElementPresent("@AFExperiments");
    await armPage.click("@AFExperiments");
    //await armPage.click("@AFExperimentRow");
    //await armPage.click("@AFExperimentRow");
    await armPage.waitForElementPresent("@AFOccurrence");
    //await armPage.click("@AFOccurrence");
    await armPage.waitForElementVisible("@nextToSettings");
    await armPage.click("@nextToSettings");
});


When(/^the user fills up response variables field$/, async (table) => {
    //response variables
    await armPage.waitForElementPresent('@responseVariable');
    await armPage.click('@responseVariable');
    for (column of table.raw()[0]) {
        switch (column) {
            case "Response Variables":
                //await emPage.genLocationSetValue(emPage.section.genLocModal.elements["locationNameInput"],table.hashes()[0][column] );
                if(table.hashes()[0][column] == "MC_CONT"){
                    await armPage.waitForElementPresent('@MCCont');
                    await armPage.click('@MCCont');
                }
                else{
                    await armPage.waitForElementPresent('@AYLDCont');
                    await armPage.click('@AYLDCont');
                } 
        }
    
    }
    await armPage.waitForElementVisible("@nextToParameters");
    await armPage.click('@nextToParameters')
});

When(/^the user fills up spatial adjusting field in parameters$/, async (table) => {
    //spatialAdjusting
    await armPage.waitForElementPresent('@spatialAdjusting');
    await armPage.click('@spatialAdjusting');
    for (column of table.raw()[0]) {
        switch (column) {
            case "Spatial Adjusting":
                //await emPage.genLocationSetValue(emPage.section.genLocModal.elements["locationNameInput"],table.hashes()[0][column] );
                if(table.hashes()[0][column] == "AR1row X AR1col"){
                    await armPage.waitForElementPresent('@AR1RowXCol');
                    await armPage.click('@AR1RowXCol');
                }
                else if(table.hashes()[0][column] == "IDrow X AR1col"){
                    await armPage.waitForElementPresent('@IDRowXCol');
                    await armPage.click('@IDRowXCol');
                }
                else if(table.hashes()[0][column] == "AR1row X IDcol"){
                    await armPage.waitForElementPresent('@AR1RowXIDCol');
                    await armPage.click('@AR1RowXIDCol');
                }
                else if(table.hashes()[0][column] == "No Spatial Adjustment"){
                    await armPage.waitForElementPresent('@noSpatialAdj');
                    await armPage.click('@noSpatialAdj');
                }
        }
    
    }
   
});

When(/^the user clicks confirm$/, async () => {
    //submit analysis request
    console.log("will click")
    await armPage.waitForElementVisible('@submitAnalysisRequest')

    await armPage.click('@submitAnalysisRequest')

    console.log("clicked")
});


Then(/^the request is created$/, async () => {
    await armPage.waitForElementNotPresent('@submitAnalysisRequest')
    await armPage.waitForElementPresent('@jobList')
});

When(/^the user is on the Add New Request page$/, async () => {
    await armPage.waitForElementPresent("@jobList")
    await armPage.click("@newRequestButton");
    //give time to load the program value
    await armPage.pause(1000);
    await armPage.waitForElementVisible('@experimentList');
}); 

When(/^the user searches for an experiment with an occurrence ready for analysis$/, async (table) => {
    await armPage.waitForElementPresent("@experimentFilter");
    await armPage.click("@experimentFilter");
    
    for (column of table.raw()[0]) {
        switch (column) {
            case "Experiment":
                await armPage.setValue("@experimentFilter", table.hashes()[0][column]);
                await armPage.setValue("@experimentFilter", client.Keys.ENTER);
                break;
            default:
                throw new Error(`Unexpected ${column} name.`);
        }
    };

    await armPage.waitForElementPresent("@AFExperiments");
}); 

When(/^the user selects all occurrences$/, async () => {
    //verify the Next button is disabled
    await armPage.pause(2000);
    await armPage.waitForElementPresent("@nextButtonDisabled");
    //click the radio button
    await armPage.click("@selectAllOccurrence"); 
    //verify that the NEXT button is enabled
    await armPage.waitForElementPresent("@nextButtonEnabled");
}); 

When(/^the occurrences are displayed$/, async () => {
    //click experiment
    await armPage.pause(2000);
    await armPage.waitForElementPresent("@selectExperiment");
    await armPage.click("@selectExperiment");
    //assertion
    await armPage.waitForElementVisible('@occurrencesAssertion1');
    await armPage.waitForElementVisible('@occurrencesAssertion2');
}); 


When(/^the user goes to the Settings tab$/, async () => {
    //click the Next button
    armPage.pause(2000);
    await armPage.click("@experimentsTabNext");
    armPage.pause(2000);
    //verify user is in the Settings tab
    await armPage.waitForElementVisible("@settingsTabPanel");

}); 

When(/^there are two available traits shown in the Response variables$/, async (table) => {
    //assert Response variable trait dropdown
    await armPage.waitForElementPresent("@traitsDropdown");
    //click Traits
    await armPage.click("@traitsDropdown");
    
    for (column of table.raw()[0]) {
        switch (column) {
            case "Trait 1":
                //get text
                await armPage.getText('css selector', '#tags-outlined-option-0', function(result) {
                    console.log(result.value, table.hashes()[0][column]);
                    //compare trait and expected value
                    armPage.expect.element('#tags-outlined-option-0').text.to.equal(table.hashes()[0][column]);
                });
                break;
            case "Trait 2":
                //get text
                await armPage.getText('css selector', '#tags-outlined-option-1', function(result) {
                    console.log(result.value, table.hashes()[0][column]);
                    //compare trait and expected value
                    armPage.expect.element('#tags-outlined-option-1').text.to.equal(table.hashes()[0][column]);
                  });
                break;
            default:
                throw new Error(`Unexpected ${column} name.`);
        }
    };
});

When(/^"([^"]*)" is auto-selected as the Analyis objective$/, async (args1) => {
    if (args1 == "Prediction") {
        //verify that Prediction is selected
        await armPage.waitForElementPresent("@analysisObjDropdown")
        await armPage.expect.element('#ao1-simple-select').text.to.equal(args1);

    }

});
