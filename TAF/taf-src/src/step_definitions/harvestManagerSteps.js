const { client } = require("nightwatch-api");
const { Given, Then, When } = require("cucumber");
//const b4rPage = client.page.b4rPage();
const hmPage = client.page.harvestManagerPage();
const isPresent = client.page.isPresent();
const b4rHelpers = require("./b4rHelpers.js");
const fs = require("fs");
const randGen = require("crypto");

/* FUNCTIONS */
//check if left nav is showing
async function checkLeftNav() {
    await hmPage.waitForElementNotPresent("div.overlay", 5000, false);

    client.isVisible("#go-to-show-left-nav", (res) => {
        console.log(res.value);
        if (res.value === false) {
            console.log("left nav showing");
        } else {
            console.log("left nav NOT showing");
            hmPage.waitForElementVisible("@showLeftNav");
            hmPage.pause(2000);
            hmPage.click("@showLeftNav");
        }
    });
}

//function for resetting harvest data
async function resetHarvestData() {
    //perform bulk delete
    await hmPage.waitForElementVisible("@bulkDelete", 3000);
    await hmPage.click("@bulkDelete");
    await hmPage.waitForElementVisible("@uncommittedData");
    await hmPage.click("@uncommittedData");
    await hmPage.click("@committedData");
    await hmPage.click("@selectAllTraits");

    //click Delete all
    hmPage.pause(2000);
    await hmPage.click("@deleteAll");
    hmPage.pause(2000);

    //check if option appears
    client.isVisible("#delete_apply_changes", (res) => {
        console.log(res.value);
        if (res.value === true) {
            hmPage.click("@confirmDelete");
            //wait until delete is finished
            hmPage.pause(2000);
            hmPage.waitForElementNotPresent("@bulkDeleteProcess", 100000, 5000);
            //hmPage.pause(4000);
        } else {
            hmPage.click("@cancelDelete");
            hmPage.waitForElementVisible("@closeModal");
            hmPage.click("@closeModal");
            //wait until delete is finished
            hmPage.pause(2000);
            hmPage.waitForElementNotPresent("@bulkDeleteProcess", 8000, 2000);
        }
    });

    console.log("Finished reset");
}

//function for clearing committed harvest data
async function clearData() {
    //clear data
    await hmPage.waitForElementVisible("@bulkDelete");
    await hmPage.click("@bulkDelete");
    await hmPage.waitForElementVisible("@uncommittedData");
    await hmPage.click("@uncommittedData");
    await hmPage.click("@committedData");
    await hmPage.click("@selectAllTraits");

    //click Delete all
    hmPage.pause(2000);
    await hmPage.click("@deleteAll");

    //confirm delete
    await hmPage.waitForElementVisible("@confirmDelete");
    await hmPage.click("@confirmDelete");

    console.log("cleared data");
}

//function for deleting selected harvest data
async function deleteSelectedHarvestData() {
    //perform bulk delete
    await hmPage.waitForElementVisible("@bulkDelete");
    await hmPage.click("@bulkDelete");
    await hmPage.waitForElementVisible("@uncommittedData");
    await hmPage.click("@uncommittedData");
    await hmPage.click("@committedData");
    await hmPage.click("@selectAllTraits");
    //delete selected
    await hmPage.waitForElementVisible("@bulkDeleteSelected");
    await hmPage.click("@bulkDeleteSelected");

    await hmPage.waitForElementVisible("@deleteModal");

    //check if option appears
    client.isVisible("#delete_apply_changes", (res) => {
        console.log(res.value);
        if (res.value === true) {
            console.log("confirm delete");
            hmPage.click("@confirmDelete");
        } else {
            console.log("cancel delete");
            hmPage.click("@cancelDelete");
            hmPage.waitForElementVisible("@closeModal");
            hmPage.click("@closeModal");
        }
    });

    //wait until delete is finished
    await hmPage.waitForElementNotPresent("@bulkDeleteProcess", 30000, 5000);
}


/* REFACTOR START */

Given(/^user logs in to B4R$/, async () => {
    //login non-docker
    //await b4rHelpers.login(client.globals.userName, client.globals.userPassword, client.globals.loginMode);
    //login docker
    await b4rHelpers.login(process.env.CMD_USER, process.env.CMD_PASS, process.env.CMD_MODE);
});

Given(/^the user is in Harvest Manager$/, async () => {
    //login non-docker
    //await b4rHelpers.login(client.globals.userName, client.globals.userPassword, client.globals.loginMode);
    //login docker
    await b4rHelpers.login(process.env.CMD_USER, process.env.CMD_PASS, process.env.CMD_MODE);
    //go to HM
    checkLeftNav();

    await hmPage.waitForElementVisible("@harvestManagerMenu");
    await hmPage.click("@harvestManagerMenu");
});


Given(/^the user loads the "([^"]*)" for seedlot creation$/, async (args1) => {
    if (args1 == "plots") {
        //click load plots
        await hmPage.click("@loadPlots");
        await hmPage.waitForElementNotPresent("@emptyQueryResults");
        await hmPage.waitForElementVisible("@dataBrowserSummary");
    } else if (args1 == "crosses") {
        //click load crosses
        await hmPage.click("@loadCrosses");
        await hmPage.waitForElementNotPresent("@emptyQueryResults");
        await hmPage.waitForElementVisible("@dataBrowserSummary");
    }
});

When(/^the search parameters are specified$/, async (table) => {
    for (column of table.raw()[0]) {
        switch (column) {
            case "Program":
                await hmPage.programSetValue(
                    hmPage.section.loadExperiment.elements["programSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Experiment":
                await hmPage.experimentSetValue(
                    hmPage.section.loadExperiment.elements["experimentSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Location Code":
                await hmPage.locationCodeSetValue(
                    hmPage.section.loadExperiment.elements["locationCodeSelect"],
                    table.hashes()[0][column]
                );
                break;
        }
    }
});

When(/^the experiment location details are displayed$/, async (table) => {
    //verify that the experiment location details is displayed
    await hmPage.waitForElementVisible("@experimentLocationDetails");

    for (column of table.raw()[0]) {
        switch (column) {
            case "Experiment Type":
                await hmPage.expTypeAssertValue(
                    hmPage.section.loadExperiment.elements["expTypeAssert"],
                    table.hashes()[0][column]
                );
                break;
            case "Site":
                await hmPage.siteAssertValue(
                    hmPage.section.loadExperiment.elements["siteAssert"],
                    table.hashes()[0][column]
                );
                break;
            case "Steward":
                await hmPage.stewardAssertValue(
                    hmPage.section.loadExperiment.elements["stewardAssert"],
                    table.hashes()[0][column]
                );
                break;
        }
    }
});

When(/^the user bulks update harvest data$/, async () => {
    //reset harvest data
    await resetHarvestData();

    //select plots to be updated and click bulk update
    await hmPage.waitForElementVisible("@selectAll");
    await hmPage.click("@selectAll");
    await hmPage.waitForElementVisible("@bulkUpdateButton");
    await hmPage.expect.element("@bulkUpdateButton").visible.after(3000);
    await hmPage.click("@bulkUpdateButton");
    await hmPage.waitForElementVisible("@bulkUpdateForm");
});

When(/^the user bulks update harvest data of all plots$/, async () => {
    //reset harvest data
    await resetHarvestData();

    //select plots to be updated and click bulk update
    await hmPage.waitForElementVisible("@bulkUpdateButton");
    await hmPage.expect.element("@bulkUpdateButton").visible.after(3000);
    await hmPage.click("@bulkUpdateButton");
    await hmPage.waitForElementVisible("@bulkUpdateForm");
});

When(/^the user specified the following harvest traits$/, async (table) => {
    let hmValue;

    //the user updates the selected plots using selected harvest method
    for (column of table.raw()[0]) {
        switch (column) {
            case "Harvest Date":
                let hvValue = table.hashes()[0][column];
                await hmPage.setValue("#harvest-date-picker", hvValue);
                break;
            case "Harvest Method":
                await hmPage.click("@selectHarvestMethod");
                await hmPage.waitForElementPresent("@harvestMethodDropdown");
                hmValue = table.hashes()[0][column];
                //select the Harvest Method
                const setHarvestMethod = {
                    selector: `//*[@id='select2-harvest-method-select-results']/li[contains(text(),'${hmValue}')]`,
                    locateStrategy: "xpath",
                };
                await hmPage.click(setHarvestMethod);
                break;
            case "Numeric Variables":
                let numValue = table.hashes()[0][column];
                //if numValue is NA, skip because Bulk has no numeric variable
                if (numValue != "NA" && hmValue != "Individual ear") {
                    await hmPage.setValue("#no-of-plants-input", numValue);
                } else if (numValue != "NA" && hmValue == "Individual ear") {
                    await hmPage.setValue("#no-of-ears-input", numValue);
                }
                break;
        }
    }

    await hmPage.waitForElementVisible("#apply-bulk-selected-btn", 2000);
    await hmPage.click("#apply-bulk-selected-btn");

    await hmPage.waitForElementPresent("#confirmation_modal > div > div");
    await hmPage.waitForElementVisible("@applyChanges", 2000);
    await hmPage.click("@applyChanges");
});

When(/^the user specified the following harvest traits for all plots$/, async (table) => {
    let hmValue;
    //the user updates the selected plots using selected harvest method
    for (column of table.raw()[0]) {
        switch (column) {
            case "Harvest Date":
                let hvValue = table.hashes()[0][column];
                await hmPage.setValue("#harvest-date-picker", hvValue);
                break;
            case "Harvest Method":
                await hmPage.click("@selectHarvestMethod");
                await hmPage.waitForElementPresent("@harvestMethodDropdown");
                hmValue = table.hashes()[0][column];
                //harvestMethodValue =  hmValue;
                //select the Harvest Method
                const setHarvestMethod = {
                    selector: `//*[@id='select2-harvest-method-select-results']/li[contains(text(),'${hmValue}')]`,
                    locateStrategy: "xpath",
                };
                await hmPage.click(setHarvestMethod);
                break;
            case "Numeric Variables":
                let numValue = table.hashes()[0][column];
                //if numValue is NA, skip because Bulk has no numeric variable
                if (numValue != "NA" && hmValue != "Individual ear") {
                    await hmPage.setValue("#no-of-plants-input", numValue);
                } else if (numValue != "NA" && hmValue == "Individual ear") {
                    await hmPage.setValue("#no-of-ears-input", numValue);
                }
                break;
        }
    }

    await hmPage.waitForElementVisible("#apply-bulk-all-btn", 2000);
    await hmPage.click("#apply-bulk-all-btn");

    await hmPage.waitForElementPresent("#confirmation_modal > div > div");
    await hmPage.waitForElementVisible("@applyChanges", 2000);
    await hmPage.click("@applyChanges");
});

When(/^the user creates seeds, packages, and germplasm$/, async () => {
    //user validates harvest data
    hmPage.pause(2000);
    await client.execute("window.scrollTo(0,0);");
    await hmPage.assert.visible("@nextStepButton");
    await hmPage.click("@nextStepButton");
    await hmPage.waitForElementVisible("@validateHarvestDataButton");
    await hmPage.click("@validateHarvestDataButton");
    //user creates seedlot
    hmPage.pause(2000);
    await client.execute("window.scrollTo(0,0);");
    await hmPage.expect.elements("@totalCount").count.to.equal(10);
    await client.execute("window.scrollTo(0,0);");
    hmPage.pause(2000);
    await hmPage.click("@createSeedlotsButton");
    await hmPage.waitForElementVisible("@confirmSeedlotCreation");
    await hmPage.click("@confirmSeedlotCreation");

});

When(/^the progress is displayed in the notification bell$/, async () => {
    //a notification appears
    await hmPage.waitForElementVisible("@notifBadge", 8000, 2000);
    await hmPage.waitForElementPresent("@notifButton", 3000);
    await hmPage.pause(2000);
    await hmPage.click("@notifButton");
    await hmPage.waitForElementVisible("@notifResult", 2000);
    await hmPage.click("@notifResult", 2000);
    await hmPage.waitForElementVisible("@seedlotBrowserVerify");
});

When(/^the create tab is emptied$/, async () => {
    //click create tab
    await hmPage.click("@createTab");
    await hmPage.waitForElementVisible("@validateHarvestDataButton");
    //assert browser is empty
    await hmPage.assert.containsText("@createBrowserEmpty", "No results found.");
});

When(/^the user bulk deletes the non-committed harvest data for selected plots$/, async () => {
    //verify there are uncommitted traits
    await hmPage.expect.elements("@totalUncommitted").count.to.equal(10);
    //user bulks delete
    await hmPage.click("@selectAll");
    await hmPage.pause(2000);
    await hmPage.click("@selectAll");
    await hmPage.waitForElementVisible("@totalSelectedItems", 2000);
    await deleteSelectedHarvestData();
}
);

When(/^the "([^"]*)" harvest data are deleted$/, async (args1) => {
    if (args1 == "non-committed") {
        await hmPage.expect.elements("@totalUncommitted").count.to.equal(10);
        await hmPage.expect.elements("@emptyHarvestMethod").count.to.equal(10);
    }
});

When(/^the user bulk deletes the non-committed harvest data for all plots$/, async () => {
    //verify there are uncommitted traits
    await hmPage.expect.elements("@totalUncommitted").count.to.equal(10);
    //user bulks delete all
    await resetHarvestData();
}
);

When(/^a success message is shown$/, async () => {
    //verify a success message is shown
    await hmPage.expect.element("@successToast").to.be.present;
});

When(/^the harvest data for all plots is displayed in the plot data browser$/, async (table) => {
    let harvestMethod;

    //the user updates the selected plots using selected harvest method
    for (column of table.raw()[0]) {
        switch (column) {
            case "Harvest Method":
                harvestMethod = table.hashes()[0][column];
                break;
        }
    }

    if (harvestMethod == "Bulk" || harvestMethod == "Individual ear" || harvestMethod == "Single Plant Selection" || harvestMethod == "Selected Bulk" || harvestMethod == "Individual Spike") {
        //toggle show hide observation data
        await hmPage.click("@showHideData");
        await hmPage.waitForElementVisible("@showHideDataModal");
        //filter results
        await hmPage.click("@variables");
        await hmPage.click("@selectAllVariable");
        await hmPage.click("@applyObservationData");
        //filter germplasm state = not_fixed
        await hmPage.click("@germplasmState");
        await hmPage.waitForElementVisible("@germplasmStateResults");
        await hmPage.click("@germplasmStatNotFixed");
        //verify no result
        hmPage.pause(2000);
        await hmPage.expect.element("@resultShowHide").to.be.present;
    }
    //clear filter
    hmPage.pause(2000);
    await hmPage.click("#clear");

    //clear data
    await hmPage.waitForElementVisible("@bulkDelete", 3000);
    await hmPage.click("@bulkDelete");
    await hmPage.waitForElementVisible("@uncommittedData");
    await hmPage.click("@uncommittedData");
    await hmPage.click("@committedData");
    await hmPage.click("@selectAllTraits");

    //click Delete all
    hmPage.pause(2000);
    await hmPage.click("@deleteAll");

    //confirm delete
    await hmPage.waitForElementVisible("@confirmDelete");
    await hmPage.click("@confirmDelete");

    //WORKAROUND
    //wait until delete is finished
    hmPage.pause(2000);
    await hmPage.click("@endDelete");

    //reset button
    //hmPage.pause(5000);
    await hmPage.waitForElementVisible("@resetGrid", 5000)
    await hmPage.click("@resetGrid");


});

/* REFACTOR END */


Then(/^the "([^"]*)" harvest for all plots is displayed in the plot data browser$/, async (args1) => {
    if (args1 == "Bulk" || args1 == "Individual") {
        //toggle show hide observation data
        await hmPage.click("@showHideData");
        await hmPage.waitForElementVisible("@showHideDataModal");
        //filter results
        await hmPage.click("@variables");
        await hmPage.click("@selectAllVariable");
        await hmPage.click("@applyObservationData");
        //verify no result
        await hmPage.expect.element("@resultShowHide").to.be.present;
    }

    //clear filter
    hmPage.pause(1000);
    await hmPage.click("@showHideSummaryClearAll");
    hmPage.pause(1000);
    await hmPage.click("@showHideSummaryClearAll");

    //clear data
    await hmPage.waitForElementVisible("@bulkDelete", 3000);
    await hmPage.click("@bulkDelete");
    await hmPage.waitForElementVisible("@uncommittedData");
    await hmPage.click("@uncommittedData");
    await hmPage.click("@committedData");
    await hmPage.click("@selectAllTraits");

    //click Delete all
    hmPage.pause(2000);
    await hmPage.click("@deleteAll");

    //confirm delete
    await hmPage.waitForElementVisible("@confirmDelete");
    await hmPage.click("@confirmDelete");

    //WORKAROUND
    //wait until delete is finished
    hmPage.pause(2000);
    await hmPage.click("@endDelete");
    //await hmPage.waitForElementNotPresent("@bulkDeleteProcess");

    //reset button
    hmPage.pause(5000);
    await hmPage.click("@resetGrid");
    hmPage.pause(3000);
}
);

When(/^the user deletes selected records$/, async () => {
    //user clicks all records
    hmPage.pause(4000);
    await hmPage.click("@checkAllRecords");
    //verify there are selected records
    hmPage.pause(2000);
    await hmPage.waitForElementVisible("@selectedItemSummary");
    //delete
    await hmPage.click("@deletePackageBulk");
    await hmPage.waitForElementVisible("@deletePackageModal");
    await hmPage.click("@deletePackage");
    //wait for deletion to be completed
    hmPage.pause(2000);
    await hmPage.waitForElementNotPresent("@deletePackageProgress");
    //verify there is success toast
    hmPage.waitForElementVisible("@deleteSuccess");
});

When(/^the selected packages are deleted$/, async () => {
    await hmPage.waitForElementVisible("@emptyManageBrowser");

    //reset data
    hmPage.pause(2000);
    await client.execute("window.scrollTo(0,0);");
    hmPage.pause(2000);
    await hmPage.waitForElementVisible("@harvestDataTab");
    await hmPage.click("@harvestDataTab");
    //hmPage.pause(2000);
    await hmPage.waitForElementVisible("@bulkDelete");

    await resetHarvestData();
});

When(/^for testing$/, async () => {
    hmPage.pause(5000);
});

When(/^the plot data browser is emptied$/, async () => {
    await hmPage.waitForElementVisible("@emptyBrowser");
});

When(/^the user clicks Reset Grid$/, async () => {
    //user clicks grid
    await hmPage.waitForElementVisible("@resSummary");
    await hmPage.pause(3000);
    await hmPage.click("@resetGrid");
});

When(/^the updates for "([^"]*)" are reverted$/, async (args1) => {
    //verify plot count
    await hmPage.waitForElementVisible("@resSummary");
});

When(/^the user exports data in "([^"]*)" format$/, async (args1) => {
    //click export button
    hmPage.waitForElementVisible("@exportButton");
    hmPage.click("@exportButton");
    hmPage.waitForElementVisible("@exportDropdown");

    //export in HTML format
    if (args1 == "HTML") {
        await hmPage.waitForElementPresent("@exportHTML");
        await hmPage.click("@exportHTML");
    } else if (args1 == "CSV") {
        await hmPage.waitForElementPresent("@exportCSV");
        await hmPage.click("@exportCSV");
    } else if (args1 == "Text") {
        await hmPage.waitForElementPresent("@exportText");
        await hmPage.click("@exportText");
    } else if (args1 == "Excel 95") {
        await hmPage.waitForElementPresent("@exportExcel95");
        await hmPage.click("@exportExcel95");
    } else if (args1 == "Excel 2007") {
        await hmPage.waitForElementPresent("@exportExcel2007");
        await hmPage.click("@exportExcel2007");
    }
});

When(/^the "([^"]*)" file is exported$/, async (args1) => {
    //verify if file is downloaded
    if (args1 == "HTML") {
        var holder = process.cwd() + "/downloads" + "Packages.html";
        fs.existsSync(holder);
    } else if (args1 == "CSV") {
        var holder = process.cwd() + "/downloads" + "Packages.csv";
        fs.existsSync(holder);
    } else if (args1 == "Text") {
        var holder = process.cwd() + "/downloads" + "Packages.txt";
        fs.existsSync(holder);
    } else if (args1 == "Excel 95") {
        var holder = process.cwd() + "/downloads" + "Packages.xls";
        fs.existsSync(holder);
    } else if (args1 == "Excel 2007") {
        var holder = process.cwd() + "/downloads" + "Packages.xlsx";
        fs.existsSync(holder);
    }
});


When(/^the query parameters are emptied$/, async () => {
    await hmPage.waitForElementVisible("@locCodeNote");
    await hmPage.waitForElementNotPresent("@expLocDetails");
});

When(/^the user clicks clear search parameters$/, async () => {
    //check if query browser is open
    client.isVisible("#reset-btn", (res) => {
        console.log(res.value);
        if (res.value === false) {
            hmPage.click("@collapsibleHeader");
            hmPage.waitForElementVisible("@clearSearchParams");
        } else {
            console.log("open, do nothing");
        }
    });

    //click clear search parameters
    await hmPage.click("@clearSearchParams");
});

When(/^the plots are loaded and updated$/, async () => {
    await hmPage.waitForElementVisible("@resSummary");
});

When(/^the user checks the query results$/, async () => {
    await hmPage.waitForElementVisible("@resSummary");
});

When(/^the user sees the germplasm state column$/, async () => {
    await hmPage.waitForElementVisible("@resSummary");
    await hmPage.waitForElementVisible("@germplasmColumnFilter");
});

When(
    /^the user filters the germplasm state using "([^"]*)"$/,
    async (args1) => {
        await hmPage.waitForElementVisible("@resSummary");
        await hmPage.waitForElementVisible("@resSummary");
        await hmPage.waitForElementVisible("@germplasmColumnFilter");

        if (args1 == "non-fixed") {
            console.log("flag 1");
            await hmPage.click("@germplasmFilter");
            console.log("flag 2");
            hmPage.pause(3000);
            await hmPage.waitForElementVisible("@germplasmNotFixed");
            await hmPage.setValue(
                "#body > span.select2-container.select2-container--krajee.select2-container--open > span > span.select2-search.select2-search--dropdown > input",
                "not_fixed"
            );
        } else if (args1 == "fixed") {
            //insert code
        }
        console.log("flag 3");
        hmPage.pause(2000);
        //assert no result found
        await hmPage.assert.visible("@emptyBrowser");
    }
);

When(/^the user filters the plot number$/, async () => {
    await hmPage.waitForElementVisible("@resSummary");

    //filter the plot number using 1
    await hmPage.setValue(
        "#search-harvest-grid-table-con-filters > td:nth-child(1) > input",
        "1"
    );

    await hmPage.pause(3000);

    //verify the summary
    await hmPage.getTagName(
        "css selector",
        "#search-harvest-grid-table-con-container > table > tbody > tr",
        function (result) {
            console.log("result", result);
            console.log("result count: " + result.value.length); //(if there are 3 li, here should be a count of 3)

            //assert result contains 1
            hmPage.assert.equal(result.value.length, 2);
            console.log("pass");
        }
    );
});

When(/^the user clicks the ShowHide Observation Data button$/, async () => {
    await hmPage.click("@showHideButton");
    //assert modal is displayed
    await hmPage.waitForElementVisible("@showHideModal");
});

When(
    /^"([^"]*)" seeds, packages, and germplasm are created$/,
    async (args1) => {
        //reset harvest data
        hmPage.pause(3000);
        await resetHarvestData();
        //hmPage.pause(5000);

        if (args1 == "Bulk") {
            //the user updates the selected plots using "Bulk" method
            //hmPage.pause(5000);
            await hmPage.waitForElementVisible("@selectAll");
            await hmPage.click("@selectAll");
            await hmPage.waitForElementVisible("@bulkUpdateButton");
            await hmPage.expect.element("@bulkUpdateButton").visible.after(3000);
            await hmPage.click("@bulkUpdateButton");
            await hmPage.waitForElementVisible("@bulkUpdateForm");
            await hmPage.setValue("#harvest-date-picker", "2021-06-11");
            await hmPage.click("@selectHarvestMethod");
            await hmPage.click("@setBulk");
            await hmPage.click("@applyToSelected");
        } else if (args1 == "Selected Bulk") {
            //set harvest method as modified bulk
            //hmPage.pause(5000);
            await hmPage.waitForElementVisible("@selectAll");
            await hmPage.click("@selectAll");
            await hmPage.waitForElementVisible("@bulkUpdateButton");
            await hmPage.expect.element("@bulkUpdateButton").visible.after(3000);
            await hmPage.click("@bulkUpdateButton");
            await hmPage.waitForElementVisible("@bulkUpdateForm");
            await hmPage.setValue("#harvest-date-picker", "2021-02-11");
            await hmPage.click("@selectHarvestMethod");
            await hmPage.click("@setSelectedBulk");
            await hmPage.setValue("@noOfPlants", 10);
            await hmPage.click("@applyToSelected");
        } else if (args1 == "Individual Spike") {
            //set harvest method as individual spike
            //hmPage.pause(5000);
            await hmPage.waitForElementVisible("@selectAll");
            await hmPage.click("@selectAll");
            await hmPage.waitForElementVisible("@bulkUpdateButton");
            await hmPage.expect.element("@bulkUpdateButton").visible.after(3000);
            await hmPage.click("@bulkUpdateButton");
            await hmPage.waitForElementVisible("@bulkUpdateForm");
            await hmPage.setValue("#harvest-date-picker", "2021-02-11");
            await hmPage.click("@selectHarvestMethod");
            await hmPage.click("@setIndividualSpike");
            await hmPage.setValue("@noOfPlants", 10);
            await hmPage.click("@applyToSelected");
        } else if (args1 == "Single Plant Selection") {
            //set harvest method as single plant selection
            await hmPage.waitForElementVisible("@selectAll");
            await hmPage.click("@selectAll");
            await hmPage.waitForElementVisible("@bulkUpdateButton");
            await hmPage.expect.element("@bulkUpdateButton").visible.after(3000);
            await hmPage.click("@bulkUpdateButton");
            await hmPage.waitForElementVisible("@bulkUpdateForm");
            await hmPage.setValue("#harvest-date-picker", "2021-02-11");
            await hmPage.click("@selectHarvestMethod");
            await hmPage.click("@setSinglePlantSelection");
            await hmPage.setValue("@noOfPlants", 10);
            await hmPage.click("@applyToSelected");
        }

        hmPage.pause(2000);

        client.isVisible("#allow_overwrite", (res) => {
            console.log(res.value);
            if (res.value === true) {
                console.log("overwrite");
                hmPage.click("@overwriteChanges");
            } else {
                console.log("apply");
                hmPage.click("@applyChanges");
            }
        });

        console.log("flag1");

        //user validates harvest data
        await client.execute("window.scrollTo(0,0);");
        hmPage.pause(2000);
        await hmPage.assert.visible("@nextStepButton");
        hmPage.pause(2000);
        await hmPage.click("@nextStepButton");
        console.log("flag2");
        await hmPage.waitForElementVisible("@validateHarvestDataButton");
        console.log("flag3");
        await hmPage.click("@validateHarvestDataButton");

        //user creates seedlot
        hmPage.pause(2000);
        await client.execute("window.scrollTo(0,0);");
        await hmPage.expect.elements("@totalCount").count.to.equal(10);
        await client.execute("window.scrollTo(0,0);");
        hmPage.pause(2000);
        await hmPage.click("@createSeedlotsButton");
        await hmPage.waitForElementVisible("@confirmSeedlotCreation");
        await hmPage.click("@confirmSeedlotCreation");

        console.log("flag4");

        //a notification appears
        await hmPage.waitForElementVisible("@notifBadge");
        hmPage.pause(5000);
        await hmPage.click("@notifButton");
        hmPage.pause(2000);
        await hmPage.waitForElementVisible("@notifResult");
        await hmPage.click("@notifResult");
        hmPage.waitForElementNotVisible("@notifResult");
        await hmPage.waitForElementVisible("@seedlotBrowserVerify");
    }
);





When(/^the user goes back to the harvest data tab$/, async () => {
    //click harvest data tab
    await hmPage.click("@harvestDataTab");
    await hmPage.waitForElementVisible("@bulkDelete");
});

When(
    /^the committed data is displayed in highlighted green background$/,
    async () => {
        //assert data are committed
        //await hmPage.assert.containsText("@createBrowserEmpty", 'No results found.');
        await hmPage.expect.elements("@totalCommitted").count.to.equal(10);
        await hmPage.expect.elements("@committedBulk").count.to.equal(10);
        await hmPage.expect.elements("@committedDate").count.to.equal(10);
    }
);


When(/^the plots for "([^"]*)" seedlot creation are loaded$/, async (args1) => {
    if (args1 == "Maize Bulk") {
        //input query parameters (program: KE Maize Breeding Program (KE), experiment: A-11)
        await hmPage.waitForElementVisible("@harvestManagerFavorite");
        hmPage.pause(2000);
        await hmPage.click("@queryProgram");
        await hmPage.waitForElementVisible("@programMaize");
        await hmPage.click("@programMaize");
        await hmPage.click("@queryParamExperiment");
        hmPage.pause(3000);
        console.log("HERE1");
        await hmPage.waitForElementVisible("@expDropdown");
        await client.setValue(
            "body > span.select2-container.select2-container--krajee.select2-container--open > span > span.select2-search.select2-search--dropdown > input",
            "KE-SEM-2019-A-001"
        );
        hmPage.pause(2000);
        console.log("HERE2");
        await hmPage.waitForElementVisible("@KEBulkExperiment");
        await hmPage.click("@KEBulkExperiment");
        console.log("HERE3");
        //assert experiment location details is displayed
        hmPage.pause(2000);
        await hmPage.waitForElementVisible("@expDetailExpType");
        await hmPage.waitForElementVisible("@expDetailSite");
        await hmPage.waitForElementVisible("@expDetailSteward");
        //load plots
        hmPage.pause(4000);
        await hmPage.click("@loadPlots");
        //wait for query results
        hmPage.pause(5000);
        await hmPage.waitForElementNotPresent("@emptyQueryResults");
        await hmPage.waitForElementVisible("@KEBulkResults");
    } else if (args1 == "Wheat Bulk") {
        //input query parameters (program: BW Wheat Breeding Program (BW), experiment: F1SIMPLEBW-1)
        await hmPage.waitForElementVisible("@harvestManagerFavorite");
        hmPage.pause(2000);
        await hmPage.click("@queryProgram");
        await hmPage.waitForElementVisible("@programWheat");
        await hmPage.click("@programWheat");
        await hmPage.click("@queryParamExperiment");
        hmPage.pause(2000);
        await hmPage.waitForElementVisible("@expDropdown");
        await client.setValue(
            "body > span.select2-container.select2-container--krajee.select2-container--open > span > span.select2-search.select2-search--dropdown > input",
            "BW-F1-2014-A-002"
        );
        hmPage.pause(2000);
        await hmPage.waitForElementVisible("@wheatQueryExperiment");
        await hmPage.click("@wheatQueryExperiment");
        //assert experiment location details is displayed
        hmPage.pause(2000);
        await hmPage.waitForElementVisible("@wheatDetailExpType");
        await hmPage.waitForElementVisible("@wheatDetailSite");
        await hmPage.waitForElementVisible("@wheatDetailSteward");
        //load plots
        hmPage.pause(4000);
        await hmPage.click("@loadPlots");
        //wait for query results
        hmPage.pause(5000);
        await hmPage.waitForElementNotPresent("@emptyQueryResults");
        await hmPage.waitForElementVisible("@WheatBulkQueryResults");
    } else if (args1 == "Maize Individual Ear") {
        //input query parameters (program: KE Maize Breeding Program (KE), experiment: A-11)
        await hmPage.waitForElementVisible("@harvestManagerFavorite");
        hmPage.pause(2000);
        await hmPage.click("@queryProgram");
        await hmPage.waitForElementVisible("@programMaize");
        await hmPage.click("@programMaize");
        await hmPage.click("@queryParamExperiment");
        hmPage.pause(3000);
        await hmPage.waitForElementVisible("@expDropdown");
        await client.setValue(
            "body > span.select2-container.select2-container--krajee.select2-container--open > span > span.select2-search.select2-search--dropdown > input",
            "KE-F4-2019-A-001"
        );
        hmPage.pause(2000);
        await hmPage.waitForElementVisible("@KEIndividualEarExperiment");
        await hmPage.click("@KEIndividualEarExperiment");
        //assert experiment location details is displayed
        hmPage.pause(2000);
        await hmPage.waitForElementVisible("@expDetailExpType");
        await hmPage.waitForElementVisible("@expDetailSite");
        await hmPage.waitForElementVisible("@KEIEarexpDetailSteward");
        //load plots
        hmPage.pause(4000);
        await hmPage.click("@loadPlots");
        //wait for query results
        hmPage.pause(5000);
        await hmPage.waitForElementNotPresent("@emptyQueryResults");
        await hmPage.waitForElementVisible("@KEIndividualEarResults");
    } else if (args1 == "Wheat Single Plant Selection") {
        //input query parameters (program: BW Wheat Breeding Program (BW), experiment: F1SIMPLEBW-1)
        await hmPage.waitForElementVisible("@harvestManagerFavorite");
        hmPage.pause(2000);
        await hmPage.click("@queryProgram");
        await hmPage.waitForElementVisible("@programWheat");
        await hmPage.click("@programWheat");
        await hmPage.click("@queryParamExperiment");
        hmPage.pause(2000);
        await hmPage.waitForElementVisible("@expDropdown");
        await client.setValue(
            "body > span.select2-container.select2-container--krajee.select2-container--open > span > span.select2-search.select2-search--dropdown > input",
            "BW-F1-2014-A-003"
        );
        hmPage.pause(2000);
        await hmPage.waitForElementVisible("@wheatSinglePlantExperiment");
        await hmPage.click("@wheatSinglePlantExperiment");
        //assert experiment location details is displayed
        hmPage.pause(2000);
        await hmPage.waitForElementVisible("@wheatDetailExpType");
        await hmPage.waitForElementVisible("@wheatDetailSite");
        await hmPage.waitForElementVisible("@wheatSPSDetailSteward");
        //load plots
        hmPage.pause(4000);
        await hmPage.click("@loadPlots");
        //wait for query results
        hmPage.pause(5000);
        await hmPage.waitForElementNotPresent("@emptyQueryResults");
        await hmPage.waitForElementVisible("@WheatBulkQueryResults");
    } else if (args1 == "Wheat Selected Bulk") {
        //input query parameters (program: BW Wheat Breeding Program (BW), experiment: F1SIMPLEBW-1)
        await hmPage.waitForElementVisible("@harvestManagerFavorite");
        hmPage.pause(2000);
        await hmPage.click("@queryProgram");
        await hmPage.waitForElementVisible("@programWheat");
        await hmPage.click("@programWheat");
        await hmPage.click("@queryParamExperiment");
        hmPage.pause(3000);
        await hmPage.waitForElementVisible("@expDropdown");
        await client.setValue(
            "body > span.select2-container.select2-container--krajee.select2-container--open > span > span.select2-search.select2-search--dropdown > input",
            "BW-F1-2014-A-004"
        );
        hmPage.pause(2000);
        await hmPage.waitForElementVisible("@wheatSelectedBulkExperiment");
        await hmPage.click("@wheatSelectedBulkExperiment");
        //assert experiment location details is displayed
        hmPage.pause(2000);
        await hmPage.waitForElementVisible("@wheatDetailExpType");
        await hmPage.waitForElementVisible("@wheatDetailSite");
        await hmPage.waitForElementVisible("@wheatSBDetailSteward");
        //load plots
        hmPage.pause(4000);
        await hmPage.click("@loadPlots");
        //wait for query results
        hmPage.pause(5000);
        await hmPage.waitForElementNotPresent("@emptyQueryResults");
        await hmPage.waitForElementVisible("@WheatBulkQueryResults");
    } else if (args1 == "Wheat Individual Spike") {
        //input query parameters (program: BW Wheat Breeding Program (BW), experiment: F1SIMPLEBW-1)
        await hmPage.waitForElementVisible("@harvestManagerFavorite");
        hmPage.pause(2000);
        await hmPage.click("@queryProgram");
        await hmPage.waitForElementVisible("@programWheat");
        hmPage.pause(2000);
        await hmPage.click("@programWheat");
        await hmPage.click("@queryParamExperiment");
        hmPage.pause(3000);
        await hmPage.waitForElementVisible("@expDropdown");
        await client.setValue(
            "body > span.select2-container.select2-container--krajee.select2-container--open > span > span.select2-search.select2-search--dropdown > input",
            "BW-F1-2014-A-005"
        );
        hmPage.pause(2000);
        await hmPage.waitForElementVisible("@wheatIndividualSpikeExperiment");
        await hmPage.click("@wheatIndividualSpikeExperiment");
        //assert experiment location details is displayed
        hmPage.pause(2000);
        await hmPage.waitForElementVisible("@wheatDetailExpType");
        await hmPage.waitForElementVisible("@wheatDetailSite");
        await hmPage.waitForElementVisible("@wheatISDetailSteward");
        //load plots
        hmPage.pause(4000);
        await hmPage.click("@loadPlots");
        //wait for query results
        hmPage.pause(5000);
        await hmPage.waitForElementNotPresent("@emptyQueryResults");
        await hmPage.waitForElementVisible("@WheatBulkQueryResults");
    }
});

When(
    /^the crosses for "([^"]*)" seedlot creation are loaded$/,
    async (args1) => {
        if (args1 == "Maize Bulk") {
            //input query parameters (program: KE Maize Breeding Program (KE), experiment: A-11)
            await hmPage.waitForElementVisible("@harvestManagerFavorite");
            hmPage.pause(2000);
            await hmPage.click("@queryProgram");
            await hmPage.waitForElementVisible("@programMaize");
            await hmPage.click("@programMaize");
            await hmPage.click("@queryParamExperiment");
            hmPage.pause(3000);
            console.log("HERE1");
            await hmPage.waitForElementVisible("@expDropdown");
            await client.setValue(
                "body > span.select2-container.select2-container--krajee.select2-container--open > span > span.select2-search.select2-search--dropdown > input",
                "MAIZE ICN All Selfing - not_fixed"
            );
            hmPage.pause(2000);
            console.log("HERE2");
            await hmPage.waitForElementVisible("@KEBulkCrossExperiment");
            await hmPage.click("@KEBulkCrossExperiment");
            console.log("HERE3");
            //assert experiment location details is displayed
            hmPage.pause(2000);
            await hmPage.waitForElementVisible("@maizeICNExpType");
            await hmPage.waitForElementVisible("@maizeICNSite");
            await hmPage.waitForElementVisible("@maizeICNSteward");
            //load plots
            hmPage.pause(4000);
            //await hmPage.click("@loadPlots");
            await hmPage.click("@loadCrosses");
            //wait for query results
            hmPage.pause(5000);
            await hmPage.waitForElementNotPresent("@emptyQueryResults");
            await hmPage.waitForElementVisible("@KEBulkResults");
        } else if (args1 == "Wheat Bulk") {
            //input query parameters (program: BW Wheat Breeding Program (BW), experiment: F1SIMPLEBW-1)
            await hmPage.waitForElementVisible("@harvestManagerFavorite");
            hmPage.pause(2000);
            await hmPage.click("@queryProgram");
            await hmPage.waitForElementVisible("@programWheat");
            await hmPage.click("@programWheat");
            await hmPage.click("@queryParamExperiment");
            hmPage.pause(2000);
            await hmPage.waitForElementVisible("@expDropdown");
            await client.setValue(
                "body > span.select2-container.select2-container--krajee.select2-container--open > span > span.select2-search.select2-search--dropdown > input",
                "BW-F1-2014-A-002"
            );
            hmPage.pause(2000);
            await hmPage.waitForElementVisible("@wheatQueryExperiment");
            await hmPage.click("@wheatQueryExperiment");
            //assert experiment location details is displayed
            hmPage.pause(2000);
            await hmPage.waitForElementVisible("@wheatDetailExpType");
            await hmPage.waitForElementVisible("@wheatDetailSite");
            await hmPage.waitForElementVisible("@wheatDetailSteward");
            //load plots
            hmPage.pause(4000);
            await hmPage.click("@loadCrosses");
            //wait for query results
            hmPage.pause(5000);
            await hmPage.waitForElementNotPresent("@emptyQueryResults");
            await hmPage.waitForElementVisible("@WheatBulkQueryResults");
        } else if (args1 == "Maize Individual Ear") {
            //input query parameters (program: KE Maize Breeding Program (KE), experiment: A-11)
            await hmPage.waitForElementVisible("@harvestManagerFavorite");
            hmPage.pause(2000);
            await hmPage.click("@queryProgram");
            await hmPage.waitForElementVisible("@programMaize");
            await hmPage.click("@programMaize");
            await hmPage.click("@queryParamExperiment");
            hmPage.pause(3000);
            await hmPage.waitForElementVisible("@expDropdown");
            await client.setValue(
                "body > span.select2-container.select2-container--krajee.select2-container--open > span > span.select2-search.select2-search--dropdown > input",
                "KE-F4-2019-A-001"
            );
            hmPage.pause(2000);
            await hmPage.waitForElementVisible("@KEIndividualEarExperiment");
            await hmPage.click("@KEIndividualEarExperiment");
            //assert experiment location details is displayed
            hmPage.pause(2000);
            await hmPage.waitForElementVisible("@expDetailExpType");
            await hmPage.waitForElementVisible("@expDetailSite");
            await hmPage.waitForElementVisible("@KEIEarexpDetailSteward");
            //load plots
            hmPage.pause(4000);
            await hmPage.click("@loadCrosses");
            //wait for query results
            hmPage.pause(5000);
            await hmPage.waitForElementNotPresent("@emptyQueryResults");
            await hmPage.waitForElementVisible("@KEIndividualEarResults");
        } else if (args1 == "Wheat Single Plant Selection") {
            //input query parameters (program: BW Wheat Breeding Program (BW), experiment: F1SIMPLEBW-1)
            await hmPage.waitForElementVisible("@harvestManagerFavorite");
            hmPage.pause(2000);
            await hmPage.click("@queryProgram");
            await hmPage.waitForElementVisible("@programWheat");
            await hmPage.click("@programWheat");
            await hmPage.click("@queryParamExperiment");
            hmPage.pause(2000);
            await hmPage.waitForElementVisible("@expDropdown");
            await client.setValue(
                "body > span.select2-container.select2-container--krajee.select2-container--open > span > span.select2-search.select2-search--dropdown > input",
                "BW-F1-2014-A-003"
            );
            hmPage.pause(2000);
            await hmPage.waitForElementVisible("@wheatSinglePlantExperiment");
            await hmPage.click("@wheatSinglePlantExperiment");
            //assert experiment location details is displayed
            hmPage.pause(2000);
            await hmPage.waitForElementVisible("@wheatDetailExpType");
            await hmPage.waitForElementVisible("@wheatDetailSite");
            await hmPage.waitForElementVisible("@wheatSPSDetailSteward");
            //load plots
            hmPage.pause(4000);
            await hmPage.click("@loadCrosses");
            //wait for query results
            hmPage.pause(5000);
            await hmPage.waitForElementNotPresent("@emptyQueryResults");
            await hmPage.waitForElementVisible("@WheatBulkQueryResults");
        } else if (args1 == "Wheat Selected Bulk") {
            //input query parameters (program: BW Wheat Breeding Program (BW), experiment: F1SIMPLEBW-1)
            await hmPage.waitForElementVisible("@harvestManagerFavorite");
            hmPage.pause(2000);
            await hmPage.click("@queryProgram");
            await hmPage.waitForElementVisible("@programWheat");
            await hmPage.click("@programWheat");
            await hmPage.click("@queryParamExperiment");
            hmPage.pause(3000);
            await hmPage.waitForElementVisible("@expDropdown");
            await client.setValue(
                "body > span.select2-container.select2-container--krajee.select2-container--open > span > span.select2-search.select2-search--dropdown > input",
                "BW-F1-2014-A-004"
            );
            hmPage.pause(2000);
            await hmPage.waitForElementVisible("@wheatSelectedBulkExperiment");
            await hmPage.click("@wheatSelectedBulkExperiment");
            //assert experiment location details is displayed
            hmPage.pause(2000);
            await hmPage.waitForElementVisible("@wheatDetailExpType");
            await hmPage.waitForElementVisible("@wheatDetailSite");
            await hmPage.waitForElementVisible("@wheatSBDetailSteward");
            //load plots
            hmPage.pause(4000);
            await hmPage.click("@loadCrosses");
            //wait for query results
            hmPage.pause(5000);
            await hmPage.waitForElementNotPresent("@emptyQueryResults");
            await hmPage.waitForElementVisible("@WheatBulkQueryResults");
        } else if (args1 == "Wheat Individual Spike") {
            //input query parameters (program: BW Wheat Breeding Program (BW), experiment: F1SIMPLEBW-1)
            await hmPage.waitForElementVisible("@harvestManagerFavorite");
            hmPage.pause(2000);
            await hmPage.click("@queryProgram");
            await hmPage.waitForElementVisible("@programWheat");
            hmPage.pause(2000);
            await hmPage.click("@programWheat");
            await hmPage.click("@queryParamExperiment");
            hmPage.pause(3000);
            await hmPage.waitForElementVisible("@expDropdown");
            await client.setValue(
                "body > span.select2-container.select2-container--krajee.select2-container--open > span > span.select2-search.select2-search--dropdown > input",
                "BW-F1-2014-A-005"
            );
            hmPage.pause(2000);
            await hmPage.waitForElementVisible("@wheatIndividualSpikeExperiment");
            await hmPage.click("@wheatIndividualSpikeExperiment");
            //assert experiment location details is displayed
            hmPage.pause(2000);
            await hmPage.waitForElementVisible("@wheatDetailExpType");
            await hmPage.waitForElementVisible("@wheatDetailSite");
            await hmPage.waitForElementVisible("@wheatISDetailSteward");
            //load plots
            hmPage.pause(4000);
            await hmPage.click("@loadCrosses");
            //wait for query results
            hmPage.pause(5000);
            await hmPage.waitForElementNotPresent("@emptyQueryResults");
            await hmPage.waitForElementVisible("@WheatBulkQueryResults");
        }
    }
);

When(
    /^the user updates the selected plots using "([^"]*)" method$/,
    async (args1) => {
        //reset any harvest data
        hmPage.pause(3000);
        await resetHarvestData();

        if (args1 == "Bulk") {
            //perform select all
            await hmPage.waitForElementVisible("@selectAll");
            await hmPage.click("@selectAll");

            console.log("here");

            //set harvest method as bulk
            await hmPage.click("@bulkUpdateButton");
            await hmPage.waitForElementVisible("@bulkUpdateForm");
            await hmPage.setValue("#harvest-date-picker", "2021-02-11");
            await hmPage.click("@selectHarvestMethod");
            await hmPage.click("@setBulk");
            await hmPage.click("@applyToSelected");
        } else if (args1 == "Individual Ear") {
            //perform select all
            //await hmPage.click("@nextPage");
            //hmPage.pause(3000);
            await hmPage.waitForElementVisible("@selectAll");
            await hmPage.click("@selectAll");

            //set harvest method as individual ear
            await hmPage.click("@bulkUpdateButton");
            await hmPage.waitForElementVisible("@bulkUpdateForm");
            await hmPage.setValue("#harvest-date-picker", "2021-02-11");
            await hmPage.click("@selectHarvestMethod");
            await hmPage.click("@setIndividualEar");
            await hmPage.setValue("@noOfEars", 10);
            await hmPage.click("@applyToSelected");
        } else if (args1 == "Single Plant Selection") {
            //perform select all
            //await hmPage.click("@nextPage");
            //hmPage.pause(3000);
            await hmPage.waitForElementVisible("@selectAll");
            await hmPage.click("@selectAll");

            //set harvest method as single plant selection
            await hmPage.click("@bulkUpdateButton");
            await hmPage.waitForElementVisible("@bulkUpdateForm");
            await hmPage.setValue("#harvest-date-picker", "2021-02-11");
            await hmPage.click("@selectHarvestMethod");
            await hmPage.click("@setSinglePlantSelection");
            await hmPage.setValue("@noOfPlants", 10);
            await hmPage.click("@applyToSelected");
        } else if (args1 == "Modified Bulk") {
            //perform select all
            //await hmPage.click("@nextPage");
            //hmPage.pause(3000);
            await hmPage.waitForElementVisible("@selectAll");
            await hmPage.click("@selectAll");

            //set harvest method as modified bulk
            await hmPage.click("@bulkUpdateButton");
            await hmPage.waitForElementVisible("@bulkUpdateForm");
            await hmPage.setValue("#harvest-date-picker", "2021-02-11");
            await hmPage.click("@selectHarvestMethod");
            await hmPage.click("@setSelectedBulk");
            await hmPage.setValue("@noOfPlants", 10);
            await hmPage.click("@applyToSelected");
        } else if (args1 == "Individual Spike") {
            //perform select all
            //await hmPage.click("@nextPage");
            //hmPage.pause(3000);
            await hmPage.waitForElementVisible("@selectAll");
            await hmPage.click("@selectAll");

            //set harvest method as modified bulk
            await hmPage.click("@bulkUpdateButton");
            await hmPage.waitForElementVisible("@bulkUpdateForm");
            await hmPage.setValue("#harvest-date-picker", "2021-02-11");
            await hmPage.click("@selectHarvestMethod");
            await hmPage.click("@setIndividualSpike");
            await hmPage.setValue("@noOfPlants", 10);
            await hmPage.click("@applyToSelected");
        }

        console.log("flag 1");
        hmPage.pause(3000);

        //check if overwrite exists or not
        client.isVisible("#apply_changes", (res) => {
            console.log(res.value);
            if (res.value === false) {
                console.log("OVERWRITE");
                hmPage.click("@overwriteChanges");
            } else {
                console.log("APPLY CHANGES");
                //confirm apply changes
                hmPage.assert.visible("@applyChanges");
                hmPage.click("@applyChanges");
            }
        });

        console.log("flag 2");
        hmPage.pause(2000);

        //verify updated data
        if (args1 == "Bulk") {
            //hmPage.pause(2000);
            //await hmPage.expect.element('@plot1AssertDate').to.have.value.that.equals('2021-02-11');
            //await hmPage.expect.element('@plot1AssertMethod').text.to.contain('Bulk');
            await hmPage.expect.elements("@dateAssert").count.to.equal(10);
            await hmPage.expect.elements("@bulkAssert").count.to.equal(11);
        } else if (args1 == "Individual Ear") {
            await hmPage.expect.elements("@dateAssert").count.to.equal(10);
            await hmPage.expect.elements("@indivdualEarAssert").count.to.equal(11);
            await hmPage.expect.elements("@numOfEarsAssert").count.to.equal(10);
        } else if (args1 == "Single Plant Selection") {
            await hmPage.expect.elements("@dateAssert").count.to.equal(10);
            await hmPage.expect.elements("@singlePlantAssert").count.to.equal(11);
            await hmPage.expect.elements("@numOfPlantsAssert").count.to.equal(10);
        } else if (args1 == "Modified Bulk") {
            await hmPage.expect.elements("@dateAssert").count.to.equal(10);
            await hmPage.expect.elements("@modifiedBulkAssert").count.to.equal(11);
            await hmPage.expect.elements("@numOfPlantsAssert").count.to.equal(10);
        } else if (args1 == "Individual Spike") {
            await hmPage.expect.elements("@dateAssert").count.to.equal(10);
            await hmPage.expect
                .elements("@individualSpikeAssert")
                .count.to.equal(11);
            await hmPage.expect.elements("@numOfPlantsAssert").count.to.equal(10);
        }
    }
);

Then(
    /^the "([^"]*)" data for all plots is displayed in the plot data browser$/,
    async (args1) => {
        if (
            args1 == "Bulk" ||
            args1 == "Single Plant Selection" ||
            args1 == "Selected Bulk" ||
            args1 == "Individual Spike"
        ) {
            //toggle show hide observation data
            await hmPage.click("@showHideData");
            await hmPage.waitForElementVisible("@showHideDataModal");
            //filter results
            await hmPage.click("@variables");
            await hmPage.click("@selectAllVariable");
            await hmPage.click("@applyObservationData");
            //verify no result
            await hmPage.expect.element("@resultShowHide").to.be.present;
        }

        hmPage.pause(2000);

        //clear filter
        await hmPage.click("@clearFilter");
        hmPage.pause(3000);

        //clear data
        await hmPage.waitForElementVisible("@bulkDelete");
        await hmPage.click("@bulkDelete");
        await hmPage.waitForElementVisible("@uncommittedData");
        await hmPage.click("@uncommittedData");
        await hmPage.click("@committedData");
        await hmPage.click("@selectAllTraits");

        //click Delete all
        hmPage.pause(2000);
        await hmPage.click("@deleteAll");

        //confirm delete
        await hmPage.waitForElementVisible("@confirmDelete");
        await hmPage.click("@confirmDelete");

        //WORKAROUND
        //wait until delete is finished
        hmPage.pause(2000);
        await hmPage.click("@endDelete");
        //await hmPage.waitForElementNotPresent("@bulkDeleteProcess");

        //reset button
        hmPage.pause(5000);
        await hmPage.click("@resetGrid");
        hmPage.pause(4000);
        await hmPage.click("@resetGrid");
        hmPage.pause(2000);
    }
);

When(/^the user validates the harvest data$/, async () => {
    //scroll to and click Next button
    //await hmPage.getLocationInView("@nextStepButton");
    await client.execute("window.scrollTo(0,0);");
    hmPage.pause(2000);
    await hmPage.assert.visible("@nextStepButton");
    hmPage.pause(2000);
    await hmPage.click("@nextStepButton");
    //await hmPage.waitForElementVisible("@nextStepButton")
    //verify in seedlot management tab
    await hmPage.waitForElementVisible("@validateHarvestDataButton");
    //click validate harvest data
    await hmPage.click("@validateHarvestDataButton");
});

When(/^the user creates seedlot$/, async () => {
    //verify if there are plots for seedlot creation
    console.log("here");
    await client.execute("window.scrollTo(0,0);");
    await hmPage.expect.elements("@totalCount").count.to.equal(10);
    //create seedlots
    await client.execute("window.scrollTo(0,0);");
    hmPage.pause(2000);
    await hmPage.click("@createSeedlotsButton");
    await hmPage.waitForElementVisible("@confirmSeedlotCreation");
    await hmPage.click("@confirmSeedlotCreation");
    //wait for success message
    //await hmPage.waitForElementVisible('@successMessage');
});

When(/^a notification appears$/, async () => {
    console.log("here1");
    hmPage.pause(7000);
    await hmPage.waitForElementVisible("@notifBadge");
    await hmPage.click("@notifButton");
    hmPage.pause(2000);
    await hmPage.waitForElementVisible("@notifResult");
    await hmPage.click("@notifResult");
    //verify user is in the seedlot management browser
    hmPage.pause(2000);
    await hmPage.waitForElementVisible("@seedlotBrowserVerify");
});

When(/^a notification message appears$/, async () => {
    console.log("here1");
    hmPage.pause(10000);
    console.log("here2");
    await hmPage.waitForElementVisible("@notifBadge");
    await hmPage.click("@notifButton");
    hmPage.pause(2000);
    console.log("here3");
    await hmPage.waitForElementVisible("@notifResult");
    await hmPage.click("@notifResult");
    console.log("here4");
    //verify user is in the seedlot management browser
    hmPage.pause(2000);
    await hmPage.waitForElementVisible("@seedlotBrowserVerify");
});

When(/^a longer notification appears$/, async () => {
    console.log("here1");
    hmPage.pause(20000);
    console.log("here2");
    await hmPage.waitForElementVisible("@notifBadge");
    await hmPage.click("@notifButton");
    await hmPage.waitForElementVisible("@notifResult");
    await hmPage.click("@notifResult");
    //verify user is in the seedlot management browser
    hmPage.pause(4000);
    await hmPage.waitForElementVisible("@seedlotBrowserVerify");
});

When(/^seedlot management browser is displayed$/, async () => {
    //verify user is in the seedlot management browser
    await hmPage.waitForElementVisible("@seedlotBrowserVerify");
});

When(/^the user updates all plots using "([^"]*)" method$/, async (args1) => {
    //reset any harvest data
    //wait resetHarvestData();
    hmPage.pause(3000);
    await resetHarvestData();

    if (args1 == "Bulk") {
        //set harvest method as bulk
        await hmPage.click("@bulkUpdateButton");
        hmPage.pause(3000);
        await hmPage.waitForElementVisible("@bulkUpdateForm");
        await hmPage.setValue("#harvest-date-picker", "2021-02-11");
        await hmPage.click("@selectHarvestMethod");
        await hmPage.click("@setBulk");
        await hmPage.click("@applyToAll");
    } else if (args1 == "Individual Ear") {
        //set harvest method as individual ear
        await hmPage.click("@bulkUpdateButton");
        hmPage.pause(3000);
        await hmPage.waitForElementVisible("@bulkUpdateForm");
        await hmPage.setValue("#harvest-date-picker", "2021-02-11");
        await hmPage.click("@selectHarvestMethod");
        await hmPage.click("@setIndividualEar");
        await hmPage.setValue("@noOfEars", 10);
        await hmPage.click("@applyToAll");
    } else if (args1 == "Single Plant Selection") {
        //set harvest method as single plant selection
        await hmPage.click("@bulkUpdateButton");
        hmPage.pause(3000);
        await hmPage.waitForElementVisible("@bulkUpdateForm");
        await hmPage.setValue("#harvest-date-picker", "2021-02-11");
        await hmPage.click("@selectHarvestMethod");
        await hmPage.click("@setSinglePlantSelection");
        await hmPage.setValue("@noOfPlants", 10);
        await hmPage.click("@applyToAll");
    } else if (args1 == "Selected Bulk") {
        //set harvest method as modified bulk
        await hmPage.click("@bulkUpdateButton");
        hmPage.pause(3000);
        await hmPage.waitForElementVisible("@bulkUpdateForm");
        await hmPage.setValue("#harvest-date-picker", "2021-02-11");
        await hmPage.click("@selectHarvestMethod");
        await hmPage.click("@setSelectedBulk");
        await hmPage.setValue("@noOfPlants", 10);
        await hmPage.click("@applyToAll");
    } else if (args1 == "Individual Spike") {
        //set harvest method as modified bulk
        await hmPage.click("@bulkUpdateButton");
        hmPage.pause(3000);
        await hmPage.waitForElementVisible("@bulkUpdateForm");
        await hmPage.setValue("#harvest-date-picker", "2021-02-11");
        await hmPage.click("@selectHarvestMethod");
        await hmPage.click("@setIndividualSpike");
        await hmPage.setValue("@noOfPlants", 10);
        await hmPage.click("@applyToAll");
    }

    //check if overwrite exists or not
    client.isVisible("#apply_changes", (res) => {
        console.log(res.value);
        if (res.value === true) {
            console.log("OVERWRITE");
            hmPage.click("@overwriteChanges");
        } else {
            console.log("APPLY CHANGES");
            //confirm apply changes
            hmPage.assert.visible("@applyChanges");
            hmPage.click("@applyChanges");
        }
    });
});

Then(
    /^the "([^"]*)" data for wheat selected plots is displayed in the plot data browser$/,
    async (args1) => {
        if (args1 == "Bulk") {
            await hmPage.expect.elements("@dateAssert").count.to.equal(10);
            await hmPage.expect.elements("@bulkAssert").count.to.equal(11);
        } else if (args1 == "Individual Ear") {
            await hmPage.expect.elements("@dateAssert").count.to.equal(10);
            await hmPage.expect.elements("@indivdualEarAssert").count.to.equal(11);
            await hmPage.expect.elements("@numOfEarsAssert").count.to.equal(10);
        } else if (args1 == "Single Plant Selection") {
            await hmPage.expect.elements("@dateAssert").count.to.equal(10);
            await hmPage.expect.elements("@singlePlantAssert").count.to.equal(11);
            await hmPage.expect.elements("@numOfPlantsAssert").count.to.equal(10);
        } else if (args1 == "Selected Bulk") {
            await hmPage.expect.elements("@dateAssert").count.to.equal(10);
            await hmPage.expect.elements("@modifiedBulkAssert").count.to.equal(11);
            await hmPage.expect.elements("@numOfPlantsAssert").count.to.equal(10);
        } else if (args1 == "Individual Spike") {
            await hmPage.expect.elements("@dateAssert").count.to.equal(10);
            await hmPage.expect
                .elements("@individualSpikeAssert")
                .count.to.equal(11);
            await hmPage.expect.elements("@numOfPlantsAssert").count.to.equal(10);
        }

        //clear data
        await hmPage.waitForElementVisible("@bulkDelete");
        await hmPage.click("@bulkDelete");
        await hmPage.waitForElementVisible("@uncommittedData");
        await hmPage.click("@uncommittedData");
        await hmPage.click("@committedData");
        await hmPage.click("@selectAllTraits");

        //click Delete all
        hmPage.pause(2000);
        await hmPage.click("@deleteAll");

        //confirm delete
        await hmPage.waitForElementVisible("@confirmDelete");
        await hmPage.click("@confirmDelete");

        //WORKAROUND
        //wait until delete is finished
        hmPage.pause(2000);
        await hmPage.click("@endDelete");
        //await hmPage.waitForElementNotPresent("@bulkDeleteProcess");

        //reset button
        hmPage.pause(5000);
        await hmPage.click("@resetGrid");
        hmPage.pause(3000);
    }
);

Then(
    /^the "([^"]*)" data for maize selected plots is displayed in the plot data browser$/,
    async (args1) => {
        if (args1 == "Bulk") {
            await hmPage.expect.elements("@dateAssert").count.to.equal(10);
            await hmPage.expect.elements("@bulkAssert").count.to.equal(11);
        } else if (args1 == "Individual Ear") {
            await hmPage.expect.elements("@dateAssert").count.to.equal(10);
            await hmPage.expect.elements("@indivdualEarAssert").count.to.equal(11);
            await hmPage.expect.elements("@numOfEarsAssert").count.to.equal(10);
        }

        //clear data
        await hmPage.waitForElementVisible("@bulkDelete");
        await hmPage.click("@bulkDelete");
        await hmPage.waitForElementVisible("@uncommittedData");
        await hmPage.click("@uncommittedData");
        await hmPage.click("@committedData");
        await hmPage.click("@selectAllTraits");

        //click Delete all
        hmPage.pause(2000);
        await hmPage.click("@deleteAll");

        //confirm delete
        await hmPage.waitForElementVisible("@confirmDelete");
        await hmPage.click("@confirmDelete");

        //WORKAROUND
        //wait until delete is finished
        hmPage.pause(2000);
        await hmPage.click("@endDelete");
        //await hmPage.waitForElementNotPresent("@bulkDeleteProcess");

        //reset button
        hmPage.pause(5000);
        await hmPage.click("@resetGrid");
        hmPage.pause(3000);
    }
);

Then(/^the user selects "([^"]*)" with no observation data$/, async (args1) => {
    if (args1 == "Harvest Date") {
        await hmPage.waitForElementVisible("@showHideInputFiled");
        await hmPage.click("@showHideInputFiled");
        await hmPage.waitForElementVisible("@showHideDropdown");
        await hmPage.click("@showHideHarvestDate");
        //click apply
        await hmPage.click("@showHideApply");
    } else if (args1 == "Harvest Method") {
        await hmPage.waitForElementVisible("@showHideInputFiled");
        await hmPage.click("@showHideInputFiled");
        await hmPage.waitForElementVisible("@showHideDropdown");
        await hmPage.click("@showHideHarvestMethod");
        //click apply
        await hmPage.click("@showHideApply");
    } else if (args1 == "Numeric Variable") {
        await hmPage.waitForElementVisible("@showHideInputFiled");
        await hmPage.click("@showHideInputFiled");
        await hmPage.waitForElementVisible("@showHideDropdown");
        await hmPage.click("@showHideNumericVariable");
        //click apply
        await hmPage.click("@showHideApply");
    } else if (args1 == "All Variables") {
        await hmPage.waitForElementVisible("@showHideInputFiled");
        await hmPage.click("@showHideInputFiled");
        await hmPage.waitForElementVisible("@showHideDropdown");
        await hmPage.click("@showHideAll");
        //click apply
        await hmPage.click("@showHideApply");
    }
});

Then(/^the user selects "([^"]*)" with observation data$/, async (args1) => {
    if (args1 == "Harvest Date") {
        await hmPage.waitForElementVisible("@showHideInputFiled");
        await hmPage.click("@showHideInputFiled");
        await hmPage.waitForElementVisible("@showHideDropdown");
        await hmPage.click("@showHideHarvestDate");
        //toggle with data
        await hmPage.click("@showHideToggle");
        //click apply
        await hmPage.click("@showHideApply");
    } else if (args1 == "Harvest Method") {
        await hmPage.waitForElementVisible("@showHideInputFiled");
        await hmPage.click("@showHideInputFiled");
        await hmPage.waitForElementVisible("@showHideDropdown");
        await hmPage.click("@showHideHarvestMethod");
        //toggle with data
        await hmPage.click("@showHideToggle");
        //click apply
        await hmPage.click("@showHideApply");
    } else if (args1 == "Numeric Variable") {
        await hmPage.waitForElementVisible("@showHideInputFiled");
        await hmPage.click("@showHideInputFiled");
        await hmPage.waitForElementVisible("@showHideDropdown");
        await hmPage.click("@showHideNumericVariable");
        //toggle with data
        await hmPage.click("@showHideToggle");
        //click apply
        await hmPage.click("@showHideApply");
    } else if (args1 == "All Variables") {
        await hmPage.waitForElementVisible("@showHideInputFiled");
        await hmPage.click("@showHideInputFiled");
        await hmPage.waitForElementVisible("@showHideDropdown");
        await hmPage.click("@showHideAll");
        //toggle with data
        await hmPage.click("@showHideToggle");
        //click apply
        await hmPage.click("@showHideApply");
    }
});

Then(/^the query results for "([^"]*)" are filtered$/, async (args1) => {
    if (args1 == "Harvest Date with No Data") {
        //assert summary is displayed
        hmPage.pause(2000);
        await hmPage.assert.visible("@showHideSummary1");
        await hmPage.assert.visible("@showHideSummaryHarvestDate");
        await hmPage.assert.visible("@showHideSummaryClearAll");
    } else if (args1 == "Harvest Method with No Data") {
        //assert summary is displayed
        hmPage.pause(2000);
        await hmPage.assert.visible("@showHideSummary1");
        await hmPage.assert.visible("@showHideSummaryHarvestMethod");
        await hmPage.assert.visible("@showHideSummaryClearAll");
    } else if (args1 == "Numeric Variable with No Data") {
        //assert summary is displayed
        hmPage.pause(2000);
        await hmPage.assert.visible("@showHideSummary1");
        await hmPage.assert.visible("@showHideSummaryNumVariable");
        await hmPage.assert.visible("@showHideSummaryClearAll");
    } else if (args1 == "All Variables with No Data") {
        //assert summary is displayed
        hmPage.pause(2000);
        await hmPage.assert.visible("@showHideSummary1");
        await hmPage.assert.visible("@showHideSummaryHarvestDate");
        await hmPage.assert.visible("@showHideSummaryNumVariable");
        await hmPage.assert.visible("@showHideSummaryHarvestMethod");
        await hmPage.assert.visible("@showHideSummaryClearAll");
    } else if (args1 == "Harvest Date with Data") {
        //assert summary is displayed
        hmPage.pause(2000);
        await hmPage.assert.visible("@showHideSummary2");
        await hmPage.assert.visible("@showHideSummaryHarvestDate");
        await hmPage.assert.visible("@showHideSummaryClearAll");
    } else if (args1 == "Harvest Method with Data") {
        //assert summary is displayed
        hmPage.pause(2000);
        await hmPage.assert.visible("@showHideSummary2");
        await hmPage.assert.visible("@showHideSummaryHarvestMethod");
        await hmPage.assert.visible("@showHideSummaryClearAll");
    } else if (args1 == "Numeric Variable with Data") {
        //assert summary is displayed
        hmPage.pause(2000);
        await hmPage.assert.visible("@showHideSummary2");
        await hmPage.assert.visible("@showHideSummaryNumVariable");
        await hmPage.assert.visible("@showHideSummaryClearAll");
    } else if (args1 == "All Variables with Data") {
        //assert summary is displayed
        hmPage.pause(2000);
        await hmPage.assert.visible("@showHideSummary2");
        await hmPage.assert.visible("@showHideSummaryHarvestDate");
        await hmPage.assert.visible("@showHideSummaryNumVariable");
        await hmPage.assert.visible("@showHideSummaryHarvestMethod");
        await hmPage.assert.visible("@showHideSummaryClearAll");
    }

    //clear and reset data
    await hmPage.click("@showHideSummaryClearAll");
    hmPage.pause(2000);
    await clearData();
    hmPage.pause(5000);
});


When(/^the status is changed to For Seedlot Creation$/, async () => {
    //verify a success message is shown
    await hmPage.expect.elements("@forCreation").count.to.equal(10);

    //reset data
    hmPage.pause(2000);
    await client.execute("window.scrollTo(0,0);");
    hmPage.pause(2000);
    await hmPage.waitForElementVisible("@harvestDataTab");
    await hmPage.click("@harvestDataTab");
    //hmPage.pause(2000);
    await hmPage.waitForElementVisible("@bulkDelete");

    await resetHarvestData();
});

When(/^the seedlot is created successfully$/, async () => {
    //verify a success message is shown
    //verify the germplasm package is created
    console.log("Flag1");
    await hmPage.expect.element("@summary").to.be.present;
    console.log("Flag2");

    /*
    //reset data
    hmPage.pause(2000);
    await hmPage.click("@harvestDataTab");
    console.log("Flag3");
    hmPage.pause(2000);
    await clearData();
    */
});



//function for resetting harvest data
async function resetHarvestDataAlt() {
    //perform bulk delete
    await hmPage.waitForElementVisible("@bulkDelete");
    await hmPage.click("@bulkDelete");
    await hmPage.waitForElementVisible("@uncommittedData");
    await hmPage.click("@uncommittedData");
    await hmPage.click("@committedData");
    await hmPage.click("@selectAllTraits");

    //click Delete all
    hmPage.pause(2000);
    await hmPage.click("@deleteAll");
    hmPage.pause(2000);

    //check if option appears
    client.isVisible("#delete_apply_changes", (res) => {
        console.log(res.value);
        if (res.value === true) {
            console.log("confirm delete");
            hmPage.click("@confirmDelete");
        } else {
            console.log("cancel delete");
            hmPage.click("@cancelDelete");
            hmPage.waitForElementVisible("@closeModal");
            hmPage.click("@closeModal");
        }
    });

}
