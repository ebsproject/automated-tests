const { client } = require("nightwatch-api");
const { Given, Then, When } = require("cucumber");
const ecPage = client.page.experimentCreationPage();
const isPresent = client.page.isPresent();
const b4rHelpers = require("./b4rHelpers.js");
const fs = require("fs");
const randGen = require("crypto");
const { assert } = require("console");
const experimentNameConst = "TestExp_" + Math.floor(Math.random() * 100000);
var locationName;
var experimentDesignFileName;
var firstOccurrence;
var lastOccurrence;


async function checkLeftNav() {
    await ecPage.waitForElementNotPresent("div.overlay", 5000, false);
    //check if left nav is showing

    client.isVisible("#go-to-show-left-nav", (res) => {
        console.log(res.value);
        if (res.value === false) {
            console.log("left nav showing");
        } else {
            console.log("left nav NOT showing");
            ecPage.waitForElementVisible("@showLeftNav");
            ecPage.pause(2000);            
            ecPage.assert.visible("@showLeftNav");
            ecPage.click("@showLeftNav");
        }
    });
}

async function select(selector, option) {
    const element = {
        selector: `//li[text()='${option}']`,
        locateStrategy: "xpath",
    };
    await ecPage.waitForElementVisible(selector);
    await ecPage.assert.visible(selector);
    await ecPage.click(selector);

    await ecPage.waitForElementVisible(element);    
    await ecPage.assert.visible(element);
    await ecPage.click(element);
    await ecPage.pause(1000);
}

async function checkCheckBox(control, check) {
    const sel = {
        selector:
            ecPage.elements[control.slice(1)].selector +
            "/ancestor::div[@class='row switch']//span",
        locateStrategy: "xpath",
    };
    
    await ecPage.waitForElementVisible(sel);
    await ecPage.assert.visible(sel);
    await ecPage.click(sel);
    await ecPage.pause(2000);

    let val;

    await ecPage.getAttribute(control, "value", ({ value }) => {
        if (value.error) {
            throw Error(value.error);
        }
        return (val = value);
    });

    await ecPage.perform(() => {
        debugger;
    });

    if (val == "false" && check == true){        
        await ecPage.waitForElementVisible(sel);
        await ecPage.assert.visible(sel);
        await ecPage.click(sel);
    }
    if (val == "true" && check == false){
        await ecPage.waitForElementVisible(sel);
        await ecPage.assert.visible(sel);
        await ecPage.click(sel);
    }
}

async function checkElementPresent(control, attribute) {
    await ecPage.getAttribute(
        "css selector",
        control,
        attribute,
        function (btn) {
            console.log("checked status", btn);
            //assert that check all is in 'checked' state, otherwise it will have a null value
            ecPage.assert.equal(btn.value, "true");
        }
    );
}

async function searchCurrentExperiment() {    
    await ecPage.waitForElementVisible("@resetButton");
    await ecPage.assert.visible("@resetButton");
    await ecPage.click("@resetButton");
    await ecPage.expect.element("@experimentFilterField").visible.after(120000);
    await ecPage.setValue("@experimentFilterField", experimentNameConst);
    await ecPage.setValue("@experimentFilterField", client.Keys.ENTER);
   
   
    await ecPage.isVisible("@editExperimentIcon", (res) => {
        console.log(res.value);
        if (res.value === false) {
            console.log("Not yet visible click reset and search");
            ecPage.setValue("@experimentFilterField", client.Keys.ENTER);
            ecPage.assert.visible("@editExperimentIcon");
            ecPage.click("@editExperimentIcon");
        } else {
            console.log("now visible, click edit");
            ecPage.setValue("@experimentFilterField", client.Keys.ENTER);
            ecPage.assert.visible("@editExperimentIcon");
            ecPage.click("@editExperimentIcon");
        }
    });
}

//Opens the data filters and set filters
//Single row only
When(/^the user sets Data filter parameters$/, async (table) => {
    await ecPage.pause(2000);
    await ecPage.waitForElementVisible("@dataFiltersMenu");
    await ecPage.assert.visible("@dataFiltersMenu");
    await ecPage.click("@dataFiltersMenu");
    //give time to load the program value
    await ecPage.pause(2000);

    for (column of table.raw()[0]) {
        switch (column) {
            case "Program":
                await ecPage.searchSelectSetValue(
                    ecPage.section.dataFilters.elements["programSearchSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Year":
                await ecPage.listBoxSetValue(
                    ecPage.section.dataFilters.elements["yearSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Season":
                await ecPage.listBoxSetValue(
                    ecPage.section.dataFilters.elements["seasonSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Stage":
                await ecPage.listBoxSetValue(
                    ecPage.section.dataFilters.elements["stageSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Site":
                await ecPage.listBoxSetValue(
                    ecPage.section.dataFilters.elements["siteSelect"],
                    table.hashes()[0][column]
                );
                break;
            case "Experiments":
                switch (table.hashes()[0][column]) {
                    case "All":
                        await ecPage.sliderSetValue(
                            ecPage.section.dataFilters.elements["experimentsSlider"],
                            "Left"
                        );
                        break;
                    case "My":
                        await ecPage.sliderSetValue(
                            ecPage.section.dataFilters.elements["experimentsSlider"],
                            "Right"
                        );
                        break;
                    default:
                        throw new Error(`Unexpected Experiment option.`);
                }
                break;
            default:
                throw new Error(`Unexpected ${column} name.`);
        }
    };

    
    await ecPage.section.dataFilters.waitForElementVisible("@applyButton");
    await ecPage.section.dataFilters.assert.visible("@applyButton");
    await ecPage.section.dataFilters.click("@applyButton");
    await ecPage.section.dataFilters.assert.not.visible("@applyButton");
});

//------OLD Experiment Creation START-------//
When(/^user goes to Experiments$/, async () => {
    await ecPage.waitForElementNotPresent("div.overlay", 5000, false);

    //await ecPage.waitForElementVisible("@showLeftNav");
    ecPage.pause(2000);
    //await ecPage.click("@showLeftNav");

    await ecPage.waitForElementVisible("@experimentCreationMenu");
    await ecPage.assert.visible("@experimentCreationMenu");
    await ecPage.click("@experimentCreationMenu");
});

When(/^user waits for design generation to finish again$/, async () => {
    await ecPage.pause(60000);
    //await ecPage.waitForElementNotPresent("@loadingStatus");
    //await new Promise(r => setTimeout(r, 30000));
});
//------OLD Experiment Creation END-------//

Given(/^the user is in Experiment Creation$/, async () => {
    //login non-docker
    //await b4rHelpers.login(client.globals.userName, client.globals.userPassword, client.globals.loginMode);
    //login docker
    await b4rHelpers.login(process.env.CMD_USER, process.env.CMD_PASS, process.env.CMD_MODE);
    await ecPage.waitForElementNotPresent("div.overlay", 5000, false);

    //go to EC
    await checkLeftNav();
    await ecPage.waitForElementVisible("@experimentCreationMenu");
    await ecPage.assert.visible("@experimentCreationMenu");
    await ecPage.click("@experimentCreationMenu");
});

When(/^user can see EC browser grid ready$/, async () => {
    await ecPage.pause(2000);
    await ecPage.waitGridReady(ecPage.section.ecGrid);
});

When(/^user set column filters in EC browser$/, async (table) => {
    for (column of table.raw()[0]) {
        switch (column) {
            case "STATUS":
                break;
            case "EXPERIMENT":
                await ecPage.section.ecColFilters.setValue(
                    "@experimentNameColFilter",
                    table.hashes()[0][column]
                );
                await ecPage.section.ecColFilters.setValue(
                    "@experimentNameColFilter",
                    client.Keys.ENTER
                );
                break;
            case "EXPERIMENT TYPE":
                await ecPage.section.ecColFilters.setValue(
                    "@experimentTypeColFilter",
                    table.hashes()[0][column]
                );
                await ecPage.section.ecColFilters.setValue(
                    "@experimentTypeColFilter",
                    client.Keys.ENTER
                );
                break;
            case "OCCURRENCE COUNT":
                await ecPage.section.ecColFilters.setValue(
                    "@occurrenceCountColFilter",
                    table.hashes()[0][column]
                );
                await ecPage.section.ecColFilters.setValue(
                    "@occurrenceCountColFilter",
                    client.Keys.ENTER
                );
                break;
            case "ENTRY COUNT":
                await ecPage.section.ecColFilters.setValue(
                    "@entryCountColFilter",
                    table.hashes()[0][column]
                );
                await ecPage.section.ecColFilters.setValue(
                    "@entryCountColFilter",
                    client.Keys.ENTER
                );
                break;
            case "SEASON":
                await ecPage.section.ecColFilters.setValue(
                    "@seasonNameColFilter",
                    table.hashes()[0][column]
                );
                await ecPage.section.ecColFilters.setValue(
                    "@seasonNameColFilter",
                    client.Keys.ENTER
                );
                break;
            case "STAGE":
                await ecPage.section.ecColFilters.setValue(
                    "@stageCodeColFilter",
                    table.hashes()[0][column]
                );
                await ecPage.section.ecColFilters.setValue(
                    "@stageCodeColFilter",
                    client.Keys.ENTER
                );
                break;
            case "YEAR":
                await ecPage.section.ecColFilters.setValue(
                    "@experimentYearColFilter",
                    table.hashes()[0][column]
                );
                await ecPage.section.ecColFilters.setValue(
                    "@experimentYearColFilter",
                    client.Keys.ENTER
                );
                break;
            case "DESIGN TYPE":
                await ecPage.section.ecColFilters.setValue(
                    "@experimentDesignTypeColFilter",
                    table.hashes()[0][column]
                );
                await ecPage.section.ecColFilters.setValue(
                    "@experimentDesignTypeColFilter",
                    client.Keys.ENTER
                );
                break;
            default:
                throw new Error(`Unexpected ${column} name.`);
        }
    }
    //wait for the browser to load fully
    await ecPage.waitGridReady(ecPage.section.ecGrid);
    await ecPage.pause(2000);
});

Then(/^matching results will show in the EC browser$/, async () => {
    await ecPage.pause(2000);
    await ecPage.waitGridReady(ecPage.section.ecGrid);

    //add assert to ensure application did not exit with error after loading grid with filtered results
    await ecPage.pause(2000);
    await ecPage.assert.visible("@pageLabelExperiments");
});

When(/^user creates Experiments$/, async () => {
    await ecPage.waitForElementVisible("@createButton");   
    await ecPage.assert.visible("@createButton");
    await ecPage.click("@createButton");
});

When(/^user sets "([^"]*)" in Experiment Type$/, async (args1) => {
    const option = {
        //selector: `//*[@id='select2-experiment_type-results']/li[text()='${args1}']`,
        selector: `//*[@id="select2-EXPERIMENT_TYPE-identification-2253-results"]/li[text()='${args1}']`,
        locateStrategy: "xpath",
    };
    await ecPage.waitForElementVisible("@experimentTypeSelect");
    await ecPage.assert.visible("@experimentTypeSelect");
    await ecPage.click("@experimentTypeSelect");

    await ecPage.waitForElementVisible(option);
    await ecPage.assert.visible(option);
    await ecPage.click(option);
});

When(/^user sets Basic tab values$/, async (table) => {
    for (column of table.raw()[0]) {
        switch (column) {
            case "CreateExperiment":
                await ecPage.waitForElementVisible("@createButton");
                await ecPage.assert.visible("@createButton");
                await ecPage.click("@createButton");
                break;
            case "ExperimentType":                
                const option = {                    
                    selector: `//*[@id="select2-EXPERIMENT_TYPE-identification-2253-results"]/li[text()='${table.hashes()[0][column]}']`,
                    locateStrategy: "xpath",
                };
                await ecPage.waitForElementVisible("@experimentTypeSelect");
                await ecPage.assert.visible("@experimentTypeSelect");
                await ecPage.click("@experimentTypeSelect");
                
                await ecPage.waitForElementVisible(option);
                await ecPage.assert.visible(option);
                await ecPage.click(option);
                break;
            case "Stage":
                await select("@stageSelect", table.hashes()[0][column]);
                break;
            case "EvaluationSeason":
                await select("@evaluationSeasonSelect", table.hashes()[0][column]);                
                break;
            case "ExperimentName":
                await checkCheckBox("@experimentNameCheckbox", false);
                await ecPage.setValue("@experimentNameField", experimentNameConst);
                break;
            case "SaveBasic":                
                await ecPage.waitForElementVisible("@saveButton");
                await ecPage.assert.visible("@saveButton");
                await ecPage.click("@saveButton");
                break;
            default:
                throw new Error(`Unexpected ${column} name.`);
        }
    }
});

When(/^user sets "([^"]*)" in Experiment Template$/, async (args1) => {
    const option = {
        selector: `//*[@id='select2-experiment_template-results']/li[text()='${args1}']`,
        locateStrategy: "xpath",
    };
    await ecPage.waitForElementVisible("@experimentTemplateSelect");
    await ecPage.assert.visible("@experimentTemplateSelect");
    await ecPage.click("@experimentTemplateSelect");
    await ecPage.waitForElementVisible(option);
    await ecPage.assert.visible(option);
    await ecPage.click(option);
});

When(/^user sets "([^"]*)" in Pipeline$/, async (args1) => {
    await select("@pipelineSelect", args1);
});

When(/^user sets Experiment Name$/, async () => {
    await checkCheckBox("@experimentNameCheckbox", false);
    await ecPage.setValue("@experimentNameField", experimentNameConst);
});

When(/^user sets "([^"]*)" in Experiment Objective$/, async (args1) => {
    await ecPage.setValue("@experimentObjectiveField", args1);
});

When(/^user sets "([^"]*)" in Stage$/, async (args1) => {
    await select("@stageSelect", args1);
});

When(/^user sets "([^"]*)" in Experiment Year$/, async (args1) => {
    await ecPage.setValue("@experimentYearField", args1);
});

When(/^user sets "([^"]*)" in Evaluation Season$/, async (args1) => {
    await select("@evaluationSeasonSelect", args1);
});

When(/^user sets "([^"]*)" in Planting Season$/, async (args1) => {
    await ecPage.setValue("@plantingSeasonField", args1);
});

When(/^user sets "([^"]*)" in Experiment Steward$/, async (args1) => {
    await select("@experimentStewardSelect", args1.replace(/#/, Date.now()));
});

When(/^user sets "([^"]*)" in Project$/, async (args1) => {
    await select("@projectSelect", args1);
});

When(/^user selects Save$/, async () => {
    await ecPage.waitForElementVisible("@saveButton");
    await ecPage.assert.visible("@saveButton");
    await ecPage.click("@saveButton");
});


When(/^user add from Saved List "([^"]*)"$/, async (args1) => {
    await ecPage.waitForElementVisible("@addButton");
    await ecPage.assert.visible("@addButton");
    await ecPage.click("@addButton");
        
    await ecPage.waitForElementVisible("@inputListButton");
    await ecPage.assert.visible("@inputListButton");
    await ecPage.click("@inputListButton");
    
    await ecPage.waitForElementVisible("@savedListButton");   
    await ecPage.assert.visible("@savedListButton");
    await ecPage.click("@savedListButton");

    const select = {
        selector: `//td[text()='${args1}'][position()=1]/preceding-sibling::td//a`,
        locateStrategy: "xpath",
    };

    await ecPage.waitForElementVisible(select);
    await ecPage.assert.visible(select);
    await ecPage.click(select);

    await ecPage.waitForElementVisible("@closeButton");    
    await ecPage.assert.visible("@closeButton");
    await ecPage.click("@closeButton");
});

When(/^user selects "([^"]*)" for rows "([^"]*)"$/, async (args1, args2) => {
    await ecPage.waitForElementVisible("@addButton");
    let selector =
        //"#grid-entry-list-container > table > tbody > tr:nth-child(1) span[role='textbox']";
        "#w8-container > table > tbody > tr:nth-child(1) span[role='textbox']";
    await ecPage.waitForElementVisible(selector);

    await client.execute(
        `let element = document.querySelector("${selector}");` +
        "return element.innerHTML='check';"
    );

    selector =
        //"#grid-entry-list-container > table > tbody > tr:nth-child(2) span[role='textbox']";
        "#w8-container > table > tbody > tr:nth-child(2) span[role='textbox']";

    await client.execute(
        `let element = document.querySelector("${selector}");` +
        "return element.innerHTML='check';"
    );
});

//args1 - an experiment name to copy entries
When(
    /^user has previously created an entry list from "([^"]*)"$/,
    async (args1) => {
        //getExperimentName();
        await ecPage.waitForElementVisible("@addButton");
        await ecPage.assert.visible("@addButton");
        await ecPage.click("@addButton");

        await ecPage.waitForElementVisible("@inputListButton");
        await ecPage.assert.visible("@inputListButton");
        await ecPage.click("@inputListButton");
        
        await ecPage.waitForElementVisible("@copyEntryListButton");
        await ecPage.assert.visible("@copyEntryListButton");
        await ecPage.click("@copyEntryListButton");

        const select = {
            selector: `//td[text()='${args1}'][position()=1]/preceding-sibling::td//a`,
            locateStrategy: "xpath",
        };

        await ecPage.waitForElementVisible(select);
        await ecPage.assert.visible(select);
        await ecPage.click(select);

        await ecPage.waitForElementVisible("@selectButton");
        await ecPage.assert.visible("@selectButton");
        await ecPage.click("@selectButton");

        await ecPage.waitForElementNotPresent("#viewAllButton");
        await ecPage.waitForElementNotPresent("#toast-container");

        await ecPage.waitGridReady(ecPage.section.ecGrid);
    }
);

//10k IRSEA-F6-2016-DS-002

When(/^user waits for entries to load$/, async () => {
    await ecPage.pause(10000);
});

When(/^user selects Next$/, async () => {
    // "#toast-container"  
    await ecPage.waitForElementNotPresent("#toast-container", 10000, false);
    
    await ecPage.waitForElementVisible("@nextButton");
    await ecPage.assert.visible("@nextButton");
    await ecPage.click("@nextButton");
});

When(/^user waits for page to finish loading$/, async () => {
    await ecPage.pause(10000);
});

When(/^user edits current experiment$/, async () => {
    await searchCurrentExperiment();
    await ecPage.waitForElementNotPresent("#toast-container", 10000, false);
    
    await ecPage.waitForElementVisible("@nextButton");
    await ecPage.assert.visible("@nextButton");
    await ecPage.click("@nextButton");
});

//returns to experiments browser when adding >1000 entries
When(/^user enters Design$/, async () => {
    await ecPage.waitForElementVisible("@nextButton");
    await ecPage.assert.visible("@nextButton");
    await ecPage.pause(25000); //need assertion if next button is enabled even if visible
    await ecPage.click("@nextButton");    
});

When(/^user sets "([^"]*)" as Design$/, async (args1) => {
    await select("@designSelect", args1);
    await ecPage.pause(5000);
});

Then(/^user sets "([^"]*)" as Number of Occurrences$/, async (args1) => {
    await ecPage.clearValue("@numberOccurrencesField");
    await ecPage.setValue("@numberOccurrencesField", args1);
});

Then(/^user sets "([^"]*)" as Number of Replicates$/, async (args1) => {
    await ecPage.clearValue("@numberReplicatesField");
    await ecPage.setValue("@numberReplicatesField", args1);
});

Then(/^user sets "([^"]*)" as Rows$/, async (args1) => {
    await select("@rowsSelect", args1);
});

Then(
    /^user sets "([^"]*)" as Plots Until Turn The Serpentine$/,
    async (args1) => {
        await select("@plotsUntilSelect", args1);
    }
);

When(/^user edits an experiment "([^"]*)"$/, async (args1) => {
    await ecPage.setValue("@experimentFilterField", args1);
    await ecPage.setValue("@experimentFilterField", client.Keys.ENTER);
    const sel = {
        selector: `//td[text()='${args1}']/ancestor::tr//i[text()='edit']`,
        locateStrategy: "xpath",
    };

    await ecPage.waitForElementVisible(sel);
    await ecPage.assert.visible(sel);
    await ecPage.click(sel);
});

Then(/^user sets "([^"]*)" as Number of Plots Per Block$/, async (args1) => {
    await select("@numberPlotsPerBlockSelect", args1);
});

When(/^user selects Generate Design$/, async () => {
    // await ecPage.click("@numberOccurrencesField");
    await ecPage.waitForElementVisible("@generateDesignButton");
    await ecPage.assert.visible("@generateDesignButton");
    await ecPage.click("@generateDesignButton");
});

When(/^user sets "([^"]*)" as Define Shape Field$/, async (args1) => {
    switch (args1) {
        case "No":
            await checkCheckBox("@shapeCheckbox", false);
            break;
        case "Yes":
            await checkCheckBox("@shapeCheckbox", true);
            break;
        default:
            throw new Error("Unexpected option " + arg1);
    }
});

Then(/^user sees the request successfully sent$/, async () => {
    //await ecPage.expect.element("#toast-container").visible.after(5000);
    await ecPage.waitForElementVisible("#toast-container");
});

When(/^user sets "([^"]*)" as Randomize 1st Step$/, async (args1) => {
    switch (args1) {
        case "No":
            await checkCheckBox("@randomizeCheckbox", false);
            break;
        case "Yes":
            await checkCheckBox("@randomizeCheckbox", true);
            break;
        default:
            throw new Error("Unexpected option " + arg1);
    }
});

When(/^user sets "([^"]*)" as Number of Row Blocks$/, async (args1) => {
    await select("@rowBlocksSelect", args1);
});

When(/^user sets "([^"]*)" as Number of Rows Per Block$/, async (args1) => {
    await select("@noRowsPerBlock", args1);
});

When(/^user sets "([^"]*)" as Number of Rows Per Rep$/, async (args1) => {
    await select("@noRowsPerRepSelect", args1);
});

When(/^user sets "([^"]*)" as Number of Blocks Per Rep$/, async (args1) => {
    await select("@noBlocksPerSelect", args1);
});

When(/^user waits for design generation to finish$/, async () => {
    ecPage.expect.element("@layoutPanel").visible.after(840000);
    await ecPage.pause(1000);
});

When(/^user sees the design generation is finished$/, async () => {
    await ecPage.pause(120000);
    //await ecPage.waitForElementNotPresent("@loadingStatus");
    //await new Promise(r => setTimeout(r, 30000));
});

Then(/^user sees no fields for parameters are be displayed$/, async () => {
    return true;
});

Then(/^users sees the "([^"]*)" is disabled$/, async (args1) => {
    console.log(args1);
    return true;
});

Then(/^users sees error message: "([^"]*)"$/, (args1) => {
    console.log(args1);
    return true;
});

When(/^user sees the "([^"]*)"$/, async (args1) => {
    if (args1 == "layout") {
        await ecPage.assert.visible("@layoutPanel");
    } else if (args1 == "fail") {
        await ecPage.assert.not.elementPresent("@layoutPanel");
    }
    
    await ecPage.waitForElementVisible("@no_action_btn");
    await ecPage.assert.visible("@no_action_btn");
    await ecPage.click("@no_action_btn");
});

When(/^user sees this result$/, async () => {
    // await ecPage.click("#section-3");
    ecPage.expect.element("@designResult").visible.after(840000);
    //await ecPage.assert.visible("@designResult");
    await ecPage.waitForElementVisible("@no_action_btn");
    await ecPage.assert.visible("@no_action_btn");
    await ecPage.click("@no_action_btn");
});
//cross
When(/^user enters Crosses$/, async () => {                 
    await ecPage.waitForElementVisible("@nextButton");
    await ecPage.assert.visible("@nextButton");
    await ecPage.pause(15000);     
    await ecPage.click("@nextButton");
});

When(/^user performs Self Cross$/, async () => {    
    await ecPage.waitForElementVisible("@femaleSelectAll");
    await ecPage.assert.visible("@femaleSelectAll");
    await ecPage.pause(2000);//need delay
    await ecPage.click("@femaleSelectAll");
    await ecPage.pause(2000);//need delay
    await ecPage.click("@femaleSelfCross");    
});

When(/^user enters Manage Crosses$/, async () => {    
    await ecPage.waitForElementNotPresent('@toastContainer');
    await ecPage.click("@manageCrossesTab");
    await ecPage.waitForElementVisible("@crossPresent");
    await ecPage.assert.visible("@crossPresent");
    await ecPage.waitForElementVisible("@nextButton");
    await ecPage.assert.visible("@nextButton");
    await ecPage.click("@nextButton");
});
//planting arrangement
Then(/^user enters Planting Arrangement-Entry Order$/, async () => {    
    await ecPage.waitForElementVisible("@selectArrangement");
    await ecPage.click("@selectArrangement");
    await ecPage.waitForElementVisible("@updateNoReps");
    await ecPage.pause(2000);//need delay
    await ecPage.click("@nextButton");
});

Then(/^user enters Protocol$/, async () => {    
    
});

Then(/^user enters Site$/, async () => {    
    await ecPage.waitForElementVisible("#planting-protocol-form");
    await ecPage.waitForElementVisible("@nextButton");
    await ecPage.assert.visible("@nextButton");
    await ecPage.click("@nextButton");
});

When(/^user tries to Save Site$/, async () => {    
    await ecPage.waitForElementVisible("@saveButton");
    await ecPage.assert.visible("@saveButton");
    await ecPage.click("@saveButton");

    await ecPage.assert.visible("#required-field-modal");
    
    await ecPage.waitForElementVisible("@siteSaveAssert");
    await ecPage.assert.visible("@siteSaveAssert");
    await ecPage.click("@siteSaveAssert");
});

When(/^user sets "([^"]*)" as Site for a Single Occurence$/, async (args1) => {
    await select("@occurrenceSiteSelect", args1);
});

//returns to experiments browser when adding >1000 entries
When(/^user re-enters Site$/, async () => {
    await searchCurrentExperiment();
    ecPage.pause(5000);
    
    await ecPage.waitForElementVisible("@saveButton");
    await ecPage.assert.visible("@saveButton");
    await ecPage.click("@saveButton");
});

Then(/^user enters Review$/, async () => {
    await ecPage.waitForElementVisible("@saveButton");
    await ecPage.assert.visible("@saveButton");
    await ecPage.click("@saveButton");
});

Then(/^user Finalizes Experiment$/, async () => {
    //to add assertions here
    await ecPage.waitForElementVisible("@finalizeButton");    
    await ecPage.assert.visible("@finalizeButton");
    ecPage.expect.element("@finalizeButton").visible.after(60000);
    await ecPage.click("@finalizeButton");
});
