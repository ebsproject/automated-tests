@wheat_multi_crop @wheat_germplasm_run_GS
Feature: B4R-7607 Item Selection in Germplasm Search

    Background: Login User
        Given the user is in "Germplasm" Search entrypoint
        And the user sets Data filters parameters for Germplasm Search
            | Program          |
            | Durum Wheat (DW) |

    Scenario: GS-SELECT-001 Checkbox selection is retained
        Given user perform generic germplasm name search with input "CMS%"
        And user selects a set of checkboxes in the current page of Germplasm Search browser
        When user navigates to "Last" page of Germplasm Search browser
        And user goes back to the "First" page of Germplasm Search browser
        Then checkbox selection should be retained in Germplasm Search browser


