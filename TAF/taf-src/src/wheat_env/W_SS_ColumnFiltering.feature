@wheat_multi_crop @wheat_column_filtering_SS
Feature: CORB-2023 Column Filtering

    Background: Login User
        Given the user is in "Seed search" Search
        And the user sets Data filters parameters for Seed Search
            | Program          |
            | Durum Wheat (DW) |
        And user sets Seed search parameters
            | Season | Experiment Type    |
            | B      | Generation Nursery |

    @wheatColumnFilterSS1 @wheatSeedsFiltering2
    Scenario: SS-BROWSER-001 User filter the column PARENTAGE from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | PARENTAGE  |
            | %RBC%      |
        Then results will show all matching package records

    @wheatColumnFilterSS2
    Scenario: SS-BROWSER-002 User filter the column GERMPLASM from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | GERMPLASM |
            | %CDSS%    |
        Then results will show all matching package records

    @wheatColumnFilterSS3
    Scenario: SS-BROWSER-003 User filter the column LABEL from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | LABEL |
            | %BV-% |
        Then results will show all matching package records

    @wheatColumnFilterSS4
    Scenario: SS-BROWSER-004 User filter the column SEED NAME from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | SEED NAME |
            | %BV-%     |
        Then results will show all matching package records

    @wheatColumnFilterSS5
    Scenario: SS-BROWSER-005 User filter the column SEED from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | SEED |
            | WSD% |
        Then results will show all matching package records

    @wheatColumnFilterSS6 @wheatSeedsFiltering2
    Scenario: SS-BROWSER-006 User filter the column PACKAGE QTY. from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | PACKAGE QTY. |
            | 0            |
        Then results will show all matching package records

    @wheatColumnFilterSS7
    Scenario: SS-BROWSER-007 User filter the column UNIT from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | UNIT |
            | g%   |
        Then results will show all matching package records

    @wheatColumnFilterSS8
    Scenario: SS-BROWSER-008 User filter the column OTHER_NAME from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | OTHER_NAME |
            | %CDSS%     |
        Then results will show all matching package records

    @wheatColumnFilterSS9
    Scenario: SS-BROWSER-009 User filter the column YEAR from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | YEAR |
            | 2017 |
        Then results will show all matching package records

    @wheatColumnFilterSS10 @wheatSeedsFiltering3
    Scenario: SS-BROWSER-009 User filter the column EXPERIMENT TYPE from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | EXPERIMENT TYPE    |
            | Generation Nursery |
        Then results will show all matching package records

    @wheatColumnFilterSS11
    Scenario: SS-BROWSER-010 User filter the column EXPERIMENT from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | EXPERIMENT |
            | D17B%      |
        Then results will show all matching package records

    @wheatColumnFilterSS12
    Scenario: SS-BROWSER-011 User filter the column OCCURRENCE from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | OCCURRENCE |
            | 0          |
        Then results will show all matching package records

    @wheatColumnFilterSS13
    Scenario: SS-BROWSER-012 User filter the column SEASON from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | SEASON |
            | A      |
        Then results will show all matching package records