@wheat_multi_crop @wheat_column_filtering_GS
Feature: SQA-570 Column Filtering

    Background: Login User
        Given the user is in "Germplasm" Search entrypoint
        And the user sets Data filters parameters for Germplasm Search
            | Program          |
            | Durum Wheat (DW) |

    #needs section: browser fro location value
    @wheatColumnFilterGS1 @wheatGermplasmFiltering1
    Scenario:
        Given user perform generic germplasm name search with input "CMS%"
        When user set column filters of the search results in Germplasm browser
            | GERMPLASM   |
            | CMSW%       |
        Then results show all matching germplasm

    @wheatColumnFilterGS2
    Scenario: GS-BROWSER-002 User filter the column OTHER NAMES from the search results
        Given user perform generic germplasm name search with input "CMS%"
        When user set column filters of the search results in Germplasm browser
            | OTHER NAMES |
            | CMSW%       |
        Then results show all matching germplasm

    @wheatColumnFilterGS3
    Scenario: GS-BROWSER-003 User filter the column PARENTAGE from the search results
        Given user perform generic germplasm name search with input "CMS%"
        When user set column filters of the search results in Germplasm browser
            | PARENTAGE |
            | %BUC%     |
        Then results show all matching germplasm

    @wheatColumnFilterGS4
    Scenario: GS-BROWSER-004 User filter the column GENERATION from the search results
        Given user perform generic germplasm name search with input "CMS%"
        When user set column filters of the search results in Germplasm browser
            | GENERATION |
            | UN%        |
        Then results show all matching germplasm

    @wheatColumnFilterGS5
    Scenario: GS-BROWSER-005 User filter the column GERMPLASM NAME TYPE from the search results
        Given user perform generic germplasm name search with input "CMS%"
        When user set column filters of the search results in Germplasm browser
            | GERMPLASM NAME TYPE |
            | bcid%               |
        Then results show all matching germplasm

    @wheatColumnFilterGS6 @wheatGermplasmFiltering2
    Scenario: GS-BROWSER-006 User filter the column GERMPLASM TYPE from the search results
        Given user perform generic germplasm name search with input "CMS%"
        When user set column filters of the search results in Germplasm browser
            | GERMPLASM TYPE |
            | segregating    |
        Then results show all matching germplasm

    @wheatColumnFilterGS7
    Scenario: GS-BROWSER-007 User filter the column GERMPLASM STATE from the search results
        Given user perform generic germplasm name search with input "CMS%"
        When user set column filters of the search results in Germplasm browser
            | GERMPLASM STATE |
            | %fixed%         |
        Then results show all matching germplasm
