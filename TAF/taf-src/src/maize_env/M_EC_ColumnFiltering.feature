@maize_multi_crop @maize_column_filtering_EC
Feature: SQA-566 Column Filtering of Experiment Creation browser

    Background: Login User
        Given the user is in Experiment Creation
        And the user sets Data filter parameters
            | Program                        |
            | KE Maize Breeding Program (KE) |

    @maizeColumnFilterEC1
    Scenario: EC-BROWSER-001 User filter the column EXPERIMENT in EC browser
        Given user can see EC browser grid ready
        When user set column filters in EC browser
            | EXPERIMENT |
            | %KE%       |
        Then matching results will show in the EC browser

    @maizeColumnFilterEC2
    Scenario: EC-BROWSER-002 User filter the column EXPERIMENT TYPE in EC browser
        Given user can see EC browser grid ready
        When user set column filters in EC browser
            | EXPERIMENT TYPE |
            | Breeding Trial  |
        Then matching results will show in the EC browser

    @maizeColumnFilterEC3
    Scenario: EM-BROWSER-003 User filter the column OCCURRENCE COUNT in EC browser
        Given user can see EC browser grid ready
        When user set column filters in EC browser
            | OCCURRENCE COUNT |
            | 1%               |
        Then matching results will show in the EC browser

    @maizeColumnFilterEC4
    Scenario: EC-BROWSER-004 User filter the column ENTRY COUNT in EC browser
        Given user can see EC browser grid ready
        When user set column filters in EC browser
            | ENTRY COUNT |
            | 1%          |
        Then matching results will show in the EC browser

    @maizeColumnFilterEC5
    Scenario: EC-BROWSER-005 User filter the column SEASON in EC browser
        Given user can see EC browser grid ready
        When user set column filters in EC browser
            | SEASON |
            | %A%    |
        Then matching results will show in the EC browser

    @maizeColumnFilterEC6
    Scenario: EC-BROWSER-006 User filter the column STAGE in EC browser
        Given user can see EC browser grid ready
        When user set column filters in EC browser
            | STAGE |
            | HB    |
        Then matching results will show in the EC browser

    @maizeColumnFilterEC7
    Scenario: EC-BROWSER-007 User filter the column YEAR in EC browser
        Given user can see EC browser grid ready
        When user set column filters in EC browser
            | YEAR |
            | 20%  |
        Then matching results will show in the EC browser

    @maizeColumnFilterEC8
    Scenario: EC-BROWSER-008 User filter the column DESIGN TYPE in EC browser
        Given user can see EC browser grid ready
        When user set column filters in EC browser
            | DESIGN TYPE |
            | Systematic% |
        Then matching results will show in the EC browser

    @maizeColumnFilterECSmoke
    Scenario: EC-BROWSER-009 Use all column filters in EC browser
        Given user can see EC browser grid ready
        When user set column filters in EC browser
            | EXPERIMENT |
            | %KE%       |
        When user set column filters in EC browser
            | EXPERIMENT TYPE |
            | Breeding Trial  |
        When user set column filters in EC browser
            | OCCURRENCE COUNT |
            | 1%               |
        When user set column filters in EC browser
            | ENTRY COUNT |
            | 1%          |
        When user set column filters in EC browser
            | SEASON |
            | %A%    |
        When user set column filters in EC browser
            | STAGE |
            | HB    |
        When user set column filters in EC browser
            | YEAR |
            | 20%  |
        When user set column filters in EC browser
            | DESIGN TYPE |
            | Systematic% |
        Then matching results will show in the EC browser