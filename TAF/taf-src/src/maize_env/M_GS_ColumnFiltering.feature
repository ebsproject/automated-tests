@maize_multi_crop @maize_column_filtering_GS
Feature: SQA-570 Column Filtering

    Background: Login User
        Given the user is in "Germplasm" Search entrypoint
        And the user sets Data filters parameters for Germplasm Search
            | Program                        |
            | KE Maize Breeding Program (KE) |

    #needs section: browser fro location value
    @maizeColumnFilterGS1 @maizeGermplasmFiltering1
    Scenario:
        Given user perform generic germplasm name search with input "CMS%"
        When user set column filters of the search results in Germplasm browser
            | GERMPLASM |
            | CMS%      |
        Then results show all matching germplasm

    @maizeColumnFilterGS2
    Scenario: GS-BROWSER-002 User filter the column OTHER NAMES from the search results
        Given user perform generic germplasm name search with input "CMS%"
        When user set column filters of the search results in Germplasm browser
            | OTHER NAMES |
            | CMS%        |
        Then results show all matching germplasm

    @maizeColumnFilterGS3
    Scenario: GS-BROWSER-003 User filter the column PARENTAGE from the search results
        Given user perform generic germplasm name search with input "CMS%"
        When user set column filters of the search results in Germplasm browser
            | PARENTAGE |
            | %3%       |
        Then results show all matching germplasm

    @maizeColumnFilterGS4
    Scenario: GS-BROWSER-004 User filter the column GENERATION from the search results
        Given user perform generic germplasm name search with input "CMS%"
        When user set column filters of the search results in Germplasm browser
            | GENERATION |
            | F%         |
        Then results show all matching germplasm

    @maizeColumnFilterGS5
    Scenario: GS-BROWSER-005 User filter the column GERMPLASM NAME TYPE from the search results
        Given user perform generic germplasm name search with input "CMS%"
        When user set column filters of the search results in Germplasm browser
            | GERMPLASM NAME TYPE |
            | selection_history   |
        Then results show all matching germplasm

    @maizeColumnFilterGS6 @maizeGermplasmFiltering2
    Scenario: GS-BROWSER-006 User filter the column GERMPLASM TYPE from the search results
        Given user perform generic germplasm name search with input "CMS%"
        When user set column filters of the search results in Germplasm browser
            | GERMPLASM TYPE |
            | segregating    |
        Then results show all matching germplasm

    @maizeColumnFilterGS7
    Scenario: GS-BROWSER-007 User filter the column GERMPLASM STATE from the search results
        Given user perform generic germplasm name search with input "CMS%"
        When user set column filters of the search results in Germplasm browser
            | GERMPLASM STATE |
            | %fixed%         |
        Then results show all matching germplasm
