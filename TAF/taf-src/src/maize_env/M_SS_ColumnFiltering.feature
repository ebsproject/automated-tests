@maize_multi_crop @maize_column_filtering_SS
Feature: CORB-2023 Column Filtering

    Background: Login User
        Given the user is in "Seed search" Search
        And the user sets Data filters parameters for Seed Search
            | Program                           |
            | Irrigated South-East Asia (IRSEA) |
        And user sets Seed search parameters
            | Season   | Experiment Type                                                  |
            | Dry, Wet | Breeding Trial, Generation Nursery, Intentional Crossing Nursery |

    @maizeColumnFilterSS1 @maizeSeedsFiltering1
    Scenario: SS-BROWSER-001 User filter the column PARENTAGE from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | PARENTAGE |
            | %IR%      |
        Then results will show all matching package records

    @maizeColumnFilterSS2
    Scenario: SS-BROWSER-002 User filter the column GERMPLASM from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | GERMPLASM |
            | %IR%      |
        Then results will show all matching package records

    @maizeColumnFilterSS3
    Scenario: SS-BROWSER-003 User filter the column LABEL from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | LABEL |
            | %G%   |
        Then results will show all matching package records

    @maizeColumnFilterSS4
    Scenario: SS-BROWSER-004 User filter the column SEED NAME from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | SEED NAME |
            | 300%      |
        Then results will show all matching package records

    @maizeColumnFilterSS5
    Scenario: SS-BROWSER-005 User filter the column SEED from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | SEED |
            | 300% |
        Then results will show all matching package records

    @maizeColumnFilterSS6 @maizeSeedsFiltering2
    Scenario: SS-BROWSER-006 User filter the column PACKAGE QTY. from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | PACKAGE QTY. |
            | %0%          |
        Then results will show all matching package records

    @maizeColumnFilterSS7
    Scenario: SS-BROWSER-007 User filter the column UNIT from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | UNIT |
            | g%   |
        Then results will show all matching package records

    @maizeColumnFilterSS8
    Scenario: SS-BROWSER-008 User filter the column OTHER_NAME from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | OTHER_NAME |
            | %IR%       |
        Then results will show all matching package records

    @maizeColumnFilterSS9
    Scenario: SS-BROWSER-009 User filter the column YEAR from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | YEAR |
            | 20%  |
        Then results will show all matching package records

    @maizeColumnFilterSS10 @maizeSeeds_filtering3
    Scenario: SS-BROWSER-009 User filter the column EXPERIMENT TYPE from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | EXPERIMENT TYPE    |
            | Generation Nursery |
        Then results will show all matching package records

    @maizeColumnFilterSS11
    Scenario: SS-BROWSER-010 User filter the column EXPERIMENT from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | EXPERIMENT |
            | %IRSEA%    |
        Then results will show all matching package records

    @maizeColumnFilterSS12
    Scenario: SS-BROWSER-011 User filter the column OCCURRENCE from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | OCCURRENCE |
            | %IRSEA%    |
        Then results will show all matching package records

    @maizeColumnFilterSS13
    Scenario: SS-BROWSER-012 User filter the column SEASON from the search results
        Given user can see grid ready
        When user set column filters of the search results
            | SEASON |
            | DS     |
        Then results will show all matching package records