Background: User created a germplasm list
    Given the user created a new "germplasm list" via "Germplasm" Search entrypoint
Scenario: SS-IMPORT-SAVED-GERMPLASM-LIST-001 Import Saved Germplasm List
    Given the user is in "Seed search" Search
    And the user imports input list items from a saved "germplasm list"
    When user clicks on Find
    Then results will show all matching package records