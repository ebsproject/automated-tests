const { client } = require("nightwatch-api");

module.exports = {
    url: "https://qa.ebsproject.org/",

    elements: {

        csLoginButton: {
            selector: "//div[@id='single-spa-application:@ebs/cs']/div/div[2]/header/div/button",
            locateStrategy: "xpath",
        },

        systemAdministratorInstance: {
            selector: "//img[contains(@src,'https://qa.ebsproject.org/mfe/ui/c07a7d83eef3dfc1270e.svg')]",
            locateStrategy: "xpath",
        },
        systemAdministratorHeader: {
            selector: "//div[@id='single-spa-application:@ebs/cs']/div/header/div/h6",
            locateStrategy: "xpath",
        },
        systemAdministratorLogo: {
            selector: "//img[contains(@src,'https://qa.ebsproject.org/mfe/ui/c07a7d83eef3dfc1270e.svg')]",
            locateStrategy: "xpath",
        },
        tenantManagementMenu: {
            selector: "//div[@id='single-spa-application:@ebs/cs']/div/div/div/ul/div/div[2]/span",
            locateStrategy: "xpath",
        },
        tenantManagementText: {
            selector: "//button[@id='tab-0']/span/p",
            locateStrategy: "xpath",
        },
        userManagementMenu: {
            selector: "//div[@id='single-spa-application:@ebs/cs']/div/div/div/ul/div[2]/div[2]/span",
            locateStrategy: "xpath",
        },
        userManagementText: {
            selector: "//button[@id='tab-0']/span/p",
            locateStrategy: "xpath",
        },
        CRMMenu: {
            selector: "//div[@id='single-spa-application:@ebs/cs']/div/div/div/ul/div[3]/div[2]/span",
            locateStrategy: "xpath",
        },
        CRMText: {
            selector: "//button[@id='tab-0']/span/p",
            locateStrategy: "xpath",
        },
        printoutMenu: {
            selector: "//div[@id='single-spa-application:@ebs/cs']/div/div/div/ul/div[4]/div[2]/span",
            locateStrategy: "xpath",
        },
        printoutText: {
            selector: "//button[@id='tab-0']/span/p",
            locateStrategy: "xpath",
        },
        shipmentManagerMenu: {
            selector: "//div[@id='single-spa-application:@ebs/cs']/div/div/div/ul/div[5]/div[2]/span",
            locateStrategy: "xpath",
        },
        // shipmentManagerText: {
        //     selector: "",
        //     locateStrategy: "xpath",
        // },

    },
    commands: [
        {

        },
    ],
};
