const { client } = require("nightwatch-api");

module.exports = {
    url: "https://cb-uat.ebsproject.org/",
    // commands: [commands.commands],
    elements: {
        loginButton: {
            selector: "//*[text()[contains(.,'Login')]]",//*[@type='submit']",
            locateStrategy: "xpath",
        }, //"body > div.login-intro > div.login-text > a",
        googleButton:
            "#auth0-lock-container-1 > div > div.auth0-lock-center > form > div > div > div:nth-child(3) > span > div > div > div > div > div > div > div > div > div > div.auth0-lock-social-buttons-container > button > div.auth0-lock-social-button-icon",
        lastAccountButton:
            "#auth0-lock-container-1 > div > div.auth0-lock-center > form > div > div > div:nth-child(3) > span > div > div > div > div > div > div > div > div > button > div.auth0-lock-social-button-icon",
        
        csLoginButton: {
            selector: "//div[@id='single-spa-application:@ebs/cs']/div/div[2]/header/div/button",
            locateStrategy: "xpath",
        },

        microsoftLogin: {
            selector: "//*[contains(@title, 'CIMMYT')]",
            locateStrategy: "xpath",
        },
        googleLogin: {
            selector: "//*[contains(@title, 'GOOGLE')]",
            locateStrategy: "xpath",
        },
        loginEmail: {
            selector: "//*[@type='email']",
            locateStrategy: "xpath",
        },
        loginPassword: {
            selector: "//*[@type='password']",
            locateStrategy: "xpath",
        },
        loginSubmitButton: {
            selector: "//*[@type='submit']",
            locateStrategy: "xpath",
        },
        staySignedinButton: "#idBtn_Back",
        googleUsernameText: "#identifierId",
        googleUsernameNextButton: "#identifierNext",
        googlePasswordText: "#password input",
        googlePasswordNextButton: "#passwordNext button",

        showLeftNav: "#go-to-show-left-nav",
        showSideNav:
            "#top-nav-bar > div > ul.menu-item-start > li.tooltipped.show-side-nav-btn",
        alwaysShowSideNav: "#slide-out > a",

        leftMenuSearch: {
            selector: "//*[@id='slide-out']/./li[6]/a[text()='Search']",
            locateStrategy: "xpath",
        },

        pageLabelGermplasm: {
            selector: "//h3[text()[contains(.,'Germplasm')]]",
            locateStrategy: "xpath",
        },
        pageLabelSeeds: {
            selector: "//h3[text()[contains(.,'Seeds')]]",
            locateStrategy: "xpath",
        },
        pageLabelTraits: {
            selector: "//h3[text()[contains(.,'Traits')]]",
            locateStrategy: "xpath",
        },

        browserPageOne: { selector: "//a[text()='1']", locateStrategy: "xpath" },

        browserLastPage: {
            selector: "//a[text()[contains(.,'Last')]]",
            locateStrategy: "xpath",
        },

        browserFirstPage: {
            selector: "//a[text()[contains(.,'First')]]",
            locateStrategy: "xpath",
        },

        //START - DATA FILTERS
        //dataFiltersMenu: "#go-to-data-filters",
        dataFiltersMenu: {
            selector: "//*[@id='go-to-data-filters']/i",
            locateStrategy: "xpath",
        },
        dataFiltersProgram: "#select2-program_select-container",
        dataFiltersYear: {
            selector: "//*[@id='year_select']/..//input[@type='search']",
            locateStrategy: "xpath",
        },
        dataFiltersSeason: {
            selector: "//*[@id='season_select']/..//input[@type='search']",
            locateStrategy: "xpath",
        },
        dataFiltersStage: {
            selector: "//*[@id='stage_select']/..//input[@type='search']",
            locateStrategy: "xpath",
        },
        dataFiltersSite: {
            selector: "//*[@id='site_select']/..//input[@type='search']",
            locateStrategy: "xpath",
        },
        dataFiltersApply: "#apply-dashboard-filter",
        dataFilterSuccessNotif: {
            selector: "//*[@id='w7-success-0']/div/text()",
            locateStrategy: "xpath",
        },

        selectAllYear: {
            selector: "//*[@id='s2-togall-year_select']/span[1]/i",
            locateStrategy: "xpath",
        },
        unselectAllYear: {
            selector: "//*[@id='s2-togall-year_select']/span[2]/i",
            locateStrategy: "xpath",
        },
        selectAllSeason: {
            selector: "//*[@id='s2-togall-season_select']/span[1]/i",
            locateStrategy: "xpath",
        },
        unselectAllSeason: {
            selector: "//*[@id='s2-togall-season_select']/span[2]/i",
            locateStrategy: "xpath",
        },
        selectAllStage: {
            selector: "//*[@id='s2-togall-stage_select']/span[1]/i",
            locateStrategy: "xpath",
        },
        unselectAllStage: {
            selector: "//*[@id='s2-togall-stage_select']/span[2]/i",
            locateStrategy: "xpath",
        },
        selectAllSite: {
            selector: "//*[@id='s2-togall-site_select']/span[1]/i",
            locateStrategy: "xpath",
        },
        unselectAllSite: {
            selector: "//*[@id='s2-togall-site_select']/span[2]/i",
            locateStrategy: "xpath",
        },

        currentFiltersMenu: {
            selector: "//*[@id='top-nav-bar']/div/ul[1]/li[8]",
            locateStrategy: "xpath",
        },
        yearFilter: {
            selector: "//*[text()[contains(.,'Year')]]/../span/span[1]/span/ul/li",
            locateStrategy: "xpath",
        },
        yearRemoveIcon: ".select2-selection__choice__remove",
        //END - DATA FILTERS

        //START - GERMPLASM
        subleftMenuSearchGermplasm: {
            selector: "//*[@id='slide-out']/./li[8]/a[text()='Germplasm']",
            locateStrategy: "xpath",
        },

        activeQueryParameters: {
            selector: "//*[@id='query-form']",
            locateStrategy: "xpath",
        },

        queryName: {
            selector: "//*[@id='query-fld-NAME']",
            locateStrategy: "xpath",
        },

        queryDataType:
            "#query-form > div:nth-child(2) > div:nth-child(2) > span > span.selection > span > ul > li > input",

        addtlParamDivGermplasmTraits: "#addl-query-acc",

        addtlParamContainer: "#additional-query-list",

        queryResultsSummary: {
            selector: "//*[@id='germplasm-grid-id']/div/div[1]/div[1]/div",
            locateStrategy: "xpath",
        },

        queryFindButton: "#query-find-btn",

        selectAllResults:
            "#germplasm-grid-id > div > div.floatThead-wrapper > div.kv-thead-float > table > thead > tr:nth-child(1) > th:nth-child(1) > label", //{selector:"//*[@id='germplasm-grid-id_select_all']", locateStrategy:"xpath"},

        selectedItemCount: "#selected-items-count",

        page2Results:
            "#germplasm-grid-id > div > div.panel-footer > div.kv-panel-pager > ul > li:nth-child(3) > a",

        previousPageResults:
            "#germplasm-grid-id > div > div.panel-footer > div.kv-panel-pager > ul > li.prev > a",

        //END - GERMPLASM

        //START - TRAITS
        resetGridGermplasmTraits: {
            selector: "//*[@id='reset-traits-grid']",
            locateStrategy: "xpath",
        },

        collapsibleSearchQuery: ".collapsible-header:nth-child(2)",

        resetQueryParam: {
            selector: "//*[@id='query-clear-all-btn']",
            locateStrategy: "xpath",
        },
        //END - TRAITS

        //START - SEEDS

        activeSeedsQueryParameters: {
            selector: "//*[@id='find-seeds-filter']",
            locateStrategy: "xpath",
        },

        resetQueryParemeters: {
            selector: "//*[@id='reset-btn']",
            locateStrategy: "xpath",
        },

        //Basic search parameters fields selector
        searchParamSection: {
            selector:
                "//*[@id='filters']//*[text()='Search Parameters']",
            locateStrategy: "xpath",
        }, //"#query-params",
        seasonfield:
            //"#variable-filters > div > div > div.basic-search-params > div:nth-child(2) > div:nth-child(2) > div.row.col-md-12 > span > span.selection > span > ul > li > input",
            "#basic_filters_div > div > div:nth-child(2) > div.row.col-md-12 > span > span.selection > span > ul > li > input",
        exptyrfield:
            "#basic_filters_div > div > div:nth-child(3) > div.row.col-md-12 > span > span.selection > span > ul > li > input",
        expttypefield:
            //"#variable-filters > div > div > div.basic-search-params > div:nth-child(2) > div:nth-child(3) > div.row.col-md-12 > span > span.selection > span > ul > li > input",
            "#basic_filters_div > div > div:nth-child(4) > div.row.col-md-12 > span > span.selection > span > ul > li > input",
        occfield:
            //"#variable-filters > div > div > div.basic-search-params > div:nth-child(2) > div:nth-child(4) > div.row.col-md-12 > span > span.selection > span > ul > li > input",
            "#basic_filters_div > div > div:nth-child(5) > div.row.col-md-12 > span > span.selection > span > ul > li > input",
        facfield:
            //"#variable-filters > div > div > div.basic-search-params > div:nth-child(2) > div:nth-child(5) > div.row.col-md-12 > span > span.selection > span > ul > li > input",
            "#basic_filters_div > div > div:nth-child(6) > div.row.col-md-12 > span > span.selection > span > ul > li > input",
        subfacfield:
            //"#variable-filters > div > div > div.basic-search-params > div:nth-child(2) > div:nth-child(6) > div.row.col-md-12 > span > span.selection > span > ul > li > input",
            "#basic_filters_div > div > div:nth-child(7) > div.row.col-md-12 > span > span.selection > span > ul > li > input",

        selectDrySeason: {
            selector:
                "//*[@id='select2-season-filter-results']//li[contains(text(),'Dry')]",
            locateStrategy: "xpath",
        },
        selectWetSeason: {
            selector:
                "//*[@id='select2-season-filter-results']//li[contains(text(),'Wet')]",
            locateStrategy: "xpath",
        },
        selectSeasonFilter: {
            selector: "//*[@id='select2-season-filter-results']",
            locateStrategy: "xpath",
        },
        selectSeasonFilterLI: "#select2-season-filter-results > li", //{selector:"//*[@id='select2-season-filter-results']/li", locateStrategy:"xpath"},
        dryseason: {
            selector: "//*[@id='variable-filters']//*[contains(@title, 'Dry')]",
            locateStrategy: "xpath",
        },
        wetseason: {
            selector: "//*[@id='variable-filters']//*[contains(@title, 'Wet')]",
            locateStrategy: "xpath",
        },

        selectCrossNursery: {
            selector:
                "//*[@id='select2-experiment_type-filter-results']//li[contains(text(),'Intentional')]",
            locateStrategy: "xpath",
        },
        selectGenNursery: {
            selector:
                "//*[@id='select2-experiment_type-filter-results']//li[contains(text(),'Generation')]",
            locateStrategy: "xpath",
        },
        selectBreedingTrial: {
            selector:
                "//*[@id='select2-experiment_type-filter-results']//li[contains(text(),'Breeding')]",
            locateStrategy: "xpath",
        },
        selectExptTypeFilter: {
            selector: "//*[@id='select2-experiment_type-filter-results']",
            locateStrategy: "xpath",
        },
        crossnursery: {
            selector:
                "//*[@id='variable-filters']//*[contains(@title, 'Intentional Crossing Nursery')]",
            locateStrategy: "xpath",
        },
        gennursery: {
            selector:
                "//*[@id='variable-filters']//*[contains(@title, 'Generation Nursery')]",
            locateStrategy: "xpath",
        },
        breedingtrial: {
            selector:
                "//*[@id='variable-filters']//*[contains(@title, 'Breeding Trial')]",
            locateStrategy: "xpath",
        },

        //Additional Search Parameters fields selector    
        addtlParamSection: {
            selector:
                "//*[@id='filters']//*[text()='Additional Search Parameters']",
            locateStrategy: "xpath",
        },
        addtlParamDiv: {
            selector: "//*[@id='additional-params-header']", //selector: "//*[@id='additional-params']",
            locateStrategy: "xpath",
        },

        filtersButton: {
            selector: "//*[@id='show-add-params-btn']",
            locateStrategy: "xpath",
        },

        filtersSelectParam: {
            selector: "//*[@id='dropdown-add-params']",
            locateStrategy: "xpath",
        },

        inputListParamButton: {
            selector: "//*[@id='show-list-params-btn']",
            locateStrategy: "xpath",
        },

        inputListParamSelection: {
            selector: "//*[@id='field-type-selector']",
            locateStrategy: "xpath",
        },

        selectLocation: {
            selector: "//*[@id='location-filter-selected']",
            locateStrategy: "xpath",
        },

        locationfield: {
            selector:
                "//*[@id='location-filter-selected']/../span/span/span/ul/li/input",
            locateStrategy: "xpath",
        },

        findSeedsButton: "#find-btn", ////*[@id="action-btns"]//*[contains(@title, 'Find Seeds')]

        configtoggle: "#dropdown-config-find-seeds",

        resultsConfigbutton: {
            selector: "//*[@title='Search results settings']",
            locateStrategy: "xpath",
        },

        sliderAutomaticSelection: {
            selector:
                "#dropdown-config-find-seeds > li:nth-child(2) > div > label > span",
        },

        sliderGroupSelection: {
            selector:
                "#dropdown-config-find-seeds > li:nth-child(2) > div > label > span",
        },

        noresultsfound: {
            selector:
                "//*[@id='search-grid-table-con-container']//*[text()='No results found.']",
            locateStrategy: "xpath",
        },

        resultsummary:
            "#search-grid-table-con > div > div.kv-panel-before > div:nth-child(2) > div",

        resultpackagecount:
            "#search-grid-table-con > div > div.kv-panel-before > div:nth-child(2) > div > b:nth-child(2)",

        workinglistCancelBtn: "#cancel-clear-list-btn",

        workinglistClearBtn: "#clear-working-list",

        workinglistnoresults: {
            selector:
                "//*[@id='dynagrid-find-seed-list-container']//*[text()='No results found.']",
            locateStrategy: "xpath",
        },

        wlistresultsummary: {
            selector: "//*[@id='dynagrid-find-seed-list']/div/div[1]/div[2]/div",
            locateStrategy: "xpath",
        },

        wlistinsertdone: {
            selector: "//*[@id='toast-container']/div/text()",
            locateStrategy: "xpath",
        },

        wlistinsertnotif: {
            selector: "//*[@id='bg-progress-notif-msg']",
            locateStrategy: "xpath",
        },

        singlepackage: {
            selector:
                "//*[@id='search-grid-table-con-container']//*[contains(text(),'Package Count: 1')]",
            locateStrategy: "xpath",
        },

        //END - SEEDS

        //START - DATA COLLECTION

        dataCollectionMenu:
            "#slide-out > li.parent-menu.data-collection-qc-quality-control-left-menu > a",
        dataCollectionBrowserTable: {
            selector: "//*[@id='operational-studies-grid-id-container']",
            locateStrategy: "xpath",
        },

        //END - DATA COLLECTION

        //------ EXPERIMENT CREATION START --------//
        experimentCreationMenu: "#slide-out li.parent-menu:nth-child(2) a:nth-child(1)",

        experimentTile: "#tools-widget-51-0-col_2-0- .material-icons.metrics-icon",

        createButton: "#create-expt-btn",

        experimentTypeSelect: {
            selector:
                //"//*[@id='experiment_type']/..//span[@class='select2-selection__arrow']",
                "//*[@id='basic-info1']/div[1]/div/div/span/span[1]/span/span[2]",
            locateStrategy: "xpath",
        },
        experimentTemplateSelect: {
            selector:
                "//*[@id='experiment_template']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },
        pipelineSelect: {
            selector:
                "//*[@id='PIPELINE-identification-2349']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },
        experimentNameField: "#EXPERIMENT_NAME-identification-2247",
        experimentObjectiveField: "#EXPERIMENT_OBJECTIVE-identification-2341",
        experimentYearField: "#EXPERIMENT_YEAR-identification-2339",
        stageSelect: {
            selector:
                "//*[@id='STAGE-identification-2337']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },
        evaluationSeasonSelect: {
            selector:
                "//*[@id='SEASON-identification-315']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },
        plantingSeasonField: "#PLANTING_SEASON-identification-2340",
        experimentStewardSelect: {
            selector:
                "//*[@id='EXPERIMENT_STEWARD-identification-2345']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },
        projectSelect: {
            selector:
                "//*[@id='PROJECT-identification-2338']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },

        designResult: {
            selector:
                "//*[@id='layout-panel-div']/div[contains(text(), 'Experiment successfully randomized. Layout cannot be shown for this design configuration.')]",
            locateStrategy: "xpath",
        },
        no_action_btn: {
            selector: "//a[@id='no_action_btn'][contains(text(), 'Next')]",
            locateStrategy: "xpath",
        },
        saveButton: {
            selector: "//a[@id='next-btn'][contains(text(), 'Save')]",
            locateStrategy: "xpath",
        },
        nextButtonEnabled: "#next-btn",
        nextButton: {
            selector: "//a[@id='next-btn'][contains(text(), 'Next')]",
            locateStrategy: "xpath",
        },
        finalizeButton: {
            selector: "//a[@id='next-btn'][contains(text(), 'Finalize')]",
            locateStrategy: "xpath",
        },

        editExperimentIcon: "a[title='Update experiment']",
        resetButton: "#reset-dynagrid-experiment-browser",
        addButton: "#add-entry-list-btn",
        selectButton: "#select-btn",
        loadingStatus: "#layout-panel-div > div",

        b4rLogo: "#top-nav-bar > div > a",

        savedListButton: "a[title='Saved List']",
        copyEntryListButton: "a[title='Copy Entry List']",
        inputListButton: "a[title='Input List']",

        designSelect: {
            selector:
                "//*[@id='design_select_id']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },

        numberOccurrencesField: "#nTrial",
        numberReplicatesField: "#nRep",
        numberPlotsPerBlockSelect: {
            selector:
                "//*[@id='select2-sBlk-container']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },
        rowsField: "#nFieldRow",
        plotsUntilField: "#nPlotBarrier",

        closeButton: "#add-entries-modal #cancel-save-btn",

        generateDesignButton: "#submit_design_btn",

        experimentFilterField: "input[name='ExperimentSearch[experimentName]']",
        rowsSelect: {
            selector:
                "//*[@id='select2-nFieldRow-container']",
            // "//*[@id='nFieldRow']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },


        plotsUntilSelect: {
            selector:
                "//*[@id='select2-nPlotBarrier-container']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },

        noRowsPerRepSelect: {
            selector:
                "//*[@id='nRowPerRep']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },

        rowBlocksSelect: {
            selector:
                "//*[@id='select2-nRowBlk-container']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },

        noBlocksPerSelect: {
            selector:
                "//*[@id='nBlk']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },

        noBlocksPerSelect: {
            selector:
                "//*[@id='nBlk']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },

        noRowsPerBlock: {
            selector:
                "//*[@id='nRowPerBlk']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },

        //checkbox, go to the row switch
        shapeCheckbox: { selector: "//*[@id='genLayout']", locateStrategy: "xpath" },
        randomizeCheckbox: { selector: "//*[@id='rand1']", locateStrategy: "xpath" },

        layoutPanel: "#design-layout-panel",

        siteSelect: {
            selector:
                "//*[@id='grid-occurrence-id-container']/table/tbody/tr/td[4]/span/span/span/span",
            locateStrategy: "xpath",
        },

        siteSaveAssert: {
            selector: "//a[contains(@class,'ok-btn btn')]",
            locateStrategy: "xpath",
        },

        //------ EXPERIMENT CREATION END --------//


        //for EM
        experimentManagerMenu: {
            selector:
                "//*[@id='PROJECT-identification-2338']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },

        designResult: {
            selector:
                "//*[@id='layout-panel-div']/div[contains(text(), 'Experiment successfully randomized. Layout cannot be shown for this design configuration.')]",
            locateStrategy: "xpath",
        },
        no_action_btn: {
            selector: "//a[@id='no_action_btn'][contains(text(), 'Next')]",
            locateStrategy: "xpath",
        },
        saveButton: {
            selector: "//a[@id='next-btn'][contains(text(), 'Save')]",
            locateStrategy: "xpath",
        },
        nextButton: {
            selector: "//a[@id='next-btn'][contains(text(), 'Next')]",
            locateStrategy: "xpath",
        },

        editExperimentIcon: "a[title='Update experiment']",
        resetButton: "#reset-dynagrid-experiment-browser",
        addButton: "#add-entry-list-btn",
        selectButton: "#select-btn",
        loadingStatus: "#layout-panel-div > div",

        b4rLogo: "#top-nav-bar > div > a",

        savedListButton: "a[title='Saved List']",
        copyEntryListButton: "a[title='Copy Entry List']",
        inputListButton: "a[title='Input List']",

        designSelect: {
            selector:
                "//*[@id='design_select_id']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },

        numberOccurrencesField: "#nTrial",
        numberReplicatesField: "#nRep",
        numberPlotsPerBlockSelect: {
            selector:
                "//*[@id='select2-sBlk-container']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },
        rowsField: "#nFieldRow",
        plotsUntilField: "#nPlotBarrier",

        closeButton: "#add-entries-modal #cancel-save-btn",

        generateDesignButton: "#submit_design_btn",

        experimentFilterField: "input[name='ExperimentSearch[experimentName]']",
        rowsSelect: {
            selector: "//*[@id='select2-nFieldRow-container']",
            // "//*[@id='nFieldRow']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },

        plotsUntilSelect: {
            selector:
                "//*[@id='select2-nPlotBarrier-container']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },

        noRowsPerRepSelect: {
            selector:
                "//*[@id='nRowPerRep']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },

        rowBlocksSelect: {
            selector:
                "//*[@id='select2-nRowBlk-container']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },

        noBlocksPerSelect: {
            selector: "//*[@id='nBlk']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },

        noBlocksPerSelect: {
            selector: "//*[@id='nBlk']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },

        noRowsPerBlock: {
            selector:
                "//*[@id='nRowPerBlk']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },

        //checkbox, go to the row switch
        shapeCheckbox: {
            selector: "//*[@id='genLayout']",
            locateStrategy: "xpath",
        },
        randomizeCheckbox: {
            selector: "//*[@id='rand1']",
            locateStrategy: "xpath",
        },

        layoutPanel: "#design-layout-panel",

        //------ EXPERIMENT CREATION END --------//
    },
    sections: {
        leftNavigation: {
            selector: "#slide-out",
            elements: {
                experimentCreationLink: {
                    selector: ".//a[normalize-space(.)='Experiment creation']",
                    locateStrategy: "xpath",
                },
                experimentManagerLink: {
                    selector: ".//a[normalize-space(.)='Experiment manager']",
                    locateStrategy: "xpath",
                },
                listManagerLink: {
                    selector: ".//a[normalize-space(.)='List manager']",
                    locateStrategy: "xpath",
                },
            },
        },
        listsGrid: {
            selector: "#my-lists-grid-pjax",
            elements: {
                personalizeGridSettingsButton:
                    "button[title='Personalize grid settings']",
                nameField: "input[name='MyList[name]']",
                summaryText: "div.summary",
                summaryTable: "tr.my-lists-grid",
                listBrowserEmpty: "div.empty"
            },
        },
        seedsSearch: {
            selector: "//i[normalize-space(.)='search']/ancestor::li[@id='filters']",
            locateStrategy: "xpath",
            elements: {
                programSelect: {
                    selector: ".//*[@id='program-filter']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                seasonSelect: {
                    selector: ".//*[@id='season-filter']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                experimentYearSelect: {
                    selector:
                        ".//*[@id='experiment_year-filter']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                experimentTypeSelect: {
                    selector:
                        ".//*[@id='experiment_type-filter']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                occurrenceSelect: {
                    selector:
                        ".//*[@id='occurrence_name-filter']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                facilitySelect: {
                    selector:
                        ".//*[@id='facility-filter']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                subFacilitySelect: {
                    selector:
                        ".//*[@id='sub_facility-filter']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                findButton: {
                    selector: ".//button[@title='Find Seeds']",
                    locateStrategy: "xpath",
                },
            },
        },
        dataFilters: {
            selector: "//*[@id='data-filters']",
            locateStrategy: "xpath",
            elements: {
                programSearchSelect: {
                    // selector: ".//*[@id='program_select']/following-sibling::span//input",
                    selector: ".//*[@id='select2-program-dashboard_select-container']", //".//*[@id='select2-program_select-container']",
                    locateStrategy: "xpath",
                },
                yearSelect: {
                    selector: ".//*[@id='year_select']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                seasonSelect: {
                    selector: ".//*[@id='season_select']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                stageSelect: {
                    selector: ".//*[@id='stage_select']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                siteSelect: {
                    selector: ".//*[@id='site_select']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                experimentsSlider: "input[id='data_filter_show_owned_experiments']",
                cancelButton: {
                    selector: ".//button[normalize-space(.)='Cancel']",
                    locateStrategy: "xpath",
                },
                applyButton: {
                    selector: ".//button[normalize-space(.)='Apply']",
                    locateStrategy: "xpath",
                },
            },
        },
        searchGrid: {
            selector: "#search-grid-pjax",
            elements: {
                settingsButton: "a[title='Search results settings']",
                addToSeedListButton: "#add-to-list-btn",
                multipleSelectionSlider: "#selectOption-switch-id",
                automaticSelectionSlider: "#autoSelectOption-switch-id",
                groupResultsSlider: "#grouped-switch-id",
                retainSelectionSlider: "#retained-switch-id",
                checkAllCheckbox: "#seed-inventory-seedlotCode_check-all",
                workingListViewerButton: "#cancel-list-btn"
            },
        },
        workingListGrid: {
            selector: "#dynagrid-find-seed-list", //"#dynagrid-find-seed-list-container", //
            elements: {
                clearListButton: "#confirm-clear-list",
                saveButton: "#save-list-btn",
            },
        },
        myListsGrid: {
            selector: "#my-lists",
            elements: {},
        },
        clearWorkingListModal: {
            selector: "#clear-list-confirm-modal",
            elements: {
                clearButton: "#clear-working-list",
                saveButton: "#save-working-list-items-btn",
                cancelButton: "#cancel-clear-list-btn",
            },
        },
        confirmSaveModal: {
            selector: "#confirm-save-modal",
            elements: {
                saveButton: "#save-btn",
                cancelButton: "#return-btn",
                closeButton: "#close-modal-btn",
            },
        },
        saveConfirmModal: {
            selector: "#save-confirm-modal",
            elements: {
                closeButton: "#close-modal-btn",
            },
        },
        createListModal: {
            selector: "#create-list-modal",
            elements: {
                nameField: "#list-name",
                abbrevField: "#list-abbrev",
                displayNameField: "#list-display-name",
                descriptionField: "#entry-list-description",
                remarksField: "#entry-list-remarks",
                createButton: "#create-list-btn",
            },
        },
    },
    commands: [
        {
            //Set values of a listbox
            //Listbox allows searching of a value
            listBoxSetValue: async function (input, value) {
                //clear existing values
                let originalSelector = input.__selector;
                input.__selector =
                    input.__selector +
                    "/../../li[@class='select2-selection__choice']/span";

                let elements = 0;
                await this.api.findElements(input, function (result) {
                    elements = result.value;
                });

                while (elements.length > 0) {
                    await this.api.elementIdClick(elements[0].getId());
                    await this.api.findElements(input, function (result) {
                        elements = result.value;
                    });
                }

                //revert original selector
                input.__selector = originalSelector;

                //split string by ,
                let listItems = value.split(",");
                for (let index = 0; index < listItems.length; index++) {
                    let item = listItems[index].trim();
                    await this.click(input);
                    //set value to display search result match
                    await this.setValue(input, item);
                    const control = {
                        selector: `//span//ul[@role='listbox']/li[normalize-space(.)='${item}']`,
                        locateStrategy: "xpath",
                    };
                    //click match
                    await this.waitForElementVisible(control);
                    await this.click(control);
                }
            },

            //use for slider style (checkbox)
            sliderSetValue: async function (input, value) {
                //save selector
                const originalSelector = input.__selector;
                switch (value) {
                    case "Left":
                        input.__selector = input.__selector + ":not(:checked)";
                        break;
                    case "Right":
                        input.__selector = input.__selector + ":checked";
                        break;
                    default:
                        throw new Error(`Unexpected Slider value (Left, Right).`);
                }
                let found;
                await this.waitForElementPresent(input, 1000, 500, false, (result) => {
                    found = result.value;
                });

                if (!found) {
                    //add + span to click the control
                    input.__selector = originalSelector + " + span";
                    await this.click(input);
                }
            },
            //Select with Search
            searchSelectSetValue: async function (input, value) {
                await this.click(input);
                await this.pause(1000);
                const control = {
                    selector: `//li[normalize-space(.)='${value.trim()}']`,
                    locateStrategy: "xpath",
                };
                await this.waitForElementVisible(control);
                await this.click(control);
            },
            //Wait Until Grid is Ready
            waitGridReady: async function (input) {
                //grid selector must be CSS
                let originalSelector = input.__selector;
                input.__selector = input.__selector + ".kv-grid-loading";
                await this.waitForElementNotPresent(input, 1200000, 1000);
                input.__selector = originalSelector;
            },

            setCheckbox: async function (input, value) {
                let status = false;
                await this.getAttribute(input, "checked", (result) => {
                    if (result.value != null) {
                        status = true;
                    }
                });
                if ((value && !status) || (!value && status)) {
                    await this.api.execute(
                        `document.querySelector("${input.__selector}").click();`
                    );
                }
            },

            getRowCount: async function (input) {
                try {
                    let originalSelector = input.__selector;
                } catch (error) {
                } finally {
                }
            },
        },
    ],
};
