const { client } = require("nightwatch-api");

module.exports = {
    url: "https://cb-uat.ebsproject.org/",
    // commands: [commands.commands],
    sections: {
        browserColFilters: {
            selector: "//*[@id='search-grid-table-con']",
            locateStrategy: "xpath",
            elements: {
                parentageColumnSearch: {
                    selector: ".//*[@id='search-grid-table-con-filters']//*[contains(@name, 'parentage')]",
                    locateStrategy: "xpath",
                },
                labelColumnSearch: {
                    selector: ".//*[@id='search-grid-table-con-filters']//*[contains(@name, 'label')]",
                    locateStrategy: "xpath",
                },
                seednameColumnSearch: {
                    selector: ".//*[@id='search-grid-table-con-filters']//*[contains(@name, 'seed_name')]",
                    locateStrategy: "xpath",
                },
                gidColumnSearch: {
                    selector: ".//*[@id='search-grid-table-con-filters']//*[contains(@name, 'gid')]",
                    locateStrategy: "xpath",
                },
                qtyColumnSearch: {
                    selector: ".//*[@id='search-grid-table-con-filters']//*[contains(@name, 'volume')]",
                    locateStrategy: "xpath",
                },
                unitColumnSearch: {
                    selector: ".//*[@id='search-grid-table-con-filters']//*[contains(@name, 'unit')]",
                    locateStrategy: "xpath",
                },
                othernameColumnSearch: {
                    selector: ".//*[@id='search-grid-table-con-filters']//*[contains(@name, 'other_name')]",
                    locateStrategy: "xpath",
                },
                yearColumnSearch: {
                    selector: ".//*[@id='search-grid-table-con-filters']//*[contains(@name, 'year')]",
                    locateStrategy: "xpath",
                },
                exptypeColumnSearch: {
                    selector: ".//*[@id='search-grid-table-con-filters']//*[contains(@name, 'experiment_type')]",
                    locateStrategy: "xpath",
                },
                expnameColumnSearch: {
                    selector: ".//*[@id='search-grid-table-con-filters']//*[contains(@name, 'experiment_name')]",
                    locateStrategy: "xpath",
                },
                occnameColumnSearch: {
                    selector: ".//*[@id='search-grid-table-con-filters']//*[contains(@name, 'occurrence_name')]",
                    locateStrategy: "xpath",
                },
                seasonColumnSearch: {
                    selector: ".//*[@id='search-grid-table-con-filters']//*[contains(@name, 'season')]",
                    locateStrategy: "xpath",
                }
            }
        },
        leftNavigation: {
            selector: "#slide-out",
            elements: {
                experimentCreationLink: {
                    selector: ".//a[normalize-space(.)='Experiment creation']",
                    locateStrategy: "xpath",
                },
                experimentManagerLink: {
                    selector: ".//a[normalize-space(.)='Experiment manager']",
                    locateStrategy: "xpath",
                },
                plantingInstructionsManagerLink: {
                    selector: ".//a[normalize-space(.)='Planting instructions manager']",
                    locateStrategy: "xpath",
                },
                dataCollectionLink: {
                    selector: ".//a[normalize-space(.)='Data collection']",
                    locateStrategy: "xpath",
                },
                harvestManagerLink: {
                    selector: ".//a[normalize-space(.)='Harvest manager']",
                    locateStrategy: "xpath",
                },
                germplasmLink: {
                    selector: ".//a[normalize-space(.)='Germplasm']",
                    locateStrategy: "xpath",
                },
                seedsearchLink: {
                    selector: ".//a[normalize-space(.)='Seed search']",
                    locateStrategy: "xpath",
                },
                traitsLink: {
                    selector: ".//a[normalize-space(.)='Traits']",
                    locateStrategy: "xpath",
                },
                listManagerLink: {
                    selector: ".//a[normalize-space(.)='List manager']",
                    locateStrategy: "xpath",
                },
            },
        },
        listsGrid: {
            selector: "#my-lists-grid-pjax",
            elements: {
                personalizeGridSettingsButton:
                    "button[title='Personalize grid settings']",
                nameField: "input[name='MyList[name]']",
                summaryText: "div.summary",
                summaryTable: "tr.my-lists-grid",
                listBrowserEmpty: "div.empty"
            },
        },
        seedsSearch: {
            selector: "//i[normalize-space(.)='search']/ancestor::li[@id='filters']",
            locateStrategy: "xpath",
            elements: {
                programSelect: {
                    selector: ".//*[@id='program-filter']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                seasonSelect: {
                    selector: ".//*[@id='season-filter']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                experimentYearSelect: {
                    selector:
                        ".//*[@id='experiment_year-filter']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                experimentTypeSelect: {
                    selector:
                        ".//*[@id='experiment_type-filter']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                occurrenceSelect: {
                    selector:
                        ".//*[@id='occurrence_name-filter']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                facilitySelect: {
                    selector:
                        ".//*[@id='facility-filter']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                subFacilitySelect: {
                    selector:
                        ".//*[@id='sub_facility-filter']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                findButton: {
                    selector: ".//button[@title='Find Seeds']",
                    locateStrategy: "xpath",
                },
            },
        },
        dataFilters: {
            selector: "//*[@id='data-filters']",
            locateStrategy: "xpath",
            elements: {
                programSearchSelect: {
                    // selector: ".//*[@id='program_select']/following-sibling::span//input",
                    selector: ".//*[@id='select2-program-dashboard_select-container']", //".//*[@id='select2-program_select-container']",
                    locateStrategy: "xpath",
                },
                yearSelect: {
                    selector: ".//*[@id='year_select']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                seasonSelect: {
                    selector: ".//*[@id='season_select']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                stageSelect: {
                    selector: ".//*[@id='stage_select']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                siteSelect: {
                    selector: ".//*[@id='site_select']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                experimentsSlider: "input[id='data_filter_show_owned_experiments']",
                cancelButton: {
                    selector: ".//button[normalize-space(.)='Cancel']",
                    locateStrategy: "xpath",
                },
                applyButton: {
                    selector: ".//button[normalize-space(.)='Apply']",
                    locateStrategy: "xpath",
                },
            },
        },
        searchGrid: {
            selector: "#search-grid-pjax",
            elements: {
                settingsButton: "a[title='Search results settings']",
                addToSeedListButton: "#add-to-list-btn",
                multipleSelectionSlider: "#selectOption-switch-id",
                automaticSelectionSlider: "#autoSelectOption-switch-id",
                groupResultsSlider: "#grouped-switch-id",
                retainSelectionSlider: "#retained-switch-id",
                checkAllCheckbox: "#seed-inventory-seedlotCode_check-all",
                workingListViewerButton: "#cancel-list-btn",
                selectedItemCount: "#selected-items-text", /*{
                selector: ".//*[@id='selected-items-text']",
                locateStrategy: "xpath",
                }*/
            },
        },
        workingListGrid: {
            selector: "#dynagrid-find-seed-list", //"#dynagrid-find-seed-list-container", //
            elements: {
                clearListButton: "#confirm-clear-list",
                saveButton: "#save-list-btn",
            },
        },
        myListsGrid: {
            selector: "#my-lists",
            elements: {},
        },
        clearWorkingListModal: {
            selector: "#clear-list-confirm-modal",
            elements: {
                clearButton: "#clear-working-list",
                saveButton: "#save-working-list-items-btn",
                cancelButton: "#cancel-clear-list-btn",
            },
        },
        confirmSaveModal: {
            selector: "#confirm-save-modal",
            elements: {
                saveButton: "#save-btn",
                cancelButton: "#return-btn",
                closeButton: "#close-modal-btn",
            },
        },
        saveConfirmModal: {
            selector: "#save-confirm-modal",
            elements: {
                closeButton: "#close-modal-btn",
            },
        },
        createListModal: {
            selector: "#create-list-modal",
            elements: {
                nameField: "#list-name",
                abbrevField: "#list-abbrev",
                displayNameField: "#list-display-name",
                descriptionField: "#entry-list-description",
                remarksField: "#entry-list-remarks",
                createButton: "#create-list-btn",
            },
        },
    },
    commands: [
        {
            //Set values of a listbox
            //Listbox allows searching of a value
            listBoxSetValue: async function (input, value) {
                //clear existing values
                let originalSelector = input.__selector;
                input.__selector =
                    input.__selector +
                    "/../../li[@class='select2-selection__choice']/span";

                let elements = 0;
                await this.api.findElements(input, function (result) {
                    elements = result.value;
                });

                while (elements.length > 0) {
                    await this.api.elementIdClick(elements[0].getId());
                    await this.api.findElements(input, function (result) {
                        elements = result.value;
                    });
                }

                //revert original selector
                input.__selector = originalSelector;

                //split string by ,
                let listItems = value.split(",");
                for (let index = 0; index < listItems.length; index++) {
                    let item = listItems[index].trim();
                    await this.click(input);
                    //set value to display search result match
                    await this.setValue(input, item);
                    const control = {
                        selector: `//span//ul[@role='listbox']/li[normalize-space(.)='${item}']`,
                        locateStrategy: "xpath",
                    };
                    //click match
                    await this.waitForElementVisible(control);
                    await this.click(control);
                }
            },

            //use for slider style (checkbox)
            sliderSetValue: async function (input, value) {
                //save selector
                const originalSelector = input.__selector;
                switch (value) {
                    case "Left":
                        input.__selector = input.__selector + ":not(:checked)";
                        break;
                    case "Right":
                        input.__selector = input.__selector + ":checked";
                        break;
                    default:
                        throw new Error(`Unexpected Slider value (Left, Right).`);
                }
                let found;
                await this.waitForElementPresent(input, 1000, 500, false, (result) => {
                    found = result.value;
                });

                if (!found) {
                    //add + span to click the control
                    input.__selector = originalSelector + " + span";
                    await this.click(input);
                }
            },
            //set column filter
            columnFilterSetValue: async function (input, value) {
                await this.click(input);
                await this.pause(1000);
                /*const control = {
                selector: `//li[normalize-space(.)='${value.trim()}']`,
                locateStrategy: "xpath",
                };*/
                await this.setValue(input, value, this.Keys.ENTER);

                //await this.waitForElementVisible(control);
                //await this.click(control);
            },
            //Select with Search
            searchSelectSetValue: async function (input, value) {
                await this.click(input);
                await this.pause(1000);
                const control = {
                    selector: `//li[normalize-space(.)='${value.trim()}']`,
                    locateStrategy: "xpath",
                };
                await this.waitForElementVisible(control);
                await this.click(control);
            },
            //Wait Until Grid is Ready
            waitGridReady: async function (input) {
                //grid selector must be CSS
                let originalSelector = input.__selector;
                input.__selector = input.__selector + ".kv-grid-loading";
                await this.waitForElementNotPresent(input, 1200000, 1000);
                input.__selector = originalSelector;
            },

            setCheckbox: async function (input, value) {
                let status = false;
                await this.getAttribute(input, "checked", (result) => {
                    if (result.value != null) {
                        status = true;
                    }
                });
                if ((value && !status) || (!value && status)) {
                    await this.api.execute(
                        `document.querySelector("${input.__selector}").click();`
                    );
                }
            },

            getRowCount: async function (input) {
                try {
                    let originalSelector = input.__selector;
                } catch (error) {
                } finally {
                }
            },
            getProgramAbbrev: function (program, fn) {
                let abbrev;
                //geocoder.geocode( { 'program': program}, function(results, status) {
                if (program == "Irrigated South-East Asia (IRSEA)") {
                    abbrev = "IRSEA";
                } else if (program == "BW Wheat Breeding Program (BW)") {
                    abbrev = "BW";
                } else if (program == "KE Maize Breeding Program (KE)") {
                    abbrev = "KE";
                } else if (program == "Barley Test Program (BTP)") {
                    abbrev = "BTP"
                }
                fn(abbrev);
            },
        },
    ],
    elements: {
        showLeftNav: "#go-to-show-left-nav",

        //START - DATA FILTERS
        //dataFiltersMenu: "#go-to-data-filters",
        dataFiltersMenu: {
            selector: "//*[@id='go-to-data-filters']/i",
            locateStrategy: "xpath",
        },
        dataFiltersProgram: "#select2-program_select-container",
        dataFiltersYear: {
            selector: "//*[@id='year_select']/..//input[@type='search']",
            locateStrategy: "xpath",
        },
        dataFiltersSeason: {
            selector: "//*[@id='season_select']/..//input[@type='search']",
            locateStrategy: "xpath",
        },
        dataFiltersStage: {
            selector: "//*[@id='stage_select']/..//input[@type='search']",
            locateStrategy: "xpath",
        },
        dataFiltersSite: {
            selector: "//*[@id='site_select']/..//input[@type='search']",
            locateStrategy: "xpath",
        },
        dataFiltersApply: "#apply-dashboard-filter",
        dataFilterSuccessNotif: {
            selector: "//*[@id='w7-success-0']/div/text()",
            locateStrategy: "xpath",
        },

        selectAllYear: {
            selector: "//*[@id='s2-togall-year_select']/span[1]/i",
            locateStrategy: "xpath",
        },
        unselectAllYear: {
            selector: "//*[@id='s2-togall-year_select']/span[2]/i",
            locateStrategy: "xpath",
        },
        selectAllSeason: {
            selector: "//*[@id='s2-togall-season_select']/span[1]/i",
            locateStrategy: "xpath",
        },
        unselectAllSeason: {
            selector: "//*[@id='s2-togall-season_select']/span[2]/i",
            locateStrategy: "xpath",
        },
        selectAllStage: {
            selector: "//*[@id='s2-togall-stage_select']/span[1]/i",
            locateStrategy: "xpath",
        },
        unselectAllStage: {
            selector: "//*[@id='s2-togall-stage_select']/span[2]/i",
            locateStrategy: "xpath",
        },
        selectAllSite: {
            selector: "//*[@id='s2-togall-site_select']/span[1]/i",
            locateStrategy: "xpath",
        },
        unselectAllSite: {
            selector: "//*[@id='s2-togall-site_select']/span[2]/i",
            locateStrategy: "xpath",
        },

        /*currentFiltersMenu: {
            selector: "//*[@id='top-nav-bar']/div/ul[1]/li[8]",
            locateStrategy: "xpath",
        },*/
        currentFiltersMenu: "#current-filters",
        filtersMenuItems: {
            selector: "//*[@id='top-nav-bar']/div/ul[1]/li[8]",
            locateStrategy: "xpath",
        },
        yearFilter: {
            selector: "//*[text()[contains(.,'Year')]]/../span/span[1]/span/ul/li",
            locateStrategy: "xpath",
        },
        yearRemoveIcon: ".select2-selection__choice__remove",

        //Tools' Entrypoint
        pageLabelExperiments: {
            selector: "//h3[text()[contains(.,'Experiments')]]",
            locateStrategy: "xpath",
        },
        pageLabelExpManager: {
            selector: "//h3[text()[contains(.,'Experiment Manager')]]",
            locateStrategy: "xpath",
        },
        pageLabelPlantingInsMgr: {
            selector: "//h3[text()[contains(.,'Planting Instructions Manager')]]",
            locateStrategy: "xpath",
        },
        pageLabelDataCollection: {
            selector: "//h3[text()[contains(.,'Data Collection')]]",
            locateStrategy: "xpath",
        },
        pageLabelHarvestMgr: {
            selector: "//h3[text()[contains(.,'Harvest Manager')]]",
            locateStrategy: "xpath",
        },
        pageLabelGermplasm: {
            selector: "//h3[text()[contains(.,'Germplasm')]]",
            locateStrategy: "xpath",
        },
        pageLabelSeeds: {
            selector: "//h3[text()[contains(.,'Seeds')]]",
            locateStrategy: "xpath",
        },
        pageLabelTraits: {
            selector: "//h3[text()[contains(.,'Traits')]]",
            locateStrategy: "xpath",
        },
        pageLabelLists: {
            selector: "//h3[text()[contains(.,'Lists')]]",
            locateStrategy: "xpath",
        },
        //END - DATA FILTERS
    },
};
