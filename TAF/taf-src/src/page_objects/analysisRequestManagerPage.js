const { client } = require("nightwatch-api");

module.exports = {
    url: "https://cb-uat.ebsproject.org/",
    // commands: [commands.commands],
    elements: {
        //-------BA ARM --------//
        breedingAnalytics: {
            selector: "//*[text()[contains(.,'Analytics')]]",
            locateStrategy: "xpath",
        },
        analysisManager: {
            selector: "//*[text()[contains(.,'Analysis Request Manager')]]",
            locateStrategy: "xpath",
        },
        jobList: {
            selector: "//*[text()[contains(.,'List of Job created')]]",
            locateStrategy: "xpath",
        },
        newRequestButton: {
            selector: "//*[text()[contains(.,'Add new analysis request')]]",
            locateStrategy: "xpath",
        },
        experimentList: {
            selector: "//*[@id='single-spa-application:@ebs/layout']//table[1]",
            locateStrategy: "xpath",
        },
        experimentFilter: {
            selector: "//*[@id='search-input2_experimentName']",
            locateStrategy: "xpath",
        },
        AFExperiments: {
            selector: "//*[@id='single-spa-application:@ebs/layout']//span/input[@type='radio']",
            locateStrategy: "xpath",
        },
        AFExperimentRow:{
            selector: "//div[@id='single-spa-application:@ebs/cs']/div/main/main/div/div/div/div/div[3]/div[2]/div/div/div/div/div/div/div[2]/table/tbody/tr/td/span/span/input",
            locateStrategy: "xpath"
        },
        AFOccurrence: {
            selector: "//*[@id='single-spa-application:@ebs/layout']//span/input[@type='checkbox']",
            locateStrategy: "xpath",
        },
        nextToSettings: {
            selector: "//button[@tabindex=0]/span[@class='ebs-MuiButton-label'][text()='Next']",
            locateStrategy: "xpath",
        },
        responseVariable: {
            selector: "//button[contains(@title, 'Open')]",
            locateStrategy: "xpath",
        },
        MCCont: {
            selector: "//ul[@id='tags-outlined-popup']/li",
            locateStrategy: "xpath",
        },
        AYLDCont: {
            selector: "//ul[@id='tags-outlined-popup']/li[2]",
            locateStrategy: "xpath",
        },
        nextToParameters: {
            selector: "//button[@tabindex=0]/span[@class='ebs-MuiButton-label'][text()='Next']",
            locateStrategy: "xpath",
        },
        spatialAdjusting: {
            selector: "//div[@id='single-spa-application:@ebs/cs']/div/main/main/div/div/div/div/div[3]/div[4]/div[2]/div/div/div/div[5]/div/div/div/div",
            locateStrategy: "xpath",
        },
        AR1RowXCol: {
            selector: "//div[@id='menu-']/div[3]/ul/li",
            locateStrategy: "xpath",
        },
        IDRowXCol: {
            selector: "//div[@id='menu-']/div[3]/ul/li[2]",
            locateStrategy: "xpath",
        },
        AR1RowXIDCol: {
            selector: "//div[@id='menu-']/div[3]/ul/li[3]",
            locateStrategy: "xpath",
        },
        noSpatialAdj: {
            selector: "//div[@id='menu-']/div[3]/ul/li[4]",
            locateStrategy: "xpath",
        },
        submitAnalysisRequest: {
            selector: "//div[@id='single-spa-application:@ebs/cs']/div/main/main/div/div/div/div/div[2]/div/div[3]/button/span",
            locateStrategy: "xpath",
        },
        AFSelectAllOccurrence: {
            selector: "//*[@id='single-spa-application:@ebs/cs']/div/main/main/div/div/div/div[1]/div[3]/div[2]/div/div/div/div/div/div/div[2]/table[1]/tbody/tr[1]/td[1]/span/span[1]/input",
            locateStrategy: "xpath",
        },
        //Scenario: Create a new request
        newRequestPageAssertion1: {
            selector: "//*[@id='single-spa-application:@ebs/cs']/div/main/main/div/div/div/div[1]/div[1]/div/div[1]/h4",
            locateStrategy: "xpath",
        },
        newRequestPageAssertion2: {
            selector: "//*[@id='single-spa-application:@ebs/cs']/div/main/main/div/div/div/div[1]/div[3]/div[2]",
            locateStrategy: "xpath",
        },
        //Scenario: Select an experiment ready for analysis
        selectExperiment: {
            selector: "//*[@id='single-spa-application:@ebs/layout']/div/main/main/div/div/div/div[1]/div[3]/div[2]/div/div/div/div/div/div/div[2]/table[1]/tbody/tr/td[1]/button",
            locateStrategy: "xpath",
        },        
        occurrencesAssertion1: {
            selector: "//tbody[@class='ebs-MuiTableBody-root']/tr[1]",
            locateStrategy: "xpath",
        },
        occurrencesAssertion2: {
            selector: "//tbody[@class='ebs-MuiTableBody-root']/tr[2]",
            locateStrategy: "xpath",
        },
        //Scenario: Select an experiment (all occurrences are selected)
        selectAllOccurrence: {
            selector: "//span[@class='ebs-MuiIconButton-label']/input",
            locateStrategy: "xpath",
        },
        nextButtonDisabled: {
            selector: "//button[@class='ebs-MuiButtonBase-root ebs-MuiButton-root ebs-MuiButton-contained ebs-MuiButton-containedPrimary ebs-MuiButton-containedSizeSmall ebs-MuiButton-sizeSmall Mui-disabled Mui-disabled']",
            locateStrategy: "xpath",
        },
        nextButtonEnabled: {
            selector: "//button[@class='ebs-MuiButtonBase-root ebs-MuiButton-root ebs-MuiButton-contained ebs-MuiButton-containedPrimary ebs-MuiButton-containedSizeSmall ebs-MuiButton-sizeSmall']",
            locateStrategy: "xpath",
        },
        //Scenario: Select all occurrences
        experimentsTabNext: {
            selector: "//button[@class='ebs-MuiButtonBase-root ebs-MuiButton-root ebs-MuiButton-contained ebs-MuiButton-containedPrimary ebs-MuiButton-containedSizeSmall ebs-MuiButton-sizeSmall']/span",
            locateStrategy: "xpath",
        }, 
        settingsTabPanel: {
            selector:"//div[@data-testid='TraitAutoSelectTestId']",
            locateStrategy: "xpath",
        }, 
        traitsDropdown: {
            selector: "//*[@id='tags-outlined']",
            locateStrategy: "xpath",
        }, 
        analysisObjDropdown: {
            selector: "//*[@id='ao1-simple-select']",
            locateStrategy: "xpath",
        }, 


    },
    sections: {
        
    },
    commands: [
        
    ],
};
