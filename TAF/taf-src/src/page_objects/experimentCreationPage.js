const {
    client
} = require("nightwatch-api");

module.exports = {
    url: "https://cb-qa.ebsproject.org/",
    // commands: [commands.commands],
    elements: {
        loginButton: "body > div.login-intro > div.login-text > a",
        googleButton: "#auth0-lock-container-1 > div > div.auth0-lock-center > form > div > div > div:nth-child(3) > span > div > div > div > div > div > div > div > div > div > div.auth0-lock-social-buttons-container > button > div.auth0-lock-social-button-icon",
        lastAccountButton: "#auth0-lock-container-1 > div > div.auth0-lock-center > form > div > div > div:nth-child(3) > span > div > div > div > div > div > div > div > div > button > div.auth0-lock-social-button-icon",

        googleUsernameText: "#identifierId",
        googleUsernameNextButton: "#identifierNext",
        googlePasswordText: "#password input",
        googlePasswordNextButton: "#passwordNext button",

        showLeftNav: "#go-to-show-left-nav",
        showSideNav: "#top-nav-bar > div > ul.menu-item-start > li.tooltipped.show-side-nav-btn",
        alwaysShowSideNav: "#slide-out > a",

        //START - DATA FILTERS

        dataFiltersMenu: {
            selector: "//*[@id='go-to-data-filters']/i",
            locateStrategy: "xpath",
        },
        dataFiltersProgram: "#select2-program_select-container",
        dataFiltersYear: {
            selector: "//*[@id='year_select']/..//input[@type='search']",
            locateStrategy: "xpath",
        },
        dataFiltersSeason: {
            selector: "//*[@id='season_select']/..//input[@type='search']",
            locateStrategy: "xpath",
        },
        dataFiltersStage: {
            selector: "//*[@id='stage_select']/..//input[@type='search']",
            locateStrategy: "xpath",
        },
        dataFiltersSite: {
            selector: "//*[@id='site_select']/..//input[@type='search']",
            locateStrategy: "xpath",
        },
        dataFiltersApply: "#apply-dashboard-filter",
        dataFilterSuccessNotif: {
            selector: "//*[@id='w7-success-0']/div/text()",
            locateStrategy: "xpath",
        },

        selectAllYear: {
            selector: "//*[@id='s2-togall-year_select']/span[1]/i",
            locateStrategy: "xpath",
        },
        unselectAllYear: {
            selector: "//*[@id='s2-togall-year_select']/span[2]/i",
            locateStrategy: "xpath",
        },
        selectAllSeason: {
            selector: "//*[@id='s2-togall-season_select']/span[1]/i",
            locateStrategy: "xpath",
        },
        unselectAllSeason: {
            selector: "//*[@id='s2-togall-season_select']/span[2]/i",
            locateStrategy: "xpath",
        },
        selectAllStage: {
            selector: "//*[@id='s2-togall-stage_select']/span[1]/i",
            locateStrategy: "xpath",
        },
        unselectAllStage: {
            selector: "//*[@id='s2-togall-stage_select']/span[2]/i",
            locateStrategy: "xpath",
        },
        selectAllSite: {
            selector: "//*[@id='s2-togall-site_select']/span[1]/i",
            locateStrategy: "xpath",
        },
        unselectAllSite: {
            selector: "//*[@id='s2-togall-site_select']/span[2]/i",
            locateStrategy: "xpath",
        },

        currentFiltersMenu: {
            selector: "//*[@id='top-nav-bar']/div/ul[1]/li[8]",
            locateStrategy: "xpath",
        },
        yearFilter: {
            selector: "//*[text()[contains(.,'Year')]]/../span/span[1]/span/ul/li",
            locateStrategy: "xpath",
        },
        yearRemoveIcon: ".select2-selection__choice__remove",
        //END - DATA FILTERS

        leftMenuSearch: {
            selector: "//*[@id='slide-out']/./li[6]/a[text()='Search']",
            locateStrategy: "xpath",
        },

        pageLabelGermplasm: {
            selector: "//h3[text()[contains(.,'Germplasm')]]",
            locateStrategy: "xpath",
        },
        pageLabelSeeds: {
            selector: "//h3[text()[contains(.,'Seeds')]]",
            locateStrategy: "xpath",
        },
        pageLabelTraits: {
            selector: "//h3[text()[contains(.,'Traits')]]",
            locateStrategy: "xpath",
        },
        pageLabelExperiments: {
            selector: "//h3[text()[contains(.,'Experiments')]]",
            locateStrategy: "xpath",
        },

        browserPageOne: {
            selector: "//a[text()='1']",
            locateStrategy: "xpath"
        },

        browserLastPage: {
            selector: "//a[text()[contains(.,'Last')]]",
            locateStrategy: "xpath",
        },

        browserFirstPage: {
            selector: "//a[text()[contains(.,'First')]]",
            locateStrategy: "xpath",
        },


        //------ EXPERIMENT CREATION START --------//
        experimentCreationMenu: "#slide-out li.parent-menu:nth-child(2) a:nth-child(1)",

        experimentTile: "#tools-widget-51-0-col_2-0- .material-icons.metrics-icon",

        createButton: "#create-expt-btn",

        experimentTypeSelect: {
            selector:
                //"//*[@id='experiment_type']/..//span[@class='select2-selection__arrow']",
                "//*[@id='basic-info1']/div[1]/div/div/span/span[1]/span/span[2]",
            locateStrategy: "xpath",
        },
        experimentTemplateSelect: {
            selector: "//*[@id='experiment_template']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },
        pipelineSelect: {
            selector: "//*[@id='PIPELINE-identification-2349']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },
        experimentNameCheckbox: {
            selector: "//*[@id='generate-EXPERIMENT_NAME-identification-2247']",
            locateStrategy: "xpath"
        },
        experimentNameField: "#EXPERIMENT_NAME-identification-2247",
        experimentObjectiveField: "#EXPERIMENT_OBJECTIVE-identification-2341",
        experimentYearField: "#EXPERIMENT_YEAR-identification-2339",
        stageSelect: {
            selector: "//*[@id='STAGE-identification-2337']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },
        evaluationSeasonSelect: {
            selector: "//*[@id='SEASON-identification-315']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },
        plantingSeasonField: "#PLANTING_SEASON-identification-2340",
        experimentStewardSelect: {
            selector: "//*[@id='EXPERIMENT_STEWARD-identification-2345']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },
        projectSelect: {
            selector: "//*[@id='PROJECT-identification-2338']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },

        designResult: {
            selector: "//*[@id='layout-panel-div']/div[contains(text(), 'Experiment successfully randomized. Layout cannot be shown for this design configuration.')]",
            locateStrategy: "xpath",
        },
        no_action_btn: {
            selector: "//a[@id='no_action_btn'][contains(text(), 'Next')]",
            locateStrategy: "xpath",
        },
        saveButton: {
            selector: "//a[@id='next-btn'][contains(text(), 'Save')]",
            locateStrategy: "xpath",
        },
        nextButtonEnabled: "#next-btn",
        nextButton: {
            selector: "//a[@id='next-btn'][contains(text(), 'Next')]",
            locateStrategy: "xpath",
        },
        finalizeButton: {
            selector: "//a[@id='next-btn'][contains(text(), 'Finalize')]",
            locateStrategy: "xpath",
        },

        editExperimentIcon: "a[title='Update experiment']",

        addingEntries: {
            selector: "//b[contains(.,'ADDING OF ENTRIES IN PROGRESS')]",
            locateStrategy: "xpath",
        },

        resetButton: "#reset-dynagrid-experiment-browser",
        addButton: "#add-entry-list-btn",
        viewAllButton: "#view-all-experiments-id",
        selectButton: "#select-btn",
        loadingStatus: "#layout-panel-div > div",

        b4rLogo: "#top-nav-bar > div > a",

        savedListButton: "a[title='Saved List']",
        copyEntryListButton: "a[title='Copy Entry List']",
        experimentName: {
            selector: "//*[@id='grid-experiment-list']/div/div/h3[2]",
            locateStrategy: "xpath",
        },
        inputListButton: "a[title='Input List']",

        designSelect: {
            selector: "//*[@id='design_select_id']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },

        numberOccurrencesField: "#nTrial",
        numberReplicatesField: "#nRep",
        numberPlotsPerBlockSelect: {
            selector: "//*[@id='select2-sBlk-container']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },
        rowsField: "#nFieldRow",
        plotsUntilField: "#nPlotBarrier",

        closeButton: "#add-entries-modal #cancel-save-btn",

        generateDesignButton: "#submit_design_btn",

        experimentFilterField: "input[name='ExperimentSearch[experimentName]']",
        rowsSelect: {
            selector: "//*[@id='select2-nFieldRow-container']",
            // "//*[@id='nFieldRow']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },


        plotsUntilSelect: {
            selector: "//*[@id='select2-nPlotBarrier-container']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },

        noRowsPerRepSelect: {
            selector: "//*[@id='nRowPerRep']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },

        rowBlocksSelect: {
            selector: "//*[@id='select2-nRowBlk-container']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },

        noBlocksPerSelect: {
            selector: "//*[@id='nBlk']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },

        noBlocksPerSelect: {
            selector: "//*[@id='nBlk']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },

        noRowsPerBlock: {
            selector: "//*[@id='nRowPerBlk']/..//span[@class='select2-selection__arrow']",
            locateStrategy: "xpath",
        },

        //checkbox, go to the row switch
        shapeCheckbox: {
            selector: "//*[@id='genLayout']",
            locateStrategy: "xpath"
        },

        randomizeCheckbox: {
            selector: "//*[@id='rand1']",
            locateStrategy: "xpath"
        },

        layoutPanel: "#design-layout-panel",

        occurrenceSiteSelect: {
            selector: "//*[@id='grid-occurrence-id-container']/table/tbody/tr/td[4]/span/span/span/span",
            locateStrategy: "xpath",
        },
        //Crosses
        femaleSelectAll:{
            selector:"//label[@for='female-grid-select-all']",
            locateStrategy: "xpath",
        },
        femaleSelfCross:{
            selector:"//a[contains(@class,'self-btn light-green')]",
            locateStrategy: "xpath",
        },
        toastContainer:{
            selector:"//div[@id='toast-container']//div[1]",
            locateStrategy: "xpath",
        },
        crossPresent:{
            selector:"//label[@id='label-for-checkbox']",
            locateStrategy: "xpath",
        },
        
        manageCrossesTab:{
            selector:"(//ul[@data-krajee-tabsx='tabsX_f1a7fb7e']//a)[2]",
            locateStrategy: "xpath",
        },
        
        //planting arrangement
        selectArrangement:{
            selector:"(//span[@class='lever'])[3]",
            locateStrategy: "xpath",
        },
        
        updateNoReps: "#pa-entry-order-entries-btn",
        
        //site
        siteSaveAssert: {
            selector: "//a[contains(@class,'ok-btn btn')]",
            locateStrategy: "xpath",
        }

        //------ EXPERIMENT CREATION END --------//
    },
    sections: {
        leftNavigation: {
            selector: "#slide-out",
            elements: {
                experimentCreationLink: {
                    selector: ".//a[normalize-space(.)='Experiment creation']",
                    locateStrategy: "xpath",
                },
                experimentManagerLink: {
                    selector: ".//a[normalize-space(.)='Experiment manager']",
                    locateStrategy: "xpath",
                },
                listManagerLink: {
                    selector: ".//a[normalize-space(.)='List manager']",
                    locateStrategy: "xpath",
                },
            },
        },
        dataFilters: {
            selector: "//*[@id='data-filters']",
            locateStrategy: "xpath",
            elements: {
                programSearchSelect: {
                    // selector: ".//*[@id='program_select']/following-sibling::span//input",
                    selector: ".//*[@id='select2-program-dashboard_select-container']", //".//*[@id='select2-program_select-container']",
                    locateStrategy: "xpath",
                },
                yearSelect: {
                    selector: ".//*[@id='year_select']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                seasonSelect: {
                    selector: ".//*[@id='season_select']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                stageSelect: {
                    selector: ".//*[@id='stage_select']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                siteSelect: {
                    selector: ".//*[@id='site_select']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                experimentsSlider: "input[id='data_filter_show_owned_experiments']",
                cancelButton: {
                    selector: ".//button[normalize-space(.)='Cancel']",
                    locateStrategy: "xpath",
                },
                applyButton: {
                    selector: ".//button[normalize-space(.)='Apply']",
                    locateStrategy: "xpath",
                },
            },
        },
        ecGrid: {
            selector: "div[id='dynagrid-experiment-browser-pjax']",
            elements: {
                createexptbtn: "#create-expt-btn",
            },
        },
        ecColFilters: {
            selector: "//div[@id='grid-experiment-list']",
            locateStrategy: "xpath",
            elements: {
                experimentNameColFilter: {
                    selector: ".//input[@name='ExperimentSearch[experimentName]']",
                    locateStrategy: "xpath",
                },
                experimentTypeColFilter: {
                    selector: ".//input[@name='ExperimentSearch[experimentType]']",
                    locateStrategy: "xpath",
                },
                occurrenceCountColFilter: {
                    selector: ".//input[@name='ExperimentSearch[occurrenceCount]']",
                    locateStrategy: "xpath",
                },
                entryCountColFilter: {
                    selector: ".//input[@name='ExperimentSearch[entryCount]']",
                    locateStrategy: "xpath",
                },
                projectNameColFilter: {
                    selector: ".//input[@name='ExperimentSearch[projectName]']",
                    locateStrategy: "xpath",
                },

                seasonNameColFilter: {
                    selector: ".//input[@name='ExperimentSearch[seasonName]']",
                    locateStrategy: "xpath",
                },

                stageCodeColFilter: {
                    selector: ".//input[@name='ExperimentSearch[stageCode]']",
                    locateStrategy: "xpath",
                },

                experimentYearColFilter: {
                    selector: ".//input[@name='ExperimentSearch[experimentYear]']",
                    locateStrategy: "xpath",
                },

                experimentDesignTypeColFilter: {
                    selector: ".//input[@name='ExperimentSearch[experimentDesignType]']",
                    locateStrategy: "xpath",
                },
                creatorColFilter: {
                    selector: ".//input[@name='ExperimentSearch[creator]']",
                    locateStrategy: "xpath",
                },
                designColFilter: {
                    selector: ".//input[@name='OccurrenceSearch[experimentDesignType]']",
                    locateStrategy: "xpath",
                },

            }
        },

    },
    commands: [
        {
            //Set values of a listbox
            //Listbox allows searching of a value
            listBoxSetValue: async function (input, value) {
                //clear existing values
                let originalSelector = input.__selector;
                input.__selector =
                    input.__selector +
                    "/../../li[@class='select2-selection__choice']/span";

                let elements = 0;
                await this.api.findElements(input, function (result) {
                    elements = result.value;
                });

                while (elements.length > 0) {
                    await this.api.elementIdClick(elements[0].getId());
                    await this.api.findElements(input, function (result) {
                        elements = result.value;
                    });
                }

                //revert original selector
                input.__selector = originalSelector;

                //split string by ,
                let listItems = value.split(",");
                for (let index = 0; index < listItems.length; index++) {
                    let item = listItems[index].trim();
                    await this.click(input);
                    //set value to display search result match
                    await this.setValue(input, item);
                    const control = {
                        selector: `//span//ul[@role='listbox']/li[normalize-space(.)='${item}']`,
                        locateStrategy: "xpath",
                    };
                    //click match
                    await this.waitForElementVisible(control);
                    await this.click(control);
                }
            },

            //use for slider style (checkbox)
            sliderSetValue: async function (input, value) {
                //save selector
                const originalSelector = input.__selector;
                switch (value) {
                    case "Left":
                        input.__selector = input.__selector + ":not(:checked)";
                        break;
                    case "Right":
                        input.__selector = input.__selector + ":checked";
                        break;
                    default:
                        throw new Error(`Unexpected Slider value (Left, Right).`);
                }
                let found;
                await this.waitForElementPresent(input, 1000, 500, false, (result) => {
                    found = result.value;
                });

                if (!found) {
                    //add + span to click the control
                    input.__selector = originalSelector + " + span";
                    await this.click(input);
                }
            },
            //Select with Search
            searchSelectSetValue: async function (input, value) {
                await this.click(input);
                await this.pause(1000);
                const control = {
                    selector: `//li[normalize-space(.)='${value.trim()}']`,
                    locateStrategy: "xpath",
                };
                await this.waitForElementVisible(control);
                await this.click(control);
            },
            //Wait Until Grid is Ready
            waitGridReady: async function (input) {
                //grid selector must be CSS
                let originalSelector = input.__selector;
                input.__selector = input.__selector + ".kv-grid-loading";
                await this.waitForElementNotPresent(input, 1200000, 1000);
                input.__selector = originalSelector;
            },
        },
    ],
};