const { client } = require("nightwatch-api");

module.exports = {
    url: "https://cb-qa.ebsproject.org/",
    // commands: [commands.commands],
    elements: {
        showLeftNav: "#go-to-show-left-nav",
        viewMenu: "#slide-out > li.parent-menu.planting-instructions-manager-left-menu > a",
        viewPlantingJob: {
            selector:
                "//*[@id='planting-instructions-browser-id-container']/table/tbody/tr[2]/td[2]/a[1]",
            locateStrategy: "xpath",
        },
        viewCollapsible: {
            selector: "//div[@id='view-planting-job-modal']/div/div/div[2]/div/ul[2]/li/div",
            locateStrategy: "xpath",
        },
        viewPlantingJobModal: {
            selector: "//*[@id='view-planting-job-modal']/div/div/div[2]/div/ul[2]",
            locateStrategy: "xpath",
        },
        viewDueDdate: {
            selector: "//div[@id='view-planting-job-modal']/div/div/div[2]/div/ul[2]/li/div[2]/div/span",
            locateStrategy: "xpath",
        },
        viewFacility: {
            selector: "//div[@id='view-planting-job-modal']/div/div/div[2]/div/ul[2]/li/div[2]/div/span[2]",
            locateStrategy: "xpath",
        },
        viewInstructions: {
            selector: "//div[@id='view-planting-job-modal']/div/div/div[2]/div/ul[2]/li/div[2]/div/span[3]",
            locateStrategy: "xpath",
        },
        viewPackingJobStatus: "#update-status-dropdown",
        statusFilter: {
            selector: "//tr[@id='planting-instructions-browser-id-filters']/td[2]/span/span/span",
            locateStrategy: "xpath",
        },
        statusLabel: {
            selector: "//div[@id='planting-instructions-browser-id-container']/table/tbody/tr/td[3]/span",
            locateStrategy: "xpath",
        },
        jobTypeFilter: {
            selector: "//tr[@id='planting-instructions-browser-id-filters']/td[3]/span/span/span",
            locateStrategy: "xpath",
        },
        jobTypeLabel: {
            selector: "//div[@id='planting-instructions-browser-id-container']/table/tbody/tr/td[4]",
            locateStrategy: "xpath",
        },
        noResultsFound: {
            selector: "//*[@id='planting-instructions-browser-id-container']//*[text()='No results found.']",
            locateStrategy: "xpath"
        },
        statusDraft: {
            selector: "//*[@id='planting-instructions-browser-id-container']//*[text()='DRAFT']",
            locateStrategy: "xpath"
        },
        statusPacked: {
            selector: "//*[@id='planting-instructions-browser-id-container']//*[text()='PACKED']",
            locateStrategy: "xpath"
        },
        statusPacking: {
            selector: "//*[@id='planting-instructions-browser-id-container']//*[text()='PACKING']",
            locateStrategy: "xpath"
        },
        statusPackingOnHold: {
            selector: "//*[@id='planting-instructions-browser-id-container']//*[text()='PACKING ON HOLD']",
            locateStrategy: "xpath"
        },
        statusPackingCancelled: {
            selector: "//*[@id='planting-instructions-browser-id-container']//*[text()='PACKING CANCELLED']",
            locateStrategy: "xpath"
        },
        statusReadyForPacking: {
            selector: "//*[@id='planting-instructions-browser-id-container']//*[text()='READY FOR PACKING']",
            locateStrategy: "xpath"
        },

        //START - DATA FILTERS
        dataFiltersMenu: {
            selector: "//*[@id='go-to-data-filters']/i",
            locateStrategy: "xpath",
        },
        dataFiltersProgram: "#select2-program_select-container",
        dataFiltersYear: {
            selector: "//*[@id='year_select']/..//input[@type='search']",
            locateStrategy: "xpath",
        },
        dataFiltersSeason: {
            selector: "//*[@id='season_select']/..//input[@type='search']",
            locateStrategy: "xpath",
        },
        dataFiltersStage: {
            selector: "//*[@id='stage_select']/..//input[@type='search']",
            locateStrategy: "xpath",
        },
        dataFiltersSite: {
            selector: "//*[@id='site_select']/..//input[@type='search']",
            locateStrategy: "xpath",
        },
        dataFiltersApply: "#apply-dashboard-filter",
        dataFilterSuccessNotif: {
            selector: "//*[@id='w7-success-0']/div/text()",
            locateStrategy: "xpath",
        },

        selectAllYear: {
            selector: "//*[@id='s2-togall-year_select']/span[1]/i",
            locateStrategy: "xpath",
        },
        unselectAllYear: {
            selector: "//*[@id='s2-togall-year_select']/span[2]/i",
            locateStrategy: "xpath",
        },
        selectAllSeason: {
            selector: "//*[@id='s2-togall-season_select']/span[1]/i",
            locateStrategy: "xpath",
        },
        unselectAllSeason: {
            selector: "//*[@id='s2-togall-season_select']/span[2]/i",
            locateStrategy: "xpath",
        },
        selectAllStage: {
            selector: "//*[@id='s2-togall-stage_select']/span[1]/i",
            locateStrategy: "xpath",
        },
        unselectAllStage: {
            selector: "//*[@id='s2-togall-stage_select']/span[2]/i",
            locateStrategy: "xpath",
        },
        selectAllSite: {
            selector: "//*[@id='s2-togall-site_select']/span[1]/i",
            locateStrategy: "xpath",
        },
        unselectAllSite: {
            selector: "//*[@id='s2-togall-site_select']/span[2]/i",
            locateStrategy: "xpath",
        },

        currentFiltersMenu: {
            selector: "//*[@id='top-nav-bar']/div/ul[1]/li[8]",
            locateStrategy: "xpath",
        },
        yearFilter: {
            selector: "//*[text()[contains(.,'Year')]]/../span/span[1]/span/ul/li",
            locateStrategy: "xpath",
        },
        yearRemoveIcon: ".select2-selection__choice__remove",
        //END - DATA FILTERS
    },
    sections: {
        leftNavigation: {
            selector: "#slide-out",
            elements: {
                experimentCreationLink: {
                    selector: ".//a[normalize-space(.)='Experiment creation']",
                    locateStrategy: "xpath",
                },
                experimentManagerLink: {
                    selector: ".//a[normalize-space(.)='Experiment manager']",
                    locateStrategy: "xpath",
                },
                listManagerLink: {
                    selector: ".//a[normalize-space(.)='List manager']",
                    locateStrategy: "xpath",
                },
            },
        },
        listsGrid: {
            selector: "#my-lists-grid-pjax",
            elements: {
                personalizeGridSettingsButton:
                    "button[title='Personalize grid settings']",
                nameField: "input[name='MyList[name]']",
                summaryText: "div.summary",
                summaryTable: "tr.my-lists-grid",
                listBrowserEmpty: "div.empty"
            },
        },
        seedsSearch: {
            selector: "//i[normalize-space(.)='search']/ancestor::li[@id='filters']",
            locateStrategy: "xpath",
            elements: {
                programSelect: {
                    selector: ".//*[@id='program-filter']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                seasonSelect: {
                    selector: ".//*[@id='season-filter']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                experimentYearSelect: {
                    selector:
                        ".//*[@id='experiment_year-filter']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                experimentTypeSelect: {
                    selector:
                        ".//*[@id='experiment_type-filter']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                occurrenceSelect: {
                    selector:
                        ".//*[@id='occurrence_name-filter']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                facilitySelect: {
                    selector:
                        ".//*[@id='facility-filter']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                subFacilitySelect: {
                    selector:
                        ".//*[@id='sub_facility-filter']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                findButton: {
                    selector: ".//button[@title='Find Seeds']",
                    locateStrategy: "xpath",
                },
            },
        },
        dataFilters: {
            selector: "//*[@id='data-filters']",
            locateStrategy: "xpath",
            elements: {
                programSearchSelect: {
                    // selector: ".//*[@id='program_select']/following-sibling::span//input",
                    selector: ".//*[@id='select2-program-dashboard_select-container']", //".//*[@id='select2-program_select-container']",
                    locateStrategy: "xpath",
                },
                yearSelect: {
                    selector: ".//*[@id='year_select']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                seasonSelect: {
                    selector: ".//*[@id='season_select']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                stageSelect: {
                    selector: ".//*[@id='stage_select']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                siteSelect: {
                    selector: ".//*[@id='site_select']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                experimentsSlider: "input[id='data_filter_show_owned_experiments']",
                cancelButton: {
                    selector: ".//button[normalize-space(.)='Cancel']",
                    locateStrategy: "xpath",
                },
                applyButton: {
                    selector: ".//button[normalize-space(.)='Apply']",
                    locateStrategy: "xpath",
                },
            },
        },
        searchGrid: {
            selector: "#germplasm-grid-pjax",
            elements: {
                addToGermplasmListButton: "#germplasm-save-new-list-btn",
            },
        },
        workingListGrid: {
            selector: "#dynagrid-find-seed-list", //"#dynagrid-find-seed-list-container", //
            elements: {
                clearListButton: "#confirm-clear-list",
                saveButton: "#save-list-btn",
            },
        },
        myListsGrid: {
            selector: "#my-lists",
            elements: {},
        },
        clearWorkingListModal: {
            selector: "#clear-list-confirm-modal",
            elements: {
                clearButton: "#clear-working-list",
                saveButton: "#save-working-list-items-btn",
                cancelButton: "#cancel-clear-list-btn",
            },
        },
        confirmSaveModal: {
            selector: "#confirm-save-modal",
            elements: {
                saveButton: "#save-btn",
                cancelButton: "#return-btn",
                closeButton: "#close-modal-btn",
            },
        },
        saveConfirmModal: {
            selector: "#save-confirm-modal",
            elements: {
                closeButton: "#close-modal-btn",
            },
        },
        createListModal: {
            selector: "#create-list-modal",
            elements: {
                nameField: "#list-name",
                abbrevField: "#list-abbrev",
                displayNameField: "#list-display-name",
                descriptionField: "#entry-list-description",
                remarksField: "#entry-list-remarks",
                createButton: "#create-list-btn",
            },
        },
        // Column filtering
        PIMbrowserColFilters: {
            selector: "//tr[@id='planting-instructions-browser-id-filters']",
            locateStrategy: "xpath",
            elements: {
                jobCodeColumnSearch: {
                    selector: ".//input[@name='PlantingJob[plantingJobCode]']",
                    locateStrategy: "xpath",
                },
                experimentsColumnSearch: {
                    selector: ".//input[@name='PlantingJob[experiments]']",
                    locateStrategy: "xpath",
                },
                occurrenceCountColumnSearch: {
                    selector: ".//input[@name='PlantingJob[occurrenceCount]']",
                    locateStrategy: "xpath",
                },
                envelopeCountColumnSearch: {
                    selector: ".//input[@name='PlantingJob[envelopeCount]']",
                    locateStrategy: "xpath",
                },
                facilityColumnSearch: {
                    selector: ".//input[@name='PlantingJob[facilityName]']",
                    locateStrategy: "xpath",
                },
            }
        },
    },
    commands: [
        {
            //Set values of a listbox
            //Listbox allows searching of a value
            listBoxSetValue: async function (input, value) {
                //clear existing values
                let originalSelector = input.__selector;
                input.__selector =
                    input.__selector +
                    "/../../li[@class='select2-selection__choice']/span";

                let elements = 0;
                await this.api.findElements(input, function (result) {
                    elements = result.value;
                });

                while (elements.length > 0) {
                    await this.api.elementIdClick(elements[0].getId());
                    await this.api.findElements(input, function (result) {
                        elements = result.value;
                    });
                }

                //revert original selector
                input.__selector = originalSelector;

                //split string by ,
                let listItems = value.split(",");
                for (let index = 0; index < listItems.length; index++) {
                    let item = listItems[index].trim();
                    await this.click(input);
                    //set value to display search result match
                    await this.setValue(input, item);
                    const control = {
                        selector: `//span//ul[@role='listbox']/li[normalize-space(.)='${item}']`,
                        locateStrategy: "xpath",
                    };
                    //click match
                    await this.waitForElementVisible(control);
                    await this.click(control);
                }
            },

            //use for slider style (checkbox)
            sliderSetValue: async function (input, value) {
                //save selector
                const originalSelector = input.__selector;
                switch (value) {
                    case "Left":
                        input.__selector = input.__selector + ":not(:checked)";
                        break;
                    case "Right":
                        input.__selector = input.__selector + ":checked";
                        break;
                    default:
                        throw new Error(`Unexpected Slider value (Left, Right).`);
                }
                let found;
                await this.waitForElementPresent(input, 1000, 500, false, (result) => {
                    found = result.value;
                });

                if (!found) {
                    //add + span to click the control
                    input.__selector = originalSelector + " + span";
                    await this.click(input);
                }
            },
            //set column filter
            columnFilterSetValue: async function (input, value) {
                await this.click(input);
                await this.pause(1000);
                /*const control = {
                selector: `//li[normalize-space(.)='${value.trim()}']`,
                locateStrategy: "xpath",
                };*/
                await this.setValue(input, value, this.Keys.ENTER);

                //await this.waitForElementVisible(control);
                //await this.click(control);
            },
            //Select with Search
            searchSelectSetValue: async function (input, value) {
                await this.click(input);
                await this.pause(1000);
                const control = {
                    selector: `//li[normalize-space(.)='${value.trim()}']`,
                    locateStrategy: "xpath",
                };
                await this.waitForElementVisible(control);
                await this.click(control);
            },
            //Wait Until Grid is Ready
            waitGridReady: async function (input) {
                //grid selector must be CSS
                let originalSelector = input.__selector;
                input.__selector = input.__selector + ".kv-grid-loading";
                await this.waitForElementNotPresent(input, 1200000, 1000);
                input.__selector = originalSelector;
            },

            setCheckbox: async function (input, value) {
                let status = false;
                await this.getAttribute(input, "checked", (result) => {
                    if (result.value != null) {
                        status = true;
                    }
                });
                if ((value && !status) || (!value && status)) {
                    await this.api.execute(
                        `document.querySelector("${input.__selector}").click();`
                    );
                }
            },

            getRowCount: async function (input) {
                try {
                    let originalSelector = input.__selector;
                } catch (error) {
                } finally {
                }
            },
        },
    ],
};
