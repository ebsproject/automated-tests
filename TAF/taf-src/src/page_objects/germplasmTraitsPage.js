const { client } = require("nightwatch-api");

module.exports = {
    url: "https://cb-qa.ebsproject.org/",
    // commands: [commands.commands],
    elements: {

        showLeftNav: "#go-to-show-left-nav",
        showSideNav:
            "#top-nav-bar > div > ul.menu-item-start > li.tooltipped.show-side-nav-btn",
        alwaysShowSideNav: "#slide-out > a",

        leftMenuSearch: {
            selector: "//*[@id='slide-out']/./li[6]/a[text()='Search']",
            locateStrategy: "xpath",
        },

        pageLabelGermplasm: {
            selector: "//h3[text()[contains(.,'Germplasm')]]",
            locateStrategy: "xpath",
        },
        pageLabelSeeds: {
            selector: "//h3[text()[contains(.,'Seeds')]]",
            locateStrategy: "xpath",
        },
        pageLabelTraits: {
            selector: "//h3[text()[contains(.,'Traits')]]",
            locateStrategy: "xpath",
        },

        browserPageOne: { selector: "//a[text()='1']", locateStrategy: "xpath" },

        browserLastPage: {
            selector: "//a[text()[contains(.,'Last')]]",
            locateStrategy: "xpath",
        },

        browserFirstPage: {
            selector: "//a[text()[contains(.,'First')]]",
            locateStrategy: "xpath",
        },

        //START - DATA FILTERS
        dataFiltersMenu: {
            selector: "//*[@id='go-to-data-filters']/i",
            locateStrategy: "xpath",
        },

        //START - GERMPLASM
        subleftMenuSearchGermplasm: {
            selector: "//*[@id='slide-out']/./li[8]/a[text()='Germplasm']",
            locateStrategy: "xpath",
        },

        activeQueryParameters: {
            selector: "//*[@id='query-form']",
            locateStrategy: "xpath",
        },

        queryName: {
            selector: "//*[@id='query-fld-NAME']",
            locateStrategy: "xpath",
        },

        queryDataType:
            "#query-form > div:nth-child(2) > div:nth-child(2) > span > span.selection > span > ul > li > input",

        addtlParamDivGermplasmTraits: "#addl-query-acc",

        addtlParamContainer: "#additional-query-list",

        queryResultsSummary: {
            selector: "//*[@id='germplasm-grid-id']/div/div[1]/div[1]/div",
            locateStrategy: "xpath",
        },

        queryFindButton: "#query-find-btn",

        selectAllResults:
            "#germplasm-grid-id > div > div.floatThead-wrapper > div.kv-thead-float > table > thead > tr:nth-child(1) > th:nth-child(1) > label", //{selector:"//*[@id='germplasm-grid-id_select_all']", locateStrategy:"xpath"},

        selectedItemCount: "#selected-count",

        page2Results:
            "#germplasm-grid-id > div > div.panel-footer > div.kv-panel-pager > ul > li:nth-child(3) > a",

        previousPageResults:
            "#germplasm-grid-id > div > div.panel-footer > div.kv-panel-pager > ul > li.prev > a",

        additionalQueryParam: {
            selector: "//ul[@id='addl-query-acc']/li/div",
            locateStrategy: "xpath",
        },
        inputListButton: {
            selector: "//button[@id='query-input-list-btn']",
            locateStrategy: "xpath",
        },
        inputTextField: {
            selector: "//textarea[@id='query-txt-area']",
            locateStrategy: "xpath",
        },
        confirmInputList: {
            selector: "//a[@id='query-confirm-list-btn']",
            locateStrategy: "xpath",
        },
        findGerm: {
            selector: "//button[@id='query-find-btn']",
            locateStrategy: "xpath",
        },
        saveGermplasmListNotif: {
            selector: "//*[@id='toast-container']",
            locateStrategy: "xpath"
        },
        //END - GERMPLASM

        //START - TRAITS
        resetGridGermplasmTraits: {
            selector: "//*[@id='reset-traits-grid']",
            locateStrategy: "xpath",
        },

        collapsibleSearchQuery: ".collapsible-header:nth-child(2)",

        resetQueryParam: {
            selector: "//*[@id='query-clear-all-btn']",
            locateStrategy: "xpath",
        },
        //END - TRAITS
    },
    sections: {
        inputListSelection: {
            selector: "//span[@id='select2-query-column-name-list-container']",
            locateStrategy: "xpath",
            elements: {
                designationSelect: {
                    // selector: ".//*[@id='program_select']/following-sibling::span//input",
                    selector: ".//li[contains(text(), 'Designation')]",
                    locateStrategy: "xpath",
                },
                germplasmCodeSelect: {
                    selector: ".//li[contains(text(), 'Germpalsm Code')]",
                    locateStrategy: "xpath",
                }
            }
        },
        browserColFilters: {
            selector: "//*[@id='search-grid-table-con']",
            locateStrategy: "xpath",
            elements: {
                labelColumnSearch: {
                    // selector: ".//*[@id='program_select']/following-sibling::span//input",
                    selector: ".//*[@id='search-grid-table-con-filters']/td[4]/input", //".//*[@id='select2-program_select-container']",
                    locateStrategy: "xpath",
                },
                seednameColumnSearch: {
                    selector: ".//*[@id='search-grid-table-con-filters']/td[5]/input",
                    locateStrategy: "xpath",
                }
            }
        },
        leftNavigation: {
            selector: "#slide-out",
            elements: {
                experimentCreationLink: {
                    selector: ".//a[normalize-space(.)='Experiment creation']",
                    locateStrategy: "xpath",
                },
                experimentManagerLink: {
                    selector: ".//a[normalize-space(.)='Experiment manager']",
                    locateStrategy: "xpath",
                },
                listManagerLink: {
                    selector: ".//a[normalize-space(.)='List manager']",
                    locateStrategy: "xpath",
                },
            },
        },
        listsGrid: {
            selector: "#my-lists-grid-pjax",
            elements: {
                personalizeGridSettingsButton:
                    "button[title='Personalize grid settings']",
                nameField: "input[name='MyList[name]']",
                summaryText: "div.summary",
                summaryTable: "tr.my-lists-grid",
                listBrowserEmpty: "div.empty"
            },
        },
        queryParam: {
            selector: "//*[@id='query-form']",
            locateStrategy: "xpath",
            elements: {
                germplasmTypeSearch: {
                    selector: ".//*[@id='query-fld-GERMPLASM_TYPE']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
            },
        },
        
        dataFilters: {
            selector: "//*[@id='data-filters']",
            locateStrategy: "xpath",
            elements: {
                programSearchSelect: {
                    // selector: ".//*[@id='program_select']/following-sibling::span//input",
                    selector: ".//*[@id='select2-program-dashboard_select-container']", //".//*[@id='select2-program_select-container']",
                    locateStrategy: "xpath",
                },
                yearSelect: {
                    selector: ".//*[@id='year_select']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                seasonSelect: {
                    selector: ".//*[@id='season_select']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                stageSelect: {
                    selector: ".//*[@id='stage_select']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                siteSelect: {
                    selector: ".//*[@id='site_select']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                experimentsSlider: "input[id='data_filter_show_owned_experiments']",
                cancelButton: {
                    selector: ".//button[normalize-space(.)='Cancel']",
                    locateStrategy: "xpath",
                },
                applyButton: {
                    selector: ".//button[normalize-space(.)='Apply']",
                    locateStrategy: "xpath",
                },
            },
        },
        searchGrid: {
            selector: "#germplasm-grid-pjax",
            elements: {
                addToGermplasmListButton: "#germplasm-save-new-list-btn",
            },
        },
        workingListGrid: {
            selector: "#dynagrid-find-seed-list", //"#dynagrid-find-seed-list-container", //
            elements: {
                clearListButton: "#confirm-clear-list",
                saveButton: "#save-list-btn",
            },
        },
        myListsGrid: {
            selector: "#my-lists",
            elements: {},
        },
        clearWorkingListModal: {
            selector: "#clear-list-confirm-modal",
            elements: {
                clearButton: "#clear-working-list",
                saveButton: "#save-working-list-items-btn",
                cancelButton: "#cancel-clear-list-btn",
            },
        },
        confirmSaveModal: {
            selector: "#confirm-save-modal",
            elements: {
                saveButton: "#save-btn",
                cancelButton: "#return-btn",
                closeButton: "#close-modal-btn",
            },
        },
        saveConfirmModal: {
            selector: "#save-confirm-modal",
            elements: {
                closeButton: "#close-modal-btn",
            },
        },
        createListModal: {
            selector: "#create-list-modal",
            elements: {
                nameField: "#list-name",
                abbrevField: "#list-abbrev",
                displayNameField: "#list-display-name",
                descriptionField: "#entry-list-description",
                remarksField: "#entry-list-remarks",
                createButton: "#create-list-btn",
            },
        },
        germBrowserColFilters: {
            selector: "//div[@id='germplasm-grid-id']",
            locateStrategy: "xpath",
            elements: {
                germColFilter: {
                    selector: ".//input[@name='GermplasmSearch[designation]']",
                    locateStrategy: "xpath",
                },
                otherNamesColFilter: {
                    selector: ".//input[@name='GermplasmSearch[designation]']",
                    locateStrategy: "xpath",
                },
                parentageColFilter: {
                    selector: ".//input[@name='GermplasmSearch[occurrenceName]']",
                    locateStrategy: "xpath",
                },
                generationColFilter: {
                    selector: ".//input[@name='GermplasmSearch[siteCode]']",
                    locateStrategy: "xpath",
                },
                germNameTypeColFilter: {
                    selector: ".//input[@name='GermplasmSearch[field]']",
                    locateStrategy: "xpath",
                },

                germTypeColFilter: {
                    selector: ".//input[@name='GermplasmSearch[location]']",
                    locateStrategy: "xpath",
                },

                germStateColFilter: {
                    selector: ".//input[@name='GermplasmSearch[experimentType]']",
                    locateStrategy: "xpath",
                },
            }
        },
        createGermplasmListModal: {
            selector: "#germplasm-save-list-modal",
            elements: {
                nameField: "#germplasm-list-name",
                abbrevField: "#germplasm-list-abbrev",
                displayNameField: "#germplasm-list-display_name",
                descriptionField: "#germplasm-list-description",
                remarksField: "#germplasm-list-remarks",
                createButton: {
                    selector: "//*[@id='germplasm-save-list-confirm-btn']",
                    locateStrategy: "xpath",
                },
            },
        },
    },
    commands: [
        {
            //Set values of a listbox
            //Listbox allows searching of a value
            listBoxSetValue: async function (input, value) {
                //clear existing values
                let originalSelector = input.__selector;
                input.__selector =
                    input.__selector +
                    "/../../li[@class='select2-selection__choice']/span";

                let elements = 0;
                await this.api.findElements(input, function (result) {
                    elements = result.value;
                });

                while (elements.length > 0) {
                    await this.api.elementIdClick(elements[0].getId());
                    await this.api.findElements(input, function (result) {
                        elements = result.value;
                    });
                }

                //revert original selector
                input.__selector = originalSelector;

                //split string by ,
                let listItems = value.split(",");
                for (let index = 0; index < listItems.length; index++) {
                    let item = listItems[index].trim();
                    await this.click(input);
                    //set value to display search result match
                    await this.setValue(input, item);
                    const control = {
                        selector: `//span//ul[@role='listbox']/li[normalize-space(.)='${item}']`,
                        locateStrategy: "xpath",
                    };
                    //click match
                    await this.waitForElementVisible(control);
                    await this.click(control);
                }
            },

            //use for slider style (checkbox)
            sliderSetValue: async function (input, value) {
                //save selector
                const originalSelector = input.__selector;
                switch (value) {
                    case "Left":
                        input.__selector = input.__selector + ":not(:checked)";
                        break;
                    case "Right":
                        input.__selector = input.__selector + ":checked";
                        break;
                    default:
                        throw new Error(`Unexpected Slider value (Left, Right).`);
                }
                let found;
                await this.waitForElementPresent(input, 1000, 500, false, (result) => {
                    found = result.value;
                });

                if (!found) {
                    //add + span to click the control
                    input.__selector = originalSelector + " + span";
                    await this.click(input);
                }
            },
            //set column filter
            columnFilterSetValue: async function (input, value) {
                await this.click(input);
                await this.pause(1000);
                /*const control = {
                selector: `//li[normalize-space(.)='${value.trim()}']`,
                locateStrategy: "xpath",
                };*/
                await this.setValue(input, value, this.Keys.ENTER);

                //await this.waitForElementVisible(control);
                //await this.click(control);
            },
            //Select with Search
            searchSelectSetValue: async function (input, value) {
                await this.click(input);
                await this.pause(1000);
                const control = {
                    selector: `//li[normalize-space(.)='${value.trim()}']`,
                    locateStrategy: "xpath",
                };
                await this.waitForElementVisible(control);
                await this.click(control);
            },
            //Wait Until Grid is Ready
            waitGridReady: async function (input) {
                //grid selector must be CSS
                let originalSelector = input.__selector;
                input.__selector = input.__selector + ".kv-grid-loading";
                await this.waitForElementNotPresent(input, 1200000, 1000);
                input.__selector = originalSelector;
            },

            setCheckbox: async function (input, value) {
                let status = false;
                await this.getAttribute(input, "checked", (result) => {
                    if (result.value != null) {
                        status = true;
                    }
                });
                if ((value && !status) || (!value && status)) {
                    await this.api.execute(
                        `document.querySelector("${input.__selector}").click();`
                    );
                }
            },

            getRowCount: async function (input) {
                try {
                    let originalSelector = input.__selector;
                } catch (error) {
                } finally {
                }
            },
        },
    ],
};
