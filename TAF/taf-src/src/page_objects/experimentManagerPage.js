const { client } = require("nightwatch-api");

module.exports = {
    url: "https://cb-qa.ebsproject.org/",
    // commands: [commands.commands],
    elements: {
        showLeftNav: "#go-to-show-left-nav",
        experimentManagerMenu: {
            selector:
                "//*[@data-sub_menu='experiment-manager-sub-menu']",
            locateStrategy: "xpath",
        },
        firstExptStatusCheck: {
            selector: "//*[@id='select2-occurrencesearch-occurrencestatus-results']/li[text()='created']",
            locateStrategy: "xpath",
        },
        firstExperiment: {
            selector:
                "//div[@id='occurrences-grid-id-container']/table/tbody/tr[2]/td/label",
            locateStrategy: "xpath",
        },
        firstExperimentLocation: {
            selector:
                "//div[@id='occurrences-grid-id-container']/table/tbody/tr[2]/td[9]/a",
            locateStrategy: "xpath",
        },
        locationCode: {
            selector: "//div[2]/div/div/div[2]",
            locateStrategy: "xpath"
        },
        printButton: {
            selector: "//*[@id='w2']",
            locateStrategy: "xpath",
        },

        plotTab: {
            selector: "//*[contains(text(),'Plot')]",
            locateStrategy: "xpath",
        },

        plotNo: {
            selector: "//*[@name='LocationPlot[plotNumber]']",
            locateStrategy: "xpath",
        },

        traitsCollected: {
            selector: "//*[@class='progress-label']",
            locateStrategy: "xpath",
        },
        exportPlotsButton: {
            selector:
                "//*[@id='export-plots-btn']",
            locateStrategy: "xpath",
        },
        plantedOccurrence: {
            selector:
                "//*[@id='occurrences-grid-id-container']/table/tbody/tr[4]/td[1]",
            locateStrategy: "xpath",
        },
        notPlantedOccurrence: {
            selector:
                "//*[@id='occurrences-grid-id-container']/table/tbody/tr[2]/td[1]",
            locateStrategy: "xpath",
        },
        downloadFiles: {
            selector: "//div[@id='occurrences-grid-id']/div/div[2]/div/div/div/a[6]",
            locateStrategy: "xpath",
        },
        downloadTraitData: {
            selector: "//*[@id='download-data-collection-files-btn']",
            locateStrategy: "xpath",
        },
        downloadDataCollection: {
            selector: "//*[contains(text(), 'Download Data Collection Files')]",
            locateStrategy: "xpath",
        },
        notReadyForDataCollection: {
            selector: "//*[contains(text(), 'The occurrence and/or location is not yet ready for Data collection.')]",
            locateStrategy: "xpath",
        },
        uploadButton: {
            selector: "//*[@id='occurrence-upload-action']",
            locateStrategy: "xpath",
        },
        uploadTraitDataButton: {
            selector: "//*[@id='upload-data-collection-files-btn']",
            locateStrategy: "xpath",
        },
        dataCollectionUpload: {
            selector: "//*[contains(text(), 'Data Collection Upload')]",
            locateStrategy: "xpath",
        },
        updateOccurrenceButton: {
            selector: "//*[@class='render-generic-form replace-attributes']",
            locateStrategy: "xpath",
        },
        updateOccurrenceDescription: {
            selector: "//input[@id='DESCRIPTION-identification-725']",
            locateStrategy: "xpath",
        },
        saveUpdateButton: {
            selector: "//div[@id='manage-basic-info-modal']/div/div/div[3]/button",
            locateStrategy: "xpath",
        },
        updateNotif: {
            selector: "//*[contains(text(), 'Successfully updated occurrence information.')]",
            locateStrategy: "xpath",
        },
        viewOccurrence: {
            selector: "//*[@class='view-occurrence']",
            locateStrategy: "xpath",
        },
        viewFirstOccurrence: {
            selector: "//*[@id='occurrences-grid-id-container']/table/tbody/tr[2]//*[@class='view-occurrence']",
            locateStrategy: "xpath",
        },
        viewOccurrenceTabs: {
            selector: "//*[@class='tabs']",
            locateStrategy: "xpath"
        },
        infoOccurrence: {
            selector: "//*[@class='viewer-display-name']",
            locateStrategy: "xpath",
        },

        headerStatus: {
            selector: "//div[@id='occurrences-grid-id']/div/div[4]/div/table/thead/tr/th[3]/a",
            locateStrategy: "xpath",
        },
        initialFirst: {
            selector: "//td[contains(.,'EXP0000635')]",
            locateStrategy: "xpath"
        },
        currentFirstStatus: {
            selector: "//td[contains(.,'EXP00000345')]",
            locateStrategy: "xpath"
        },
        currentFirstStatusLabel: {
            selector: "//div[@id='occurrences-grid-id-container']/table/tbody/tr/td[4]",
            locateStrategy: "xpath"
        },
        headerCode: {
            selector: "//div[@id='occurrences-grid-id']/div/div[4]/div/table/thead/tr/th[4]/a",
            locateStrategy: "xpath",
        },
        currentFirstCode: {
            selector: "//td[contains(.,'EXP00000001')]",
            locateStrategy: "xpath"
        },
        headerExperiment: {
            selector: "//div[@id='occurrences-grid-id']/div/div[4]/div/table/thead/tr/th[5]/a",
            locateStrategy: "xpath",
        },
        currentFirstExperiment: {
            selector: "//td[contains(.,'EXP00000017')]",
            locateStrategy: "xpath"
        },
        headerOccurrence: {
            selector: "//div[@id='occurrences-grid-id']/div/div[4]/div/table/thead/tr/th[6]/a",
            locateStrategy: "xpath",
        },
        currentFirstOccurrence: {
            selector: "//td[contains(.,'EXP00000017')]",
            locateStrategy: "xpath"
        },
        headerSite: {
            selector: "//div[@id='occurrences-grid-id']/div/div[4]/div/table/thead/tr/th[7]/a",
            locateStrategy: "xpath",
        },
        currentFirstSite: {
            selector: "//td[contains(.,'EXP00000271')]",
            locateStrategy: "xpath"
        },
        headerLocation: {
            selector: "//div[@id='occurrences-grid-id']/div/div[4]/div/table/thead/tr/th[9]/a",
            locateStrategy: "xpath",
        },
        currentFirstLocation: {
            selector: "//td[contains(.,'EXP00000124')]",
            locateStrategy: "xpath"
        },
        headerExpType: {
            selector: "//div[@id='occurrences-grid-id']/div/div[4]/div/table/thead/tr/th[10]/a",
            locateStrategy: "xpath",
        },
        currentFirstExpType: {
            selector: "//td[contains(.,'EXP00000124')]",
            locateStrategy: "xpath"
        },
        headerStage: {
            selector: "//div[@id='occurrences-grid-id']/div/div[4]/div/table/thead/tr/th[11]/a",
            locateStrategy: "xpath",
        },
        currentFirstStage: {
            selector: "//td[contains(.,'EXP00000124')]",
            locateStrategy: "xpath"
        },
        headerYear: {
            selector: "//div[@id='occurrences-grid-id']/div/div[4]/div/table/thead/tr/th[12]/a",
            locateStrategy: "xpath",
        },
        currentFirstYear: {
            selector: "//td[contains(.,'EXP00000004')]",
            locateStrategy: "xpath"
        },
        headerSeason: {
            selector: "//div[@id='occurrences-grid-id']/div/div[4]/div/table/thead/tr/th[13]/a",
            locateStrategy: "xpath",
        },
        currentFirstSeason: {
            selector: "//td[contains(.,'EXP00000340')]",
            locateStrategy: "xpath"
        },
        headerDesign: {
            selector: "//div[@id='occurrences-grid-id']/div/div[4]/div/table/thead/tr/th[14]/a",
            locateStrategy: "xpath",
        },
        currentFirstDesign: {
            selector: "//td[contains(.,'EXP00000341')]",
            locateStrategy: "xpath"
        },
        headerEntryCount: {
            selector: "//div[@id='occurrences-grid-id']/div/div[4]/div/table/thead/tr/th[15]/a",
            locateStrategy: "xpath",
        },
        currentFirstEntryCount: {
            selector: "//td[contains(.,'EXP00000064')]",
            locateStrategy: "xpath"
        },
        headerPlotCount: {
            selector: "//div[@id='occurrences-grid-id']/div/div[4]/div/table/thead/tr/th[16]/a",
            locateStrategy: "xpath",
        },
        currentFirstPlotCount: {
            selector: "//td[contains(.,'EXP00000002')]",
            locateStrategy: "xpath"
        },

        dataFilters: {
            selector: "//nav[@id='top-nav-bar']/div/ul/li[5]/a",
            locateStrategy: "xpath",
        },
        dataFiltersBody: {
            selector: "//ul[@id='data-filters']",
            locateStrategy: "xpath",
        },
        programFilters: {
            selector: "//ul[@id='data-filters']/li[2]/span[2]/span/span/span[2]",
            locateStrategy: "xpath",
        },
        firstProgramFilters: {
            selector: "//span/input",
            locateStrategy: "xpath",
        },
        yearFilters: {
            selector: "//ul[@id='data-filters']/li[3]/span[2]/span/span/ul/li/input",
            locateStrategy: "xpath",
        },
        firstYearFilters: {
            selector: "//ul[@id='data-filters']/li[3]/span[2]/span/span/ul/li/input",
            locateStrategy: "xpath",
        },
        seasonFilters: {
            selector: "//ul[@id='data-filters']/li[4]/span[2]/span/span/ul/li/input",
            locateStrategy: "xpath",
        },
        firstSeasonFilters: {
            selector: "//ul[@id='data-filters']/li[4]/span[2]/span/span/ul/li/input",
            locateStrategy: "xpath",
        },
        stageFilters: {
            selector: "//ul[@id='data-filters']/li[5]/span[2]/span/span/ul/li/input",
            locateStrategy: "xpath",
        },
        firstStageFilters: {
            selector: "//ul[@id='data-filters']/li[5]/span[2]/span/span/ul/li/input",
            locateStrategy: "xpath",
        },
        siteFilters: {
            selector: "//ul[@id='data-filters']/li[6]/span[2]/span/span/ul/li/input",
            locateStrategy: "xpath",
        },
        firstSiteFilters: {
            selector: "//ul[@id='data-filters']/li[6]/span[2]/span/span/ul/li/input",
            locateStrategy: "xpath",
        },
        applyFiltersButton: {
            selector: "//*[@id='apply-dashboard-filter']",
            locateStrategy: "xpath",
        },
        applyFiltersNotif: {
            selector: ".card-panel:nth-child(1)",
            locateStrategy: "css",
        },
        resetGrid: {
            selector: "//a[@id='reset-occurrences-grid-id']",
            locateStrategy: "xpath",
        },
        personalizeGrid: {
            selector: "//div[@id='occurrences-grid-id']/div/div[2]/div/div/div/button",
            locateStrategy: "xpath",
        },
        pageSizeInput: {
            selector: "//input[@id='pageSize-occurrences-grid']",
            locateStrategy: "xpath",
        },
        applyPageSize: {
            selector: "//div[@id='occurrences-grid-grid-modal']/div/div/div[3]/button[3]",
            locateStrategy: "xpath",
        },
        browserExpFilterExp: {
            selector: "//tr[@id='occurrences-grid-id-filters']/td[3]/input",
            locateStrategy: "xpath",
        },
        browserExpFilterExpResult: {
            selector: "//div[@id='occurrences-grid-id-container']/table/tbody/tr/td[5]",
            locateStrategy: "xpath",
        },
        expMgrTable: {
            selector: "//div[@id='occurrences-grid-id-container']/table/tbody/tr",
            locateStrategy: "xpath",
        },
        occurrenceDownload: {
            selector: "//div[@id='occurrences-grid-id']/div/div[2]/div/div/div/a[7]",
            locateStrategy: "xpath",
        },
        occurrenceDownloadCSV: {
            selector: "//li[@id='download-mapping-files-btn']/a",
            locateStrategy: "xpath",
        },

        occurrenceDownloadJSON: {
            selector: "//li[@id='download-mapping-files-json-btn']/a",
            locateStrategy: "xpath",
        },
        experimentName: {
            selector: "//div[@id='occurrences-grid-id-container']/table/tbody/tr/td[5]",
            locateStrategy: "xpath",
        },
        traitDataCollection: {
            selector: "//li[@id='download-data-collection-files-btn']/a",
            locateStrategy: "xpath",
        },
        traitDataCollectionOccurrence: {
            selector: "//ul[@id='data-collection-dropdown-actions']/li/a",
            locateStrategy: "xpath",
        },

        traitDataCollectionLocation: {
            selector: "//ul[@id='data-collection-dropdown-actions']/li[2]/a",
            locateStrategy: "xpath",
        },
        selectAllOccurrences: {
            selector: "//div[@id='occurrences-grid-id']/div/div[4]/div/table/thead/tr/th/label",
            locateStrategy: "xpath",
        },
        downloadDataCollectionHeader: {
            selector: "//h3[contains(.,'Download Data Collection Files')]",
            locateStrategy: "xpath",
        },
        toastOccurrenceWarning: {
            selector: "//div[@id='toast-container']/div",
            locateStrategy: "xpath",
        },
        filterExpButton: {
            selector: "//div[@id='occurrences-grid-id']/div/div[2]/div/div/div/a[5]",
            locateStrategy: "xpath",
        },
        filterExpNav: {
            selector: "//ul[@id='occurrence-filters-panel']",
            locateStrategy: "xpath",
        },
        expFilterProgram: {
            selector: "//ul[@id='occurrence-filters-panel']/li[2]/span/span/span",
            locateStrategy: "xpath",
        },

        expFilterYear: {
            selector: "//ul[@id='occurrence-filters-panel']/li[3]/span/span/span/ul/li/input",
            locateStrategy: "xpath",
        },
        expFilterSeason: {
            selector: "//ul[@id='occurrence-filters-panel']/li[4]/span/span/span/ul/li/input",
            locateStrategy: "xpath",
        },
        expFilterStage: {
            selector: "//ul[@id='occurrence-filters-panel']/li[5]/span/span/span/ul/li/input",
            locateStrategy: "xpath",
        },
        expFilterSite: {
            selector: "//ul[@id='occurrence-filters-panel']/li[6]/span/span/span/ul/li/input",
            locateStrategy: "xpath",
        },
        expFilterField: {
            selector: "//ul[@id='occurrence-filters-panel']/li[7]/span/span/span/ul/li/input",
            locateStrategy: "xpath",
        },
        expFilterExpCode: {
            selector: "//ul[@id='occurrence-filters-panel']/li[8]/span/span/span/ul/li/input",
            locateStrategy: "xpath",
        },
        expFilterExp: {
            selector: "//ul[@id='occurrence-filters-panel']/li[9]/span/span/span/ul/li/input",
            locateStrategy: "xpath",
        },
        expFilterOccurrence: {
            selector: "//ul[@id='occurrence-filters-panel']/li[10]/span/span/span/ul/li/input",
            locateStrategy: "xpath",
        },

        expFilterLocation: {
            selector: "//ul[@id='occurrence-filters-panel']/li[11]/span/span/span/ul/li/input",
            locateStrategy: "xpath",
        },

        applyExpButton: {
            selector: "//ul[@id='occurrence-filters-panel']/li[13]/button",
            locateStrategy: "xpath",
        },

        toastFilterApplicationSuccess: {
            selector: "xpath=//div[10]/div/div",
            locateStrategy: "xpath",
        },
        clearExpFilter: {
            selector: "//span[@class='select2-selection__clear']",
            locateStrategy: "xpath",
        },
        locationFilterExpMgr: {
            selector: "//tr[@id='occurrences-grid-id-filters']/td[7]/input",
            locateStrategy: "xpath",
        },
        statusFilterExpMgr: {
            selector: "//tr[@id='occurrences-grid-id-filters']/td/span/span/span",
            locateStrategy: "xpath",
        },
        statusFilterExpMgrInput: {
            selector: "(//input[@type='search'])[14]",
            locateStrategy: "xpath",
        },
        statusLabelExpMgr: {
            selector: "//div[4]/div[2]/table/tbody/tr/td[3]",
            locateStrategy: "xpath",
        },
        //
        expMgrExpTypeFilterInput: {
            selector: "//tr[@id='occurrences-grid-id-filters']/td[8]/input",
            locateStrategy: "xpath",
        },
        generateLocButton: {
            selector: "//button[@id='w8']",
            locateStrategy: "xpath",
        },
        generateLocModal: {
            selector: "//div[@id='generate-location-modal']/div/div/div",
            locateStrategy: "xpath",
        },
        generateLocationName: {
            selector: "//div[@id='basic-info1']/div/div/div/input",
            locateStrategy: "xpath",
        },
        generateLocationDesc: {
            selector: "//div[@id='basic-info2']/div[2]/div/div/input",
            locateStrategy: "xpath",
        },
        generateLocationModalNext: {
            selector: "//div[@id='generate-location-modal']/div/div/div[3]/button[2]",
            locateStrategy: "xpath",
        },
        generateLocationProceedingText: {
            selector: "//p[contains(.,'Below location and mapped plots will be created and populated. Click confirm to proceed.')]",
            locateStrategy: "xpath",
        },
        generateLocationModalConfirm: {
            selector: "//div[@id='generate-location-modal']/div/div/div[3]/button",
            locateStrategy: "xpath",
        },
        generateLocationModalSuccessToast: {
            selector: "//div[@id='toast-container']/div",
            locateStrategy: "xpath",
        },
        occurrenceViewStatusLabel: {
            selector: "//div[5]/div/div/div[2]/div/span",
            locateStrategy: "xpath",
        },
        locationCommitButton: {
            selector: "//div[5]/div/div/div/div/div[3]/button[2]",
            locateStrategy: "xpath",
        },
        locationCommitModal: {
            selector: "//div[@id='commit-occurrence-modal']/div/div/div/h4",
            locateStrategy: "xpath",
        },
        locationCommitConfirm: {
            selector: "//div[@id='commit-occurrence-modal']/div/div/div[3]/button",
            locateStrategy: "xpath",
        },
        experimentDesignFileName: {
            selector: "//div[@id='occurrences-grid-id-container']/table/tbody/tr[2]/td[6]/span",
            locateStrategy: "xpath",
        },
        expHasDesignViewButton: {
            selector: "//div[@id='occurrences-grid-id-container']/table/tbody/tr[2]/td[2]/a/i",
            locateStrategy: "xpath",
        },
        designLayoutButton: {
            selector: "//a[contains(text(),'Design Layout')]",
            locateStrategy: "xpath",
        },
        designLayout: {
            selector: "//div[@id='layout-viewer-panel']/div",
            locateStrategy: "xpath",
        },
        downlaodDesignLayout: {
            selector: "//div[@id='layout-viewer-panel']/a[5]",
            locateStrategy: "xpath",
        },
        sortByStatus: {
            selector: "//div[@id='occurrences-grid-id']/div/div[4]/div/table/thead/tr/th[3]/a",
            locateStrategy: "xpath",
        },
        experimentSearchInput: {
            selector: "//input[@name='OccurrenceSearch[experimentCode]']",
            locateStrategy: "xpath",
        },
        ascendingFirst: {
            selector: "//div[@id='occurrences-grid-id-container']/table/tbody/tr[2]/td[6]/span",
            locateStrategy: "xpath",
        },
        descendingFirst: {
            selector: "//div[@id='occurrences-grid-id-container']/table/tbody/tr[6]/td[4]/span",
            locateStrategy: "xpath",
        },
        navDataFilter: {
            selector: "//nav[@id='top-nav-bar']/div/ul/li[5]/a",
            locateStrategy: "xpath",
        },
        dataFilterSideBar: {
            selector: "//ul[@id='data-filters']n",
            locateStrategy: "xpath",
        },
        programDataFilter: {
            selector: "//ul[@id='data-filters']/li[2]/span[2]/span/span",
            locateStrategy: "xpath",
        },
        programDataFilterInput: {
            selector: "(//input[@type='search'])[14]",
            locateStrategy: "xpath",
        },
        yearDataFilter: {
            selector: "//ul[@id='data-filters']/li[3]/span[2]/span/span/ul/li/input",
            locateStrategy: "xpath",
        },
        seasonDataFilter: {
            selector: "//ul[@id='data-filters']/li[4]/span[2]/span/span/ul/li/input",
            locateStrategy: "xpath",
        },
        stageDataFilter: {
            selector: "//ul[@id='data-filters']/li[5]/span[2]/span/span/ul/li/input",
            locateStrategy: "xpath",
        },
        siteDataFilter: {
            selector: "//ul[@id='data-filters']/li[6]/span[2]/span/span/ul/li/input",
            locateStrategy: "xpath",
        },
        applyNavDataFilter: {
            selector: "//ul[@id='data-filters']/li[8]/button",
            locateStrategy: "xpath",
        },
        onekentriesExperiment: {
            selector: "//td[contains(.,'GEN-LOCATION-TAF-PARALLEL-1K')]",
            locateStrategy: "xpath",
        },
        one5kentriesExperiment: {
            selector: "//td[contains(.,'GEN-LOCATION-TAF-PARALLEL-1.5K')]",
            locateStrategy: "xpath",
        },
        twokentriesExperiment: {
            selector: "//td[contains(.,'GEN-LOCATION-TAF-PARALLEL-2K')]",
            locateStrategy: "xpath",
        },
        two5kentriesExperiment: {
            selector: "//td[contains(.,'GEN-LOCATION-TAF-PARALLEL-2.5k')]",
            locateStrategy: "xpath",
        },
        genLocPlusIcon: {
            selector: "//i[contains(.,'add_location')]",
            locateStrategy: "xpath",
        },

        occurrenceStatusLabel: {
            selector: "//div[@id='occurrences-grid-id-container']/table/tbody/tr[2]/td[3]/span",
            locateStrategy: "xpath",
        },
        dataFiltersMenu: {
            selector: "//a[@id='go-to-data-filters']/i",
            locateStrategy: "xpath",
        },
        saveListNotif: {
            selector: "//*[@id='toast-container']",
            locateStrategy: "xpath"
        },
        filterStatusOption: {
            selector: "//*[@id='occurrences-grid-id-filters']/td[1]/span/span[1]/span",
            locateStrategy: "xpath",
        },
        statusInput: {
            selector: "/html/body/span[2]/span/span[1]/input",
            locateStrategy: "xpath",
        },
        plantedStatus: {
            selector: "//span[@title='Status: PLANTED']",
            locateStrategy: "xpath",
        },
        viewOccurrencDetails: {
            selector: "//*[@id='occurrences-grid-id-container']/table/tbody/tr[2]/td[2]/a",
            locateStrategy: "xpath",
        },
        browserDetails: {
            selector: "/html/body/div[8]/div[5]",
            locateStrategy: "xpath",
        },
        goToPlotTab: {
            selector: "/html/body/div[8]/div[1]/ul/li[3]/a",
            locateStrategy: "xpath",
        },
        plotColumn: {
            selector: "//*[@id='occurrence-plot-grid-id']/div/div[3]/div[1]/table/thead/tr[1]/th[2]/a",
            locateStrategy: "xpath",
        },
        plotTabAsc: {
            selector: "//a[@class='asc' and @data-sort='-plotNumber']",
            locateStrategy: "xpath",
        },
        plotTabDesc: {
            selector: "//a[@class='desc' and @data-sort='plotNumber']",
            locateStrategy: "xpath",
        },
        entryColumn: {
            selector: "//*[@id='occurrence-plot-grid-id']/div/div[3]/div[1]/table/thead/tr[1]/th[3]/a",
            locateStrategy: "xpath",
        },
        entryTabAsc: {
            selector: "//a[@class='asc' and @data-sort='-entryNumber']",
            locateStrategy: "xpath",
        },
        entryTabDesc: {
            selector: "//a[@class='desc' and @data-sort='entryNumber']",
            locateStrategy: "xpath",
        },
        germplasmNameColumn: {
            selector: "//*[@id='occurrence-plot-grid-id']/div/div[3]/div[1]/table/thead/tr[1]/th[4]/a",
            locateStrategy: "xpath",
        },
        germplasmNameTabAsc: {
            selector: "//a[@class='asc' and @data-sort='-entryName']",
            locateStrategy: "xpath",
        },
        germplasmNameTabDesc: {
            selector: "//a[@class='desc' and @data-sort='entryName']",
            locateStrategy: "xpath",
        },
        germplasmStateColumn: {
            selector: "//*[@id='occurrence-plot-grid-id']/div/div[3]/div[1]/table/thead/tr[1]/th[5]/a",
            locateStrategy: "xpath",
        },
        germplasmStateTabAsc: {
            selector: "//a[@class='asc' and @data-sort='-germplasmState']",
            locateStrategy: "xpath",
        },
        germplasmStateTabDesc: {
            selector: "//a[@class='desc' and @data-sort='germplasmState']",
            locateStrategy: "xpath",
        },
        expMngrTitle: {
            selector: "/html/body/div[8]/h3/a[contains(text(),'Experiment Manager')]",
            locateStrategy: "xpath",
        }
    },
    sections: {
        browserDataFilters: {
            selector: "//ul[@id='data-filters']",
            locateStrategy: "xpath",
            elements: {
                programFilter: {
                    selector: ".//ul[@id='data-filters']/li[2]/span/span/span",
                    locateStrategy: "xpath",
                },
                yearFilter: {
                    selector: ".//ul[@id='data-filters']/li[3]/span/span/span/ul/li/input",
                    locateStrategy: "xpath",
                },
                seasonFilter: {
                    selector: ".//ul[@id='data-filters']/li[4]/span/span/span/ul/li/input",
                    locateStrategy: "xpath",
                },
                stageFilter: {
                    selector: ".//ul[@id='data-filters']/li[5]/span/span/span/ul/li/input",
                    locateStrategy: "xpath",
                },
                siteFilter: {
                    selector: ".//ul[@id='data-filters']/li[6]/span/span/span/ul/li/input",
                    locateStrategy: "xpath",
                },
            }
        },
        genLocModal: {
            selector: "//div[@id='generate-location-modal']/div/div/div[2]",
            locateStrategy: "xpath",
            elements: {
                locationNameInput: {
                    selector: ".//div[@id='basic-info1']/div/div/div/input",
                    locateStrategy: "xpath",
                },
                descriptionInput: {
                    selector: ".//div[@id='basic-info2']/div/div/div/input",
                    locateStrategy: "xpath",
                }
            }
        },
        dataFilters: {
            selector: "//ul[@id='data-filters']",
            locateStrategy: "xpath",
            elements: {
                programSearchSelect: {
                    // selector: ".//*[@id='program_select']/following-sibling::span//input",
                    selector: ".//*[@id='select2-program-dashboard_select-container']", //".//*[@id='select2-program_select-container']",
                    locateStrategy: "xpath",
                },
                yearSelect: {
                    selector: ".//*[@id='year_select']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                seasonSelect: {
                    selector: ".//*[@id='season_select']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                stageSelect: {
                    selector: ".//*[@id='stage_select']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                siteSelect: {
                    selector: ".//*[@id='site_select']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                experimentsSlider: "input[id='data_filter_show_owned_experiments']",
                cancelButton: {
                    selector: ".//button[normalize-space(.)='Cancel']",
                    locateStrategy: "xpath",
                },
                applyButton: {
                    selector: ".//button[normalize-space(.)='Apply']",
                    locateStrategy: "xpath",
                },
            },
        },
        searchGrid: {
            selector: "div[id='occurrences-grid-pjax']",
            elements: {
            },
        },
        emColFilters: {
            selector: "//div[@id='occurrences-grid-id']",
            locateStrategy: "xpath",
            elements: {
                expCodeColFilter: {
                    selector: ".//input[@name='OccurrenceSearch[experimentCode]']",
                    locateStrategy: "xpath",
                },
                expColFilter: {
                    selector: ".//input[@name='OccurrenceSearch[experiment]']",
                    locateStrategy: "xpath",
                },
                occurrenceColFilter: {
                    selector: ".//input[@name='OccurrenceSearch[occurrenceName]']",
                    locateStrategy: "xpath",
                },
                siteColFilter: {
                    selector: ".//input[@name='OccurrenceSearch[siteCode]']",
                    locateStrategy: "xpath",
                },
                fieldColFilter: {
                    selector: ".//input[@name='OccurrenceSearch[field]']",
                    locateStrategy: "xpath",
                },

                locationColFilter: {
                    selector: ".//input[@name='OccurrenceSearch[location]']",
                    locateStrategy: "xpath",
                },

                expTypeColFilter: {
                    selector: ".//input[@name='OccurrenceSearch[experimentType]']",
                    locateStrategy: "xpath",
                },

                stageColFilter: {
                    selector: ".//input[@name='OccurrenceSearch[experimentStageCode]']",
                    locateStrategy: "xpath",
                },

                yearColFilter: {
                    selector: ".//input[@name='OccurrenceSearch[experimentYear]']",
                    locateStrategy: "xpath",
                },
                seasonColFilter: {
                    selector: ".//input[@name='OccurrenceSearch[experimentSeasonCode]']",
                    locateStrategy: "xpath",
                },
                designColFilter: {
                    selector: ".//input[@name='OccurrenceSearch[experimentDesignType]']",
                    locateStrategy: "xpath",
                },
                entryCountColFilter: {
                    selector: ".//input[@name='OccurrenceSearch[entryCount]']",
                    locateStrategy: "xpath",
                },
            }
        },
        occurrencePlotGrid: {
            selector: "//*[@id='occurrence-plot-grid-pjax']",
            locateStrategy: "xpath",
            elements: {
                shoppingCart: {
                    selector: ".//*[@id='plot-save-list-btn']",
                    locateStrategy: "xpath",
                },
            },
        },
        createListModal: {
            selector: "#experiment_manager-save-list-modal",
            elements: {
                nameField: "#plot-list-name",
                abbrevField: "#plot-list-abbrev",
                displayNameField: "#plot-list-display_name",
                descriptionField: "#plot-list-description",
                remarksField: "#plot-list-remarks",
                createButton: {
                    selector: "//*[@id='experiment_manager-save-list-confirm-btn']",//"#experiment_manager-save-list-confirm-btn",
                    locateStrategy: "xpath",
                },
                listTypeOptions: {
                    selector: "//*[@id='select2-plot-list-type-results']",
                    locateStrategy: "xpath",
                },//"#select2-plot-list-type-results",
                listType: "#select2-plot-list-type-container", /*{
                    selector: "//*[@id='select2-plot-list-type-container']",
                    locateStrategy: "xpath",
                    elements: {},
                },*/
            }, //"#select2-plot-list-type-container", //"#plot-list-type",
        },
    },
    commands: [{
        filterSetValue: async function (input, value) {
            console.log(input);
            console.log(value);
            await this.click(input);
            await this.pause(2000);
            await client.setValue(input, value);
            await this.pause(1000);
            await client.keys(client.Keys.ENTER);
        },
        filterClearValue: async function (input) {
            console.log(input);
            await this.click(input);
            await this.pause(2000);
            await client.setValue(input, client.Keys.BACK_SPACE);
            await this.pause(1000);
        },
        genLocationSetValue: async function (input, value) {
            console.log(input);
            console.log(value);
            await this.click(input);
            await this.pause(2000);
            await client.setValue(input, value);
            await this.pause(1000);
            await client.keys(client.Keys.ENTER);
        },
        //Set values of a listbox
        //Listbox allows searching of a value
        listBoxSetValue: async function (input, value) {
            //clear existing values
            let originalSelector = input.__selector;
            input.__selector =
                input.__selector +
                "/../../li[@class='select2-selection__choice']/span";

            let elements = 0;
            await this.api.findElements(input, function (result) {
                elements = result.value;
            });

            while (elements.length > 0) {
                await this.api.elementIdClick(elements[0].getId());
                await this.api.findElements(input, function (result) {
                    elements = result.value;
                });
            }

            //revert original selector
            input.__selector = originalSelector;

            //split string by ,
            let listItems = value.split(",");
            for (let index = 0; index < listItems.length; index++) {
                let item = listItems[index].trim();
                await this.click(input);
                //set value to display search result match
                await this.setValue(input, item);
                const control = {
                    selector: `//span//ul[@role='listbox']/li[normalize-space(.)='${item}']`,
                    locateStrategy: "xpath",
                };
                //click match
                await this.waitForElementVisible(control);
                await this.click(control);
            }
        },

        //use for slider style (checkbox)
        sliderSetValue: async function (input, value) {
            //save selector
            const originalSelector = input.__selector;
            switch (value) {
                case "Left":
                    input.__selector = input.__selector + ":not(:checked)";
                    break;
                case "Right":
                    input.__selector = input.__selector + ":checked";
                    break;
                default:
                    throw new Error(`Unexpected Slider value (Left, Right).`);
            }
            let found;
            await this.waitForElementPresent(input, 1000, 500, false, (result) => {
                found = result.value;
            });

            if (!found) {
                //add + span to click the control
                input.__selector = originalSelector + " + span";
                await this.click(input);
            }
        },
        //set column filter
        columnFilterSetValue: async function (input, value) {
            await this.click(input);
            await this.pause(1000);
            /*const control = {
            selector: `//li[normalize-space(.)='${value.trim()}']`,
            locateStrategy: "xpath",
            };*/
            await this.setValue(input, value, this.Keys.ENTER);

            //await this.waitForElementVisible(control);
            //await this.click(control);
        },
        //Select with Search
        searchSelectSetValue: async function (input, value) {
            await this.click(input);
            await this.pause(1000);
            const control = {
                selector: `//li[normalize-space(.)='${value.trim()}']`,
                locateStrategy: "xpath",
            };
            await this.waitForElementVisible(control);
            await this.click(control);
        },
        select: async function (selector, option) {
            //await this.click(selector);
            const element = {
                selector: `//li[text()='${option}']`,
                locateStrategy: "xpath",
            };
            await this.waitForElementVisible(element);
            await this.click(element);
        },
        //Wait Until Grid is Ready
        waitGridReady: async function (input) {
            //grid selector must be CSS
            let originalSelector = input.__selector;
            input.__selector = input.__selector + ".kv-grid-loading";
            await this.waitForElementNotPresent(input, 1200000, 1000);
            input.__selector = originalSelector;
        },

        setCheckbox: async function (input, value) {
            let status = false;
            await this.getAttribute(input, "checked", (result) => {
                if (result.value != null) {
                    status = true;
                }
            });
            if ((value && !status) || (!value && status)) {
                await this.api.execute(
                    `document.querySelector("${input.__selector}").click();`
                );
            }
        },

        getRowCount: async function (input) {
            try {
                let originalSelector = input.__selector;
            }
            catch (error) {
            }
            finally {
            }
        },

    }]
};