const { client } = require("nightwatch-api");

module.exports = {
    url: "https://qa.ebsproject.org/",

    elements: {

        // MENU NAVIGATION

        serviceManagementInstance: {
            selector: "//div[@id='single-spa-application:@ebs/cs']/div/main/div/div/div[3]/div/div/div[2]/div/button[2]/div/div/img",
            locateStrategy: "xpath",
        },
        serviceManagementHeader: {
            selector: "//div[@id='single-spa-application:@ebs/cs']/div/header/div/h6",
            locateStrategy: "xpath",
        },
        serviceManagementLogo: {
            selector: "//div[@id='single-spa-application:@ebs/cs']/div/header/div/span/img",
            locateStrategy: "xpath",
        },
        requestManagerMenu: {
            selector: "//div[@id='single-spa-application:@ebs/cs']/div/div/div/ul/div/div[2]/span",
            locateStrategy: "xpath",
        },
        requestManagerText: {
            selector: "//div[@id='single-spa-application:@ebs/cs']/div/main/div[2]/div/div/div/div/h6",
            locateStrategy: "xpath",
        },
        genotypingServiceManagerMenu: {
            selector: "//div[@id='single-spa-application:@ebs/cs']/div/div/div/ul/div[2]/div[2]/span",
            locateStrategy: "xpath",
        },
        genotypingServiceManagerText: {
            selector: "//div[@id='single-spa-application:@ebs/cs']/div/main/main/div/div/h6",
            locateStrategy: "xpath",
        },
        inspectMenu: {
            selector: "//div[@id='single-spa-application:@ebs/cs']/div/div/div/ul/div[3]/div[2]/span",
            locateStrategy: "xpath",
        },
        inspectText: {
            selector: "//div[@id='single-spa-application:@ebs/cs']/div/main/main/div/div/h6",
            locateStrategy: "xpath",
        },
        designMenu: {
            selector: "//div[@id='single-spa-application:@ebs/cs']/div/div/div/ul/div[4]/div[2]/span",
            locateStrategy: "xpath",
        },
        designText: {
            selector: "//div[@id='single-spa-application:@ebs/cs']/div/main/main/div/div/h6",
            locateStrategy: "xpath",
        },
        vendorMenu: {
            selector: "//div[@id='single-spa-application:@ebs/cs']/div/div/div/ul/div[5]/div[2]/span",
            locateStrategy: "xpath",
        },
        // vendorText: {
        //     selector: "",
        //     locateStrategy: "xpath",
        // },
        serviceCatalogMenu: {
            selector: "//div[@id='single-spa-application:@ebs/cs']/div/div/div/ul/div[6]/div[2]/span",
            locateStrategy: "xpath",
        },
        // serviceCatalogText: {
        //     selector: "",
        //     locateStrategy: "xpath",
        // },
        shipmentToolMenu: {
            selector: "//div[@id='single-spa-application:@ebs/cs']/div/div/div/ul/div[7]/div[2]/span",
            locateStrategy: "xpath",
        },
        // shipmentToolText: {
        //     selector: "",
        //     locateStrategy: "xpath",
        // },



        // REQUEST MANAGER
        rmStatusFilter: {
            selector: "//div[@id='menu-']/div[3]/ul/li",
            locateStrategy: "xpath",
        },
        rmRequestFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div/div[2]/div/div/div[2]/table/thead/tr/th[4]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        rmRequesterFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div/div[2]/div/div/div[2]/table/thead/tr/th[5]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        rmCropFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div/div[2]/div/div/div[2]/table/thead/tr/th[6]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        rmServiceTypeFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div/div[2]/div/div/div[2]/table/thead/tr/th[7]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        rmServiceProviderFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div/div[2]/div/div/div[2]/table/thead/tr/th[8]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        rmServicesFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div/div[2]/div/div/div[2]/table/thead/tr/th[9]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        rmPurposeFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div/div[2]/div/div/div[2]/table/thead/tr/th[10]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        rmTotalFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div/div[2]/div/div/div[2]/table/thead/tr/th[11]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        //SM_RequestManager_Create same with Edit function
        rmCreateRequest: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div/div[2]/div/div/div/header/div/div[4]/a/span",
            locateStrategy: "xpath",
        },
        rmGermplasmList: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div[3]/div/div/div/div/div/div[2]/table/tbody/tr/td/div/div/span/span/input",
            locateStrategy: "xpath",
        },
        rmExtryListNext: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div[7]/div/button",
            locateStrategy: "xpath",
        },
        rmBasicTabNext: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div[4]/div/div/div/div[2]/div/form/div[2]/button[3]/span/p",
            locateStrategy: "xpath",
        },
        rmServiceProviderSelection: {
            selector: "//div[@id='react-select-3-option-0']",
            locateStrategy: "xpath",
        },
        rmServiceTypeSelection: {
            selector: "//div[@id='react-select-4-option-0']",
            locateStrategy: "xpath",
        },
        rmPurposeSelection: {
            selector: "//div[@id='react-select-5-option-1']",
            locateStrategy: "xpath",
        },
        rmServiceSelection: {
            selector: "//div[@id='react-select-6-option-1']",
            locateStrategy: "xpath",
        },
        rmServicesTabNext: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div[5]/div/div/div/div[2]/form/div[2]/button[3]/span/p",
            locateStrategy: "xpath",
        },
        rmReviewTabNext: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div[7]/div/button/span/p",
            locateStrategy: "xpath",
        },
        //SM_RequestManager_Download
        rmDownload: {
            selector: ".ebs-jss37 > .ebs-MuiButtonBase-root:nth-child(2) .ebs-MuiSvgIcon-root",
            locateStrategy: "css",
        },
        //SM_RequestManager_View
        rmViewRequest: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div/div[2]/div/div/div[2]/table/tbody/tr/td[2]/div/div/a",
            locateStrategy: "xpath",
        },
        rmViewBasicTab: {
            selector: "//button[@id='simple-tab-0']/span",
            locateStrategy: "xpath",
        },
        rmViewGermplasmTab: {
            selector: "//button[@id='simple-tab-1']/span",
            locateStrategy: "xpath",
        },
        rmViewBackButton: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/a/span",
            locateStrategy: "xpath",
        },
        //SM_RequestManager_Edit
        rmEditRequest: {
            selector: ".ebs-jss196 .ebs-MuiSvgIcon-root",
            locateStrategy: "css",
        },
        //SM_RequestManager_Delete
        rmDeleteRequest: {
            selector: ".ebs-jss500 path",
            locateStrategy: "css",
        },
        rmDeleteConfirm: {
            selector: "//div[3]/div/div[3]/button[2]/span",
            locateStrategy: "xpath",
        },



        // GENOTYPING SERVICE MANAGER
        gsmStatusFilter: {
            selector: "//div[@id='menu-']/div[3]/ul/li",
            locateStrategy: "xpath",
        },
        gsmStatusFilterNew: {
            selector: "//div[@id='menu-']/div[3]/ul/li[2]",
            locateStrategy: "xpath",
        },
        gsmRequestFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/main/div/div[2]/div/div/div[2]/table/thead/tr/th[3]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        gsmRequesterFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/main/div/div[2]/div/div/div[2]/table/thead/tr/th[4]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        gsmCropFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/main/div/div[2]/div/div/div[2]/table/thead/tr/th[5]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        gsmProgramFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/main/div/div[2]/div/div/div[2]/table/thead/tr/th[6]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        gsmICCFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/main/div/div[2]/div/div/div[2]/table/thead/tr/th[7]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        gsmPurposeFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/main/div/div[2]/div/div/div[2]/table/thead/tr/th[8]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        gsmServiceFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/main/div/div[2]/div/div/div[2]/table/thead/tr/th[9]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        gsmServiceProviderFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/main/div/div[2]/div/div/div[2]/table/thead/tr/th[10]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        gsmTotalFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/main/div/div[2]/div/div/div[2]/table/thead/tr/th[11]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        gsmSubmitDateFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/main/div/div[2]/div/div/div[2]/table/thead/tr/th[12]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        gsmCompletedByFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/main/div/div[2]/div/div/div[2]/table/thead/tr/th[13]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        gsmServiceTypeFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/main/div/div[2]/div/div/div[2]/table/thead/tr/th[14]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        gsmApproveButton: {
            selector: ".ebs-jss464 div:nth-child(2) .ebs-MuiSvgIcon-root",
            locateStrategy: "css",
        },
        gsmApproveConfirmButton: {
            selector: "//div[3]/div/div[3]/button[2]/span",
            locateStrategy: "xpath",
        },
        gsmRejectButton: {
            selector: ".ebs-MuiTableRow-root:nth-child(1) .ebs-MuiBox-root div:nth-child(3) path",
            locateStrategy: "css",
        },
        gsmRejectConfirmButton: {
            selector: "//div[3]/div/div[3]/button[2]/span",
            locateStrategy: "xpath",
        },



        // INSPECT MANAGER
        imBatchNameFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/main/div/div[2]/div/div/div[2]/table/thead/tr/th[2]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        imDescriptionFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/main/div/div[2]/div/div/div[2]/table/thead/tr/th[3]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        imTotalFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/main/div/div[2]/div/div/div[2]/table/thead/tr/th[4]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        imDownload: {
            selector: ".ebs-jss36 > .ebs-MuiButtonBase-root:nth-child(1) .ebs-MuiSvgIcon-root",
            locateStrategy: "css",
        },



        // DESIGN MANAGER
        dmBatchNameFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/main/div/div[2]/div/div/div[2]/table/thead/tr/th[2]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        dmBatchNameFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/main/div/div[2]/div/div/div[2]/table/thead/tr/th[3]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        dmBatchNameFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/main/div/div[2]/div/div/div[2]/table/thead/tr/th[4]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        dmBatchNameFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/main/div/div[2]/div/div/div[2]/table/thead/tr/th[5]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        dmBatchNameFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/main/div/div[2]/div/div/div[2]/table/thead/tr/th[6]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        dmBatchNameFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/main/div/div[2]/div/div/div[2]/table/thead/tr/th[7]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        dmBatchNameFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/main/div/div[2]/div/div/div[2]/table/thead/tr/th[8]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        dmBatchNameFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/main/div/div[2]/div/div/div[2]/table/thead/tr/th[9]/div/div[3]/div/div/input",
            locateStrategy: "xpath",
        },
        dmDownload: {
            selector: "//*[@id='single-spa-application:@ebs/layout']/div/main/main/div/div[2]/div/div[1]/div[2]/table/tbody/tr[1]/td[1]/div/div/div/table/tbody/tr/td[2]/button/span[1]/svg",
            locateStrategy: "xpath",
        },



        // SHIPMENT TOOL
        stStatusFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div[2]/div/div/div/div[3]/div[2]/div/div/div/input",
            locateStrategy: "xpath",
        },
        stSenderProgramFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div[2]/div/div/div/div[4]/div[2]/div/div/div/input",
            locateStrategy: "xpath",
        },
        stShipmentNameFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div[2]/div/div/div/div[5]/div[2]/div/div/div/input",
            locateStrategy: "xpath",
        },
        stShipmentCodeFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div[2]/div/div/div/div[6]/div[2]/div/div/div/input",
            locateStrategy: "xpath",
        },
        stShipmentTypeFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div[2]/div/div/div/div[7]/div[2]/div/div/div/input",
            locateStrategy: "xpath",
        },
        stShipmentPurposeFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div[2]/div/div/div/div[8]/div[2]/div/div/div/input",
            locateStrategy: "xpath",
        },
        stTotalItemsFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div[2]/div/div/div/div[12]/div[2]/div/div/div/input",
            locateStrategy: "xpath",
        },
        stRequestorNameFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div[2]/div/div/div/div[13]/div[2]/div/div/div/input",
            locateStrategy: "xpath",
        },
        stRequestorEmailFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div[2]/div/div/div/div[14]/div[2]/div/div/div/input",
            locateStrategy: "xpath",
        },
        stRequestorAddressFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div[2]/div/div/div/div[15]/div[2]/div/div/div/input",
            locateStrategy: "xpath",
        },
        stRequestorInstitutionFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div[2]/div/div/div/div[16]/div[2]/div/div/div/input",
            locateStrategy: "xpath",
        },
        stRequestorCountryFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div[2]/div/div/div/div[17]/div[2]/div/div/div/input",
            locateStrategy: "xpath",
        },
        stSenderNameFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div[2]/div/div/div/div[20]/div[2]/div/div/div/input",
            locateStrategy: "xpath",
        },
        stSenderInstitutionFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div[2]/div/div/div/div[21]/div[2]/div/div/div/input",
            locateStrategy: "xpath",
        },
        stSenderCountryFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div[2]/div/div/div/div[22]/div[2]/div/div/div/input",
            locateStrategy: "xpath",
        },
        stRecipientNameFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div[2]/div/div/div/div[23]/div[2]/div/div/div/input",
            locateStrategy: "xpath",
        },
        stRecipientEmailFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div[2]/div/div/div/div[25]/div[2]/div/div/div/input",
            locateStrategy: "xpath",
        },
        stRecipientInstitutionFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div[2]/div/div/div/div[26]/div[2]/div/div/div/input",
            locateStrategy: "xpath",
        },
        stRecipientCountryFilter: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div[2]/div/div/div/div[9]/div[2]/div/div/div/input",
            locateStrategy: "xpath",
        },
        stNotes: {
            selector: ".ebs-jss376 path",
            locateStrategy: "css",
        },
        stNotePad: {
            selector: "//div[2]/div/div/textarea",
            locateStrategy: "xpath",
        },
        stNoteButton: {
            selector: "//div[2]/button[2]/span",
            locateStrategy: "xpath",
        },
        stView: {
            selector: ".ebs-jss384 .ebs-MuiSvgIcon-root",
            locateStrategy: "css",
        },
        stViewBasicInfoTab: {
            selector: "//div/div/button[2]/span",
            locateStrategy: "xpath",
        },
        stViewEntriesTab: {
            selector: "//button[3]/span",
            locateStrategy: "xpath",
        },
        stViewDocumentsTab: {
            selector: "//div[2]/div/div/div/div/button/span",
            locateStrategy: "xpath",
        },
        stModify: {
            selector: ".ebs-jss994 path",
            locateStrategy: "css",
        },
        stModifyBasicSave: {
            selector: "//div[@id='horizontal-tabpanel-0']/div/div/form/div[2]/div[2]/button/span",
            locateStrategy: "xpath",
        },
        stModifyEntriesTab: {
            selector: "//button[@id='vertical-tab-1']/span",
            locateStrategy: "xpath",
        },
        stModifyDocumentsTab: {
            selector: "//button[@id='vertical-tab-2']/span",
            locateStrategy: "xpath",
        },
        stSearchBox: {
            selector: "//div[@id='single-spa-application:@ebs/layout']/div/main/div[2]/div/div/div/div[2]/div[4]/div/div/div/input",
            locateStrategy: "xpath",
        },
        stSearchButton: {
            selector: ".ebs-jss1267:nth-child(2) .ebs-MuiSvgIcon-root",
            locateStrategy: "css",
        },
        stDownload: {
            selector: ".ebs-MuiIconButton-colorSecondary:nth-child(1) .ebs-MuiSvgIcon-root:nth-child(1)",
            locateStrategy: "css",
        },
        stPersonalizeGrid: {
            selector: ".ebs-jss1255",
            locateStrategy: "css",
        },
    },
    commands: [
        {

        },
    ],
};
