const { client } = require("nightwatch-api");

module.exports = {
    url: "https://cb-qa.ebsproject.org/",
    // commands: [commands.commands],
    elements: {
        /*REFACTOR USE*/
        showLeftNav: "#go-to-show-left-nav",
        harvestManagerMenu: "#slide-out > li.parent-menu.seeds-harvest-manager-left-menu > a",
        harvestManagerFavorite: "#favorite-a-tool-icon-seedInventoryharvest-manager",
        experimentLocationDetails: "#study_details_id",
        loadPlots: "#find-btn-harvest",
        loadCrosses: "#load-crosses-btn",
        applyChanges: "#apply_changes",
        dataBrowserSummary: {
            selector: "//*[@id='search-harvest-grid-table-con']/div/div[1]/div[2]/div",
            locateStrategy: "xpath",
        },
        uncommittedData: {
            selector: "//*[@id='bulk-delete-main-body']/div[1]/div/div[2]/div[2]/ul/li[1]/label",
            locateStrategy: "xpath",
        },
        committedData: {
            selector: "//*[@id='bulk-delete-main-body']/div[1]/div/div[2]/div[2]/ul/li[2]/label",
            locateStrategy: "xpath",
        },
        selectHarvestMethod: "#select2-harvest-method-select-container > span",
        harvestMethodDropdown: "#select2-harvest-method-select-results",
        noOfPlants: "#no-of-plants-input",
        applyToSelected: "#apply-bulk-selected-btn",
        notificationMessage: {
            selector: "//*[@id='notifications-dropdown-creation']/li[3]/a/text()[normalize-space(.)='Seed creation successful!']",
            locateStrategy: "xpath",
        },
        notifBadge: {
            selector: "//*[@id='creation-notif-btn-id']",
            locateStrategy: "xpath",
        },
        selectAll: "#search-harvest-grid-table-con-container > table > thead > tr:nth-child(1) > th:nth-child(2) > label",
        notifResult: {
            selector: "//*[@id='notifications-dropdown-creation']/li[3]",
            locateStrategy: "xpath",
        },
        seedlotBrowserVerify: "#manage-seedlot-grid-table-con-container",
        notifButton: "#creation-notif-btn-id",
        totalUncommitted: {
            selector:
                "//span[@title='No seeds created yet' and contains(strong, 'NO HARVEST')]",
            locateStrategy: "xpath",
        },
        emptyHarvestMethod: {
            selector:
                "//span[@class='select2-selection__placeholder' and contains(text(), 'Select a method')]",
            locateStrategy: "xpath",
        },
        showHideSummaryClearAll: "#clear",
        germplasmState: {
            selector:
                "//*[@id='select2-plotbrowsermodel-state-container']/span",
            locateStrategy: "xpath",
        },
        germplasmStateResults: "#select2-plotbrowsermodel-state-results",
        germplasmStatNotFixed: {
            selector:
                "//*[@id='select2-plotbrowsermodel-state-result-5yel-not_fixed']",
            locateStrategy: "xpath"
        },
        /*REFACTOR END */
        queryProgram: "#select2-program-filter-container",
        clearFilter: "#clear",
        createTab: "#create-seedlots-tab",
        closeModal: "#bulk-delete-modal > div > div > div.modal-header > button",
        clearSearchParams: "#reset-btn",
        collapsibleHeader: "#filters > div.collapsible-header.query-div",
        harvestDataTab: "#select-plot-and-method-tab > a",
        checkAllRecords:
            "#manage-seedlot-grid-table-con-container > table > thead > tr:nth-child(1) > th:nth-child(2) > label",
        selectedItemSummary: "#selected-items-text-seedlot",
        totalSelectedItems: "#selected-items-text",
        bulkDeleteSelected: "#apply-bulk-delete-selected-btn",
        deletePackageBulk: "#delete-package-bulk",
        deletePackageModal: "#package-delete-content-div",
        deletePackage: "#delete-bulk-btn",
        deletePackageProgress: "#package-delete-progress-textarea",
        deleteSuccess: "#toast-container > div",
        exportButton: "#plot-browser-export-btn",
        exportDropdown: {
            selector: "//ul[@class='dropdown-menu']",
            locateStrategy: "xpath",
        },
        exportHTML: {
            selector: "//a[@class='export-full-html']",
            locateStrategy: "xpath",
        },
        exportCSV: {
            selector: "//a[@class='export-full-csv']",
            locateStrategy: "xpath",
        },
        exportText: {
            selector: "//a[@class='export-full-txt']",
            locateStrategy: "xpath",
        },
        exportExcel95: {
            selector: "//a[@class='export-full-xls']",
            locateStrategy: "xpath",
        },
        exportExcel2007: {
            selector: "//a[@class='export-full-xlsx']",
            locateStrategy: "xpath",
        },
        resSummary:
            "#search-harvest-grid-table-con > div > div.kv-panel-before > div:nth-child(2) > div",
        germplasmColumn: {
            selector:
                "//*[@id='search-harvest-grid-table-con-container']/table/thead/tr[1]/th[6]/a",
            locateStrategy: "xpath",
        },
        germplasmColumnFilter: {
            selector: " //*[@id='select2-plotbrowsermodel-state-container']",
            locateStrategy: "xpath",
        },
        plotFilterSummary: {
            selector:
                "//*[@id='search-harvest-grid-table-con']/div/div[1]/div[2]/div[contains(text(),'1-1')]",
            locateStrategy: "xpath",
        },
        expLocDetails: "#experiment_detail_id",
        locCodeNote: {
            selector:
                "//*[@id='location-code-warning'][contains(text(), 'Location code is required. Please select one.')]",
            locateStrategy: "xpath",
        },
        emptyBrowser: {
            selector:
                "//*[@id='search-harvest-grid-table-con-container']/table/tbody/tr/td/div[contains(text(), 'No results found.')]",
            locateStrategy: "xpath",
        },
        emptyManageBrowser: {
            selector:
                "//*[@id='manage-seedlot-grid-table-con-container']/table/tbody/tr/td/div[contains(text(), 'No results found.')]",
            locateStrategy: "xpath",
        },
        notifSuccess: {
            selector:
                "//*[@id='notifications-dropdown-creation']/li[3]/a/span[contains(text(), ' Seed creation successful!')]",
            locateStrategy: "xpath",
        },
        expDropdown: {
            selector: "/html/body/span[2]/span/span[1]/input",
            locateStrategy: "xpath",
        },
        germplasmFilter:
            "#search-harvest-grid-table-con-filters > td:nth-child(4) > span > span.selection > span",
        germplasmFilterInput:
            "#body > span.select2-container.select2-container--krajee.select2-container--open > span > span.select2-search.select2-search--dropdown > input",
        germplasmNotFixed: {
            selector: "/html/body/span[2]/span/span[2]/ul/li[2]",
            locateStrategy: "xpath",
        },
        programMaize: {
            selector: "//*[@id='select2-program-filter-results']/li[3]",
            locateStrategy: "xpath",
        },
        programWheat: {
            selector: "//*[@id='select2-program-filter-results']/li[2]",
            locateStrategy: "xpath",
        },
        queryParamExperiment: {
            selector: "//*[@id='select2-experiment_name-filter-container']",
            locateStrategy: "xpath",
        },
        queryExperiment: {
            selector:
                "//*[@id='select2-experiment_name-filter-results']/li[contains(text(), 'KE-SEM-2019-A-001')]",
            locateStrategy: "xpath",
        },
        wheatQueryExperiment: {
            selector:
                "//*[@id='select2-experiment_name-filter-results']/li[contains(text(), 'BW-F1-2014-A-002')]",
            locateStrategy: "xpath",
        },
        wheatSinglePlantExperiment: {
            selector:
                "//*[@id='select2-experiment_name-filter-results']/li[contains(text(), 'BW-F1-2014-A-003')]",
            locateStrategy: "xpath",
        },
        wheatSelectedBulkExperiment: {
            selector:
                "//*[@id='select2-experiment_name-filter-results']/li[contains(text(), 'BW-F1-2014-A-004')]",
            locateStrategy: "xpath",
        },
        wheatIndividualSpikeExperiment: {
            selector:
                "//*[@id='select2-experiment_name-filter-results']/li[contains(text(), 'BW-F1-2014-A-005')]",
            locateStrategy: "xpath",
        },
        KEIndividualEarExperiment: {
            selector:
                "//*[@id='select2-experiment_name-filter-results']/li[contains(text(), 'KE-F4-2019-A-001')]",
            locateStrategy: "xpath",
        },
        KEBulkExperiment: {
            selector:
                "//*[@id='select2-experiment_name-filter-results']/li[contains(text(), 'KE-SEM-2019-A-001')]",
            locateStrategy: "xpath",
        },
        KEBulkCrossExperiment: {
            selector:
                "//*[@id='select2-experiment_name-filter-results']/li[contains(text(), 'MAIZE ICN All Selfing - not_fixed')]",
            locateStrategy: "xpath",
        },
        KEBulkResults: {
            selector:
                "//*[@id='search-harvest-grid-table-con']/div/div[1]/div[1]/div",
            locateStrategy: "xpath",
        },
        KEIndividualEarResults: {
            selector:
                "//*[@id='search-harvest-grid-table-con']/div/div[1]/div[1]/div",
            locateStrategy: "xpath",
        },
        wheatSBDetailSteward: {
            selector:
                "//*[@id='study_details_id']/div[2]/div[6]/dl/dd/div[contains(text(),'Goff, Khadija')]",
            locateStrategy: "xpath",
        },
        wheatISDetailSteward: {
            selector:
                "//*[@id='study_details_id']/div[2]/div[6]/dl/dd/div[contains(text(),'Allan, Jennifer')]",
            locateStrategy: "xpath",
        },
        wheatSPSDetailSteward: {
            selector:
                "//*[@id='study_details_id']/div[2]/div[6]/dl/dd/div[contains(text(),'Ramos, Asiya')]",
            locateStrategy: "xpath",
        },
        expDetailExpType: {
            selector:
                "//*[@id='study_details_id']/div[2]/div[1]/dl/dd/div[contains(text(),'Generation Nursery')]",
            locateStrategy: "xpath",
        },
        expDetailSite: {
            selector:
                "//*[@id='study_details_id']/div[2]/div[4]/dl/dd/div[contains(text(),'Kiboko')]",
            locateStrategy: "xpath",
        },
        expDetailSteward: {
            selector:
                "//*[@id='study_details_id']/div[2]/div[6]/dl/dd/div[contains(text(),'Anderson, Colin')]",
            locateStrategy: "xpath",
        },
        maizeICNExpType: {
            selector:
                "//*[@id='study_details_id']/div[2]/div[1]/dl/dd/div[contains(text(),'Intentional Crossing Nursery')]",
            locateStrategy: "xpath",
        },
        maizeICNSite: {
            selector:
                "//*[@id='study_details_id']/div[2]/div[4]/dl/dd/div[contains(text(),'Harare')]",
            locateStrategy: "xpath",
        },
        maizeICNSteward: {
            selector:
                "//*[@id='study_details_id']/div[2]/div[6]/dl/dd/div[contains(text(),'Bantay, Jaymar')]",
            locateStrategy: "xpath",
        },
        KEIEarexpDetailSteward: {
            selector:
                "//*[@id='study_details_id']/div[2]/div[6]/dl/dd/div[contains(text(),'Reid, Alexandra')]",
            locateStrategy: "xpath",
        },
        wheatDetailExpType: {
            selector:
                "//*[@id='study_details_id']/div[2]/div[1]/dl/dd/div[contains(text(),'Generation Nursery')]",
            locateStrategy: "xpath",
        },
        wheatDetailSite: {
            selector:
                "//*[@id='study_details_id']/div[2]/div[4]/dl/dd/div[contains(text(),'El Batan')]",
            locateStrategy: "xpath",
        },
        wheatDetailSteward: {
            selector:
                "//*[@id='study_details_id']/div[2]/div[6]/dl/dd/div[contains(text(),'Carlson, Ansh')]",
            locateStrategy: "xpath",
        },
        showHideButton: "#show-hide-current-observations-btn",
        showHideModal: "#show-hide-current-observations-modal > div > div",
        A11QueryResults: {
            selector:
                "//*[@id='search-harvest-grid-table-con']/div/div[1]/div[1]/div",
            locateStrategy: "xpath",
        },
        WheatBulkQueryResults: {
            selector:
                "//*[@id='search-harvest-grid-table-con']/div/div[1]/div[1]/div",
            locateStrategy: "xpath",
        },
        emptyQueryResults: {
            selector:
                "//*[@id='search-harvest-grid-table-con-container']/div/table/tbody/tr/td/div[contains(text(),'No results found.')]",
            locateStrategy: "xpath",
        },
        verifyElement: {
            selector:
                "//*[@id='delete_confirmation_modal_additional_content']/p[contains(text(),'Cannot perform bulk delete.']",
            locateStrategy: "xpath",
        },
        plot1:
            "#search-harvest-grid-table-con-container > div > table > tbody > tr:nth-child(1) > td:nth-child(2) > label",
        plot2:
            "#search-harvest-grid-table-con-container > div > table > tbody > tr:nth-child(2) > td:nth-child(2) > label",
        plot3:
            "#search-harvest-grid-table-con-container > div > table > tbody > tr:nth-child(3) > td:nth-child(2) > label",
        plot4:
            "#search-harvest-grid-table-con-container > div > table > tbody > tr:nth-child(4) > td:nth-child(2) > label",
        plot5:
            "#search-harvest-grid-table-con-container > div > table > tbody > tr:nth-child(5) > td:nth-child(2) > label",
        bulkUpdateButton: "#add-harvest-manager-plot-btn-id",
        bulkUpdateForm: "#bulk-update-modal > div > div > div.modal-header > h4",
        checkAllPlots:
            "#search-harvest-grid-table-con-container > div > table > thead > tr:nth-child(1) > th:nth-child(2) > label",
        setBulk: {
            selector:
                "//*[@id='select2-harvest-method-select-results']/li[contains(text(),'Bulk')]",
            locateStrategy: "xpath",
        },
        setIndividualEar: {
            selector:
                "//*[@id='select2-harvest-method-select-results']/li[contains(text(),'Individual ear')]",
            locateStrategy: "xpath",
        },
        setSinglePlantSelection: {
            selector:
                "//*[@id='select2-harvest-method-select-results']/li[contains(text(),'Single Plant Selection')]",
            locateStrategy: "xpath",
        },
        setSelectedBulk: {
            selector:
                "//*[@id='select2-harvest-method-select-results']/li[contains(text(),'Selected bulk')]",
            locateStrategy: "xpath",
        },
        setIndividualSpike: {
            selector:
                "//*[@id='select2-harvest-method-select-results']/li[contains(text(),'Individual spike')]",
            locateStrategy: "xpath",
        },
        cannotDelete: {
            selector:
                "//*[@id='delete_confirmation_modal_additional_content']/p[contains(text(),'Cannot perform bulk delete.')]",
            locateStrategy: "xpath",
        },
        applyToAll: "#apply-bulk-all-btn",
        overwriteChanges: "#allow_overwrite",
        showHideData: "#show-hide-current-observations-btn",
        showHideDataModal:
            "#show-hide-current-observations-modal > div > div > div.modal-header > h4",
        selectAllVariable:
            "#s2-togall-current-observations-variable_select > span.s2-select-label",
        selectAllPlots:
            "#search-harvest-grid-table-con-container > div > table > thead > tr:nth-child(1) > th:nth-child(2) > label",
        applyObservationData: "#current-observations-done-btn",
        createBrowserEmpty:
            "#create-seedlot-grid-table-con-container > table > tbody > tr > td > div",
        variables: {
            selector:
                "//*[@id='current-observations-modal-body']/span[2]/span[1]/span/ul/li/input",
            locateStrategy: "xpath",
        },
        updateNotif: {
            selector:
                "//*[@id='toast-container]/div[contains(text(),'Harvest data was successfully updated')]",
            locateStrategy: "xpath",
        },
        resultShowHide: {
            selector:
                "//*[@id='search-harvest-grid-table-con-container']/table/tbody/tr/td/div[contains(text(),'No results found.')]",
            locateStrategy: "xpath",
        },
        plot1AssertDate:
            "#search-harvest-grid-table-con-container > div > table > tbody > tr:nth-child(1) > td:nth-child(7) > div > div > input",
        plot1AssertMethod:
            "#select2-harvest_method-HV_METH_DISC-1184610-1450846-1-1--harvest_plant-NO_OF_PLANTS-1184610-1450846-1-1--harvest_plant-PANNO_SEL-1184610-1450846-1-1--harvest_plant-SPECIFIC_PLANT-1184610-1450846-1-1--harvest_plant-numeric_var_placeholder-1184610-1450846-1-1--harvest_plant-NO_OF_EARS-1184610-1450846-1-1---container",
        dateAssert: {
            selector: "//input[@value='2021-02-11']",
            locateStrategy: "xpath",
        },
        bulkAssert: {
            selector: "//span[@title='Bulk']",
            locateStrategy: "xpath",
        },
        indivdualEarAssert: {
            selector: "//span[@title='Individual ear']",
            locateStrategy: "xpath",
        },
        singlePlantAssert: {
            selector: "//span[@title='Single Plant Selection']",
            locateStrategy: "xpath",
        },
        modifiedBulkAssert: {
            selector: "//span[@title='Selected bulk']",
            locateStrategy: "xpath",
        },
        individualSpikeAssert: {
            selector: "//span[@title='Individual spike']",
            locateStrategy: "xpath",
        },
        numOfEarsAssert: {
            selector: "//input[@placeholder='No of ears' and @value='10']",
            locateStrategy: "xpath",
        },
        numOfPlantsAssert: {
            selector: "//input[@placeholder='No of plants' and @value='10']",
            locateStrategy: "xpath",
        },
        nextStepButton: "#harvest-manager-next-step-btn",
        validateHarvestDataButton: "#harvest-manager-validate-hdata-btn",
        totalCount: {
            selector:
                "//span[@title='Ready for seed and package creation' and contains(strong, 'FOR CREATION')]",
            locateStrategy: "xpath",
        },
        totalCommitted: {
            selector:
                "//span[@title='Seeds and packages have been created' and contains(strong, 'HARVEST COMPLETE')]",
            locateStrategy: "xpath",
        },
        committedBulk: {
            selector:
                "//span[@title='Committed harvest method' and @class='badge center light-green darken-4 white-text']",
            locateStrategy: "xpath",
        },
        committedDate: {
            selector:
                "//span[@title='Committed harvest date' and @class='badge center light-green darken-4 white-text']",
            locateStrategy: "xpath",
        },
        createSeedlotsButton: "#harvest-manager-create-seedlots-btn",
        confirmSeedlotCreation: "#proceed-seedlot-creation-btn",
        successMessage: {
            selector:
                "//div[@class='toast panning' and contains(i, 'Seedlots created successfully.')]",
            locateStrategy: "xpath",
        },
        successToast: "#toast-container > div",
        forCreation: "#valid > span",
        summary:
            "#manage-seedlot-grid-table-con > div > div.kv-panel-before > div:nth-child(2) > div",
        seed1: {
            selector:
                "//*[@id='manage-seedlot-grid-table-con-container']/table/tbody/tr[1]/td[3][contains(text(),'KI19A-A-11--1')]",
            locateStrategy: "xpath",
        },
        verifyHarvestDate: {
            selector: "//input[@type='text' and contains(value,'2021-02-11')]",
            locateStrategy: "xpath",
        },
        verifyTotalBulk: {
            selector: "//span[@title='Bulk']",
            locateStrategy: "xpath",
        },
        verifySinglePlantSelection: {
            selector: "//span[@title='Single Plant Selection']",
            locateStrategy: "xpath",
        },
        verifyModifiedBulk: {
            selector: "//span[@title='Modified Bulk']",
            locateStrategy: "xpath",
        },
        verifyIndividualSpike: {
            selector: "//span[@title='Individual Spike']",
            locateStrategy: "xpath",
        },
        verifyIndividualEar: {
            selector: "//span[@title='Individual Ear']",
            locateStrategy: "xpath",
        },
        deleteModal:
            "#delete_confirmation_modal > div > div > div.modal-header > h4",
        resetGrid: "#reset-search-harvest-grid",
        resetAlt: "#reset-search-harvest-grid",
        endDelete: "#bulk-delete-modal > div > div > div.modal-header > button",
        bulkDelete: "#bulk-delete-harvest-manager-plot-btn-id",
        selectAllTraits:
            "#bulk-delete-main-body > div.panel.panel-default > div > div:nth-child(2) > ul > li:nth-child(1) > label",
        cancelDelete: "#cancel_delete",
        confirmCancel: "#bulk-delete-modal > div > div > div.modal-header > button",
        confirmDelete: "#delete_apply_changes",
        bulkDeleteProcess: "#delete-progress-textarea",
        deleteAll: "#apply-bulk-delete-all-btn",
        nextPage:
            "#search-harvest-grid-table-con > div > div.panel-footer > div.kv-panel-pager > ul > li:nth-child(3) > a",
        noOfEars: "#no-of-ears-input",
        deleteConfirmationModal:
            "#delete_confirmation_modal_body > p > br:nth-child(4)",
        cannotPerformDelete:
            "#delete_confirmation_modal_additional_content > p > b:nth-child(4)",
        showHideInputFiled: {
            selector:
                "//*[@id='current-observations-modal-body']/span[2]/span[1]/span/ul/li/input",
            locateStrategy: "xpath",
        },
        showHideDropdown: {
            selector:
                "//*[@id='select2-current-observations-variable_select-results']",
            locateStrategy: "xpath",
        },
        showHideNumericVariable: {
            selector:
                "//*[@id='select2-current-observations-variable_select-results']/li[3]",
            locateStrategy: "xpath",
        },
        showHideHarvestDate: {
            selector:
                "//*[@id='select2-current-observations-variable_select-results']/li[1]",
            locateStrategy: "xpath",
        },
        showHideHarvestMethod: {
            selector:
                "//*[@id='select2-current-observations-variable_select-results']/li[2]",
            locateStrategy: "xpath",
        },
        showHideAll: {
            selector: "//*[@id='s2-togall-current-observations-variable_select']",
            locateStrategy: "xpath",
        },
        showHideApply: "#current-observations-done-btn",
        showHideToggle: {
            selector: "//*[@id='current-observations-modal-body']/div[2]/label/span",
            locateStrategy: "xpath",
        },
        showHideSummary1: {
            selector:
                "//*[@id='harvest-data-filter-div']/dl/dt[contains(text(), 'Showing plots WITHOUT :')]",
            locateStrategy: "xpath",
        },
        showHideSummary2: {
            selector:
                "//*[@id='harvest-data-filter-div']/dl/dt[contains(text(), 'Showing plots WITH :')]",
            locateStrategy: "xpath",
        },
        showHideSummaryHarvestDate: "#harvestDate",
        showHideSummaryHarvestMethod: "#harvestMethod",
        showHideSummaryNumVariable: "#numericVar",
    },
    sections: {
        loadExperiment: {
            selector: "//i[normalize-space(.)='search']/ancestor::li[@id='filters']",
            locateStrategy: "xpath",
            elements: {
                programSelect: {
                    selector: ".//*[@id='program-filter']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                experimentSelect: {
                    selector: ".//*[@id='experiment_name-filter']/following-sibling::span//input",
                    locateStrategy: "xpath",
                },
                locationCodeSelect: {
                    selector: ".//*[@id='location_name-filter']/following-sibling::span//input",
                    locateStrategy: "xpath",
                }
            }
        },

        verifyExperimentDetails: {
            selector: "//*[@id='experiment_detail_id']",
            locateStrategy: "xpath",
            elements: {
                expTypeAssert: {
                    selector: "//*[@id='experimentType']",
                    locateStrategy: "xpath",
                },
                siteAssert: {
                    selector: "//*[@id='site']",
                    locateStrategy: "xpath",
                },
                stewardAssert: {
                    selector: "//*[@id='steward']",
                    locateStrategy: "xpath",
                }
            }
        },

    },
    commands: [
        {
            //set program search parameter values
            programSetValue: async function (input, value) {
                await this.waitForElementVisible("#favorite-a-tool-icon-seedInventoryharvest-manager");
                //set program   
                await this.click("#select2-program-filter-container");
                await this.waitForElementVisible("#select2-program-filter-results");
                const programInput = {
                    selector: "/html/body/span[2]/span/span[1]/input",
                    locateStrategy: "xpath",
                };
                await this.click(programInput);
                await this.pause(2000);
                await client.setValue(programInput, value);
                await this.pause(1000);
                await client.keys(client.Keys.ENTER);
            },
            //set experiment search parameter values
            experimentSetValue: async function (input, value) {
                await this.waitForElementVisible("#favorite-a-tool-icon-seedInventoryharvest-manager");
                //set experiment
                await this.click("#select2-experiment_name-filter-container");
                await this.waitForElementVisible("#select2-experiment_name-filter-results");
                const experimentInput = {
                    selector: "/html/body/span[2]/span/span[1]/input",
                    locateStrategy: "xpath",
                };
                await this.click(experimentInput);
                await this.pause(2000);
                await client.setValue(experimentInput, value);
                await this.pause(1000);
                await client.keys(client.Keys.ENTER);

            },
            //set experiment location code search parameter values
            locationCodeSetValue: async function (input, value) {
                await this.waitForElementVisible("#favorite-a-tool-icon-seedInventoryharvest-manager");
                //set experiment
                await this.click("#select2-location_name-filter-container");
                await this.waitForElementVisible("#select2-location_name-filter-results");
                const locationCodeInput = {
                    selector: "/html/body/span[2]/span/span[1]/input",
                    locateStrategy: "xpath",
                };
                await this.click(locationCodeInput);
                await this.pause(2000);
                await client.setValue(locationCodeInput, value);
                await this.pause(1000);
                await client.keys(client.Keys.ENTER);



            },
            //assert experiment type displayed in the experiment details
            expTypeAssertValue: async function (input, value) {
                const expLocator = {
                    selector: `//*[@id="study_details_id"]/div[2]/div[1]/dl/dd/div[contains(text(),'${value}')]`,
                    locateStrategy: "xpath",
                };
                await this.waitForElementVisible(expLocator);
            },
            //assert site displayed in the experiment details
            siteAssertValue: async function (input, value) {
                const siteLocator = {
                    selector: `//*[@id="site"][contains(text(),'${value}')]`,
                    locateStrategy: "xpath",
                };
                await this.waitForElementVisible(siteLocator);
            },
            //assert steward displayed in the experiment details
            stewardAssertValue: async function (input, value) {
                const stewardLocator = {
                    selector: `//*[@id="steward"][contains(text(),'${value}')]`,
                    locateStrategy: "xpath",
                };
                await this.waitForElementVisible(stewardLocator);
            },
        }
    ],
};