Feature: Create a new analysis request for Multiple Occurrence SESL
    Background: Background name: User logs in to BA
        Given user logs in to BA systems

    Scenario: Create new Analysis Request
        When the user click the new request button
        And the user selects multiple occurrence in a specific experiment
        And the user fills up all fields in settings
        And the user fills up all fields in parameters
        And the user clicks confirm
        Then the request is created
    
    #atomic tests
    #@smokeBA
    Scenario: Create a new request
        When the user clicks the Add New Request button
        Then the user is redirected to the Add New Request page
        And experiment occurrences for analysis are displayed

    @smokeBA
    Scenario: Select an experiment ready for analysis
        And the user is on the Add New Request page
        When the user searches for an experiment with an occurrence ready for analysis
             | Experiment |
             | AF2021_001 |
        Then the occurrences are displayed

    # Scenario: Select an experiment (all occurrences are selected)
    #     When the user is on the Add New Request page
    #     When the user searches for an experiment with an occurrence ready for analysis
    #         | Experiment |
    #         | AF2021_001 |
    #     And the user tick the experiment
    #     Then all the occurrences ready for analysis within the experiment are automatically tick
    #     And the "NEXT" button is enable
        
    # Scenario: Deselect the experiment
    #     When the user is on the Add New Request page
    #     And the user selected the experiment with an occurrence ready for analysis
    #         | Experiment |
    #         | AF2021_001 |
    #     And all the occurrences ready for analysis within the experiment are automatically tick
    #     When the user deselect the experiment
    #     Then all the occurrences are deselected 
    #     And the "NEXT" button is disable

    @smokeBA
    Scenario: ARM-REQUEST-CREATION-003 Select all occurrences
        And the user is on the Add New Request page
        When the user searches for an experiment with an occurrence ready for analysis
            | Experiment |
            | AF2021_001 |
        And the user selects all occurrences
        And the user goes to the Settings tab
        Then there are two available traits shown in the Response variables
            | Trait 1 | Trait 2   |
            | MC_CONT | AYLD_CONT |
        And "Prediction" is auto-selected as the Analyis objective

    # Scenario Outline: Specify the settings (traits and analysis objective)
    #     When the user is in the Settings tab
    #     When the user selects the <Traits>
    #     And the user sets the Analysis Objective
    #         | Analysis Objective |
    #         | Prediction         |
    #     And the user goes to the next step
    #     Then the Parameters tab is displayed

    #     Examples:
    #         | Traits    |
    #         | all       |
    #         | MC_CONT   |
    #         | AYLD_CONT |

    # Scenario: Multiple Occurrence - option to run SESL or SEML model
    #     When the user is in the Parameters Tab
    #     When the user selects the the Experiment Location Analysis Pattern
    #     Then there are two available options shown
    #         | Option 1                              | Option 2                             |
    #         | Single Experiment - Multiple Location | Single Experiment - Single Location  |

    # Scenario Outline: Set the Experiment Location Analysis Pattern (SESL)
    #     When the user is in the Parameters Tab
    #     And the "Univariate" is auto-selected as Trait Analysis Pattern
    #     When the user selects the Experiment Location Analysis Pattern
    #         | ExpLoc Analysis Pattern             |
    #         | Sinlge Experiment - Single Location |
    #     Then <ANALYSIS CONFIGURATION> in Analysis Configuration is auto-selected
    #     And <MAIN MODEL> in Main Model is auto-selected
        
    #     Examples:
    #         | ANALYSIS CONFIGURATION   | MAIN MODEL                                                        |
    #         | RCBD Univariate          | RCBD SESL Univariate entry as random                              |
    #         | Alpha-Lattice univariate | Alpha-Lattice single location, univariate, entry as random factor |

    # Scenario: Options for the Spatial Adjusting
    #     When the user is in the Parameters Tab
    #     And the user selected the the Experiment Location Analysis Pattern
    #         | ExpLoc Analysis Pattern |
    #         | SESL                    |
    #     When the user selects the Spatial Adjusting
    #     Then there are four possible choices shown
    #         | SA 1              | SA 2             | SA 3             | SA 4                  |
    #         | (AR1row x AR1col) | (IDrow x AR1col) | (AR1row x IDcol) | No spatial adjustment |

    # Scenario Outline: Select the Spatial Adjusting
    #     When the user is in the Parameters Tab
    #     And the user selected the Experiment Location Analysis Pattern
    #         | ExpLoc Analysis Pattern |
    #         | SESL                    |
    #     When the user selects the <Spatial Adjusting>
    #     Then the Submit Analysis Request button is enabled

    #     Examples:
    #         | Spatial Adjusting     |
    #         | (AR1row x AR1col)     |
    #         | (IDrow x AR1col)      |
    #         | (AR1row x IDcol)      |
    #         | No spatial adjustment |

    # Scenario: Submission of request
    #     When the user is in the Parameters tab
    #     And the user has specified all the parameters
    #     When the user submits the analysis request
    #     Then the user lands on the job list page
    #     And the new request is displayed with "Pending" status