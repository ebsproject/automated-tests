@SA_MenuNavigation
Feature: SQA-521 Tenant Portal Login Scenario
    The user should be able to see Printout Server in the menu options

    Background: Login User
        Given the user is in Dashboard EBS page
        And the user selects System Administrator instance

    @SA_MenuNavigation1
    Scenario: SA-MENU-NAVIGATION-001 Select Tenant Management menu
        Given the user is in System Administrator page
        When the user clicks on "Tenant Management" SA menu
        Then the user will be able to view the "Tenant Management" SA page

    @SA_MenuNavigation2
    Scenario: SA-MENU-NAVIGATION-002 Select User Management menu
        Given the user is in System Administrator page
        When the user clicks on "User Management" SA menu
        Then the user will be able to view the "User Management" SA page

    @SA_MenuNavigation3
    Scenario: SA-MENU-NAVIGATION-003 Select CRM menu
        Given the user is in System Administrator page
        When the user clicks on "CRM" SA menu
        Then the user will be able to view the "CRM" SA page

    @SA_MenuNavigation4
    Scenario: SA-MENU-NAVIGATION-004 Select Printout menu
        Given the user is in System Administrator page
        When the user clicks on "Printout" SA menu
        Then the user will be able to view the "Printout" SA page

    @SA_MenuNavigation5
    Scenario: SA-MENU-NAVIGATION-005 Select Shipment Manager menu
        Given the user is in System Administrator page
        When the user clicks on "Shipment Manager" SA menu
        Then the user will be able to view the "Shipment Manager" SA page