@SM_DesignManager
Feature: SQA-973 Service Management - Design Manager
    The user should be able to navigate the Service Management - Design Manager

    Background: Login User
        Given the user is in EBS Dashboard page
        And the user selects Service Management instance
        And the user clicks on "Design" SM menu


    #--------------------------------------#
    #   DESIGN MANAGER COLUMN FILTERING    #
    #--------------------------------------#

    @SM_DesignManager_Filter1
    Scenario: SM-DESIGN-MANAGER-001 User filter the column BATCH NAME from the browser
        Given user can see Design Manager data browser
        When user set column filters of the Design Manager data browser
            | BATCH NAME  |
            | BGWPGDivers |
        Then matching results will show in the Design Manager data browser

    @SM_DesignManager_Filter2
    Scenario: SM-DESIGN-MANAGER-002 User filter the column DESCRIPTION from the browser
        Given user can see Design Manager data browser
        When user set column filters of the Design Manager data browser
            | DESCRIPTION  |
            | create batch |
        Then matching results will show in the Design Manager data browser

    @SM_DesignManager_Filter3
    Scenario: SM-DESIGN-MANAGER-003 User filter the column TOTAL from the browser
        Given user can see Design Manager data browser
        When user set column filters of the Design Manager data browser
            | TOTAL |
            | 2     |
        Then matching results will show in the Design Manager data browser

    @SM_DesignManager_Filter4
    Scenario: SM-DESIGN-MANAGER-004 User filter the column TOTAL # PLATES from the browser
        Given user can see Design Manager data browser
        When user set column filters of the Design Manager data browser
            | Total # Plates |
            | 1              |
        Then matching results will show in the Design Manager data browser

    @SM_DesignManager_Filter5
    Scenario: SM-DESIGN-MANAGER-005 User filter the column FIRST PLATE NAME from the browser
        Given user can see Design Manager data browser
        When user set column filters of the Design Manager data browser
            | FIRST PLATE NAME |
            | BGWPGDivers      |
        Then matching results will show in the Design Manager data browser

    @SM_DesignManager_Filter6
    Scenario: SM-DESIGN-MANAGER-006 User filter the column LAST PLATE NAME from the browser
        Given user can see Design Manager data browser
        When user set column filters of the Design Manager data browser
            | LAST PLATE NAME |
            | BGWPGDivers     |
        Then matching results will show in the Design Manager data browser

    @SM_DesignManager_Filter7
    Scenario: SM-DESIGN-MANAGER-007 User filter the column STATUS UPDATE (DATE) from the browser
        Given user can see Design Manager data browser
        When user set column filters of the Design Manager data browser
            | STATUS UPDATE (DATE) |
            | 2021                 |
        Then matching results will show in the Design Manager data browser

    @SM_DesignManager_Filter8
    Scenario: SM-DESIGN-MANAGER-008 User filter the column STATUS UPDATE BY (NAME) from the browser
        Given user can see Design Manager data browser
        When user set column filters of the Design Manager data browser
            | STATUS UPDATE BY (NAME) |
            | BGWPGDivers             |
        Then matching results will show in the Design Manager data browser


    #--------------------#
    #   ACTION BUTTONS   #
    #--------------------#

    @SM_DesignManager_Create_Plate
    Scenario: SM-DESIGN-MANAGER-010 Create Plate
        Given user can see Design Manager data browser
        When the user clicks on the Design Manager Create Plate
        And the user provides the plate information
        When the user clicks Plate Preview button
        And the user clicks the Generate Plate button
        Then the user should see the generated design plate in the Design Manager browser

    @SM_DesignManager_Download
    Scenario: SM-DESIGN-MANAGER-013 Download Plates
        Given user can see Design Manager data browser
        When the user clicks on the Design Manager download button
        Then the user must be able to download the Design Plates in XLSX file