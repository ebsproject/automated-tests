@baARM  
Feature: Create a new analysis request for Single Occurrence SESL
    The user should be able to create a new analysis request for a When experiment
    Background: User logs in to BA
        Given user logs in to BA systems
    #atomic tests
    
    # Scenario: Create a new request
    #     When the user is in the Analysis Request Manager
    #     When the user clicks the Add New Request button
    #     Then the user is redirected to the Add New Request page
    #     And experiment occurrences for analysis are displayed

    # Scenario: Select an experiment ready for analysis
    #     When the user is on the Add New Request page
    #     When the user searches for an experiment with an occurrence ready for analysis
    #         | Experiment |
    #         | AF2021_001 |
    #     Then the experiments are displayed
    @baARM_smoke_run
    Scenario:BA-SESL-SINGLE-OCCURRENCE-001 Create new Analysis Request
        When the user click the new request button
        And selects a single occurrence in a specific experiment
        And the user fills up response variables field 
            |    Response Variables   |
            |          MC_CONT        |
        #The next statements will be implemented soon
        # And the user fills up spatial adjusting field in parameters
        #    |           Spatial Adjusting           |
        #    |            AR1row X AR1col            |
        #And the user clicks confirm
        #Then the request is created

    Scenario:BA-SESL-SINGLE-OCCURRENCE-002 Create new Analysis Request
        When the user click the new request button
        And selects a single occurrence in a specific experiment
        And the user fills up response variables field 
            |    Response Variables   |
            |          MC_CONT        |
        And the user fills up spatial adjusting field in parameters 
            |           Spatial Adjusting           |
            |             IDrow X AR1col            |
        And the user clicks confirm
        Then the request is created

    Scenario:BA-SESL-SINGLE-OCCURRENCE-003 Create new Analysis Request
        When the user click the new request button
        And selects a single occurrence in a specific experiment
        And the user fills up response variables field 
            |    Response Variables   |
            |          MC_CONT        |
        And the user fills up spatial adjusting field in parameters 
            |           Spatial Adjusting           |
            |             AR1row X IDcol            |
        And the user clicks confirm
        Then the request is created
    Scenario:BA-SESL-SINGLE-OCCURRENCE-004 Create new Analysis Request
        When the user click the new request button
        And selects a single occurrence in a specific experiment
        And the user fills up response variables field 
            |    Response Variables   |
            |          MC_CONT        |
        And the user fills up spatial adjusting field in parameters 
            |           Spatial Adjusting           |
            |          No Spatial Adjustment        |
        And the user clicks confirm
        Then the request is created
    Scenario:BA-SESL-SINGLE-OCCURRENCE-005 Create new Analysis Request
        When the user click the new request button
        And selects a single occurrence in a specific experiment
        And the user fills up response variables field 
            |    Response Variables   |
            |         AYLD_CONT       |
        And the user fills up spatial adjusting field in parameters 
            |           Spatial Adjusting           |
            |            AR1row X AR1col            |
        And the user clicks confirm
        Then the request is created
    Scenario:BA-SESL-SINGLE-OCCURRENCE-006 Create new Analysis Request
        When the user click the new request button
        And selects a single occurrence in a specific experiment
        And the user fills up response variables field 
            |    Response Variables   |
            |         AYLD_CONT       |
        And the user fills up spatial adjusting field in parameters 
            |           Spatial Adjusting           |
            |             IDrow X AR1col            |
        And the user clicks confirm
        Then the request is created
    Scenario:BA-SESL-SINGLE-OCCURRENCE-007 Create new Analysis Request
        When the user click the new request button
        And selects a single occurrence in a specific experiment
        And the user fills up response variables field 
            |    Response Variables   |
            |         AYLD_CONT       |
        And the user fills up spatial adjusting field in parameters 
            |           Spatial Adjusting           |
            |             AR1row X IDcol            |
        And the user clicks confirm
        Then the request is created
    Scenario:BA-SESL-SINGLE-OCCURRENCE-008 Create new Analysis Request
        When the user click the new request button
        And selects a single occurrence in a specific experiment
        And the user fills up response variables field 
            |    Response Variables   |
            |         AYLD_CONT       |
        And the user fills up spatial adjusting field in parameters 
            |           Spatial Adjusting           |
            |          No Spatial Adjustment        |
        And the user clicks confirm
        Then the request is created
    # Scenario: Select an experiment (all occurrences are selected)
    #     When the user is on the Add New Request page
    #     When the user searches for an experiment with an occurrence ready for analysis
    #         | Experiment |
    #         | AF2021_001 |
    #     And the user tick the experiment
    #     Then all the occurrences ready for analysis within the experiment are automatically tick
    #     And the "NEXT" button is enable
        
    # Scenario: Deselect the experiment
    #     When the user is on the Add New Request page
    #     And the user selected the experiment with an occurrence ready for analysis
    #         | Experiment |
    #         | AF2021_001 |
    #     And all the occurrences ready for analysis within the experiment are automatically tick
    #     When the user deselect the experiment
    #     Then all the occurrences are deselected 
    #     And the "NEXT" button is disable
