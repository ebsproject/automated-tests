@SM_ShipmentTool
Feature: SQA-954 Service Management - Shipment Tool
    The user should be able to navigate the Service Management - Shipment Tool

    Background: Login User
        Given the user is in EBS Dashboard page
        And the user selects Service Management instance
        And the user clicks on "Shipment Tool" SM menu


    #------------------------------------#
    #   SHIPMENT TOOL COLUMN FILTERING   #
    #------------------------------------#

    @SM_ShipmentTool_Filter1
    Scenario: SM_SHIPMENT-TOOL-001 User filter the column STATUS from the browser
        Given user can see Shipment Tool data browser
        When user set column filters of the Shipment Tool data browser
            | STATUS  |
            | Created |
        Then matching results will show in the Shipment Tool data browser

    @SM_ShipmentTool_Filter2
    Scenario: SM_SHIPMENT-TOOL-002 User filter the column SENDER PROGRAM from the browser
        Given user can see Shipment Tool data browser
        When user set column filters of the Shipment Tool data browser
            | SENDER PROGRAM |
            | IRSEA          |
        Then matching results will show in the Shipment Tool data browser

    @SM_ShipmentTool_Filter3
    Scenario: SM_SHIPMENT-TOOL-003 User filter the column SHIPMENT NAME from the browser
        Given user can see Shipment Tool data browser
        When user set column filters of the Shipment Tool data browser
            | SHIPMENT NAME |
            #TODO: Update this value:
            | Test          |
        Then matching results will show in the Shipment Tool data browser

    @SM_ShipmentTool_Filter4
    Scenario: SM_SHIPMENT-TOOL-004 User filter the column SHIPMENT CODE from the browser
        Given user can see Shipment Tool data browser
        When user set column filters of the Shipment Tool data browser
            | SHIPMENT CODE |
            # TODO: Update this value:
            | No Data       |
        Then matching results will show in the Shipment Tool data browser

    @SM_ShipmentTool_Filter5
    Scenario: SM_SHIPMENT-TOOL-005 User filter the column SHIPMENT TYPE from the browser
        Given user can see Shipment Tool data browser
        When user set column filters of the Shipment Tool data browser
            | SHIPMENT TYPE |
            | within CG     |
        Then matching results will show in the Shipment Tool data browser

    @SM_ShipmentTool_Filter6
    Scenario: SM_SHIPMENT-TOOL-006 User filter the column SHIPMENT PURPOSE from the browser
        Given user can see Shipment Tool data browser
        When user set column filters of the Shipment Tool data browser
            | SHIPMENT PURPOSE |
            #TODO: Update this value:
            | Experiment       | 
        Then matching results will show in the Shipment Tool data browser

    @SM_ShipmentTool_Filter7
    Scenario: SM_SHIPMENT-TOOL-007 User filter the column TOTA ITEMS from the browser
        Given user can see Shipment Tool data browser
        When user set column filters of the Shipment Tool data browser
            | TOTAL ITEMS |
            | 0           |
        Then matching results will show in the Shipment Tool data browser

    @SM_ShipmentTool_Filter8
    Scenario: SM_SHIPMENT-TOOL-008 User filter the column REQUESTOR NAME from the browser
        Given user can see Shipment Tool data browser
        When user set column filters of the Shipment Tool data browser
            | REQUESTOR NAME |
            #TODO: Update this value:
            | Test           |
        Then matching results will show in the Shipment Tool data browser

    @SM_ShipmentTool_Filter9
    Scenario: SM_SHIPMENT-TOOL-009 User filter the column REQUESTOR EMAIL from the browser
        Given user can see Shipment Tool data browser
        When user set column filters of the Shipment Tool data browser
            | REQUESTOR EMAIL |
            #TODO: Update this value:
            | irri.org        |
        Then matching results will show in the Shipment Tool data browser

    @SM_ShipmentTool_Filter10
    Scenario: SM_SHIPMENT-TOOL-010 User filter the column REQUESTOR ADDRESS from the browser
        Given user can see Shipment Tool data browser
        When user set column filters of the Shipment Tool data browser
            | REQUESTOR ADDRESS |
            #TODO: Update this value:
            | Test              |
        Then matching results will show in the Shipment Tool data browser

    @SM_ShipmentTool_Filter11
    Scenario: SM_SHIPMENT-TOOL-011 User filter the column REQUESTOR INSTITUTION from the browser
        Given user can see Shipment Tool data browser
        When user set column filters of the Shipment Tool data browser
            | REQUESTOR INSTITUTION |
            | CIMMYT                |
        Then matching results will show in the Shipment Tool data browser

    @SM_ShipmentTool_Filter12
    Scenario: SM_SHIPMENT-TOOL-012 User filter the column REQUESTOR COUNTRY from the browser
        Given user can see Shipment Tool data browser
        When user set column filters of the Shipment Tool data browser
            | REQUESTOR COUNTRY |
            | Philippines       |
        Then matching results will show in the Shipment Tool data browser

    @SM_ShipmentTool_Filter13
    Scenario: SM_SHIPMENT-TOOL-013 User filter the column SENDER NAME from the browser
        Given user can see Shipment Tool data browser
        When user set column filters of the Shipment Tool data browser
            | SENDER NAME |
            #TODO: Update this value:
            | Test        |
        Then matching results will show in the Shipment Tool data browser

    @SM_ShipmentTool_Filter14
    Scenario: SM_SHIPMENT-TOOL-014 User filter the column SENDER INSTITUTION from the browser
        Given user can see Shipment Tool data browser
        When user set column filters of the Shipment Tool data browser
            | SENDER INSTITUTION |
            | IRRI               |
        Then matching results will show in the Shipment Tool data browser

    @SM_ShipmentTool_Filter15
    Scenario: SM_SHIPMENT-TOOL-015 User filter the column SENDER COUNTRY from the browser
        Given user can see Shipment Tool data browser
        When user set column filters of the Shipment Tool data browser
            | SENDER COUNTRY |
            | Philippines    |
        Then matching results will show in the Shipment Tool data browser

    @SM_ShipmentTool_Filter16
    Scenario: SM_SHIPMENT-TOOL-016 User filter the column RECEPIENT NAME from the browser
        Given user can see Shipment Tool data browser
        When user set column filters of the Shipment Tool data browser
            | RECEPIENT NAME |
            #TODO: Update this value:
            | Test           |
        Then matching results will show in the Shipment Tool data browser

    @SM_ShipmentTool_Filter17
    Scenario: SM_SHIPMENT-TOOL-017 User filter the column RECEPIENT EMAIL from the browser
        Given user can see Shipment Tool data browser
        When user set column filters of the Shipment Tool data browser
            | RECEPIENT EMAIL |
            #TODO: Update this value:
            | irri.org        |
        Then matching results will show in the Shipment Tool data browser

    @SM_ShipmentTool_Filter18
    Scenario: SM_SHIPMENT-TOOL-018 User filter the column RECEPIENT INSTITUTION from the browser
        Given user can see Shipment Tool data browser
        When user set column filters of the Shipment Tool data browser
            | RECEPIENT INSTITUTION |
            | IRRI                  |
        Then matching results will show in the Shipment Tool data browser

    @SM_ShipmentTool_Filter19
    Scenario: SM_SHIPMENT-TOOL-019 User filter the column RECEPIENT COUNTRY from the browser
        Given user can see Shipment Tool data browser
        When user set column filters of the Shipment Tool data browser
            | RECEPIENT COUNTRY |
            | Philippines       |
        Then matching results will show in the Shipment Tool data browser

    #--------------------#
    #   ACTION BUTTONS   #
    #--------------------#

    @SM_ShipmentTool_Notes_Shipment
    Scenario: SM_SHIPMENT-TOOL-020 Add a note for the shipment
        Given user can see Shipment Tool data browser
        When the user clicks the Notes button 
        And the user provides a note about the Shipment
        Then the user must be able to save the notes

    @SM_ShipmentTool_View_Shipment
    Scenario: SM_SHIPMENT-TOOL-021 View Shipment information
        Given user can see Shipment Tool data browser
        When the user clicks the View Shipment button
        Then the user must be able to view the selected Shipment information

    @SM_ShipmentTool_Modify_Shipment
    Scenario: SM_SHIPMENT-TOOL-022 Modify Shipment information
        Given user can see Shipment Tool data browser
        When the user clicks the Modify Shipment button 
        And the user modify the information about the Shipment
        Then the user must be able to save the modified Shipment information

    @SM_ShipmentTool_Search_Shipment
    Scenario: SM_SHIPMENT-TOOL-023 General Search
        Given user can see Shipment Tool data browser
        When the user clicks the search box and provides information about the Search
        And the user clicks the search button
        Then the user must be able to view the search results in the data browser

    @SM_ShipmentTool_Create_Shipment
    Scenario: SM_SHIPMENT-TOOL-024 Create new shipment
        Given user can see Shipment Tool data browser
        When the user clicks on the New button
        And the user provides the basic information of the Shipment Tool service
        And the user clicks the Save button
        Then the user must be able to view the newly created Shipment request in the data browser

    @SM_ShipmentTool_Download_Shipment
    Scenario: SM_SHIPMENT-TOOL-025 Download CSV
        Given user can see Shipment Tool data browser
        When the user clicks on the Download CSV button
        Then the user must be able to download the Batch list in CSV file

    @SM_ShipmentTool_Personalize_Grid
    Scenario: SM_SHIPMENT-TOOL-026 Personalize Grid Settings
        Given user can see Shipment Tool data browser
        When the user clicks on the Personalize grid settings button
        And the user customize the displayed columns
        Then the user must be able to view the personalized Shipment tool grid