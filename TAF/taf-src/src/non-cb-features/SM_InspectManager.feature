@SM_InspectManager
Feature: SQA-972 Service Management - Inspect Manager
    The user should be able to navigate the Service Management - Inspect Manager

    Background: Login User
        Given the user is in EBS Dashboard page
        And the user selects Service Management instance
        And the user clicks on "Inspect" SM menu


    #--------------------------------------#
    #   INSPECT MANAGER COLUMN FILTERING   #
    #--------------------------------------#

    @SM_InspectManager_Filter1
    Scenario: SM-INSPECT-MANAGER-001 User filter the column BATCH NAME from the browser
        Given user can see Inspect Manager data browser
        When user set column filters of the Inspect Manager data browser
            | BATCH NAME  |
            | BGWPGDivers |
        Then matching results will show in the Inspect Manager data browser

    # @SM_InspectManager_Filter2
    # Scenario: SM-INSPECT-MANAGER-002 User filter the column DESCRIPTION from the browser
    #     Given user can see Inspect Manager data browser
    #     When user set column filters of the Inspect Manager data browser
    #         | DESCRIPTION |
    #         # TODO: Update this value:
    #         | No Data     |
    #     Then matching results will show in the Inspect Manager data browser

    @SM_InspectManager_Filter3
    Scenario: SM-INSPECT-MANAGER-003 User filter the column TOTAL from the browser
        Given user can see Inspect Manager data browser
        When user set column filters of the Inspect Manager data browser
            | TOTAL |
            | 2     |
        Then matching results will show in the Inspect Manager data browser

    #--------------------#
    #   ACTION BUTTONS   #
    #--------------------#

    @SM_InspectManager_Approve
    Scenario: SM-INSPECT-MANAGER-004 Approve Batch
        Given user can see Inspect Manager data browser
        When the user clicks on the Inspect Manager approve button
        Then the user sees that the Inspect Manager status is Approved

    @SM_InspectManager_Reject
    Scenario: SM-INSPECT-MANAGER-005 Reject Batch
        Given user can see Inspect Manager data browser
        When the user clicks on the Inspect Manager reject button
        Then the user sees that the Inspect Manager status is Rejected

    @SM_InspectManager_Download
    Scenario: SM-INSPECT-MANAGER-006 Download Batch
        Given user can see Inspect Manager data browser
        When the user clicks on the Inspect Manager download button
        Then the user must be able to download the Batch list in CSV file

    # @SM_InspectManager_Create
    # Scenario: SM-INSPECT-MANAGER-007 Create Batch
    #     Given user can see Inspect Manager data browser
    #     And the user clicks on the Inspect Manager CREATE BATCH button
    #     And the user provides the basic information of the Inspect Manager service
    #     And the user provides the services information of the Inspect Manager
    #     And the user review the Inspect Manager information before submitting the request
    #     Then the user clicks the Inspect Manager Submit