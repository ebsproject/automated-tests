@SM_MenuNavigation
Feature: SQA-922 Service Management Menu Options Navigation
    The user should be able to navigate the Service Management menu options

    Background: Login User
        Given the user is in EBS Dashboard page
        And the user selects Service Management instance

    @SM_MenuNavigation1
    Scenario: SM-MENU-NAVIGATION-001 Select Request Manager menu
        Given the user is in Service Management page
        When the user clicks on "Request Manager" SM menu
        Then the user will be able to view the "Request Manager" SM page

    @SM_MenuNavigation2
    Scenario: SM-MENU-NAVIGATION-002 Select Genotyping Service Manager menu
        Given the user is in Service Management page
        When the user clicks on "Genotyping Service Manager" SM menu
        Then the user will be able to view the "Genotyping Service Manager" SM page

    @SM_MenuNavigation3
    Scenario: SM-MENU-NAVIGATION-003 Select Inspect menu
        Given the user is in Service Management page
        When the user clicks on "Inspect" SM menu
        Then the user will be able to view the "Inspect" SM page

    @SM_MenuNavigation4
    Scenario: SM-MENU-NAVIGATION-004 Select Design menu
        Given the user is in Service Management page
        When the user clicks on "Design" SM menu
        Then the user will be able to view the "Design" SM page

    @SM_MenuNavigation5
    Scenario: SM-MENU-NAVIGATION-005 Select Vendor menu
        Given the user is in Service Management page
        When the user clicks on "Vendor" SM menu
        Then the user will be able to view the "Vendor" SM page

    @SM_MenuNavigation6
    Scenario: SM-MENU-NAVIGATION-006 Select Service Catalog menu
        Given the user is in Service Management page
        When the user clicks on "Service Catalog" SM menu
        Then the user will be able to view the "Service Catalog" SM page

    @SM_MenuNavigation7
    Scenario: SM-MENU-NAVIGATION-007 Select Shipment Tool menu
        Given the user is in Service Management page
        When the user clicks on "Shipment Tool" SM menu
        Then the user will be able to view the "Shipment Tool" SM page