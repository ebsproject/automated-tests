@SM_GenotypingServiceManager
Feature: SQA-971 Service Management - Genotyping Service Manager
    The user should be able to navigate the Service Management - Genotyping Service Manager

    Background: Login User
        Given the user is in EBS Dashboard page
        And the user selects Service Management instance
        And the user clicks on "Genotyping Service Manager" SM menu


    #-------------------------------------------------#
    #   GENOTYPING SERVICE MANAGER COLUMN FILTERING   #
    #-------------------------------------------------#

    @SM_GenotypingServiceManager_Filter1
    Scenario: SM-GENOTYPING-SERVICE-MANAGER-001 User filter the column STATUS from the browser
        Given user can see Genotyping Service Manager data browser
        When user set column filters of the Genotyping Service Manager data browser
            | STATUS |
            | New    |
        Then matching results will show in the Genotyping Service Manager data browser

    @SM_GenotypingServiceManager_Filter2
    Scenario: SM-GENOTYPING-SERVICE-MANAGER-002 User filter the column REQUEST from the browser
        Given user can see Genotyping Service Manager data browser
        When user set column filters of the Genotyping Service Manager data browser
            | REQUEST |
            | RLB     |
        Then matching results will show in the Genotyping Service Manager data browser

    # @SM_GenotypingServiceManager_Filter3
    # Scenario: SM-GENOTYPING-SERVICE-MANAGER-003 User filter the column REQUESTER from the browser
    #     Given user can see Genotyping Service Manager data browser
    #     When user set column filters of the Genotyping Service Manager data browser
    #         | REQUESTER |
    #         # TODO: Update this value:
    #         | No Data   |
    #     Then matching results will show in the Genotyping Service Manager data browser

    @SM_GenotypingServiceManager_Filter4
    Scenario: SM-GENOTYPING-SERVICE-MANAGER-004 User filter the column CROP from the browser
        Given user can see Genotyping Service Manager data browser
        When user set column filters of the Genotyping Service Manager data browser
            | CROP  |
            | Wheat |
        Then matching results will show in the Genotyping Service Manager data browser

    @SM_GenotypingServiceManager_Filter5
    Scenario: SM-GENOTYPING-SERVICE-MANAGER-005 User filter the column PROGRAM from the browser
        Given user can see Genotyping Service Manager data browser
        When user set column filters of the Genotyping Service Manager data browser
            | PROGRAM              |
            | Global Maize Program |
        Then matching results will show in the Genotyping Service Manager data browser

    # @SM_GenotypingServiceManager_Filter6
    # Scenario: SM-GENOTYPING-SERVICE-MANAGER-006 User filter the column ICC from the browser
    #     Given user can see Genotyping Service Manager data browser
    #     When user set column filters of the Genotyping Service Manager data browser
    #         | ICC     |
    #         # TODO: Update this value:
    #         | No Data |
    #     Then matching results will show in the Genotyping Service Manager data browser

    @SM_GenotypingServiceManager_Filter7
    Scenario: SM-GENOTYPING-SERVICE-MANAGER-007 User filter the column PURPOSE from the browser
        Given user can see Genotyping Service Manager data browser
        When user set column filters of the Genotyping Service Manager data browser
            | PURPOSE         |
            | Quality Control |
        Then matching results will show in the Genotyping Service Manager data browser

    @SM_GenotypingServiceManager_Filter8
    Scenario: SM-GENOTYPING-SERVICE-MANAGER-008 User filter the column SERVICES from the browser
        Given user can see Genotyping Service Manager data browser
        When user set column filters of the Genotyping Service Manager data browser
            | SERVICE            |
            | 200 SNP_Panel 2 V1 |
        Then matching results will show in the Genotyping Service Manager data browser

    @SM_GenotypingServiceManager_Filter9
    Scenario: SM-GENOTYPING-SERVICE-MANAGER-009 User filter the column SERVICE PROVIDER from the browser
        Given user can see Genotyping Service Manager data browser
        When user set column filters of the Genotyping Service Manager data browser
            | SERVICE PROVIDER                           |
            | CIMMYT Mexico Wheat Seed Health Laboratory |
        Then matching results will show in the Genotyping Service Manager data browser

    @SM_GenotypingServiceManager_Filter10
    Scenario: SM-GENOTYPING-SERVICE-MANAGER-010 User filter the column TOTAL from the browser
        Given user can see Genotyping Service Manager data browser
        When user set column filters of the Genotyping Service Manager data browser
            | TOTAL |
            | 150   |
        Then matching results will show in the Genotyping Service Manager data browser

    @SM_GenotypingServiceManager_Filter11
    Scenario: SM-GENOTYPING-SERVICE-MANAGER-011 User filter the column SUBMIT DATE from the browser
        Given user can see Genotyping Service Manager data browser
        When user set column filters of the Genotyping Service Manager data browser
            | SUBMIT DATE |
            | 2021        |
        Then matching results will show in the Genotyping Service Manager data browser

    # @SM_GenotypingServiceManager_Filter12
    # Scenario: SM-GENOTYPING-SERVICE-MANAGER-012 User filter the column COMPLETE BY from the browser
    #     Given user can see Genotyping Service Manager data browser
    #     When user set column filters of the Genotyping Service Manager data browser
    #         | COMPLETE BY |
    #         # TODO: Update this value:
    #         | No Data     |
    #     Then matching results will show in the Genotyping Service Manager data browser

    @SM_GenotypingServiceManager_Filter13
    Scenario: SM-GENOTYPING-SERVICE-MANAGER-013 User filter the column SERVICE TYPE from the browser
        Given user can see Genotyping Service Manager data browser
        When user set column filters of the Genotyping Service Manager data browser
            | SERVICE TYPE        |
            | Genotyping Analysis |
        Then matching results will show in the Genotyping Service Manager data browser


    #--------------------#
    #   ACTION BUTTONS   #
    #--------------------#

    @SM_GenotypingServiceManager_Approve
    Scenario: SM-GENOTYPING-SERVICE-MANAGER-013 Approve Request
        Given user can see Genotyping Service Manager data browser
        When the user clicks on the Genotyping Service Manager approve button
        Then the user sees that the Genotyping Service Manager status is Approved

    @SM_GenotypingServiceManager_Reject
    Scenario: SM-GENOTYPING-SERVICE-MANAGER-014 Reject Request
        Given user can see Genotyping Service Manager data browser
        When the user clicks on the Genotyping Service Manager reject button
        Then the user sees that the Genotyping Service Manager status is Rejected

    @SM_GenotypingServiceManager_Create
    Scenario: SM-GENOTYPING-SERVICE-MANAGER-016 Create Request Service
        Given user can see Genotyping Service Manager data browser
        And the user clicks on the Genotyping Service Manager ADD NEW REQUEST button
        And the user select a germplasm on the Genotyping Service Manager list
        And the user provides the basic information of the Genotyping Service Manager service
        And the user provides the services information of the Genotyping Service Manager
        And the user review the Genotyping Service Manager information before submitting the request
        Then the user click Genotyping Service Manager Submit

    @SM_GenotypingServiceManager_Download
    Scenario: SM-GENOTYPING-SERVICE-MANAGER-017 Download Batch
        Given user can see Genotyping Service Manager data browser
        When the user clicks on the Genotyping Service Manager download button
        Then the user must be able to download the Genotyping Service Manager list in CSV file