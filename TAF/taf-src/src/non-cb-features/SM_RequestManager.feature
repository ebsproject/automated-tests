@SM_RequestManager
Feature: SQA-970 Service Management - Request Manager
    The user should be able to navigate the Service Management - Request Manager

    Background: Login User
        Given the user is in EBS Dashboard page
        And the user selects Service Management instance
        And the user clicks on "Request Manager" SM menu


    #------------------------------------#
    #   REQUEST MANAGER FILTER COLUMNS   #
    #------------------------------------#

    @SM_RequestManager_Filter1
    Scenario: SM-REQUEST-MANAGER-001 User filter the column STATUS from the browser
        Given user can see Request Manager data browser
        When user set column filters of the Request Manager data browser
            | STATUS |
            | New    |
        Then matching results will show in the Request Manager data browser

    @SM_RequestManager_Filter2
    Scenario: SM-REQUEST-MANAGER-002 User filter the column REQUEST from the browser
        Given user can see Request Manager data browser
        When user set column filters of the Request Manager data browser
            | REQUEST |
            | RLB     |
        Then matching results will show in the Request Manager data browser

    # @SM_RequestManager_Filter3
    # Scenario: SM-REQUEST-MANAGER-003 User filter the column REQUESTER from the browser
    #     Given user can see Request Manager data browser
    #     When user set column filters of the Request Manager data browser
    #         | REQUESTER |
    #         # TODO: Update this value:
    #         | No Data   |
    #     Then matching results will show in the Request Manager data browser

    @SM_RequestManager_Filter4
    Scenario: SM-REQUEST-MANAGER-004 User filter the column CROP from the browser
        Given user can see Request Manager data browser
        When user set column filters of the Request Manager data browser
            | CROP  |
            | Wheat |
        Then matching results will show in the Request Manager data browser

    @SM_RequestManager_Filter5
    Scenario: SM-REQUEST-MANAGER-005 User filter the column SERVICE TYPE from the browser
        Given user can see Request Manager data browser
        When user set column filters of the Request Manager data browser
            | SERVICE TYPE         |
            | Seed Health Analysis |
        Then matching results will show in the Request Manager data browser

    @SM_RequestManager_Filter6
    Scenario: SM-REQUEST-MANAGER-006 User filter the column SERVICE PROVIDER   from the browser
        Given user can see Request Manager data browser
        When user set column filters of the Request Manager data browser
            | SERVICE PROVIDER                           |
            | CIMMYT Mexico Wheat Seed Health Laboratory |
        Then matching results will show in the Request Manager data browser

    @SM_RequestManager_Filter7
    Scenario: SM-REQUEST-MANAGER-007 User filter the column SERVICES from the browser
        Given user can see Request Manager data browser
        When user set column filters of the Request Manager data browser
            | SERVICES      |
            | Seed Shipment |
        Then matching results will show in the Request Manager data browser

    @SM_RequestManager_Filter8
    Scenario: SM-REQUEST-MANAGER-008 User filter the column PURPOSE from the browser
        Given user can see Request Manager data browser
        When user set column filters of the Request Manager data browser
            | PURPOSE         |
            | Quality Control |
        Then matching results will show in the Request Manager data browser

    @SM_RequestManager_Filter9
    Scenario: SM-REQUEST-MANAGER-009 User filter the column TOTAL from the browser
        Given user can see Request Manager data browser
        When user set column filters of the Request Manager data browser
            | TOTAL |
            | 100   |
        Then matching results will show in the Request Manager data browser


    #--------------------#
    #   ACTION BUTTONS   #
    #--------------------#

    @SM_RequestManager_View
    Scenario: SM-REQUEST-MANAGER-010 View Request
        Given user can see Request Manager data browser
        When the user clicks on the Request Manager view action
        Then the user sees the Request Manager Basic Information

    @SM_RequestManager_Edit
    Scenario: SM-REQUEST-MANAGER-011 Edit Request
        Given user can see Request Manager data browser
        When the user clicks on the Request Manager edit action
        Then the user can edit the Request Manager Service

    @SM_RequestManager_Delete
    Scenario: SM-REQUEST-MANAGER-012 Delete Request
        Given user can see Request Manager data browser
        When the user clicks on the Request Manager delete action
        Then the user clicks the Request Manager confirm button to delete the request

    @SM_RequestManager_Download
    Scenario: SM-REQUEST-MANAGER-013 Download Batch
        Given user can see Request Manager data browser
        When the user clicks on the Request Manager download button
        Then the user must be able to download the Request Manager list in CSV file

    @SM_RequestManager_Create
    Scenario: SM-REQUEST-MANAGER-014 Create Request Service
        Given user can see Request Manager data browser
        And the user clicks on the Request Manager ADD NEW REQUEST button
        And the user select a germplasm on the Request Manager list
        And the user provides the basic information of the Request Manager service
        And the user provides the services information of the Request Manager
        And the user review the Request Manager information before submitting the request
        Then the user clicks the Request Manager Submit