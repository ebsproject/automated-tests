#!/bin/bash
#Colors
RED='\033[0;31m'
GREEN='\033[0;32m'

if [ $# -ne 3 ]
  then
    printf "${RED}Please be sure that you are passing (1) user (2) password and (3) URL correctly. \n Aborted Process"
    exit
fi

echo "Starting the process..."
# Strings to assemble the gmail run instruction "target": "TC000-Login"
gmail_1='"target": '
gmail_2='"TC000-Login"'
# Strings to assemble the microsoft run instruction "TC000-Login-Microsoft"
microsoft_1='"target": '
microsoft_2='"TC000-Login-Microsoft"'
# Assembling the strings
gmail_login=$gmail_1$gmail_2
microsoft_login=$microsoft_1$microsoft_2
# User & password
user=$1
password=$2
# Strings to assemble the URL/Environment run instruction "target": "/"
targeturl_1='"target": '
targeturl_2='"/"'

# Assembling the URL strings
url1='"'
url2=$3
url3='"'
targeturl=$targeturl_1$targeturl_2
switchurl=$targeturl_1$url1$url2$url3

#echo "Converting smoke tests..."
################ 1- SMOKE ################
#Replace the credentials in the first_authentication.side
sed -i "s/user@ebs.org/${user}/g" ./smoke/*.side
sed -i "s/ebspassword/${password}/g" ./smoke/*.side

echo "Converting .side files into the Functional testing folder..."
################ 2- SWITCH - USER - PASSWORD ################
#FOR This "for" aplies the switch of the LOGIN and set the USER & PASSWORD, and set the URL to all the .side files
for f in *.side
do
    #Replace the gmail login by the microsoft login
    sed -i "s/${gmail_login}/${microsoft_login}/g" $f
    #Replace the user (the user can come from a argument)
    sed -i "s/user@ebs.org/${user}/g" $f
    #Replace the password (The password can come from a argument)
    sed -i "s/ebspassword/${password}/g" $f
    #Replace the url (The url can come from a argument)
    printf "${targeturl} versus ${switchurl}\n"
    #sed -i "s/${targeturl}/${switchurl}/g" $f
    sed -i "s~${targeturl}~${switchurl}~g" $f
done
printf "${GREEN}Process finished successfully"