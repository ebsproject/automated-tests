**Versions**

Google Chrome 80.0.3987.106

ChromeDriver 80.0.3987.106

Mozilla Firefox 73.0

geckodriver 0.26.0

selenium-side-runner 3.16.0

**Pre-deployment**
1. Clone the repository in your local and create your feature branch from develop branch where selenium-docker image is located
    $ git clone https://dpagtananan@bitbucket.org/ebsproject/automated-tests.git
    $ git checkout develop
    $ git flow feature start my_feature_branch


2. Open windows explorer or VS Code editor where the repository was cloned and add/create folders where you would like the selenium side files and test outputs to be saved to
    e.g. /home/dpagtananan/Documents/dekal/automated-tests/Functional_Testing/Selenium >> input directory
        /home/dpagtananan/Documents/dekal/automated-tests/Functional_Testing/outputs >> output directory, *create manually*

3. Open the 'env' file and add/modify the variable below to indicate the input and output directory from the previous step:
    EBS_OUTPUTS_DIR=/home/dpagtananan/Documents/dekal/automated-tests/Functional_Testing/outputs
    EBS_TESTS_DIR=/home/dpagtananan/Documents/dekal/automated-tests/Functional_Testing/Selenium

4. Open the 'docker-compose.yml' and modify the input and output directory with reference on the previous step
      - ${EBS_TESTS_DIR}:/home/dpagtananan/automated-tests/Functional_Testing/Selenium
      - ${EBS_OUTPUTS_DIR}:/home/dpagtananan/automated-tests/Functional_Testing/outputs

5. Verify that you have the selenium tests (.side) inside Functional_Testing/Selenium folder. Otherwise, you can download/pull from:
    https://bitbucket.org/ebsproject/automated-tests/src/develop/Functional_Testing/Selenium/

6. Save all the .side files, and others, inside the folder path defined in your EBS_TESTS_DIR
    e.g. If my EBS_TESTS_DIR has the path '/home/dpagtananan/Documents/dekal/automated-tests/Functional_Testing/Selenium'
        Then inside Selenium/ is where all the following .side files, and others, are saved:
            - Selenium/Dashboard.side
            - Selenium/Dashboard_Seasons.side
            - Selenium/Data_Collection.side
            - Selenium/Experiment_Creation.side
            - Selenium/Experiment_Manager.side
            - Selenium/Find_Seeds_List.side
            - Selenium/Germplasm.side
            - Selenium/Harvest_Manager.side
            - Selenium/Data_Collection.side
            - Selenium/Manage_Persons.side
            - Selenium/Traits.side
            - Selenium/Find_Seeds_List.side
            - Selenium/convertor.sh
            - Selenium/smoke/

7. Modify and save test-chrome.sh (or test-firefox.sh) to reflect the correct test environment, input and output folder of your selenium tests

*-- Update the following in your test-chrome.sh:*

*-- Set the correct folder path where your .side files reside*
    e.g. SELENIUM_SIDE_FILES_PATH=../Functional_Testing/Selenium

*-- Set the correct url of the environment where you will run the selenium tests*
    e.g. SUT_HOST=https://b4r-uat.ebsproject.org

*-- Look for --output-directory and update the output directory for the xml results and input directory of the .side files*
    e.g. --output-directory /home/dpagtananan/Documents/ebs-selenium/ebs-tests/outputs /home/dpagtananan/Documents/ebs-selenium/ebs-tests/Selenium/'$file''
    
*-- To run a specific test suite, add --filter "smoke" and specify the .side file instead of using the $file variable*
    e.g. --filter "smoke" --output-format=junit --output-directory /home/dpagtananan/automated-tests/Functional_Testing/outputs  /home/dpagtananan/automated-tests/Functional_Testing/Selenium/Review_21.04/Data_Collection.side'

8. Now you can start to run the selenium tests in the docker image. Make sure the cloned git folder especially your *outputs* directory has the 777 access so there will be no problem saving the xml results during the test run

**Deployment**

Execute the following to deploy: 
    $ sudo bash deploy.sh

--In the terminal, the following should be visible if the deployment is successful:
Successfully tagged selenium-side-runner:latest
Creating selenium-standalone-firefox ... done
Creating selenium-standalone-chrome  ... done
Creating selenium-side-runner        ... done

Should you encounter problems running deploy.sh, try to run 'undeploy.sh' to terminate existing selenium containers
    $ sudo sh undeploy.sh

**Validation**
1. *This step is optional when running in your local, but RECOMMENDED when running tests in the AWS server to bypass the GOOGLE LOGIN using OTP*
    To successfully run in headless chrome/firefox, go to the folder where convertor.sh is located and run with the following parameters
    $ cd ../Functional_Testing/Selenium/
    $ sudo sh convertor.sh *(1) microsoft account* *(2) microsoft account password* *(3) URL of the server instance e.g. https://b4r-qa.ebsproject.org/index.php/auth/default/logout*

-- In the terminal, the following should be visible:
Starting the process...
Converting smoke tests...
Converting .side files into the Functional testing folder...
Process finished successfully

2. Now you can go back to the selenium-docker directory where test-chrome.sh is and run the tests by executing the command:
    $ cd ../../selenium-docker/
    $ sudo sh test-chrome.sh
    OR
    $ sudo sh test-firefox.sh

-- Depending on the parameter of 'docker exec' in your test-chrome.sh, if it's like 'docker exec -ti', then you will see the following in your terminal:

info:    Running /home/dpagtananan/Documents/ebs-selenium/ebs-tests/Sprint_21.03/Find_Seeds_List.side
added 26 packages from 24 contributors and audited 26 packages in 3.431s
found 0 vulnerabilities

-- If it's like 'docker exec -d', then you will not see any logs in your terminal.

3. Once an individual side file is finished running, you will see the run status like this, again if it's 'docker exec -ti' in your test-chrome.sh:

Test Suites: 5 passed, 5 total
Tests:       8 passed, 8 total
Snapshots:   0 total
Time:        463.62s
Ran all test suites.

**Congrats, you're all setup!**

**For questions, kindly contact/email the following members of the SQA Team:**
d.pagtananan@irri.org
p.sinohin@irri.org
l.b.go@irri.org
