#!/bin/bash

export SELENIUM_STANDALONE_DIR=`pwd`

env $(cat env | grep ^[A-Z] | xargs) docker-compose ps

