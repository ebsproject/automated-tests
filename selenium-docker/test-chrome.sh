#!/bin/bash

SELENIUM_SIDE_FILES_PATH=../Functional_Testing/Selenium
SUT_HOST=https://cb-uat.ebsproject.org

#FOR RUNNING MULTIPLE SIDE FILES ALL AT ONCE: Uncomment SELENIUM_SIDE_FILES_PATH and below lines up to line 'done' 
#AND comment out the lines after #FOR RUNNING SINGLE SIDE FILE

for filePath in $SELENIUM_SIDE_FILES_PATH/*.side
do
  file=$(basename $filePath)

  docker exec -d selenium-side-runner bash -c 'selenium-side-runner \
    --server http://chrome:4444/wd/hub  \
    --baseUrl '$SUT_HOST' \
    -c "browserName='chrome' chromeOptions.args=['--start-maximized']" \
    --output-format=junit \
    --output-directory /home/seluser/outputs /home/seluser/sides/'$file''
done

#FOR RUNNING SINGLE SIDE FILE
#docker exec -ti selenium-side-runner bash -c 'selenium-side-runner \
#    --server http://chrome:4444/wd/hub  \
#    --baseUrl '$SUT_HOST'  \
#    --detectOpenHandles \
#    -c "browserName='chrome'" \
#    --output-format=junit --output-directory /home/dpagtananan/automated-tests/Functional_Testing/outputs  /home/dpagtananan/automated-tests/Functional_Testing/Selenium/Find_Seeds_List.side'