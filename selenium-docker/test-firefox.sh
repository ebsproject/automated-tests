#!/bin/bash

docker exec -ti selenium-side-runner bash -c 'selenium-side-runner \
    --server http://firefox:4444/wd/hub  \
    -c "browserName='firefox'" \
    --output-format=junit /home/seluser/sides/Functional_Testing/B4R-Microsoft_Authentication/initial_authentication.side'
