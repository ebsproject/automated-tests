#!/bin/bash

docker exec -ti selenium-side-runner-debug bash -c 'selenium-side-runner \
    --server http://firefox-debug:4444/wd/hub  \
    -c "browserName='firefox'" \
    --output-format=junit \
    --filter smoke \
    /home/seluser/sides/Functional_Testing/B4R-Microsoft_Authentication/TS001_Experiment_Creation_Cross_Pre-planning_Experiment_for_Hybrids.side'
