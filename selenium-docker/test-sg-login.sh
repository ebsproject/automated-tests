#!/bin/bash

SELENIUM_SIDE_FILES_PATH=../Functional_Testing/Selenium/smoke
SUT_HOST=https://cb-uat.ebsproject.org
LOGOUT_URL=$SUT_HOST/index.php/auth/default/logout

sed -i --expression "5s|.*|\"url\": \"$LOGOUT_URL\",|" ../Functional_Testing/Selenium/smoke/sg_authentication.side

#Run first the authentication smoke test under 'smoke' folder
docker exec -ti selenium-side-runner bash -c 'selenium-side-runner \
    --server http://chrome:4444/wd/hub  \
    --baseUrl '$SUT_HOST'  \
    --detectOpenHandles \
    -c "browserName='chrome'" \
    --output-directory /home/seluser/outputs /home/seluser/sides/smoke/sg_authentication.side'
